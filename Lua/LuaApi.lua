json = require "json"


--[[
    玩家业务私有数据结构体
    struct COMDT_UGC_CUSTOM_STRING
    {
        char m_szCustomString[1024];                            //自定义数据string
    }

    struct COMDT_UGC_CUSTOMDATA
    {
            int32_t m_CustomIntArray[200];                     //自定义数据int数组
            COMDT_UGC_CUSTOM_STRING m_astCustomStringArray[5]; //自定义数据string数组
    }

    struct FixedData
    {
        uint32_t m_MatchScore[10];
    }

    struct PrivData
    {
        FixedData m_stFixedData;                               //业务通用数据，如匹配分等
        COMDT_UGC_CUSTOMDATA m_stCustomData;                   //自定义数据
    }

    struct COMDT_UGC_CUSTOM_INT_ARRAY
    {
        int32_t m_CustomIntArray[1280]; //自定义数据int数组
    }

    struct COMDT_UGC_BIG_CUSTOM_STRING
    {
        char m_szBigCustomDataString[5120];
    }

    struct COMDT_UGC_UPDATE_BIG_CUSTOM_DATA //除init接口外， 其余所有接口一次性修改的int不超过100个，str不超过两个
    {
        COMDT_UGC_CUSTOM_INT_ARRAY m_astIntArray[13]; //自定义int数组
        COMDT_UGC_BIG_CUSTOM_STRING m_astStringArray[10]; //自定义string buffer
    }

   struct Operator
    {
        uint16_t m_wOperType; //操作类型,业务方自己定义和解析，可以不使用
        char m_szOperBuffer[256]; //操作buffer，业务自己进行解析
        uint8_t m_bOperCnt; //buffer操作的重复次数，比如米大师发送物品时可能是多个物品
    }



--[[
    暴露的接口：
    日志类：
        ErrLog(logstr)
        TraceLog(logstr)
        DebugLog(logstr)
        RunLog(logstr)
    物品类：
    所有的接口正常调用则表示成功，如果失败，业务方直接抛出异常即可
]]--


--[[
    lua_appid: 玩法ID，服务器会自动读取, 一定要配置
]]--
lua_appid=60





--[[
    在开发者后台上传Json, 针对每个玩法的一些配置参数(可选), 大小不能超过4K
    (比如每局最大杀怪数，最大可以获得的经验值等等)
]]--
function InitConfig(szConfBuff)
    local data =  json.decode(szConfBuff)
     --如何失败,直接assert(false), 相当于return false,不写assert相当于成功
    assert(false)
end





--[[
    全局只会调用一次, 用来初始化数据
    param [in/out] oAppPrivData 玩法私有数据
    oBigCustomData  参见COMDT_UGC_UPDATE_BIG_CUSTOM_DATA
    客户端调用：LuaCallCs_PersistentData.GetBigCustomDataInt(firstIdx, secondIdx) 获取int字段
                LuaCallCs_PersistentData.GetBigCustomDataString(idx) 获取字符串字段
 ]]--
function InitAppPrivData(oAppPrivData, oBigCustomData)
    oAppPrivData.m_stFixedData.m_MatchScore[0] = 1
    for index=0,199,1 do
        oAppPrivData.m_stCustomData.m_CustomIntArray[index] = 0
    end

    for index=0,4,1 do
        oAppPrivData.m_stCustomData.m_astCustomStringArray[index].m_szCustomString = ""
    end

    TraceLog("call default init func")
     --如何失败,直接assert(false), 相当于return false,不写assert相当于成功
    --assert(false)
end






--[[
    参见PrivData 结构体定义, 默认采用全覆盖的方式
    结算时私有数据处理处理，开发者可以根据自己的需求进行数据的全量覆盖或者增量修改
    oSvrAppPrivData：服务器保存的开局前的数据
    oCltAppPrivData： 结算时客户端上报的数据
]]--
function CheckSettleData(oSvrAppPrivData, oCltAppPrivData, oBigCustomData)
    --自动上报oSvrAppPrivData为最终结果
    PerfectCopy(oSvrAppPrivData.m_stCustomData, oCltAppPrivData.m_stCustomData)
    -- print(oBigCustomData.m_astIntArray[0].m_CustomIntArray[0])
    -- oBigCustomData.m_astIntArray[0].m_CustomIntArray[0]  = oBigCustomData.m_astIntArray[0].m_CustomIntArray[0] + 1112
    -- oBigCustomData.m_astStringArray[0].m_szBigCustomDataString="2435453"
    -- oBigCustomData.m_astStringArray[1].m_szBigCustomDataString="243545378797584"
    --[[ 
    print(oSvrAppPrivData.m_stCustomData.m_CustomIntArray[0])
    print(oCltAppPrivData.m_stCustomData.m_CustomIntArray[0])
    print(oSvrAppPrivData.m_stCustomData.m_CustomIntArray[0])
    print(oCltAppPrivData.m_stCustomData.m_CustomIntArray[0])
    for index=0,199,1 do
        oSvrAppPrivData.m_stCustomData.m_CustomIntArray[index] = oCltAppPrivData.m_stCustomData.m_CustomIntArray[index]
    end
    for index=0,4,1 do
        oSvrAppPrivData.m_stCustomData.m_astCustomStringArray[index] = oCltAppPrivData.m_stCustomData.m_astCustomStringArray[index]
    end
    oSvrAppPrivData.m_stFixedData.m_dwMatchScore = oCltAppPrivData.m_stFixedData.m_dwMatchScore
    ]]--
    TraceLog("call default check settle func v2")
    end




--[[
    参见PrivData  和 Operator 结构体定义
    处理客户端上行的修改业务私有数据请求
    oSvrAppPrivData ： 服务器保存的业务私有数据
    oPeratorList    ： 客户端上行的修改请求，具体buffer由业务方自己解析,
    --oPeratorList是struct PrivData的List, operCnt为List的长度
   --客户端调用：LuaCallCs_PersistentData.SendCustomDataOpearation(int opType, string opBuffer,  int opCnt)
   --Lua侧的响应回调是：function OnSendCustomDataOpearationRsp(result)
    ]]--
function UpdateAppPrivData(oSvrAppPrivData, oPeratorList, operCnt, oBigCustomData)
    --客户端Lua会提供一个自定义操作的接口(可以用来修改自定义数据),客户端可以Call这个函数
    print("operCnt :", operCnt) 
    for index=0, operCnt - 1, 1 do
        print("operbuf:", oPeratorList[index].m_szOperBuffer)
        local oper = json.decode(oPeratorList[index].m_szOperBuffer)
	    print("oper val:", oper["add"])
        oSvrAppPrivData.m_stCustomData.m_CustomIntArray[0] = oSvrAppPrivData.m_stCustomData.m_CustomIntArray[0] + oper["add"]
        oBigCustomData.m_astIntArray[0].m_CustomIntArray[0]  = oBigCustomData.m_astIntArray[0].m_CustomIntArray[0] + oper["add"]
    end
    ErrLog("call default update func")
    --如何失败,直接assert(false), 相当于return false,不写assert相当于成功
    --assert(false)
end



--[[
    参见PrivData  和 Operator 结构体定义
    官方商城和saas系统发货接口,服务器触发调用
    oSvrAppPrivData ： 服务器保存的业务私有数据
    oPeratorList    ： 客户端上行的修改请求，具体buffer由业务方自己解析
        -- m_wOperType
        -- m_szOperBuffer
    ]]--
function GiveAwardToAppPrivData(oSvrAppPrivData, oPeratorList, operCnt, oBigCustomData)
    print("operCnt :", operCnt)
    for index=0, operCnt - 1, 1 do
        local oPerator = oPeratorList[index]

        local parm = json.decode(oPerator.m_szOperBuffer)
        local type = oPerator.m_wOperType
        local addNum = parm["addNum"]
        -- local type = parm["type"]
        print("type :", type)
        print("addNum :", addNum)


        --[[
            intArrInfo = {
                [1] = "总场次"
                [2] = "通关场次"
                [3] = "通关难度"
                [4] = "积分"
                [5] = "满星经验英雄个数"
                [6] = "英雄经验总等级"
                [7] = "通用经验值"
                [8] = "游戏时长"
                [9] = "游戏时长"
                [10] = "时间戳"
                [11] = "今日活跃度"
            }
        ]]

        local i = nil -- 此索引从1开始

        if type then
            if type >= 1 and type <= 5 then
                i = 11 -- 此索引从1开始

            elseif type >= 6 and type <= 12 then
                i = 4 -- 此索引从1开始

            elseif type >= 13 and type <= 20 then
                i = 7
            else
                ErrLog("cant find this type")
                assert(false)
            end

            if i then
                oSvrAppPrivData.m_stCustomData.m_CustomIntArray[i-1] = oSvrAppPrivData.m_stCustomData.m_CustomIntArray[i-1] + addNum -- 这里的索引是从0开始的
            end
        else
            ErrLog("cant find this type")
            assert(false)
        end
    end
    -- ErrLog("call default give award func")
    --如何失败,直接assert(false), 相当于return false,不写assert相当于成功
    -- assert(false)
end







--[[
    参见PrivData  和 Operator 结构体定义
    官方商城和saas系统发货前的检查接口
    oSvrAppPrivData ： 服务器保存的业务私有数据
    oPeratorList    ： 客户端上行的修改请求，具体buffer由业务方自己解析
    ]]--
function CheckAwardToAppPrivData(oSvrAppPrivData, oPeratorList, operCnt, oBigCustomData)
    ErrLog("call default check award func")
     --如何失败,直接assert(false), 相当于return false,不写assert相当于成功
    assert(false)
end


