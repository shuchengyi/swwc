GameDataMgr = require"OnlineMode/G_GameDataMgr.lua"
-- if not UIData then UIData = require"UIData.lua" end
G_GameData = GameDataMgr.GameData
G_EventLoop = GameDataMgr.EventLoop
 
local formSelf
local l_mapdrop
local l_heroid
local l_heroiddrop
local l_heroidlist
local l_roll_hero
local l_HeroList
local l_selectedIndex = nil
local l_PlayerList
local PlayerData = {}
local selfPlayerID
local l_confirmedBtn
local l_time
local suijiHeroCntInfo = {}
local baoshiye = 1

local xuanrenbaoShiYeShuXingInfo1 = {}
local xuanrenshuxingstringInfoyouxu1 = {}
local xuanrenbaoShiYeShuXingInfo2 = {}
local xuanrenshuxingstringInfoyouxu2 = {}
local xuanrenbaoShiYeShuXingInfo3 = {}
local xuanrenshuxingstringInfoyouxu3 = {}
local xuanrenallJiejiNum1 = 0
local xuanrenallDengjiNum1 = 0
local xuanrenallJiejiNum2 = 0
local xuanrenallDengjiNum2 = 0
local xuanrenallJiejiNum3 = 0
local xuanrenallDengjiNum3 = 0
local tzsxBaoShiJieJi = {
    {12, "金币获取", "+100%"},
    {24, "生命值上限", "+200%"},
    {36, "全吸血", "+10%"},
    {48, "全穿透", "+45%"},
    {60, "最终伤害", "+250%"},
}


local tzsxBaoShiDengJi = {
    {12, "全攻击力", "+10%"},
    {24, "全攻击力", "+20%"},
    {36, "全攻击力", "+30%"},
    {48, "全攻击力", "+50%"},
    {60, "全攻击力", "+80%"},
    {72, "全攻击力", "+120%"},
}




local dengjijiaqiangInfo = {
    [1] = 1,
    [2] = 1.1,
    [3] = 1.25,
    [4] = 1.45,
    [5] = 1.7,
    [6] = 2.1,
}


local function get_heroidlist() --获取英雄列表
	math.randomseed(tostring(os.time()):reverse():sub(1,7))
	math.random()
	heroidlist = LuaCallCs_Data.GetAllHeroInfo();
	local newTab = {}

	local table = UIConfigData.heroList
	local count = 3
	local length = #table
	for i = 1, count do
		local ri = math.random(i, length)
		local tmp = table[i]
		table[i] = table[ri]
		table[ri] = tmp
		newTab[i] = table[i]
	end

	return newTab
end

--初始化
function OnStartupOpen(LuaUIEvent)
	local PID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID

	local stringArrData = LuaCallCs_PersistentData.GetCustomizeDataStringArr()
	checkHeroExFunc(stringArrData[1])

    local baoshiZhuangPeiInfoString = stringArrData[5]
    if baoshiZhuangPeiInfoString == "" then
        UIDynamicData.baoshiZhuangPeiInfo = {
            {-- 第一页
                {},{},{}, -- 红
                {},{},{}, -- 蓝
                {},{},{}, -- 紫
                {},{},{}  -- 绿
            },
            {{},{},{},{},{},{},{},{},{},{},{},{}},
            {{},{},{},{},{},{},{},{},{},{},{},{}},
        }
    else
        UIDynamicData.baoshiZhuangPeiInfo = json.decode(baoshiZhuangPeiInfoString)
    end

	local intArr = LuaCallCs_PersistentDataInBattle.GetCustomizeDataIntArrInBattle(PID)

	local jifen = intArr[4]

	suijiHeroCntInfo[PID] = 1
	if jifen >= 250 then
		suijiHeroCntInfo[PID] = suijiHeroCntInfo[PID] + 1
		LuaCallCs_Common.Log(suijiHeroCntInfo[PID])
	end
	if jifen >= 1000 then
		suijiHeroCntInfo[PID] = suijiHeroCntInfo[PID] + 1
		LuaCallCs_Common.Log(suijiHeroCntInfo[PID])
	end

	--注册监听函数
	G_EventLoop:AddEventListener("RefreshUI", RefreshPlayerList)
	G_EventLoop:AddEventListener("Confirmed", Confirmed)

	--获取Form
	formSelf = LuaUIEvent.SrcForm;
	--获取确定按钮
	l_confirmedBtn = formSelf:GetWidgetProxyByName("confirmedBtn")
	--获取地图的DropList组件
	-- l_mapdrop = formSelf:GetWidgetProxyByName("MapNameDropList");
	--获取project下可用的level名字
	-- mapname = LuaCallCs_Level.GetAllLevelFiles();
	-- l_mapdrop:SetDropTextContents(mapname);
	-- l_mapdrop:SelectElement(0,true);
	--获取玩家自己的PlayerID
	selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
	--获取可以选用的英雄ID
	-- heroidlist =  get_heroidlist()





	for i=1, #G_GameData.playerInfos do
		if G_GameData.playerInfos[i]["playerID"] == selfPlayerID then
			if G_GameData.playerInfos[i]["h1"]  then
				heroidlist = {
					G_GameData.playerInfos[i].h1,
					G_GameData.playerInfos[i].h2,
					G_GameData.playerInfos[i].h3,
				}
				if G_GameData.playerInfos[i]["index"] then
					l_selectedIndex = G_GameData.playerInfos[i].index-1
				end

				if G_GameData.playerInfos[i]["suijiCnt"] then
					suijiHeroCntInfo[PID] = G_GameData.playerInfos[i].suijiCnt
				end

				if G_GameData.playerInfos[i]["bs"] then
					baoshiye = G_GameData.playerInfos[i].bs
					if G_GameData.playerInfos[i]["bs"] == 1 then
						formSelf:GetWidgetProxyByName("TextLabel(1070)"):GetText():SetContent("宝石页1")
					elseif G_GameData.playerInfos[i]["bs"] == 2 then
						formSelf:GetWidgetProxyByName("TextLabel(1070)"):GetText():SetContent("宝石页2")
					else
						formSelf:GetWidgetProxyByName("TextLabel(1070)"):GetText():SetContent("宝石页3")
					end
				end
			else
				heroidlist =  get_heroidlist()
				local operateCmd = {}
				operateCmd.playerID = selfPlayerID
				operateCmd.cmdType = 5;
				operateCmd.hero1 = heroidlist[1]
				operateCmd.hero2 = heroidlist[2]
				operateCmd.hero3 = heroidlist[3]
				operateCmd.SelectedHero = heroidlist[1]
				local operatData = json.encode(operateCmd)
				--发送自定义操作命令
				LuaCallCs_UGCStateDriver.SendOperateCmd(operatData)
			end
		end
	end




	l_HeroList = formSelf:GetWidgetProxyByName("SelectHeroList")
	l_HeroList:SetElementAmount(#heroidlist)
	--初始化玩家List上的玩家数量
	l_PlayerList = formSelf:GetWidgetProxyByName("ConfirmHeroList")
	l_PlayerList:SetElementAmount(4)
	l_roll_hero = formSelf:GetWidgetProxyByName("roll_hero");
	l_roll_hero:GetText():SetContent("随机 X " .. suijiHeroCntInfo[PID])
	if suijiHeroCntInfo[PID] <= 0 then
		l_roll_hero:EnableInput(false)
	end


	l_time = formSelf:GetWidgetProxyByName("xzsj_time");
	if G_GameData.time then
		l_time:SetTotalTime(G_GameData.time)
		l_time:SetCurrentTime(G_GameData.time)
	end
	l_time:StartTimer()
end


function OnCElementEnable(LuaUIEvent)
	local elem = LuaUIEvent.SrcWidget
	local index = elem:GetIndexInBelongedList()

	local playerCnt = #G_GameData.playerInfos

	local Player_name = elem:GetWidgetProxyByName("CPlayerName"):GetText()
	local Player_icon = elem:GetWidgetProxyByName("CHeroIcon"):GetImage()

	if index > (playerCnt-1) then
		Player_icon:SetRes("Texture/Sprite/suo.sprite")
		elem:GetWidgetProxyByName("CPlayerName"):SetActive(false)
		return
	end


	local AllPlayerInfo = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()

	Player_name:SetContent(AllPlayerInfo[index+1].playerName)

	--ImageComponentProxy
	--TextComponentProxy
	local comfirmedIcon = elem:GetWidgetProxyByName("Toggle")
	if G_GameData.playerInfos[index+1].SelectedHero ~= nil and G_GameData.playerInfos[index+1].index ~= nil then
		Player_icon:SetRes(LuaCallCs_Resource.GetHeroIcon(1, G_GameData.playerInfos[index+1].SelectedHero))
	else
		Player_icon:SetRes("UGCResource/UGCUI/Sprite/Hero/QuestionIcon.prefab")
	end

	if G_GameData.playerInfos[index+1].confirmed == 1 then
		comfirmedIcon:SetActive(true)
	end

end

function HeroListOnElementEnable(LuaUIEvent)
	local elem = LuaUIEvent.SrcWidget
	local index = elem:GetIndexInBelongedList()
	--ImageComponentProxy
	local Hero_icon = elem:GetWidgetProxyByName("HeroIcon"):GetImage()
	--TextComponentProxy
	local Hero_name = elem:GetWidgetProxyByName("HeroName"):GetText()
	local Outline = elem:GetWidgetProxyByName("Panel(1061)")
	if l_selectedIndex ~= nil and l_selectedIndex == index then
		Outline:SetActive(true)
	else
		Outline:SetActive(false)
	end
	-- local cfgID = heroidlist[index+1].cfgID
	local cfgID = heroidlist[index+1]
	local heroInfo = LuaCallCs_Data.GetHeroInfo(cfgID)
	local iconPath = LuaCallCs_Resource.GetHeroIcon(2, cfgID)

	local heroIndex = UIConfigData.heroEXInfo[cfgID]
	local heroExNum = UIDynamicData.stringArrDataNew[heroIndex][2]
    local heroLevel
    if heroExNum >= UIConfigData.heroExLevelInfo[5] then
        heroLevel = 5
    elseif heroExNum >= UIConfigData.heroExLevelInfo[4] then
        heroLevel = 4
    elseif heroExNum >= UIConfigData.heroExLevelInfo[3] then
        heroLevel = 3
    elseif heroExNum >= UIConfigData.heroExLevelInfo[2] then
        heroLevel = 2
    elseif heroExNum >= UIConfigData.heroExLevelInfo[1] then
        heroLevel = 1
    elseif heroExNum >= 0 then
        heroLevel = 0
    end


	local img1
	local img2
	local img3
	local text1
	local text2
	local text3
	if UIConfigData.tanKeInfo[cfgID] ~= nil then
		text1 = "坦克：伤害减免5%"
		img1 = "UGUI/Sprite/Dynamic/Skill/921022.prefab"
	elseif UIConfigData.zhanShiInfo[cfgID] ~= nil then
		text1 = "战士：物理攻击力提升5%"
		img1 = "UGUI/Sprite/Dynamic/Skill/921062.prefab"
	elseif UIConfigData.sheShouInfo[cfgID] ~= nil then
		text1 = "射手：攻击速度提升5%"
		img1 = "UGUI/Sprite/Dynamic/Skill/921032.prefab"
	elseif UIConfigData.faShiInfo[cfgID] ~= nil then
		text1 = "法师：法强提升5%"
		img1 = "UGUI/Sprite/Dynamic/Skill/921052.prefab"
	end

	if UIConfigData.jinZhanInfo[cfgID] ~= nil then
		text2 = "近战：防御提升5%"
		img2 = "UGUI/Sprite/Dynamic/Skill/skill6_1.prefab"
	elseif UIConfigData.yuanChenInfo[cfgID] ~= nil then
		text2 = "远程：普攻造成范围伤害"
		img2 = "UGUI/Sprite/Dynamic/Skill/skill3_1.prefab"
	end

	if LuaCallCs_Data.GetHeroInfo(cfgID).energyType == 0 then -- 有蓝条
		text3 = "有蓝：回蓝/每秒0.8%"
		img3 = "Texture/Sprite/blueBuff.sprite"
	else
		text3 = "无蓝：回血/每秒0.8%"
		img3 = "Texture/Sprite/2041.sprite"
	end


	elem:GetWidgetProxyByName("heroType1"):GetImage():SetRes(img1)
	elem:GetWidgetProxyByName("heroType2"):GetImage():SetRes(img2)
	elem:GetWidgetProxyByName("heroType3"):GetImage():SetRes(img3)

	elem:GetWidgetProxyByName("heroTypeText1"):GetText():SetContent(text1)
	elem:GetWidgetProxyByName("heroTypeText2"):GetText():SetContent(text2)
	elem:GetWidgetProxyByName("heroTypeText3"):GetText():SetContent(text3)

	elem:GetWidgetProxyByName("TextLabel(1050)"):GetText():SetContent(heroLevel .. "级熟练度")

	Hero_icon:SetRes(iconPath)
	Hero_name:SetContent(heroInfo.heroName)
end







--选择英雄
function OnHeroIconClick(LuaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

	local elem = LuaUIEvent.SrcWidget
	local index = elem:GetIndexInBelongedList()
	--记录选择的英雄Index序号
	l_selectedIndex = index
	--刷新英雄List
	l_HeroList:SetElementAmount(#heroidlist)
	--构造命令
	local operateCmd = {}
	operateCmd.cmdType = 1;
	operateCmd.playerID = selfPlayerID
	operateCmd.heroID = heroidlist[l_selectedIndex+1]
	operateCmd.index = l_selectedIndex+1
	operateCmd.confirmed = 0
	operatData = json.encode(operateCmd)
	--发送自定义操作命令
	LuaCallCs_UGCStateDriver.SendOperateCmd(operatData)
end


function heroTypeClicked(LuaUIEvent)
	local index = l_HeroList:GetSelectedIndex()
	local elem = l_HeroList:GetListElement(index)
	local lastIndex = l_HeroList:GetLastSelectedIndex()
	if lastIndex == index then
		local textInfo = elem:GetWidgetProxyByName("Image(1056)")
		if textInfo:IsActived() == false then
			textInfo:SetActive(true)
		else
			textInfo:SetActive(false)
		end
	else
		OnHeroIconClick(LuaUIEvent)
	end
end

--刷新玩家List内容
function RefreshPlayerList()
	l_PlayerList:SetElementAmount(4)
end


function Confirmed()
	l_PlayerList:SetElementAmount(4)
	--[[
	--关闭确定按钮监听
	l_confirmedBtn:EnableInput(false)
	--关闭List操作监听
	l_HeroList:EnableInput(false)
	--关闭Map下拉选择监听
	l_mapdrop:EnableInput(false)
	l_roll_hero:EnableInput(false)]]
end

function SetTime()
	if l_time:GetCurrentTime() > 5 then
		l_time:ReStartTimer ()
		l_time:SetTotalTime (5)
	end
end


--确定选择英雄
function ConfirmedBtnClick(LuaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

	--如果没有选择任何英雄
	if l_selectedIndex == nil then
		return
	end
	--构造
	local operateCmd = {}
	operateCmd.cmdType = 3;
	operateCmd.playerID = selfPlayerID
	operateCmd.heroID = heroidlist[l_selectedIndex+1]
	operateCmd.confirmed = 1
	operatData = json.encode(operateCmd)
	--发送自定义操作命令
	LuaCallCs_UGCStateDriver.SendOperateCmd(operatData)
	local PID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
	if selfPlayerID == PID then
		l_PlayerList:SetElementAmount(4)
		--关闭确定按钮监听
		l_confirmedBtn:EnableInput(false)
		--关闭List操作监听
		l_HeroList:EnableInput(false)
		--关闭Map下拉选择监听
		-- l_mapdrop:EnableInput(false)
		l_roll_hero:EnableInput(false)
	end
end

--选择关卡
function OnLevelDropChange(LuaUIEvent)
	local nameidx = l_mapdrop:GetSelectedIndex()
	local dropp = l_mapdrop:GetDropListElement(nameidx)
	local operateCmd = {}
	operateCmd.cmdType = 2;
	operateCmd.levelName = dropp:GetItemText():GetContent()
	local operatData = json.encode(operateCmd)
	LuaCallCs_UGCStateDriver.SendOperateCmd(operatData)
end

--随机按钮
function roll_hero(LuaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")
	local PID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID


	if suijiHeroCntInfo[PID] <= 0 then
		return
	end

	suijiHeroCntInfo[PID] = suijiHeroCntInfo[PID] - 1
	l_roll_hero:GetText():SetContent("随机 X " .. suijiHeroCntInfo[PID])


	heroidlist =  get_heroidlist()
	local operateCmd = {}
	operateCmd.playerID = selfPlayerID
	operateCmd.cmdType = 5;
	operateCmd.hero1 = heroidlist[1]
	operateCmd.hero2 = heroidlist[2]
	operateCmd.hero3 = heroidlist[3]
	operateCmd.suijiCnt = suijiHeroCntInfo[PID]
	operateCmd.SelectedHero = heroidlist[1]
	local operatData = json.encode(operateCmd)
	--发送自定义操作命令
	LuaCallCs_UGCStateDriver.SendOperateCmd(operatData)

	



	l_HeroList:SetElementAmount(#heroidlist)
	if selfPlayerID == PID and l_selectedIndex then
		--刷新英雄List
		l_HeroList:SetElementAmount(#heroidlist)
		--构造命令
		local operateCmd = {}
		operateCmd.cmdType = 1;
		operateCmd.playerID = selfPlayerID
		operateCmd.heroID = heroidlist[l_selectedIndex+1]
		operateCmd.confirmed = 0
		if l_selectedIndex == nil then
			operateCmd.index = nil
		else
			operateCmd.index = l_selectedIndex+1
		end

		operatData = json.encode(operateCmd)
		--发送自定义操作命令
		LuaCallCs_UGCStateDriver.SendOperateCmd(operatData)
	end


end





function xuanrenbaoshiye1Func()
    local ZPInfo1 = UIDynamicData.baoshiZhuangPeiInfo[1]
    xuanrenbaoShiYeShuXingInfo1 = {}
    xuanrenshuxingstringInfoyouxu1 = {}
    local baoshizhuangpeiinfo = ZPInfo1
    for i=1, #baoshizhuangpeiinfo do
        if #baoshizhuangpeiinfo[i] ~= 0 then
            local jieji = baoshizhuangpeiinfo[i][1]
            local dengji = baoshizhuangpeiinfo[i][2]
            local shuxing = baoshizhuangpeiinfo[i][3]
            local shuxingNum = baoshizhuangpeiinfo[i][4]
            local shuxingString
            local normal = true
            if i >= 1 and i <= 3 then
                shuxingString = UIConfigData.redshuxingStringInfo[shuxing]
            elseif i >= 4 and i <= 6 then
                shuxingString = UIConfigData.blueshuxingStringInfo[shuxing]
            elseif i >= 7 and i <= 9 then
                shuxingString = UIConfigData.purpleshuxingStringInfo[shuxing]
            elseif i >= 10 and i <= 12 then
                shuxingString = UIConfigData.greenshuxingStringInfo[shuxing]
                if shuxing == 2 or shuxing == 3 then
                    normal = false
                end
            end
            if xuanrenbaoShiYeShuXingInfo1[shuxingString] then
                xuanrenbaoShiYeShuXingInfo1[shuxingString][1] = xuanrenbaoShiYeShuXingInfo1[shuxingString][1] + shuxingNum * dengjijiaqiangInfo[dengji]
            else
                xuanrenbaoShiYeShuXingInfo1[shuxingString] = {shuxingNum * dengjijiaqiangInfo[dengji], normal}
            end
        end
    end
    for key, value in pairs(xuanrenbaoShiYeShuXingInfo1) do
        local num = value[1]

        local nDecimal = 10 ^ 2
        num = math.floor(num * nDecimal)
        num = num / nDecimal
        table.insert(xuanrenshuxingstringInfoyouxu1, {key, num, value[2]})
    end
    formSelf:GetWidgetProxyByName("List(1100)"):SetElementAmount(#xuanrenshuxingstringInfoyouxu1)
end


function xuanrenbaoshiye2Func()
    local ZPInfo2 = UIDynamicData.baoshiZhuangPeiInfo[2]
    xuanrenbaoShiYeShuXingInfo2 = {}
    xuanrenshuxingstringInfoyouxu2 = {}
    local baoshizhuangpeiinfo = ZPInfo2
    for i=1, #baoshizhuangpeiinfo do
        if #baoshizhuangpeiinfo[i] ~= 0 then
            local jieji = baoshizhuangpeiinfo[i][1]
            local dengji = baoshizhuangpeiinfo[i][2]
            local shuxing = baoshizhuangpeiinfo[i][3]
            local shuxingNum = baoshizhuangpeiinfo[i][4]
            local shuxingString
            local normal = true
            if i >= 1 and i <= 3 then
                shuxingString = UIConfigData.redshuxingStringInfo[shuxing]
            elseif i >= 4 and i <= 6 then
                shuxingString = UIConfigData.blueshuxingStringInfo[shuxing]
            elseif i >= 7 and i <= 9 then
                shuxingString = UIConfigData.purpleshuxingStringInfo[shuxing]
            elseif i >= 10 and i <= 12 then
                shuxingString = UIConfigData.greenshuxingStringInfo[shuxing]
                if shuxing == 2 or shuxing == 3 then
                    normal = false
                end
            end
            if xuanrenbaoShiYeShuXingInfo2[shuxingString] then
                xuanrenbaoShiYeShuXingInfo2[shuxingString][1] = xuanrenbaoShiYeShuXingInfo2[shuxingString][1] + shuxingNum * dengjijiaqiangInfo[dengji]
            else
                xuanrenbaoShiYeShuXingInfo2[shuxingString] = {shuxingNum * dengjijiaqiangInfo[dengji], normal}
            end
        end
    end
    for key, value in pairs(xuanrenbaoShiYeShuXingInfo2) do
        local num = value[1]

        local nDecimal = 10 ^ 2
        num = math.floor(num * nDecimal)
        num = num / nDecimal
        table.insert(xuanrenshuxingstringInfoyouxu2, {key, num, value[2]})
    end
    formSelf:GetWidgetProxyByName("List(1083)"):SetElementAmount(#xuanrenshuxingstringInfoyouxu2)
end


function xuanrenbaoshiye3Func()
    local ZPInfo3 = UIDynamicData.baoshiZhuangPeiInfo[3]
    xuanrenbaoShiYeShuXingInfo3 = {}
    xuanrenshuxingstringInfoyouxu3 = {}
    local baoshizhuangpeiinfo = ZPInfo3
    for i=1, #baoshizhuangpeiinfo do
        if #baoshizhuangpeiinfo[i] ~= 0 then
            local jieji = baoshizhuangpeiinfo[i][1]
            local dengji = baoshizhuangpeiinfo[i][2]
            local shuxing = baoshizhuangpeiinfo[i][3]
            local shuxingNum = baoshizhuangpeiinfo[i][4]
            local shuxingString
            local normal = true
            if i >= 1 and i <= 3 then
                shuxingString = UIConfigData.redshuxingStringInfo[shuxing]
            elseif i >= 4 and i <= 6 then
                shuxingString = UIConfigData.blueshuxingStringInfo[shuxing]
            elseif i >= 7 and i <= 9 then
                shuxingString = UIConfigData.purpleshuxingStringInfo[shuxing]
            elseif i >= 10 and i <= 12 then
                shuxingString = UIConfigData.greenshuxingStringInfo[shuxing]
                if shuxing == 2 or shuxing == 3 then
                    normal = false
                end
            end
            if xuanrenbaoShiYeShuXingInfo3[shuxingString] then
                xuanrenbaoShiYeShuXingInfo3[shuxingString][1] = xuanrenbaoShiYeShuXingInfo3[shuxingString][1] + shuxingNum * dengjijiaqiangInfo[dengji]
            else
                xuanrenbaoShiYeShuXingInfo3[shuxingString] = {shuxingNum * dengjijiaqiangInfo[dengji], normal}
            end
        end
    end
    for key, value in pairs(xuanrenbaoShiYeShuXingInfo3) do
        local num = value[1]

        local nDecimal = 10 ^ 2
        num = math.floor(num * nDecimal)
        num = num / nDecimal
        table.insert(xuanrenshuxingstringInfoyouxu3, {key, num, value[2]})
    end
    formSelf:GetWidgetProxyByName("List(1118)"):SetElementAmount(#xuanrenshuxingstringInfoyouxu3)
end


function xuanrenbsy1AllNumFunc()
    xuanrenallJiejiNum1 = 0
    xuanrenallDengjiNum1 = 0
    local baoshizhuangpeiinfo = UIDynamicData.baoshiZhuangPeiInfo[1]
    for i=1, #baoshizhuangpeiinfo do
        if #baoshizhuangpeiinfo[i] ~= 0 then
            local jieji = baoshizhuangpeiinfo[i][1]
            local dengji = baoshizhuangpeiinfo[i][2]

            xuanrenallJiejiNum1 = xuanrenallJiejiNum1 + jieji
            xuanrenallDengjiNum1 = xuanrenallDengjiNum1 + dengji
        end
    end
end


function xuanrenbsy2AllNumFunc()
    xuanrenallJiejiNum2 = 0
    xuanrenallDengjiNum2 = 0
    local baoshizhuangpeiinfo = UIDynamicData.baoshiZhuangPeiInfo[2]
    for i=1, #baoshizhuangpeiinfo do
        if #baoshizhuangpeiinfo[i] ~= 0 then
            local jieji = baoshizhuangpeiinfo[i][1]
            local dengji = baoshizhuangpeiinfo[i][2]

            xuanrenallJiejiNum2 = xuanrenallJiejiNum2 + jieji
            xuanrenallDengjiNum2 = xuanrenallDengjiNum2 + dengji
        end
    end
end


function xuanrenbsy3AllNumFunc()
    xuanrenallJiejiNum3 = 0
    xuanrenallDengjiNum3 = 0
    local baoshizhuangpeiinfo = UIDynamicData.baoshiZhuangPeiInfo[3]
    for i=1, #baoshizhuangpeiinfo do
        if #baoshizhuangpeiinfo[i] ~= 0 then
            local jieji = baoshizhuangpeiinfo[i][1]
            local dengji = baoshizhuangpeiinfo[i][2]

            xuanrenallJiejiNum3 = xuanrenallJiejiNum3 + jieji
            xuanrenallDengjiNum3 = xuanrenallDengjiNum3 + dengji
        end
    end
end








function xuanrenbsy1SXOnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()

    local zuostring = xuanrenshuxingstringInfoyouxu1[index+1][1]
    local youstring = xuanrenshuxingstringInfoyouxu1[index+1][2]
    local isNormal = xuanrenshuxingstringInfoyouxu1[index+1][3]
    local shuxingZhiText
    if isNormal == true then
        shuxingZhiText = "+" .. youstring .. "%"
    else
        shuxingZhiText = "+" .. youstring
    end

    elem:GetWidgetProxyByName("TextLabel(1102)"):GetText():SetContent(zuostring)
    elem:GetWidgetProxyByName("TextLabel(1103)"):GetText():SetContent(shuxingZhiText)
end


function xuanrenbsy2SXOnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()

    local zuostring = xuanrenshuxingstringInfoyouxu2[index+1][1]
    local youstring = xuanrenshuxingstringInfoyouxu2[index+1][2]
    local isNormal = xuanrenshuxingstringInfoyouxu2[index+1][3]
    local shuxingZhiText
    if isNormal == true then
        shuxingZhiText = "+" .. youstring .. "%"
    else
        shuxingZhiText = "+" .. youstring
    end

    elem:GetWidgetProxyByName("TextLabel(1085)"):GetText():SetContent(zuostring)
    elem:GetWidgetProxyByName("TextLabel(1086)"):GetText():SetContent(shuxingZhiText)
end



function xuanrenbsy3SXOnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()

    local zuostring = xuanrenshuxingstringInfoyouxu3[index+1][1]
    local youstring = xuanrenshuxingstringInfoyouxu3[index+1][2]
    local isNormal = xuanrenshuxingstringInfoyouxu3[index+1][3]
    local shuxingZhiText
    if isNormal == true then
        shuxingZhiText = "+" .. youstring .. "%"
    else
        shuxingZhiText = "+" .. youstring
    end

    elem:GetWidgetProxyByName("TextLabel(1120)"):GetText():SetContent(zuostring)
    elem:GetWidgetProxyByName("TextLabel(1121)"):GetText():SetContent(shuxingZhiText)
end


function xuanrentzsxBSY1OnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()

    if index >= 0 and index <= 4 then
        if xuanrenallJiejiNum1 >= tzsxBaoShiJieJi[index+1][1] then
            elem:GetWidgetProxyByName("TextLabel(1109)"):GetText():SetContent("<color=#FFFFFF>总阶级 " .. tzsxBaoShiJieJi[index+1][1] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1110)"):GetText():SetContent("<color=#FFFFFF>" .. tzsxBaoShiJieJi[index+1][2] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1111)"):GetText():SetContent("<color=#FFFF00>" .. tzsxBaoShiJieJi[index+1][3] .. "</color>")
        else
            elem:GetWidgetProxyByName("TextLabel(1109)"):GetText():SetContent("<color=#A9A9A9>总阶级 " .. tzsxBaoShiJieJi[index+1][1] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1110)"):GetText():SetContent("<color=#A9A9A9>" .. tzsxBaoShiJieJi[index+1][2] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1111)"):GetText():SetContent("<color=#A9A9A9>" .. tzsxBaoShiJieJi[index+1][3] .. "</color>")

        end
    elseif index == 5 then
        elem:GetWidgetProxyByName("TextLabel(1109)"):GetText():SetContent("")
        elem:GetWidgetProxyByName("TextLabel(1110)"):GetText():SetContent("")
        elem:GetWidgetProxyByName("TextLabel(1111)"):GetText():SetContent("")
    else
        if xuanrenallDengjiNum1 >= tzsxBaoShiDengJi[index+1-6][1] then
            elem:GetWidgetProxyByName("TextLabel(1109)"):GetText():SetContent("<color=#FFFFFF>总等级 " .. tzsxBaoShiDengJi[index+1-6][1] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1110)"):GetText():SetContent("<color=#FFFFFF>" .. tzsxBaoShiDengJi[index+1-6][2] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1111)"):GetText():SetContent("<color=#FFFF00>" .. tzsxBaoShiDengJi[index+1-6][3] .. "</color>")

        else
            elem:GetWidgetProxyByName("TextLabel(1109)"):GetText():SetContent("<color=#A9A9A9>总等级 " .. tzsxBaoShiDengJi[index+1-6][1] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1110)"):GetText():SetContent("<color=#A9A9A9>" .. tzsxBaoShiDengJi[index+1-6][2] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1111)"):GetText():SetContent("<color=#A9A9A9>" .. tzsxBaoShiDengJi[index+1-6][3] .. "</color>")

        end
    end
end



function xuanrentzsxBSY2OnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()

    if index >= 0 and index <= 4 then
        if xuanrenallJiejiNum2 >= tzsxBaoShiJieJi[index+1][1] then
            elem:GetWidgetProxyByName("TextLabel(1092)"):GetText():SetContent("<color=#FFFFFF>总阶级 " .. tzsxBaoShiJieJi[index+1][1] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1093)"):GetText():SetContent("<color=#FFFFFF>" .. tzsxBaoShiJieJi[index+1][2] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1094)"):GetText():SetContent("<color=#FFFF00>" .. tzsxBaoShiJieJi[index+1][3] .. "</color>")
        else
            elem:GetWidgetProxyByName("TextLabel(1092)"):GetText():SetContent("<color=#A9A9A9>总阶级 " .. tzsxBaoShiJieJi[index+1][1] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1093)"):GetText():SetContent("<color=#A9A9A9>" .. tzsxBaoShiJieJi[index+1][2] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1094)"):GetText():SetContent("<color=#A9A9A9>" .. tzsxBaoShiJieJi[index+1][3] .. "</color>")

        end
    elseif index == 5 then
        elem:GetWidgetProxyByName("TextLabel(1092)"):GetText():SetContent("")
        elem:GetWidgetProxyByName("TextLabel(1093)"):GetText():SetContent("")
        elem:GetWidgetProxyByName("TextLabel(1094)"):GetText():SetContent("")
    else
        if xuanrenallDengjiNum2 >= tzsxBaoShiDengJi[index+1-6][1] then
            elem:GetWidgetProxyByName("TextLabel(1092)"):GetText():SetContent("<color=#FFFFFF>总等级 " .. tzsxBaoShiDengJi[index+1-6][1] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1093)"):GetText():SetContent("<color=#FFFFFF>" .. tzsxBaoShiDengJi[index+1-6][2] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1094)"):GetText():SetContent("<color=#FFFF00>" .. tzsxBaoShiDengJi[index+1-6][3] .. "</color>")

        else
            elem:GetWidgetProxyByName("TextLabel(1092)"):GetText():SetContent("<color=#A9A9A9>总等级 " .. tzsxBaoShiDengJi[index+1-6][1] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1093)"):GetText():SetContent("<color=#A9A9A9>" .. tzsxBaoShiDengJi[index+1-6][2] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1094)"):GetText():SetContent("<color=#A9A9A9>" .. tzsxBaoShiDengJi[index+1-6][3] .. "</color>")

        end
    end
end



function xuanrentzsxBSY3OnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()

    if index >= 0 and index <= 4 then
        if xuanrenallJiejiNum3 >= tzsxBaoShiJieJi[index+1][1] then
            elem:GetWidgetProxyByName("TextLabel(1127)"):GetText():SetContent("<color=#FFFFFF>总阶级 " .. tzsxBaoShiJieJi[index+1][1] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1128)"):GetText():SetContent("<color=#FFFFFF>" .. tzsxBaoShiJieJi[index+1][2] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1129)"):GetText():SetContent("<color=#FFFF00>" .. tzsxBaoShiJieJi[index+1][3] .. "</color>")
        else
            elem:GetWidgetProxyByName("TextLabel(1127)"):GetText():SetContent("<color=#A9A9A9>总阶级 " .. tzsxBaoShiJieJi[index+1][1] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1128)"):GetText():SetContent("<color=#A9A9A9>" .. tzsxBaoShiJieJi[index+1][2] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1129)"):GetText():SetContent("<color=#A9A9A9>" .. tzsxBaoShiJieJi[index+1][3] .. "</color>")

        end
    elseif index == 5 then
        elem:GetWidgetProxyByName("TextLabel(1127)"):GetText():SetContent("")
        elem:GetWidgetProxyByName("TextLabel(1128)"):GetText():SetContent("")
        elem:GetWidgetProxyByName("TextLabel(1129)"):GetText():SetContent("")
    else
        if xuanrenallDengjiNum3 >= tzsxBaoShiDengJi[index+1-6][1] then
            elem:GetWidgetProxyByName("TextLabel(1127)"):GetText():SetContent("<color=#FFFFFF>总等级 " .. tzsxBaoShiDengJi[index+1-6][1] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1128)"):GetText():SetContent("<color=#FFFFFF>" .. tzsxBaoShiDengJi[index+1-6][2] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1129)"):GetText():SetContent("<color=#FFFF00>" .. tzsxBaoShiDengJi[index+1-6][3] .. "</color>")

        else
            elem:GetWidgetProxyByName("TextLabel(1127)"):GetText():SetContent("<color=#A9A9A9>总等级 " .. tzsxBaoShiDengJi[index+1-6][1] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1128)"):GetText():SetContent("<color=#A9A9A9>" .. tzsxBaoShiDengJi[index+1-6][2] .. "</color>")
            elem:GetWidgetProxyByName("TextLabel(1129)"):GetText():SetContent("<color=#A9A9A9>" .. tzsxBaoShiDengJi[index+1-6][3] .. "</color>")

        end
    end
end







function xuanrenbaoshiyeClicked(LuaUIEvent)
    xuanrenbsy1AllNumFunc()
    xuanrenbsy2AllNumFunc()
    xuanrenbsy3AllNumFunc()

    xuanrenbaoshiye1Func()
    xuanrenbaoshiye2Func()
    xuanrenbaoshiye3Func()


    formSelf:GetWidgetProxyByName("List(1107)"):SetElementAmount(12)
    formSelf:GetWidgetProxyByName("List(1090)"):SetElementAmount(12)
    formSelf:GetWidgetProxyByName("List(1125)"):SetElementAmount(12)

    if baoshiye == 1 then
        formSelf:GetWidgetProxyByName("Button(1112)"):SetActive(false)
        formSelf:GetWidgetProxyByName("Button(1130)"):SetActive(true)
        formSelf:GetWidgetProxyByName("Button(1131)"):SetActive(true)
    elseif baoshiye == 2 then
        formSelf:GetWidgetProxyByName("Button(1112)"):SetActive(true)
        formSelf:GetWidgetProxyByName("Button(1130)"):SetActive(false)
        formSelf:GetWidgetProxyByName("Button(1131)"):SetActive(true)
    else
        formSelf:GetWidgetProxyByName("Button(1112)"):SetActive(true)
        formSelf:GetWidgetProxyByName("Button(1130)"):SetActive(true)
        formSelf:GetWidgetProxyByName("Button(1131)"):SetActive(false)
    end

	formSelf:GetWidgetProxyByName("Panel(1073)"):SetActive(true)
end



function xuanrenbsyBJ1Clicked(LuaUIEvent)
	baoshiye = 1
	local operateCmd = {}
	operateCmd.cmdType = 6;
	operateCmd.playerID = selfPlayerID
	operateCmd.bsy = 1
	operatData = json.encode(operateCmd)
	LuaCallCs_UGCStateDriver.SendOperateCmd(operatData)

    formSelf:GetWidgetProxyByName("TextLabel(1070)"):GetText():SetContent("宝石页1")

	LuaCallCs_UGCStateDriver.SendOperateCmd(operatData)

    formSelf:GetWidgetProxyByName("Panel(1073)"):SetActive(false)
end

function xuanrenbsyBJ2Clicked(LuaUIEvent)
	baoshiye = 2
	local operateCmd = {}
	operateCmd.cmdType = 6;
	operateCmd.playerID = selfPlayerID
	operateCmd.bsy = 2
	operatData = json.encode(operateCmd)
	LuaCallCs_UGCStateDriver.SendOperateCmd(operatData)

    formSelf:GetWidgetProxyByName("TextLabel(1070)"):GetText():SetContent("宝石页2")

    formSelf:GetWidgetProxyByName("Panel(1073)"):SetActive(false)
end

function xuanrenbsyBJ3Clicked(LuaUIEvent)
	baoshiye = 3
	local operateCmd = {}
	operateCmd.cmdType = 6;
	operateCmd.playerID = selfPlayerID
	operateCmd.bsy = 3
	operatData = json.encode(operateCmd)
	LuaCallCs_UGCStateDriver.SendOperateCmd(operatData)




    formSelf:GetWidgetProxyByName("TextLabel(1070)"):GetText():SetContent("宝石页3")

    formSelf:GetWidgetProxyByName("Panel(1073)"):SetActive(false)
end




function xuanrenbaoshiyeClosed(LuaUIEvent)
    formSelf:GetWidgetProxyByName("Panel(1073)"):SetActive(false)
end





--选择时间结束
function xzsj_end(LuaUIEvent)
	--操作完成，通知服务器操作完成，所有的玩家都确认后服务器下发开局协议
	LuaCallCs_UGCStateDriver.SendCustomOperationCompleted(UIConfigData.levelName)
	LuaCallCs_UI.CloseForm("UI/OnlineMode/OperationForm.uixml")--关闭选择
end

--选择时间变化
function xzsj_change(LuaUIEvent)
	math.randomseed(tostring(os.time()):reverse():sub(1,7))
	math.random()
	local time = l_time:GetCurrentTime()
	if time then
		local operateCmd = {}
		operateCmd.cmdType = 4;
		operateCmd.playerID = selfPlayerID
		operateCmd.time = time
		operatData = json.encode(operateCmd)
		LuaCallCs_UGCStateDriver.SendOperateCmd(operatData)
	end

	if math.floor(time) <= 10 then
		l_time:GetDisplayText():SetColor(BluePrint.UGC.UI.Core.Color(1,0,0,1));
	end
	if math.floor(time) <= 5 then
		--如果没有选择任何英雄
		if l_selectedIndex == nil then
			l_selectedIndex = math.random(1,#heroidlist) - 1
			local operateCmd = {}
			operateCmd.cmdType = 1;
			operateCmd.playerID = selfPlayerID
			operateCmd.heroID = heroidlist[l_selectedIndex+1]
			operateCmd.confirmed = 0
			operateCmd.index = nil
			operatData = json.encode(operateCmd)
			LuaCallCs_UGCStateDriver.SendOperateCmd(operatData)
		end

		local PID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
		if selfPlayerID == PID then
			l_PlayerList:SetElementAmount(4)
			--关闭确定按钮监听
			l_confirmedBtn:EnableInput(false)
			--关闭List操作监听
			l_HeroList:EnableInput(false)
			--关闭Map下拉选择监听
			-- l_mapdrop:EnableInput(false)
			l_roll_hero:EnableInput(false)
		end
		--构造
		local operateCmd = {}
		operateCmd.cmdType = 3;
		operateCmd.playerID = selfPlayerID
		operateCmd.heroID = heroidlist[l_selectedIndex+1]
		operateCmd.confirmed = 1
		operatData = json.encode(operateCmd)
		--发送自定义操作命令
		LuaCallCs_UGCStateDriver.SendOperateCmd(operatData)
	end
end


function OnStartupClose()
	formSelf = nil
	l_mapdrop = nil
	l_heroid = nil
	l_heroiddrop = nil
	l_heroidlist = nil
	l_roll_hero = nil
	l_HeroList = nil
	l_selectedIndex = nil
	l_PlayerList = nil
	PlayerData = {}
	selfPlayerID = nil
	l_confirmedBtn = nil
	l_time = nil
	suijiHeroCntInfo = {}
	baoshiye = 1
	xuanrenbaoShiYeShuXingInfo1 = {}
	xuanrenshuxingstringInfoyouxu1 = {}
	xuanrenbaoShiYeShuXingInfo2 = {}
	xuanrenshuxingstringInfoyouxu2 = {}
	xuanrenbaoShiYeShuXingInfo3 = {}
	xuanrenshuxingstringInfoyouxu3 = {}
	xuanrenallJiejiNum1 = 0
	xuanrenallDengjiNum1 = 0
	xuanrenallJiejiNum2 = 0
	xuanrenallDengjiNum2 = 0
	xuanrenallJiejiNum3 = 0
	xuanrenallDengjiNum3 = 0
end