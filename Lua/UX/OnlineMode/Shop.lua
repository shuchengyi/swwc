json = require "json.lua"

local formSelf

local itemName

local shopBtnMoneyText

local buff_panel

local equipBuffInfo = {0, 0, 0}
local isJNSPanelOpen = false

local nowChallenge
local isChallengePanelShow = false


local fuMoPanel
local fuMoList

local jnsList


local BagBuffText
local BagBuffImg

local jnsPanel

local tiaozhanList
local txyzText
local txyzPanel

local txyzImg


local bagBuffTipsPanel
local bagBuffTipsText





local challengeInfo = {
    ['粮草押运官'] = {"Texture/Sprite/jinbiguai.sprite",'召唤10名粮草押运官，击杀后可获得高额的金币奖励；\n并且获得最大生命值+500，攻击+10，护甲+10',500,1000},
    ['魔袋长老'] = {"Texture/Sprite/jinkuang.sprite",'召唤魔袋长老，魔袋长老会给周围敌军提供强力光环效果，请尽快击杀，击杀后可获得黄金魔袋，魔袋等级随着召唤次数的增加而提高；\n并且获得最大生命值+500，攻击+50，护甲+50',1000,1000},
    ['深渊魔王'] = {"Texture/Sprite/diyuhuo.sprite",'召唤深渊魔王，深渊魔王非常恐怖，请慎重召唤，击杀后可升级王城圣物，品质随着召唤次数的增加而提升；\n并且获得最大生命值+1500，攻击+100，护甲+100',1000,2000},
}


local bagInfo = {
    {'黄金魔袋1级',"Texture/Sprite/modai1.sprite",'激活后刷钱效率+10%，击败50个敌人后黄金魔袋被消耗，奖励2000金币，同时只能激活一个魔袋',50,2000}, -- 击败数，奖励金钱数
    {'黄金魔袋2级',"Texture/Sprite/modai2.sprite",'激活后刷钱效率+10%，击败50个敌人后黄金魔袋被消耗，奖励2600金币，同时只能激活一个魔袋',50,2600},
    {'黄金魔袋3级',"Texture/Sprite/modai3.sprite",'激活后刷钱效率+10%，击败50个敌人后黄金魔袋被消耗，奖励3380金币，同时只能激活一个魔袋',50,3380},
    {'黄金魔袋4级',"Texture/Sprite/modai4.sprite",'激活后刷钱效率+10%，击败50个敌人后黄金魔袋被消耗，奖励4394金币，同时只能激活一个魔袋',50,4394},
    {'黄金魔袋5级',"Texture/Sprite/modai4.sprite",'激活后刷钱效率+10%，击败50个敌人后黄金魔袋被消耗，奖励5712金币，同时只能激活一个魔袋',50,5712},
    {'黄金魔袋6级',"Texture/Sprite/modai4.sprite",'激活后刷钱效率+10%，击败50个敌人后黄金魔袋被消耗，奖励7426金币，同时只能激活一个魔袋',50,7426},
    {'黄金魔袋7级',"Texture/Sprite/modai4.sprite",'激活后刷钱效率+10%，击败50个敌人后黄金魔袋被消耗，奖励9654金币，同时只能激活一个魔袋',50,9654},
    {'黄金魔袋8级',"Texture/Sprite/modai4.sprite",'激活后刷钱效率+10%，击败50个敌人后黄金魔袋被消耗，奖励12550金币，同时只能激活一个魔袋',50,12550},
    {'黄金魔袋9级',"Texture/Sprite/modai4.sprite",'激活后刷钱效率+10%，击败50个敌人后黄金魔袋被消耗，奖励16315金币，同时只能激活一个魔袋',50,16315},
    {'黄金魔袋10级',"Texture/Sprite/modai4.sprite",'激活后刷钱效率+10%，击败50个敌人后黄金魔袋被消耗，奖励21209金币，同时只能激活一个魔袋',50,21209},
    {'黄金魔袋11级',"Texture/Sprite/modai4.sprite",'激活后刷钱效率+10%，击败50个敌人后黄金魔袋被消耗，奖励27572金币，同时只能激活一个魔袋',50,27572},
    {'黄金魔袋12级',"Texture/Sprite/modai4.sprite",'激活后刷钱效率+10%，击败50个敌人后黄金魔袋被消耗，奖励35843金币，同时只能激活一个魔袋',50,35843},
    {'黄金魔袋13级',"Texture/Sprite/modai4.sprite",'激活后刷钱效率+10%，击败50个敌人后黄金魔袋被消耗，奖励46596金币，同时只能激活一个魔袋',50,46596},
    {'黄金魔袋14级',"Texture/Sprite/modai4.sprite",'激活后刷钱效率+10%，击败50个敌人后黄金魔袋被消耗，奖励60575金币，同时只能激活一个魔袋',50,60575},
    {'黄金魔袋15级',"Texture/Sprite/modai4.sprite",'激活后刷钱效率+10%，击败50个敌人后黄金魔袋被消耗，奖励78748金币，同时只能激活一个魔袋',50,78748},
    {'黄金魔袋16级',"Texture/Sprite/modai4.sprite",'激活后刷钱效率+10%，击败50个敌人后黄金魔袋被消耗，奖励102372金币，同时只能激活一个魔袋',50,102372},
}




local fumoInfo = {
    {
        {"Texture/Sprite/wuqi.sprite",'攻击力<color=#f18d00>+25</color>\n法术攻击<color=#f18d00>+25</color>',{25, 25},1000},
        {"Texture/Sprite/wuqi.sprite",'攻击力<color=#f18d00>+50</color>\n法术攻击<color=#f18d00>+50</color>',{63, 63},2500},
        {"Texture/Sprite/wuqi.sprite",'攻击力<color=#f18d00>+100</color>\n法术攻击<color=#f18d00>+100</color>',{125, 125},5000},
        {"Texture/Sprite/wuqi.sprite",'攻击力<color=#f18d00>+150</color>\n法术攻击<color=#f18d00>+150</color>',{200, 200},8000},
        {"Texture/Sprite/wuqi.sprite",'攻击力<color=#f18d00>+225</color>\n法术攻击<color=#f18d00>+225</color>',{300, 300},12000},
        {"Texture/Sprite/wuqi.sprite",'攻击力<color=#f18d00>+225</color>\n法术攻击<color=#f18d00>+225</color>',{500, 500},20000},
        {"Texture/Sprite/wuqi.sprite",'攻击力<color=#f18d00>+225</color>\n法术攻击<color=#f18d00>+225</color>',{1250, 1250},50000},
        {"Texture/Sprite/wuqi.sprite",'攻击力<color=#f18d00>+225</color>\n法术攻击<color=#f18d00>+225</color>',{2000, 2000},80000},
    },
    {
        {"Texture/Sprite/hujia.sprite",'护甲<color=#f18d00>+25</color>\n魔抗<color=#f18d00>+25</color>',{1000,10,10},1000},
        {"Texture/Sprite/hujia.sprite",'护甲<color=#f18d00>+50</color>\n魔抗<color=#f18d00>+50</color>',{1000,25,25},2500},
        {"Texture/Sprite/hujia.sprite",'护甲<color=#f18d00>+100</color>\n魔抗<color=#f18d00>+100</color>',{1000,50,50},5000},
        {"Texture/Sprite/hujia.sprite",'护甲<color=#f18d00>+150</color>\n魔抗<color=#f18d00>+150</color>',{1000,80,80},8000},
        {"Texture/Sprite/hujia.sprite",'护甲<color=#f18d00>+225</color>\n魔抗<color=#f18d00>+225</color>',{1000,120,120},12000},
        {"Texture/Sprite/hujia.sprite",'护甲<color=#f18d00>+225</color>\n魔抗<color=#f18d00>+225</color>',{1000,200,200},20000},
        {"Texture/Sprite/hujia.sprite",'护甲<color=#f18d00>+225</color>\n魔抗<color=#f18d00>+225</color>',{1000,500,500},50000},
        {"Texture/Sprite/hujia.sprite",'护甲<color=#f18d00>+225</color>\n魔抗<color=#f18d00>+225</color>',{1000,800,800},80000},
    },
}


local jnsInfo = {
    {"Texture/Sprite/Db.sprite",'获得一本D级被动技能书，每次购买所需金币翻倍，单局最多购买10次',1,1000}, -- 被动
    {"Texture/Sprite/Cb.sprite",'获得一本C级被动技能书，每次购买所需金币翻倍，单局最多购买10次',2,2500},
    {"Texture/Sprite/Bb.sprite",'获得一本B级被动技能书，每次购买所需金币翻倍，单局最多购买10次',3,6000},
    {"Texture/Sprite/Ab.sprite",'获得一本A级被动技能书，每次购买所需金币翻倍，单局最多购买10次',4,15000},

    {"Texture/Sprite/Dz.sprite",'获得一本D级主动技能书，每次购买所需金币翻倍，单局最多购买10次',1,1000}, -- 主动
    {"Texture/Sprite/Cz.sprite",'获得一本C级主动技能书，每次购买所需金币翻倍，单局最多购买10次',2,2500},
    {"Texture/Sprite/Bz.sprite",'获得一本B级主动技能书，每次购买所需金币翻倍，单局最多购买10次',3,6000},
    {"Texture/Sprite/Az.sprite",'获得一本A级主动技能书，每次购买所需金币翻倍，单局最多购买10次',4,15000},
}

local yaoShuiInfo = {
    {"Texture/Sprite/HPMedicine.sprite",'获得一瓶恢复药水，可在5秒内恢复30%生命值',0,500}, -- ['恢 复 药 水']
    {"Texture/Sprite/MPMedicine.sprite",'获得一瓶回蓝药水，可在5秒内恢复30%法力值',0,300}, -- ['回 蓝 药 水']
}



local allTipsInfo = {
    ["粮草押运官"] = {"粮草押运官", "级        难度：<color=#00FF00>低</color>\n状态：", "\n\n召唤10名粮草押运官，击杀后可获得: \n\n<color=#00FF00>金币+","\n最大生命值+500\n攻击+10\n护甲+10</color>"},
    ["魔袋长老"] = {"魔袋长老", "级        难度：<color=#FFFF00>中</color>\n状态：", "\n\n召唤魔袋长老，魔袋长老会给周围敌军提供强力光环效果，请尽快击杀，击杀后可获得:\n\n<color=#00FF00>黄金魔袋Lv", "\n最大生命值+500\n攻击+50\n护甲+50</color>"},
    ["深渊魔王"] = {"深渊魔王", "级        难度：<color=#FF0000>高</color>\n状态：", "\n\n召唤深渊魔王，深渊魔王非常恐怖，请慎重召唤，击杀后可获得：\n\n<color=#00FF00>王城圣物等级+1\n最大生命值+1500\n攻击+100\n护甲+100</color>"},


    ["武器附魔"] = {"武器附魔", "\n下一阶段：", "\n状态：", "\n\n升级后获得："},
    ["护甲附魔"] = {"护甲附魔", "\n下一阶段：", "\n状态：", "\n\n升级后获得："},
    ["王城圣物"] = {},
}


-- OnOpen事件函数
--商店初始化
function OnShopUIOpen(LuaUIEvent)
    -- 废掉第一次随机
    math.randomseed(tostring(os.time()):reverse():sub(1,7))
    math.random()

    formSelf = LuaUIEvent.SrcForm
    formSelf:Hide()

    buff_panel = formSelf:GetWidgetProxyByName("buff_panel")

    BagBuffText = formSelf:GetWidgetProxyByName("BagBuffText"):GetText()
    BagBuffImg = formSelf:GetWidgetProxyByName("BagBuff"):GetImage()
    formSelf:GetWidgetProxyByName("BagBuff"):SetHoldStartTime(0)

    formSelf:GetWidgetProxyByName("Panel(1221)"):SetActive(false)

    txyzText = formSelf:GetWidgetProxyByName("TextLabel(1194)")

    txyzPanel = formSelf:GetWidgetProxyByName("Panel(1219)")
    txyzPanel:SetActive(false)


    bagBuffTipsPanel = formSelf:GetWidgetProxyByName("bagBuffTipsPanel")
    bagBuffTipsText = formSelf:GetWidgetProxyByName("bagBuffTipsText"):GetText()
    bagBuffTipsPanel:SetActive(false)

    fuMoList = formSelf:GetWidgetProxyByName("fuMoList")
    fuMoList:SetElementAmount(#fumoInfo)
    for i = 0, (#fumoInfo-1) do
        fuMoList:GetListElement(i):GetWidgetProxyByName("ClickedImg"):SetHoldStartTime(0.2)
    end

    tiaozhanList = formSelf:GetWidgetProxyByName("List(1199)")
    tiaozhanList:SetElementAmount(3)
    for i = 0, 2 do
        tiaozhanList:GetListElement(i):GetWidgetProxyByName("Image(1207)"):SetHoldStartTime(0.2)
    end

    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    allTipsInfo["王城圣物"][selfPlayerID] = "王城圣物Lv0\n\n暴击+0%\n吸血+0%\n移速++0%\n\n通过击败[深渊魔王]来升级王城圣物"


    jnsList = formSelf:GetWidgetProxyByName("jnsList")
    jnsList:SetElementAmount(#jnsInfo)


    fuMoPanel = formSelf:GetWidgetProxyByName("fuMoPanel")
    fuMoPanel:SetActive(true)

    jnsPanel = formSelf:GetWidgetProxyByName("jnsPanel")
    jnsPanel:SetActive(false)

    shopBtnMoneyText = formSelf:GetWidgetProxyByName("shopBtnMoney"):GetText()

    buff_panel:SetActive(false)

    txyzImg = formSelf:GetWidgetProxyByName("Image(1193)")
    txyzImg:SetHoldStartTime(0)

    formSelf:GetWidgetProxyByName("Timer(1074)"):StartTimer()
end

function shopAppear()
    formSelf:Appear()
end


local jnsNameInfo = {
    [1] = "<color=#00FF00>D</color>级被动技能书",
    [2] = "<color=#1E90FF>C</color>级被动技能书",
    [3] = "<color=#8A2BE2>B</color>级被动技能书",
    [4] = "<color=#EE7600>A</color>级被动技能书",
    [5] = "<color=#00FF00>D</color>级主动技能书",
    [6] = "<color=#1E90FF>C</color>级主动技能书",
    [7] = "<color=#8A2BE2>B</color>级主动技能书",
    [8] = "<color=#EE7600>A</color>级主动技能书",
}



function jnsListOnEnable(LuaUIEvent)
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    if UIDynamicData.itsOKInfo[selfPlayerID] ~= true then
        return
    end

    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()
    local imgPath = jnsInfo[index+1][1]
    elem:GetWidgetProxyByName("jnsImg"):GetImage():SetRes(imgPath)


    -- elem:GetWidgetProxyByName("TextLabel(1250)"):GetText():SetContent(jnsNameInfo[index+1])


    local buyCount = UIDynamicData.skillBookBuyCountList[selfPlayerID][index+1]
    elem:GetWidgetProxyByName("jnsCnt"):GetText():SetContent(10-buyCount)
    local needMoney = (2^buyCount)*jnsInfo[index+1][4]
    elem:GetWidgetProxyByName("jnsBuyBtn"):SetActive(true)
    if index == 0 or index == 4 then
        elem:GetWidgetProxyByName("jnsBuyBtn"):GetText():SetContent("   " .. needMoney)
        if UIDynamicData.money >= needMoney then
            elem:GetWidgetProxyByName("jnsBuyBtn"):EnableInput(true)
            if (10-buyCount) <= 0 then
                elem:GetWidgetProxyByName("jnsBuyBtn"):EnableInput(false)
            end
        else
            elem:GetWidgetProxyByName("jnsBuyBtn"):EnableInput(false)
        end
    elseif index == 1 or index == 5 then
        if UIDynamicData.boci < 5 then
            elem:GetWidgetProxyByName("jnsBuyBtn"):GetText():SetContent("    5波开放")
            elem:GetWidgetProxyByName("jnsBuyBtn"):EnableInput(false)
        else
            elem:GetWidgetProxyByName("jnsBuyBtn"):GetText():SetContent("    " .. needMoney)
            if UIDynamicData.money >= needMoney then
                elem:GetWidgetProxyByName("jnsBuyBtn"):EnableInput(true)
                if (10-buyCount) <= 0 then
                    elem:GetWidgetProxyByName("jnsBuyBtn"):EnableInput(false)
                end
            else
                elem:GetWidgetProxyByName("jnsBuyBtn"):EnableInput(false)
            end
        end
    elseif index == 2  or index == 6 then
        if UIDynamicData.boci < 15 then
            elem:GetWidgetProxyByName("jnsBuyBtn"):GetText():SetContent("    15波开放")
            elem:GetWidgetProxyByName("jnsBuyBtn"):EnableInput(false)
        else
            elem:GetWidgetProxyByName("jnsBuyBtn"):GetText():SetContent("    " .. needMoney)
            if UIDynamicData.money >= needMoney then
                elem:GetWidgetProxyByName("jnsBuyBtn"):EnableInput(true)
                if (10-buyCount) <= 0 then
                    elem:GetWidgetProxyByName("jnsBuyBtn"):EnableInput(false)
                end
            else
                elem:GetWidgetProxyByName("jnsBuyBtn"):EnableInput(false)
            end
        end
    elseif index == 3  or index == 7 then
        if UIDynamicData.boci < 25 then
            elem:GetWidgetProxyByName("jnsBuyBtn"):GetText():SetContent("    25波开放")
            elem:GetWidgetProxyByName("jnsBuyBtn"):EnableInput(false)
        else
            elem:GetWidgetProxyByName("jnsBuyBtn"):GetText():SetContent("    " .. needMoney)
            if UIDynamicData.money >= needMoney then
                elem:GetWidgetProxyByName("jnsBuyBtn"):EnableInput(true)
                if (10-buyCount) <= 0 then
                    elem:GetWidgetProxyByName("jnsBuyBtn"):EnableInput(false)
                end
            else
                elem:GetWidgetProxyByName("jnsBuyBtn"):EnableInput(false)
            end
        end
    end

end




function checkIsCanBuySkillBook(selfPlayerId, num, needMoney)
    local isHave = false
    local iscanBuy = true

    if num == 1 then
        for index = 1, #UIDynamicData.daojuInfo[selfPlayerId] do
            if UIDynamicData.daojuInfo[selfPlayerId][index][1] == "D级被动技能书" then
                isHave = true
            end
        end

        if isHave == false then
            if #UIDynamicData.daojuInfo[selfPlayerId] >= 6 then
                iscanBuy = false
            end
        end
    elseif num == 5 then
        for index = 1, #UIDynamicData.daojuInfo[selfPlayerId] do
            if UIDynamicData.daojuInfo[selfPlayerId][index][1] == "D级主动技能书" then
                isHave = true
            end
        end

        if isHave == false then
            if #UIDynamicData.daojuInfo[selfPlayerId] >= 6 then
                iscanBuy = false
            end
        end
    elseif num == 2 then
        for index = 1, #UIDynamicData.daojuInfo[selfPlayerId] do
            if UIDynamicData.daojuInfo[selfPlayerId][index][1] == "C级被动技能书" then
                isHave = true
            end
        end

        if isHave == false then
            if #UIDynamicData.daojuInfo[selfPlayerId] >= 6 then
                iscanBuy = false
            end
        end
    elseif num == 6 then
        for index = 1, #UIDynamicData.daojuInfo[selfPlayerId] do
            if UIDynamicData.daojuInfo[selfPlayerId][index][1] == "C级主动技能书" then
                isHave = true
            end
        end

        if isHave == false then
            if #UIDynamicData.daojuInfo[selfPlayerId] >= 6 then
                iscanBuy = false
            end
        end
    elseif num == 3 then
        for index = 1, #UIDynamicData.daojuInfo[selfPlayerId] do
            if UIDynamicData.daojuInfo[selfPlayerId][index][1] == "B级被动技能书" then
                isHave = true
            end
        end

        if isHave == false then
            if #UIDynamicData.daojuInfo[selfPlayerId] >= 6 then
                iscanBuy = false
            end
        end
    elseif num == 7 then
        for index = 1, #UIDynamicData.daojuInfo[selfPlayerId] do
            if UIDynamicData.daojuInfo[selfPlayerId][index][1] == "B级主动技能书" then
                isHave = true
            end
        end

        if isHave == false then
            if #UIDynamicData.daojuInfo[selfPlayerId] >= 6 then
                iscanBuy = false
            end
        end
    elseif num == 4 then
        for index = 1, #UIDynamicData.daojuInfo[selfPlayerId] do
            if UIDynamicData.daojuInfo[selfPlayerId][index][1] == "A级被动技能书" then
                isHave = true
            end
        end

        if isHave == false then
            if #UIDynamicData.daojuInfo[selfPlayerId] >= 6 then
                iscanBuy = false
            end
        end
    elseif num == 8 then
        for index = 1, #UIDynamicData.daojuInfo[selfPlayerId] do
            if UIDynamicData.daojuInfo[selfPlayerId][index][1] == "A级主动技能书" then
                isHave = true
            end
        end

        if isHave == false then
            if #UIDynamicData.daojuInfo[selfPlayerId] >= 6 then
                iscanBuy = false
            end
        end
    elseif num == 9 then
        for index = 1, #UIDynamicData.daojuInfo[selfPlayerId] do
            if UIDynamicData.daojuInfo[selfPlayerId][index][1] == "回蓝药水" then
                isHave = true
            end
        end

        if isHave == false then
            if #UIDynamicData.daojuInfo[selfPlayerId] >= 6 then
                iscanBuy = false
            end
        end
    elseif num == 10 then
        for index = 1, #UIDynamicData.daojuInfo[selfPlayerId] do
            if UIDynamicData.daojuInfo[selfPlayerId][index][1] == "恢复药水" then
                isHave = true
            end
        end

        if isHave == false then
            if #UIDynamicData.daojuInfo[selfPlayerId] >= 6 then
                iscanBuy = false
            end
        end
    end

    refreshEquipInfo(selfPlayerId)


    if iscanBuy == false then
        addGMGongGaoInfo(selfPlayerId, "bbm")
    end

    return iscanBuy
end





-- 发送技能书给逻辑图的函数
local function sendSkillBook(buyCountListIndex)
    local msg = {}
    msg.cmd = "buySkillBook"
    msg.num = buyCountListIndex
    LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg)) -- 发给蓝图
end


function jnsBuyBtnClicked(LuaUIEvent)
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    if UIDynamicData.itsOKInfo[selfPlayerID] ~= true then
        return
    end

    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    local index = jnsList:GetSelectedIndex()
    sendSkillBook(index+1) -- A级主动
end



function jnsPanelCloseBtnClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    jnsPanel:SetActive(false)
    isJNSPanelOpen = false
end




function OnJNSClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    if isJNSPanelOpen == true then
        jnsPanel:SetActive(false)
        isJNSPanelOpen = false
    else
        jnsList:SetElementAmount(#jnsInfo)
        jnsPanel:SetActive(true)
        isJNSPanelOpen = true
    end
end






function fuMoOnEnable(LuaUIEvent)
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    if UIDynamicData.itsOKInfo[selfPlayerID] ~= true then
        return
    end

    if UIDynamicData.grandListInfo[selfPlayerID] == nil then
        return
    end

    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()
    local imgPath = fumoInfo[index+1][1][1]

    local level = UIDynamicData.grandListInfo[selfPlayerID][index+1][1]
    local jieji = UIDynamicData.grandListInfo[selfPlayerID][index+1][2]
    local isBossLive = UIDynamicData.grandListInfo[selfPlayerID][index+1][3]

    elem:GetWidgetProxyByName("fuMoImg"):GetImage():SetRes(imgPath)

    if index == 0 then
        elem:GetWidgetProxyByName("TextLabel(1203)"):GetText():SetContent("武器附魔")
    else
        elem:GetWidgetProxyByName("TextLabel(1203)"):GetText():SetContent("护甲附魔")
    end

    elem:GetWidgetProxyByName("jiejiHanZi"):GetImage():SetRes(UIConfigData.equipLevelImgInfo[jieji])


    local needMoney
    local needMoneyText
    if level > 5 then
        needMoney = 0
    else
        needMoney = fumoInfo[index+1][jieji][4]
    end
    needMoneyText = "   " .. needMoney

    if level > 5 then
        if jieji >= 8 then
            elem:GetWidgetProxyByName("lvBtn"):SetActive(false)
            UIDynamicData.fumoZhuangTaiInfo[selfPlayerID][index+1] = 9
            needMoneyText = ""
            elem:GetWidgetProxyByName("Image(1209)"):SetActive(false)
        else
            elem:GetWidgetProxyByName("lvText"):SetActive(false)
            elem:GetWidgetProxyByName("jiejiHanZiShang"):GetImage():SetRes(UIConfigData.equipLevelImgInfo[jieji+1])
            elem:GetWidgetProxyByName("jiejiHanZiShang"):SetActive(true)
            elem:GetWidgetProxyByName("lvBtn"):SetActive(true)
            needMoneyText = "挑战"
            elem:GetWidgetProxyByName("Image(1209)"):SetActive(false)
            if isBossLive == true then
                elem:GetWidgetProxyByName("fuMoImg"):EnableInput(false)
                UIDynamicData.fumoZhuangTaiInfo[selfPlayerID][index+1] = 8
            else
                elem:GetWidgetProxyByName("fuMoImg"):EnableInput(true)
                UIDynamicData.fumoZhuangTaiInfo[selfPlayerID][index+1] = 7
            end
        end
    else
        elem:GetWidgetProxyByName("lvBtn"):SetActive(true)
        elem:GetWidgetProxyByName("jiejiHanZiShang"):SetActive(false)
        elem:GetWidgetProxyByName("lvText"):GetText():SetContent(level)
        elem:GetWidgetProxyByName("lvText"):SetActive(true)
        elem:GetWidgetProxyByName("Image(1209)"):SetActive(true)

        if UIDynamicData.money < needMoney then
            elem:GetWidgetProxyByName("fuMoImg"):EnableInput(false)
            UIDynamicData.fumoZhuangTaiInfo[selfPlayerID][index+1] = 6
        else
            elem:GetWidgetProxyByName("fuMoImg"):EnableInput(true)
            UIDynamicData.fumoZhuangTaiInfo[selfPlayerID][index+1] = 5
        end
    end
    elem:GetWidgetProxyByName("TextLabel(1208)"):GetText():SetContent(needMoneyText)
end

local zhuangtaiInfo = {
    [0] = "<color=#00FF00>可以召唤</color>",
    [1] = "<color=#FF0000>冷却中</color>",
    [2] = "<color=#FFFF00>魔袋未消耗掉</color>",
    [3] = "<color=#FFFF00>上一级金矿还未击败</color>",
    [4] = "<color=#FFFF00>37波之后禁止召唤</color>",
    [5] = "<color=#00FF00>可以升级</color>",
    [6] = "<color=#FF0000>金币不足</color>",
    [7] = "<color=#00FF00>可以挑战</color>",
    [8] = "<color=#FFFF00>尚未击败附魔挑战怪物</color>",
    [9] = "<color=#EE7600>已经满阶满级了</color>",
}






function fuMoHoldStart(LuaUIEvent) -- zdc
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    local index = LuaUIEvent.SrcWidget:GetIndexInBelongedList()

    local level = UIDynamicData.grandListInfo[selfPlayerID][index+1][1]
    local jieji = UIDynamicData.grandListInfo[selfPlayerID][index+1][2]
    local isBossLive = UIDynamicData.grandListInfo[selfPlayerID][index+1][3]

    local text

    local zhuangTaiNum = UIDynamicData.fumoZhuangTaiInfo[selfPlayerID][index+1]
    local zhuangTaiText = zhuangtaiInfo[zhuangTaiNum]

    if index == 0 then
        local addShuXing = fumoInfo[index+1][jieji][3][1]
        local addShuXingText = "\n\n物理攻击+" .. addShuXing .. "\n法术攻击+" .. addShuXing
        local levelText = level
        if level > 5 then
            levelText = 5
        end
        text = allTipsInfo["武器附魔"][1].. jieji .. "阶" .. levelText .. "级" ..  allTipsInfo["武器附魔"][2]

        local nextJieDuanText
        if level > 5 then
            if jieji >= 8 then
                nextJieDuanText = "无"
                addShuXingText = "无"
            else
                if isBossLive == true then
                    nextJieDuanText = "武器附魔" .. (jieji+1) .. "阶1级"
                else
                    nextJieDuanText = "挑战附魔升阶怪物"
                end

                addShuXingText = "\n\n属性增强幅度增加：\n物理攻击+" .. fumoInfo[index+1][jieji+1][3][1] .. "\n法术攻击+" .. fumoInfo[index+1][jieji+1][3][1]
            end
        else
            if level == 5 then
                nextJieDuanText = "挑战附魔升阶怪物"
            else
                nextJieDuanText = "武器附魔" .. jieji .. "阶" .. (level+1) .. "级<color=#00FF00>↑</color>"
            end
        end
        text = text .. nextJieDuanText .. allTipsInfo["武器附魔"][3] .. zhuangTaiText .. allTipsInfo["武器附魔"][4]
        text = text .. addShuXingText
    else
        local addShuXing = fumoInfo[index+1][jieji][3][2]
        local addShuXingText = "\n\n护甲+" .. addShuXing .. "\n魔抗+" .. addShuXing
        local levelText = level
        if level > 5 then
            levelText = 5
        end
        text = allTipsInfo["护甲附魔"][1].. jieji .. "阶" .. levelText .. "级" ..  allTipsInfo["护甲附魔"][2]

        local nextJieDuanText
        if level > 5 then
            if jieji >= 8 then
                nextJieDuanText = "无"
                addShuXingText = "无"
            else
                if isBossLive == true then
                    nextJieDuanText = "护甲附魔" .. (jieji+1) .. "阶1级"
                else
                    nextJieDuanText = "挑战附魔升阶怪物"
                end

                addShuXingText = "\n\n属性增强幅度增加：\n护甲+" .. fumoInfo[index+1][jieji+1][3][2] .. "\n魔抗+" .. fumoInfo[index+1][jieji+1][3][2]
            end
        else
            if level == 5 then
                nextJieDuanText = "挑战附魔升阶怪物"
            else
                nextJieDuanText = "护甲附魔" .. jieji .. "阶" .. (level+1) .. "级<color=#00FF00>↑</color>"
            end
        end
        text = text .. nextJieDuanText .. allTipsInfo["护甲附魔"][3] .. zhuangTaiText .. allTipsInfo["护甲附魔"][4]
        text = text .. addShuXingText
    end

    txyzText:GetText():SetContent(text)
    txyzPanel:SetActive(true)
    LuaCallCs_Common.Log("hold start")
end


function fuMoHoldEnd(LuaUIEvent)
    LuaCallCs_Common.Log("hold end")
    txyzPanel:SetActive(false)
end







function yaoShuiOnEnable(LuaUIEvent) -- zdc
    local pid = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    if UIDynamicData.itsOKInfo[pid] == false then
        return
    end

    -- UIDynamicData.yaoshuiTimeInfo[pid][2][2]
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()

    local imgPath = yaoShuiInfo[index+1][1]
    elem:GetWidgetProxyByName("yaoShuiBtn"):GetImage():SetRes(imgPath)


    if UIDynamicData.yaoshuiTimeInfo[pid][index+1][1] ~= 0 then
        if UIDynamicData.time - UIDynamicData.yaoshuiTimeInfo[pid][index+1][2] < 50 then
            elem:GetWidgetProxyByName("yaoshuiDJSText"):GetText():SetContent(50-(UIDynamicData.time - UIDynamicData.yaoshuiTimeInfo[pid][index+1][2]))
            elem:GetWidgetProxyByName("yaoshuiDJSText"):SetActive(true)
            elem:GetWidgetProxyByName("yaoShuiBtn"):EnableInput(false)
            elem:GetWidgetProxyByName("Image(1173)"):SetActive(false)
        else
            elem:GetWidgetProxyByName("yaoshuiDJSText"):SetActive(false)
            elem:GetWidgetProxyByName("yaoShuiBtn"):EnableInput(true)
            elem:GetWidgetProxyByName("Image(1173)"):SetActive(true)
            if UIDynamicData.playerLiveInfo[pid] == false then
                elem:GetWidgetProxyByName("yaoShuiBtn"):EnableInput(false)
                elem:GetWidgetProxyByName("Image(1173)"):SetActive(false)
            end
        end
    else
        elem:GetWidgetProxyByName("yaoshuiDJSText"):SetActive(false)
        elem:GetWidgetProxyByName("yaoShuiBtn"):EnableInput(true)
        elem:GetWidgetProxyByName("Image(1173)"):SetActive(true)
        if UIDynamicData.playerLiveInfo[pid] == false then
            elem:GetWidgetProxyByName("yaoShuiBtn"):EnableInput(false)
            elem:GetWidgetProxyByName("Image(1173)"):SetActive(false)
        end
    end
end



function fuMoClicked1(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    local index = fuMoList:GetSelectedIndex()
    if index == 0 then
        -- UIDynamicData.grandListInfo[selfPlayerID] = {{1,1,false},{1,1,false},{1,1,false}} -- {1级， 零阶， boss是否存活}
        local jieji = UIDynamicData.grandListInfo[selfPlayerID][1][2]
        local level = UIDynamicData.grandListInfo[selfPlayerID][1][1]

        if jieji >= 8 and level > 5 then -- 到最大阶级了
            return
        end

        if level > 5 then
            -- 召唤附魔boss
            local msg = {}
            msg.cmd = "weapoBoss"
            LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg)) -- 发给蓝图
        else
            -- 购买附魔加属性
            local needMoney = fumoInfo[1][jieji][4]

            if UIDynamicData.money >= needMoney then
                local msg = {}
                msg.cmd = "wp" -- 武器附魔
                LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg)) -- 发给蓝图
            end
        end
    else
        local jieji = UIDynamicData.grandListInfo[selfPlayerID][2][2]
        local level = UIDynamicData.grandListInfo[selfPlayerID][2][1]

        if jieji >= 8 and level > 5 then -- 到最大阶级了
            return
        end

        if level > 5 then
            -- 召唤附魔boss
            local msg = {}
            msg.cmd = "hujiaBoss"
            LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg)) -- 发给蓝图
        else
            -- 购买附魔加属性
            local needMoney = fumoInfo[2][jieji][4]

            if UIDynamicData.money >= needMoney then
                local msg = {}
                msg.cmd = "hj" -- 护甲附魔
                LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg)) -- 发给蓝图
            end
        end
    end
    shuaxinfumo(selfPlayerID)
end








function challengeOnEnable(LuaUIEvent)  -- 新的挑战panel
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    if UIDynamicData.itsOKInfo[selfPlayerID] ~= true then
        return
    end

    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()

    local challengeNameText
    local imgWgt = elem:GetWidgetProxyByName("Image(1201)")
    local daojishi = elem:GetWidgetProxyByName("TextLabel(1206)")

    if index == 0 then
        challengeNameText = "粮草押运官"

        if UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][1][1] ~= 0 then
            if UIDynamicData.time - UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][1][2] < 60 then
                daojishi:GetText():SetContent("<size=30>" .. (60-(UIDynamicData.time - UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][1][2])) .. "</size>")
                daojishi:SetActive(true)
                imgWgt:SetAlpha(0.6)
                UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 1
            else
                daojishi:SetActive(false)
                imgWgt:SetAlpha(1)
                UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 0
            end
        else
            imgWgt:SetAlpha(1)
            daojishi:SetActive(false)
            UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 0
        end
    elseif index == 1 then
        challengeNameText = "魔袋长老"

        if UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][2][1] ~= 0 then
            if UIDynamicData.time - UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][2][2] < 60 then

                daojishi:GetText():SetContent("<size=30>" .. (60-(UIDynamicData.time - UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][2][2])) .. "</size>")
                daojishi:SetActive(true)
                imgWgt:SetAlpha(0.6)
                UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 1
            else
                daojishi:SetActive(false)
                imgWgt:SetAlpha(1)
                UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 0
                if UIDynamicData.modaiHaveInfo[selfPlayerID][2] == true then
                    daojishi:GetText():SetContent("尚有\n魔袋")
                    daojishi:SetActive(true)
                    imgWgt:SetAlpha(0.6)
                    UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 2
                else
                    if UIDynamicData.challengeBossInfo[selfPlayerID][2][2] == true then
                        daojishi:GetText():SetContent("金矿\n存活")
                        daojishi:SetActive(true)
                        imgWgt:SetAlpha(0.6)
                        UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 3
                    end
                end
            end

            if UIDynamicData.boci >= 37 then
                daojishi:GetText():SetContent("禁止\n召唤")
                daojishi:SetActive(true)
                imgWgt:SetAlpha(0.6)
                UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 4
            end
        end
    else
        challengeNameText = "深渊魔王"

        if UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][3][1] ~= 0 then
            if UIDynamicData.time - UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][3][2] < 90 then
                daojishi:GetText():SetContent("<size=30>" .. (90-(UIDynamicData.time - UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][3][2])) .. "</size>")
                daojishi:SetActive(true)
                imgWgt:SetAlpha(0.6)
                UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 1
            else
                daojishi:SetActive(false)
                imgWgt:SetAlpha(1)
                UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 0
            end
        else
            daojishi:SetActive(false)
            imgWgt:SetAlpha(1)
            UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 0
        end
    end

    if UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] == 0 then
        elem:GetWidgetProxyByName("Image(1207)"):SetOnClickedEvent("tiaozhanClicked", {})
    else
        elem:GetWidgetProxyByName("Image(1207)"):SetOnClickedEvent("", {})
    end

    elem:GetWidgetProxyByName("TextLabel(1202)"):GetText():SetContent(challengeNameText)

    local challengIconPath = challengeInfo[challengeNameText][1]
    elem:GetWidgetProxyByName("Image(1201)"):GetImage():SetRes(challengIconPath)
end


function tiaozhanClicked(LuaUIEvent)
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    local index = tiaozhanList:GetSelectedIndex()
    tiaozhanList:GetListElement(index):GetWidgetProxyByName("Image(1207)"):SetOnClickedEvent("", {})
    if index == 0 then
        UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][1][1] = UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][1][1] + 1
        UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][1][2] = UIDynamicData.time
        tiaozhanList:GetListElement(index):GetWidgetProxyByName("TextLabel(1206)"):GetText():SetContent("<size=30>60</size>")
        local msg = {}
        msg.cmd = "CallMoneyBoss"
        LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg)) -- 发给蓝图
    elseif index == 1 then
        UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][2][1] = UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][2][1] +1
        UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][2][2] = UIDynamicData.time
        tiaozhanList:GetListElement(index):GetWidgetProxyByName("TextLabel(1206)"):GetText():SetContent("<size=30>60</size>")
        local msg = {}
        msg.cmd = "CallGoldOre"
        LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg)) -- 发给蓝图
    else
        UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][3][1] = UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][3][1] +1
        UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][3][2] = UIDynamicData.time
        tiaozhanList:GetListElement(index):GetWidgetProxyByName("TextLabel(1206)"):GetText():SetContent("<size=30>90</size>")
        local msg = {}
        msg.cmd = "CallHellFire"
        LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg)) -- 发给蓝图

    end
    tiaozhanList:GetListElement(index):GetWidgetProxyByName("TextLabel(1206)"):SetActive(true)
    tiaozhanList:GetListElement(index):GetWidgetProxyByName("Image(1201)"):SetAlpha(0.6)
end





function txyzHoldStart(LuaUIEvent)
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID

    txyzText:GetText():SetContent(allTipsInfo["王城圣物"][selfPlayerID])
    txyzPanel:SetActive(true)
end


function txyzHoldEnd(LuaUIEvent)
    txyzPanel:SetActive(false)
end

function OnShopUIClose()
    formSelf = nil
    itemName = nil
    shopBtnMoneyText = nil
    buff_panel = nil
    equipBuffInfo = {0, 0, 0}
    isJNSPanelOpen = false
    nowChallenge = nil
    isChallengePanelShow = false
    fuMoPanel = nil
    fuMoList = nil
    jnsList = nil
    BagBuffText = nil
    BagBuffImg = nil
    jnsPanel = nil
    tiaozhanList = nil
    txyzText = nil
    txyzPanel = nil
    txyzImg = nil
    bagBuffTipsPanel = nil
    bagBuffTipsText = nil
end

























function tiaozhanHoldStart(LuaUIEvent)
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    local index = LuaUIEvent.SrcWidget:GetIndexInBelongedList()
    local text

    local zhuangtaiNum = UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1]
    local zhaungtaiText
    if zhuangtaiNum == 0 then
        zhaungtaiText = "<color=#00FF00>" .. zhuangtaiInfo[zhuangtaiNum] .. "</color>"
    else
        zhaungtaiText = "<color=#FF0000>" .. zhuangtaiInfo[zhuangtaiNum] .. "</color>"
    end

    local tiaozhanLevel = UIDynamicData.challengeBossInfo[selfPlayerID][index+1][1]

    if index == 0 then
        text = allTipsInfo["粮草押运官"][1] .. tiaozhanLevel .. allTipsInfo["粮草押运官"][2]
        text = text .. zhaungtaiText .. allTipsInfo["粮草押运官"][3] .. (UIConfigData.jinbiguaiShuXing[tiaozhanLevel][5]*10) .. allTipsInfo["粮草押运官"][4]
    elseif index == 1 then
        text = allTipsInfo["魔袋长老"][1] .. tiaozhanLevel .. allTipsInfo["魔袋长老"][2]
        text = text .. zhaungtaiText .. allTipsInfo["魔袋长老"][3] .. tiaozhanLevel .. allTipsInfo["魔袋长老"][4]
    else
        text = allTipsInfo["深渊魔王"][1] .. tiaozhanLevel .. allTipsInfo["深渊魔王"][2]
        text = text .. zhaungtaiText .. allTipsInfo["深渊魔王"][3]
    end

    txyzText:GetText():SetContent(text)
    txyzPanel:SetActive(true)
end


function tiaozhanHoldEnd(LuaUIEvent)
    txyzPanel:SetActive(false)
end









function refreshChallenge()
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID



    if UIDynamicData.itsOKInfo[selfPlayerID] ~= true then
        return
    end



    local isjinbiOK
    local isjinkuangOK
    local isdiyuhuoOK

    jinbiWgt:SetAlpha(1)
    jinbidaojishi:SetActive(false)
    if UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][1][1] ~= 0 then
        if UIDynamicData.time - UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][1][2] < 60 then
            jinbidaojishi:GetText():SetContent(os.date("%M:%S", 60-(UIDynamicData.time - UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][1][2])))
            jinbidaojishi:SetActive(true)
            jinbiWgt:SetAlpha(0.6)
            isjinbiOK = false
        else
            jinbidaojishi:SetActive(false)
            jinbiWgt:SetAlpha(1)
            isjinbiOK = true
        end
    else
        jinbiWgt:SetAlpha(1)
        jinbidaojishi:SetActive(false)
        isjinbiOK = true
    end



    jinkuangWgt:SetAlpha(1)
    jinkuangdaojishi:SetActive(false)
    isjinkuangOK = true
    if UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][2][1] ~= 0 then
        if UIDynamicData.time - UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][2][2] < 60 then
            jinkuangdaojishi:GetText():SetContent(os.date("%M:%S", 60-(UIDynamicData.time - UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][2][2])))
            jinkuangdaojishi:SetActive(true)
            jinkuangWgt:SetAlpha(0.6)
            isjinkuangOK = false
        else
            jinkuangdaojishi:SetActive(false)
            jinkuangWgt:SetAlpha(1)
            isjinkuangOK = true
            if UIDynamicData.modaiHaveInfo[selfPlayerID][2] == true then
                jinkuangdaojishi:GetText():SetContent("尚有\r\n魔袋")
                jinkuangdaojishi:SetActive(true)
                jinkuangWgt:SetAlpha(0.6)
                isjinkuangOK = false
            else
                if UIDynamicData.challengeBossInfo[selfPlayerID][2][2] == true then
                    jinkuangdaojishi:GetText():SetContent("金矿\r\n存活")
                    jinkuangdaojishi:SetActive(true)
                    jinkuangWgt:SetAlpha(0.6)
                    isjinkuangOK = false
                end
            end
        end

        if UIDynamicData.boci >= 37 then
            jinkuangdaojishi:GetText():SetContent("禁止\r\n召唤")
            jinkuangdaojishi:SetActive(true)
            jinkuangWgt:SetAlpha(0.6)
            isjinkuangOK = false
        end
    end


    diyuhuoWgt:SetAlpha(1)
    diyuhuodaojishi:SetActive(false)
    if UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][3][1] ~= 0 then
        if UIDynamicData.time - UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][3][2] < 90 then
            diyuhuodaojishi:GetText():SetContent(os.date("%M:%S", 90-(UIDynamicData.time - UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][3][2])))
            diyuhuodaojishi:SetActive(true)
            diyuhuoWgt:SetAlpha(0.6)
            isdiyuhuoOK = false
        else
            diyuhuodaojishi:SetActive(false)
            diyuhuoWgt:SetAlpha(1)
            isdiyuhuoOK = true
        end
    else
        diyuhuodaojishi:SetActive(false)
        diyuhuoWgt:SetAlpha(1)
        isdiyuhuoOK = true
    end


    if nowChallenge == nil then
        return
    end

    if nowChallenge == "粮草押运官" then
        if isjinbiOK == true then
            challengeGouMaiHuang:EnableInput(true)
        else
            challengeGouMaiHuang:EnableInput(false)
        end
    elseif nowChallenge == "魔袋长老" then
        if isjinkuangOK == true then
            challengeGouMaiHuang:EnableInput(true)
        else
            challengeGouMaiHuang:EnableInput(false)
        end
    elseif nowChallenge == "深渊魔王" then
        if isdiyuhuoOK == true then
            challengeGouMaiHuang:EnableInput(true)
        else
            challengeGouMaiHuang:EnableInput(false)
        end
    end
end






function tipsCloseBtnClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    topTipsPanel:SetActive(false)
    zhizhen:SetActive(false)
end







function shuaxintiaozhan1(selfPlayerID)
    -- if UIDynamicData.itsOKInfo[selfPlayerID] ~= true then
    --     return
    -- end

    local index = 0
    local elem = tiaozhanList:GetListElement(index)
    local challengeNameText
    local imgWgt = elem:GetWidgetProxyByName("Image(1201)")
    local daojishi = elem:GetWidgetProxyByName("TextLabel(1206)")

    challengeNameText = "粮草押运官"


    if UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][1][1] ~= 0 then
        if UIDynamicData.time - UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][1][2] < 60 then
            daojishi:GetText():SetContent("<size=30>" .. (60-(UIDynamicData.time - UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][1][2])) .. "</size>")
            daojishi:SetActive(true)
            imgWgt:SetAlpha(0.6)
            UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 1
        else
            daojishi:SetActive(false)
            imgWgt:SetAlpha(1)
            UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 0
        end
    else
        imgWgt:SetAlpha(1)
        daojishi:SetActive(false)
        UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 0
    end



    if UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] == 0 then
        elem:GetWidgetProxyByName("Image(1207)"):SetOnClickedEvent("tiaozhanClicked", {})
    else
        elem:GetWidgetProxyByName("Image(1207)"):SetOnClickedEvent("", {})
    end

    elem:GetWidgetProxyByName("TextLabel(1202)"):GetText():SetContent(challengeNameText)

    local challengIconPath = challengeInfo[challengeNameText][1]
    elem:GetWidgetProxyByName("Image(1201)"):GetImage():SetRes(challengIconPath)
end





function shuaxintiaozhan2(selfPlayerID)
    local index = 1

    local elem = tiaozhanList:GetListElement(index)
    local challengeNameText
    local imgWgt = elem:GetWidgetProxyByName("Image(1201)")
    local daojishi = elem:GetWidgetProxyByName("TextLabel(1206)")




    challengeNameText = "魔袋长老"

    if UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][2][1] ~= 0 then
        if UIDynamicData.time - UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][2][2] < 60 then

            daojishi:GetText():SetContent("<size=30>" .. (60-(UIDynamicData.time - UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][2][2])) .. "</size>")
            daojishi:SetActive(true)
            imgWgt:SetAlpha(0.6)
            UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 1
        else
            daojishi:SetActive(false)
            imgWgt:SetAlpha(1)
            UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 0
            if UIDynamicData.modaiHaveInfo[selfPlayerID][2] == true then
                daojishi:GetText():SetContent("尚有\n魔袋")
                daojishi:SetActive(true)
                imgWgt:SetAlpha(0.6)
                UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 2
            else
                if UIDynamicData.challengeBossInfo[selfPlayerID][2][2] == true then
                    daojishi:GetText():SetContent("金矿\n存活")
                    daojishi:SetActive(true)
                    imgWgt:SetAlpha(0.6)
                    UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 3
                end
            end
        end

        if UIDynamicData.boci >= 37 then
            daojishi:GetText():SetContent("禁止\n召唤")
            daojishi:SetActive(true)
            imgWgt:SetAlpha(0.6)
            UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 4
        end
    end

    if UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] == 0 then
        elem:GetWidgetProxyByName("Image(1207)"):SetOnClickedEvent("tiaozhanClicked", {})
    else
        elem:GetWidgetProxyByName("Image(1207)"):SetOnClickedEvent("", {})
    end

    elem:GetWidgetProxyByName("TextLabel(1202)"):GetText():SetContent(challengeNameText)

    local challengIconPath = challengeInfo[challengeNameText][1]
    elem:GetWidgetProxyByName("Image(1201)"):GetImage():SetRes(challengIconPath)
end




function shuaxintiaozhan3(selfPlayerID)
    local index = 2

    local elem = tiaozhanList:GetListElement(index)
    local challengeNameText
    local imgWgt = elem:GetWidgetProxyByName("Image(1201)")
    local daojishi = elem:GetWidgetProxyByName("TextLabel(1206)")


    challengeNameText = "深渊魔王"

    if UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][3][1] ~= 0 then
        if UIDynamicData.time - UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][3][2] < 90 then
            daojishi:GetText():SetContent("<size=30>" .. (90-(UIDynamicData.time - UIDynamicData.shopBossBuyTimeInfoNew[selfPlayerID][3][2])) .. "</size>")
            daojishi:SetActive(true)
            imgWgt:SetAlpha(0.6)
            UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 1
        else
            daojishi:SetActive(false)
            imgWgt:SetAlpha(1)
            UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 0
        end
    else
        daojishi:SetActive(false)
        imgWgt:SetAlpha(1)
        UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] = 0
    end

    if UIDynamicData.challengeZhaungTaiInfo[selfPlayerID][index+1] == 0 then
        elem:GetWidgetProxyByName("Image(1207)"):SetOnClickedEvent("tiaozhanClicked", {})
    else
        elem:GetWidgetProxyByName("Image(1207)"):SetOnClickedEvent("", {})
    end

    elem:GetWidgetProxyByName("TextLabel(1202)"):GetText():SetContent(challengeNameText)

    local challengIconPath = challengeInfo[challengeNameText][1]
    elem:GetWidgetProxyByName("Image(1201)"):GetImage():SetRes(challengIconPath)
end


function shuaxintiaozhan(selfPlayerID)
    shuaxintiaozhan1(selfPlayerID)
    shuaxintiaozhan2(selfPlayerID)
    shuaxintiaozhan3(selfPlayerID)
end



function shuaxinfumo1(selfPlayerID, index)
    if UIDynamicData.grandListInfo[selfPlayerID] == nil then
        return
    end

    -- local index = 0
    local elem = fuMoList:GetListElement(index)

    local imgPath = fumoInfo[index+1][1][1]

    local level = UIDynamicData.grandListInfo[selfPlayerID][index+1][1]
    local jieji = UIDynamicData.grandListInfo[selfPlayerID][index+1][2]
    local isBossLive = UIDynamicData.grandListInfo[selfPlayerID][index+1][3]

    elem:GetWidgetProxyByName("fuMoImg"):GetImage():SetRes(imgPath)

    if index == 0 then
        elem:GetWidgetProxyByName("TextLabel(1203)"):GetText():SetContent("武器附魔")
    else
        elem:GetWidgetProxyByName("TextLabel(1203)"):GetText():SetContent("护甲附魔")
    end

    elem:GetWidgetProxyByName("jiejiHanZi"):GetImage():SetRes(UIConfigData.equipLevelImgInfo[jieji])


    local needMoney
    local needMoneyText
    if level > 5 then
        needMoney = 0
    else
        needMoney = fumoInfo[index+1][jieji][4]
    end
    needMoneyText = "   " .. needMoney

    if level > 5 then
        if jieji >= 8 then
            elem:GetWidgetProxyByName("lvBtn"):SetActive(false)
            UIDynamicData.fumoZhuangTaiInfo[selfPlayerID][index+1] = 9
            needMoneyText = ""
            elem:GetWidgetProxyByName("Image(1209)"):SetActive(false)
        else
            elem:GetWidgetProxyByName("lvText"):SetActive(false)
            elem:GetWidgetProxyByName("jiejiHanZiShang"):GetImage():SetRes(UIConfigData.equipLevelImgInfo[jieji+1])
            elem:GetWidgetProxyByName("jiejiHanZiShang"):SetActive(true)
            elem:GetWidgetProxyByName("lvBtn"):SetActive(true)
            needMoneyText = "挑战"
            elem:GetWidgetProxyByName("Image(1209)"):SetActive(false)
            if isBossLive == true then
                elem:GetWidgetProxyByName("fuMoImg"):EnableInput(false)
                UIDynamicData.fumoZhuangTaiInfo[selfPlayerID][index+1] = 8
            else
                elem:GetWidgetProxyByName("fuMoImg"):EnableInput(true)
                UIDynamicData.fumoZhuangTaiInfo[selfPlayerID][index+1] = 7
            end
        end
    else
        elem:GetWidgetProxyByName("lvBtn"):SetActive(true)
        elem:GetWidgetProxyByName("jiejiHanZiShang"):SetActive(false)
        elem:GetWidgetProxyByName("lvText"):GetText():SetContent(level)
        elem:GetWidgetProxyByName("lvText"):SetActive(true)
        elem:GetWidgetProxyByName("Image(1209)"):SetActive(true)

        if UIDynamicData.money < needMoney then
            elem:GetWidgetProxyByName("fuMoImg"):EnableInput(false)
            UIDynamicData.fumoZhuangTaiInfo[selfPlayerID][index+1] = 6
        else
            elem:GetWidgetProxyByName("fuMoImg"):EnableInput(true)
            UIDynamicData.fumoZhuangTaiInfo[selfPlayerID][index+1] = 5
        end
    end
    elem:GetWidgetProxyByName("TextLabel(1208)"):GetText():SetContent(needMoneyText)
end

function shuaxinfumo2(selfPlayerID)
end






function shuaxinfumo(selfPlayerID)
    shuaxinfumo1(selfPlayerID, 0)
    shuaxinfumo1(selfPlayerID, 1)
    -- shuaxinfumo2(selfPlayerID)
end

function refresh_shop_item(playerID) -- 第一个元素是当前所选itemList中的名字列表，第二个元素是该shop在shopList中的索引
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    if UIDynamicData.itsOKInfo[selfPlayerID] ~= true then
        return
    end

    if UIDynamicData.GameOver[selfPlayerID] == true then
        return
    end

    shopBtnMoneyText:SetContent(UIDynamicData.money)
    jnsList:SetElementAmount(#jnsInfo)

    shuaxintiaozhan(selfPlayerID)
    shuaxinfumo(selfPlayerID)

    refreshEquipInfo(selfPlayerID)
end



function setIsGoldOrePrizeLive(playerID)
    if playerID ~= LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
        return
    end

    UIDynamicData.challengeBossInfo[playerID][2][2] = false
    refresh_shop_item(playerID)
end



local modaiTipsInfo = {
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>2000</color>金币，同时只能激活一个魔袋",
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>2600</color>金币，同时只能激活一个魔袋",
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>3380</color>金币，同时只能激活一个魔袋",
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>4394</color>金币，同时只能激活一个魔袋",
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>5712</color>金币，同时只能激活一个魔袋",
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>7426</color>金币，同时只能激活一个魔袋",
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>9654</color>金币，同时只能激活一个魔袋",
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>12550</color>金币，同时只能激活一个魔袋",
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>16315</color>金币，同时只能激活一个魔袋",
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>21209</color>金币，同时只能激活一个魔袋",
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>27572</color>金币，同时只能激活一个魔袋",
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>35843</color>金币，同时只能激活一个魔袋",
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>46596</color>金币，同时只能激活一个魔袋",
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>60575</color>金币，同时只能激活一个魔袋",
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>78748</color>金币，同时只能激活一个魔袋",
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>102372</color>金币，同时只能激活一个魔袋",
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>102372</color>金币，同时只能激活一个魔袋",
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>102372</color>金币，同时只能激活一个魔袋",
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>102372</color>金币，同时只能激活一个魔袋",
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>102372</color>金币，同时只能激活一个魔袋",
    "击败<color=#f18d00>50</color>个敌人后黄金魔袋被消耗，奖励<color=#f18d00>102372</color>金币，同时只能激活一个魔袋",
}



function BagBuffHoldStart(LuaUIEvent)
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    bagBuffTipsPanel:SetActive(true)
    bagBuffTipsText:SetContent("<color=#f18d00>" .. UIDynamicData.modaiHaveInfo[selfPlayerID][1] .. "</color>级魔袋\r\n" .. modaiTipsInfo[UIDynamicData.modaiHaveInfo[selfPlayerID][1]])
end


function BagBuffHoldEnd(LuaUIEvent)
    bagBuffTipsPanel:SetActive(false)
end




function modaiKillEnough_callBack(pid, mdLv)
    UIDynamicData.modaiHaveInfo[pid][2] = false
    UIDynamicData.modaiHaveInfo[pid][3] = 0
    if pid == LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
        LuaCallCs_FightUI.CreateFloatText(2, "魔袋收益:+" .. bagInfo[mdLv][5] .. "g", 6, LuaCallCs_Battle.GetHostActorWorldPos(), true, 0, 4000, -0.3)
        buff_panel:SetActive(false)
    end
end


function modaiKill_callBack(pid, killCountNum, modaiNeedKillCount)
	UIDynamicData.modaiHaveInfo[pid][3] = killCountNum
    if pid == LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
        BagBuffText:SetContent(killCountNum .. "/" .. modaiNeedKillCount)
    end
end


function diyuhuoDead(playerID)
    UIDynamicData.challengeBossInfo[playerID][3][2] = false
end

function jinbiguaiDead(playerID)
    UIDynamicData.challengeBossInfo[playerID][1][2] = false
end


function jinkuangDead(playerID)
    UIDynamicData.challengeBossInfo[playerID][2][2] = false
end




function RefreshEquipBuff()
    local buffCount = 0
    for i = 1, #equipBuffInfo do
        if equipBuffInfo[i] ~= 0 then
            buffCount = buffCount + 1
        end
    end
    if buffCount > 0 then
        equipBuff:SetActive(true)
    else
        return
    end

    local equipBuffList = formSelf:GetWidgetProxyByName("equipBuffList")
    equipBuffList:SetElementAmount(buffCount)

    local buffSort = 0
    for i = 1, 3 do
        local iconImg = ""
        if i == 1 then
            iconImg = "Texture/Sprite/wuqi.sprite" 
        elseif i == 2 then
            iconImg = "Texture/Sprite/hujia.sprite"
        else
            iconImg = "Texture/Sprite/jiezhi.sprite"
        end

        if equipBuffInfo[i] ~= 0 then
            equipBuffList:GetListElement(buffSort):GetWidgetProxyByName("equipBuff"):GetImage():SetRes(iconImg)
            equipBuffList:GetListElement(buffSort):GetWidgetProxyByName("equipLevel"):GetText():SetContent(equipBuffInfo[i])
            buffSort = buffSort + 1
        end
    end
end











function CallHellFire()  -- zdc 这里要一大堆的callBack 时机注意，别弄错了
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID

    diyuhuodaojishi:GetText():SetContent("01:30")
    diyuhuodaojishi:SetActive(true)
    diyuhuoWgt:SetAlpha(0.6)

    local msg = {}
    msg.cmd = "CallHellFire"
    LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg)) -- 发给蓝图
end

function CallMoneyBoss()
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID

    jinbidaojishi:GetText():SetContent("01:30")
    jinbidaojishi:SetActive(true)
    jinbiWgt:SetAlpha(0.6)

    local msg = {}
    msg.cmd = "CallMoneyBoss"
    LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg)) -- 发给蓝图
end


function CallGoldOre()
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID

    jinkuangdaojishi:GetText():SetContent("01:30")
    jinkuangdaojishi:SetActive(true)
    jinkuangWgt:SetAlpha(0.6)

    local msg = {}
    msg.cmd = "CallGoldOre"
    LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg)) -- 发给蓝图
end

-- function AllMedicine()
--     local needMoney =  itemInfo[4]
--     if UIDynamicData.money >= needMoney then
--         local msg = {}
--         msg.cmd = "AllMedicine"
--         msg.needMoney = needMoney
--         LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg)) -- 发给蓝图
--         UIDynamicData.money = UIDynamicData.money - needMoney
--     end
-- end



function jinbiBossClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    if isChallengePanelShow == true then
        if nowChallenge == "粮草押运官" then
            challengeTipsPanel:SetActive(false)
            isChallengePanelShow = false
            kuang1:SetActive(false)
        else
            itemName = "粮草押运官"
            nowChallenge = itemName
            itemInfo = challengeInfo[itemName]
            challengeTipsText:SetContent(itemName)
            local tips = "难度: " .. "<color=#CDC9C9>低</color>\r\n" .. "击败可以获得金币奖励"
            challengeTipsText1:SetContent(tips)
            kuang1:SetActive(true)
            kuang2:SetActive(false)
            kuang3:SetActive(false)
        end
    else
        itemName = "粮草押运官"
        nowChallenge = itemName
        itemInfo = challengeInfo[itemName]
        challengeTipsText:SetContent(itemName)
        local tips = "难度: " .. "<color=#CDC9C9>低</color>\r\n" .. "击败可以获得金币奖励"
        challengeTipsText1:SetContent(tips)
        challengeTipsPanel:SetActive(true)
        isChallengePanelShow = true

        kuang1:SetActive(true)
        kuang2:SetActive(false)
        kuang3:SetActive(false)
    end
    refreshChallenge()
end


function jinkuangBossClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    if isChallengePanelShow == true then
        if nowChallenge == "魔袋长老" then
            challengeTipsPanel:SetActive(false)
            isChallengePanelShow = false
            kuang2:SetActive(false)
        else
            itemName = "魔袋长老"
            nowChallenge = itemName
            itemInfo = challengeInfo[itemName]
            challengeTipsText:SetContent(itemName)
            local tips = "难度: " .. "<color=#00FF00>中</color>\r\n" .. "击败可以获得金币奖励"
            challengeTipsText1:SetContent(tips)

            kuang1:SetActive(false)
            kuang2:SetActive(true)
            kuang3:SetActive(false)
        end
    else
        itemName = "魔袋长老"
        itemInfo = challengeInfo[itemName]
        nowChallenge = itemName
        challengeTipsText:SetContent(itemName)
        local tips = "难度: " .. "<color=#00FF00>中</color>\r\n" .. "击败可以获得金币奖励"
        challengeTipsText1:SetContent(tips)
        challengeTipsPanel:SetActive(true)
        isChallengePanelShow = true

        
        kuang1:SetActive(false)
        kuang2:SetActive(true)
        kuang3:SetActive(false)
    end
    refreshChallenge()
end


function diyuhuoBossClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    if isChallengePanelShow == true then
        if nowChallenge == "深渊魔王" then
            challengeTipsPanel:SetActive(false)
            isChallengePanelShow = false
            kuang3:SetActive(false)
        else
            itemName = "深渊魔王"
            nowChallenge = itemName
            itemInfo = challengeInfo[itemName]
            challengeTipsText:SetContent(itemName)
            local tips = "难度: " .. "<color=#FF0000>超级难</color>\r\n" .. "击败可以升级王城圣物"
            challengeTipsText1:SetContent(tips)

            kuang1:SetActive(false)
            kuang2:SetActive(false)
            kuang3:SetActive(true)
        end
    else
        itemName = "深渊魔王"
        itemInfo = challengeInfo[itemName]
        nowChallenge = itemName
        challengeTipsText:SetContent(itemName)
        local tips = "难度: " .. "<color=#FF0000>超级难</color>\r\n" .. "击败可以升级王城圣物"
        challengeTipsText1:SetContent(tips)
        challengeTipsPanel:SetActive(true)
        isChallengePanelShow = true

        kuang1:SetActive(false)
        kuang2:SetActive(false)
        kuang3:SetActive(true)
    end
    refreshChallenge()
end



function jinbiguaiHoldStart(LuaUIEvent)
    challengeTipsPanel:SetActive(false)
    isChallengePanelShow = false
    nowChallenge = nil
    kuang1:SetActive(true)
    kuang2:SetActive(false)
    kuang3:SetActive(false)

    local jinbiguaiTipsPanel = formSelf:GetWidgetProxyByName("jinbiguaiTipsPanel")
    local jinbiguaiTipsText = formSelf:GetWidgetProxyByName("jinbiguaiTipsText"):GetText()

    local tips = challengeInfo["粮草押运官"][2]
    jinbiguaiTipsText:SetContent(tips)
    jinbiguaiTipsPanel:SetActive(true)
end

function changeHoldEnd()
    kuang1:SetActive(false)
    kuang2:SetActive(false)
    kuang3:SetActive(false)
    local jinbiguaiTipsPanel = formSelf:GetWidgetProxyByName("jinbiguaiTipsPanel")
    local jinkuangTipsPanel = formSelf:GetWidgetProxyByName("jinkuangTipsPanel")
    local diyuhuoTipsPanel = formSelf:GetWidgetProxyByName("diyuhuoTipsPanel")
    jinbiguaiTipsPanel:SetActive(false)
    jinkuangTipsPanel:SetActive(false)
    diyuhuoTipsPanel:SetActive(false)
end


function jinbiguaiHoldEnd(LuaUIEvent)
    kuang1:SetActive(false)
    local jinbiguaiTipsPanel = formSelf:GetWidgetProxyByName("jinbiguaiTipsPanel")
    jinbiguaiTipsPanel:SetActive(false)
end



function jinkuangHoldStart(LuaUIEvent)
    challengeTipsPanel:SetActive(false)
    isChallengePanelShow = false
    nowChallenge = nil
    kuang1:SetActive(false)
    kuang2:SetActive(true)
    kuang3:SetActive(false)


    local jinkuangTipsPanel = formSelf:GetWidgetProxyByName("jinkuangTipsPanel")
    local jinkuangTipsText = formSelf:GetWidgetProxyByName("jinkuangTipsText"):GetText()

    local tips = challengeInfo["魔袋长老"][2]
    jinkuangTipsText:SetContent(tips)
    jinkuangTipsPanel:SetActive(true)
end


function jinkuangHoldEnd(LuaUIEvent)
    kuang2:SetActive(false)
    local jinkuangTipsPanel = formSelf:GetWidgetProxyByName("jinkuangTipsPanel")
    jinkuangTipsPanel:SetActive(false)
end


function diyuhuoHoldStart(LuaUIEvent)
    challengeTipsPanel:SetActive(false)
    isChallengePanelShow = false
    nowChallenge = nil
    kuang1:SetActive(false)
    kuang2:SetActive(false)
    kuang3:SetActive(true)


    local diyuhuoTipsPanel = formSelf:GetWidgetProxyByName("diyuhuoTipsPanel")
    local diyuhuoTipsText = formSelf:GetWidgetProxyByName("diyuhuoTipsText"):GetText()

    local tips = challengeInfo["深渊魔王"][2]
    diyuhuoTipsText:SetContent(tips)
    diyuhuoTipsPanel:SetActive(true)
end


function diyuhuoHoldEnd(LuaUIEvent)
    kuang3:SetActive(false)
    local diyuhuoTipsPanel = formSelf:GetWidgetProxyByName("diyuhuoTipsPanel")
    diyuhuoTipsPanel:SetActive(false)
end


function challengeTipsPanelClosed(LuaUIEvent)
    challengeTipsPanel:SetActive(false)
end


local FuncList = {
    ['深渊魔王'] = CallHellFire,
    ['粮草押运官'] = CallMoneyBoss,
    ['魔袋长老'] = CallGoldOre,
}


function challengeChooseClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")


    if nowChallenge == "粮草押运官" then
        if FuncList[nowChallenge] then
            FuncList[nowChallenge]()
        end
        challengeTipsPanel:SetActive(false)
        isChallengePanelShow = false

        kuang1:SetActive(false)
        kuang2:SetActive(false)
        kuang3:SetActive(false)

    elseif nowChallenge == "魔袋长老" then
        if FuncList[nowChallenge] then
            FuncList[nowChallenge]()
        end
        challengeTipsPanel:SetActive(false)
        isChallengePanelShow = false

        kuang1:SetActive(false)
        kuang2:SetActive(false)
        kuang3:SetActive(false)

    elseif nowChallenge == "深渊魔王" then
        if FuncList[nowChallenge] then
            FuncList[nowChallenge]()
        end
        challengeTipsPanel:SetActive(false)
        isChallengePanelShow = false

        kuang1:SetActive(false)
        kuang2:SetActive(false)
        kuang3:SetActive(false)
    end
end



function challengeChooseCloseClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    nowChallenge = nil
    challengeTipsPanel:SetActive(false)
    isChallengePanelShow = false

    kuang1:SetActive(false)
    kuang2:SetActive(false)
    kuang3:SetActive(false)
end








-- 魔袋长老
function GoldOrePrize(playerID, mdLv) -- zdc  gameplay跟UI的两边魔袋的数据都打印出来看看
    UIDynamicData.challengeBossInfo[playerID][2][2] = false

    if UIDynamicData.modaiHaveInfo[playerID][2] == false then
        if playerID == LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
            LuaCallCs_FightUI.CreateFloatText(2, "获得魔袋Lv" .. mdLv, 6, LuaCallCs_Battle.GetHostActorWorldPos(), true, 0, 3000, -0.3)
        end
        local needKillCount = bagInfo[UIDynamicData.challengeBossInfo[playerID][2][1]][4] -- 一般都是50

        UIDynamicData.modaiHaveInfo[playerID][1] = UIDynamicData.challengeBossInfo[playerID][2][1]-1
        UIDynamicData.modaiHaveInfo[playerID][2] = true
        UIDynamicData.modaiHaveInfo[playerID][3] = 0
        UIDynamicData.modaiHaveInfo[playerID][4] = needKillCount



        if playerID == LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
            local bagIcon = "Texture/Sprite/modai4.sprite"
            BagBuffImg:SetRes(bagIcon)
            BagBuffText:SetContent(0 .. "/" .. needKillCount)
            buff_panel:SetActive(true)
        end


        addGMGongGaoInfo(playerID, "md", mdLv, needKillCount) -- zdc有问题，看魔袋等级的存储是否有问题，看gameplay那边的处理
    end
    refresh_shop_item(playerID)
end


function SetWeaponBossGrade(playerID)
    -- 重置装备在商店中的等级
    UIDynamicData.grandListInfo[playerID][1][2] = UIDynamicData.grandListInfo[playerID][1][2] + 1
    UIDynamicData.grandListInfo[playerID][1][1] = 1
    UIDynamicData.grandListInfo[playerID][1][3] = false
    refresh_shop_item(playerID)
end

function SetGuardBossGrade(playerID)
    -- 重置装备在商店中的等级
    UIDynamicData.grandListInfo[playerID][2][2] = UIDynamicData.grandListInfo[playerID][2][2] + 1
    UIDynamicData.grandListInfo[playerID][2][1] = 1
    UIDynamicData.grandListInfo[playerID][2][3] = false
    refresh_shop_item(playerID)
end

function SetRingBossGrade(playerID)
    -- 重置装备在商店中的等级
    UIDynamicData.grandListInfo[playerID][3][2] = UIDynamicData.grandListInfo[playerID][3][2] + 1
    UIDynamicData.grandListInfo[playerID][3][1] = 1
    UIDynamicData.grandListInfo[playerID][3][3] = false
    refresh_shop_item(playerID)
end


-- 刷新 前台moneyText 和 后台money
function RefreshMoney(playerID, moneyNum)
    if playerID == LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
        UIDynamicData.money = moneyNum
        shopBtnMoneyText:SetContent(moneyNum)
        refresh_shop_item(playerID)
    end
end


function refreshMoney_time_change(LuaUIEvent)
    local selfPID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    if UIDynamicData.itsOKInfo[selfPID] == false then
        return
    end

    local msg = {}
    msg.cmd = "RefreshMoney"
    LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg)) -- 发给蓝图
end



function bigPassSkill_callBack(pid, skill1, skill2)
    if pid ~= LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
        return
    end
    formSelf:GetWidgetProxyByName("Panel(1221)"):SetActive(true)

    formSelf:GetWidgetProxyByName("Image(1212)"):GetImage():SetRes(UIConfigData.erxuanyiInfo[skill1][2])
    formSelf:GetWidgetProxyByName("Image(1213)"):GetImage():SetRes(UIConfigData.erxuanyiInfo[skill2][2])

    formSelf:GetWidgetProxyByName("TextLabel(1214)"):GetText():SetContent(skill1)
    formSelf:GetWidgetProxyByName("TextLabel(1215)"):GetText():SetContent(skill2)
    formSelf:GetWidgetProxyByName("TextLabel(1210)"):GetText():SetContent(UIConfigData.erxuanyiInfo[skill1][1])
    formSelf:GetWidgetProxyByName("TextLabel(1211)"):GetText():SetContent(UIConfigData.erxuanyiInfo[skill2][1])
end


function skill1Clicked(LuaUIEvent)
    formSelf:GetWidgetProxyByName("Panel(1221)"):SetActive(false)

    local msg = {}
    msg.cmd = "chooseErXuanYi"
    msg.skill = 1
    LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg)) -- 发给蓝图
end


function skill2Clicked(LuaUIEvent)
    formSelf:GetWidgetProxyByName("Panel(1221)"):SetActive(false)

    local msg = {}
    msg.cmd = "chooseErXuanYi"
    msg.skill = 2
    LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg)) -- 发给蓝图
end


function zdcClicked(LuaUIEvent)
    local msg = {}
    msg.cmd = "zdc"
    LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg))
end


function shuaxinUICiTiao(pid, text)
    LuaCallCs_Common.Log(text)
    allTipsInfo["王城圣物"][pid] = text
end



function shopXSJCShow()
    formSelf:GetWidgetProxyByName("shopXSJCPanel(7)"):SetActive(true)
end


function XSJCNext7(LuaUIEvent)
    formSelf:GetWidgetProxyByName("shopXSJCPanel(7)"):SetActive(false)
    formSelf:GetWidgetProxyByName("shopXSJCPanel(8)"):SetActive(true)
end

function tiaoguo7()
    formSelf:GetWidgetProxyByName("MCXSJCPanel(7)"):SetActive(false)
end

function XSJCNext8(LuaUIEvent)
    formSelf:GetWidgetProxyByName("shopXSJCPanel(8)"):SetActive(false)
end

function tiaoguo8()
    formSelf:GetWidgetProxyByName("MCXSJCPanel(8)"):SetActive(false)
end