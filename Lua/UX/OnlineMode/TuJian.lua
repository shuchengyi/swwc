G_GameDataMgr = require"OnlineMode/G_GameDataMgr.lua"
json = require "json.lua"

local formSelf

local biaoQianList
local biaoQianInfo = {
    "怪物图鉴",
    "技能图鉴",
    "物品图鉴",
    "词条图鉴",
    "天赋图鉴",
    "职业图鉴",
    "随机事件图鉴",
}

local tuJianList


function tuJianFormOnOpen(LuaUIEvent)
    math.randomseed(tostring(os.time()):reverse():sub(1,7))
    math.random()

    formSelf = LuaUIEvent.SrcForm

    biaoQianList = formSelf:GetWidgetProxyByName("List(1008)")
    biaoQianList:SetElementAmount(#biaoQianInfo)
    biaoQianList:SelectElement(0, false)
    biaoQianList:SetElementAmount(#biaoQianInfo)
    tuJianList = formSelf:GetWidgetProxyByName("List(1013)")
    tuJianList:SetElementAmount(#UIConfigData.tuJianAllInfo[1])
    tuJianList:SelectElement(0, false)
    formSelf:GetWidgetProxyByName("Panel(1017)"):SetActive(false)
end







function biaoQianOnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()
    local selectIndex = biaoQianList:GetSelectedIndex()
    elem:GetWidgetProxyByName("Button(1007)"):GetText():SetContent(biaoQianInfo[index+1])
    if index == selectIndex then
        elem:GetWidgetProxyByName("Button(1007)"):GetImage():SetRes("Texture/xuanzhong.sprite")
        elem:GetWidgetProxyByName("Button(1007)"):GetText():SetColor(BluePrint.UGC.UI.Core.Color.White)
    else
        elem:GetWidgetProxyByName("Button(1007)"):GetImage():SetRes("Texture/Sprite/toumingtupian.sprite")
        elem:GetWidgetProxyByName("Button(1007)"):GetText():SetColor(BluePrint.UGC.UI.Core.Color(157/255,194/255,239/255,1))
    end
end




function tuJianClosed(LuaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    LuaCallCs_UI.CloseForm("UI/OnlineMode/TuJian.uixml")
end




function biaoQianClicked(LuaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")
    local index = biaoQianList:GetSelectedIndex()
    local lastIndex = biaoQianList:GetLastSelectedIndex()


    if index == 3 then
        formSelf:GetWidgetProxyByName("List(1013)"):SetActive(false)
        formSelf:GetWidgetProxyByName("List(1021)"):SetActive(true)
    else
        formSelf:GetWidgetProxyByName("List(1013)"):SetActive(true)
        formSelf:GetWidgetProxyByName("List(1021)"):SetActive(false)
    end


    if index ~= lastIndex then
        formSelf:GetWidgetProxyByName("Panel(1017)"):SetActive(false) -- 右侧详细信息栏
    end

    biaoQianList:SetElementAmount(#biaoQianInfo)
    tuJianList:SetElementAmount(#UIConfigData.tuJianAllInfo[index+1])
end



function tuJianClicked(LuaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    local selectIndex = biaoQianList:GetSelectedIndex()

    local index = tuJianList:GetSelectedIndex()

    formSelf:GetWidgetProxyByName("Image(1018)"):GetImage():SetRes(UIConfigData.tuJianAllInfo[selectIndex+1][index+1][2])
    formSelf:GetWidgetProxyByName("TextLabel(1019)"):GetText():SetContent(UIConfigData.tuJianAllInfo[selectIndex+1][index+1][1])

    formSelf:GetWidgetProxyByName("TextLabel(1020)"):GetText():SetContent(UIConfigData.tuJianAllInfo[selectIndex+1][index+1][4])

    formSelf:GetWidgetProxyByName("Panel(1017)"):SetActive(true)
end



function tuJianOnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()
    local selectIndex = biaoQianList:GetSelectedIndex()

    elem:GetWidgetProxyByName("TextLabel(1016)"):GetText():SetContent(UIConfigData.tuJianAllInfo[selectIndex+1][index+1][1])
    elem:GetWidgetProxyByName("Button(1015)"):GetImage():SetRes(UIConfigData.tuJianAllInfo[selectIndex+1][index+1][2])
end


function tuJianFormOnClose()
    formSelf = nil
    biaoQianList = nil
    tuJianList = nil
end