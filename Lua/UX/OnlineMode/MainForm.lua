json = require "json.lua"

local MainForm = {}
local formSelf


local jiantou
local nowPlayerCntChoose = 0 -- 索引从0开始

local ismoshiBigPanelShow = false
local nowNandu = 0
local nanduList

local duanweijindutiao
local duanweijinduText

local Timer1
local msxzBigPanel
local renshuChooseList


local renshuText
local nanduText

local duanweiNuberImg
local duanweiNuberText
local duanweiBigImg

local wanfaPanel
local intArr

local shensuoPanel
local shensuoBtn
local zhankaiText
local shouqiText


local function getHeroExLv(num)
	local num1 = 0
	for i = 1, #UIConfigData.chengjiuInfo["英雄等级"] do
		if num >= UIConfigData.chengjiuInfo["英雄等级"][i][1] then
			num1 = UIConfigData.chengjiuInfo["英雄等级"][i][2]
		else
			break
		end
	end
	return num1
end


function checkHeroExFunc(stringData)
	local sldJson = stringData
	printTb("sldJson")
	printTb(sldJson)

	if sldJson ~= "" and sldJson ~= nil then
		printTb(sldJson)
		UIDynamicData.mainFormsldTb = json.decode(sldJson)
		for i=1, #UIConfigData.heroList do
			if UIDynamicData.mainFormsldTb[tostring(UIConfigData.heroList[i])] == nil then
				UIDynamicData.mainFormsldTb[tostring(UIConfigData.heroList[i])] = 0
			end
		end
	else
		for i=1, #UIConfigData.heroList do
			UIDynamicData.mainFormsldTb[tostring(UIConfigData.heroList[i])] = 0
		end
	end

	UIDynamicData.stringArrDataNew =  {}
	UIDynamicData.tankeStringDataInfo = {}
	UIDynamicData.zhanshiStringDataInfo = {}
	UIDynamicData.fashiStringDataInfo = {}
	UIDynamicData.sheshouStringDataInfo = {}
	UIDynamicData.manjiHeroCnt = 0

	for i=1, #UIConfigData.heroList do
		local cfgid = UIConfigData.heroList[i]
		UIDynamicData.stringArrDataNew[i] = {cfgid, UIDynamicData.mainFormsldTb[tostring(cfgid)]}
	end


	table.sort(UIDynamicData.stringArrDataNew, function (a,b)
		return (a[2] > b[2])
	end)

	-- printTb(UIDynamicData.stringArrDataNew)


	UIDynamicData.allheroExLv = 0 -- 计算总等级
	for y=1, #UIDynamicData.stringArrDataNew do
		local heroEx = UIDynamicData.stringArrDataNew[y][2]
		local heroExLv = getHeroExLv(heroEx)
		UIDynamicData.allheroExLv = UIDynamicData.allheroExLv + heroExLv
	end

	for i=1, #UIDynamicData.stringArrDataNew do
		local cfgID = UIDynamicData.stringArrDataNew[i][1]
		if UIConfigData.tanKeInfo[cfgID] ~= nil then
			table.insert(UIDynamicData.tankeStringDataInfo, {UIDynamicData.stringArrDataNew[i][1], UIDynamicData.stringArrDataNew[i][2]})
		elseif UIConfigData.zhanShiInfo[cfgID] ~= nil then
			table.insert(UIDynamicData.zhanshiStringDataInfo, {UIDynamicData.stringArrDataNew[i][1], UIDynamicData.stringArrDataNew[i][2]})
		elseif UIConfigData.sheShouInfo[cfgID] ~= nil then
			table.insert(UIDynamicData.sheshouStringDataInfo, {UIDynamicData.stringArrDataNew[i][1], UIDynamicData.stringArrDataNew[i][2]})
		elseif UIConfigData.faShiInfo[cfgID] ~= nil then
			table.insert(UIDynamicData.fashiStringDataInfo, {UIDynamicData.stringArrDataNew[i][1], UIDynamicData.stringArrDataNew[i][2]})
		end
	end
	for i = 1, #UIDynamicData.stringArrDataNew do
		if UIDynamicData.stringArrDataNew[i][2] >= UIConfigData.heroExLevelInfo[5] then -- 满星门槛计算
			UIDynamicData.manjiHeroCnt = UIDynamicData.manjiHeroCnt + 1
		else
			break
		end
	end



	LuaCallCs_Common.Log("满星个数：" .. UIDynamicData.manjiHeroCnt)
	LuaCallCs_Common.Log("英雄熟练度总等级：" .. UIDynamicData.allheroExLv)

	LuaCallCs_PersistentData.SetCustomizeDataIntArr({4}, {UIDynamicData.manjiHeroCnt}) -- 索引从0开始
	LuaCallCs_PersistentData.SetCustomizeDataIntArr({5}, {UIDynamicData.allheroExLv}) -- 索引从0开始
end


-- 根据匹配分判断是什么段位的
function getDuanwei()
	local jiFen
	local num = 0

	while jiFen == nil do
		LuaCallCs_Common.Log("积分_大厅:--------------------")
		num  = num + 1
		jiFen = LuaCallCs_PersistentData.GetCustomizeDataIntArr()[4]
		if jiFen < 0 then
			jiFen = 0
		end

		if jiFen then
			break
		end
		if num == 10 then
			return false
		end
	end
	LuaCallCs_Common.Log("积分_大厅: " .. jiFen)

	local jiFenLvText
	for i=#UIConfigData.jifenLevelInfo, 0 ,-1 do
		if i <= 0 then
			jiFenLvText = "积分等级\nLv" .. i
			duanweiBigImg:SetRes("Texture/Sprite/rywz.sprite")

			local leftNum = jiFen
			local rightNum = UIConfigData.jifenLevelInfo[1]

			duanweijinduText:SetContent(leftNum .. "/" .. rightNum)
			duanweijindutiao:SetProgressValue(leftNum/rightNum)
			break
		elseif i >= #UIConfigData.jifenLevelInfo then -- 最大的索引
			jiFenLvText = "积分等级\nLv" .. i
			local jifenLvNum = UIConfigData.jifenLevelInfo[i]
			if jiFen >= jifenLvNum then
				duanweiBigImg:SetRes("Texture/Sprite/rywz.sprite")

				local leftNum = jiFen - jifenLvNum
				local rightNum = 99999

				if leftNum > rightNum then
					leftNum = rightNum
				end

				duanweijinduText:SetContent(leftNum .. "/" .. rightNum)
				duanweijindutiao:SetProgressValue(leftNum/rightNum)
				break
			end
		else
			jiFenLvText = "积分等级\nLv" .. i
			local jifenLvNum = UIConfigData.jifenLevelInfo[i]
			if jiFen >= jifenLvNum then
				duanweiBigImg:SetRes("Texture/Sprite/rywz.sprite")

				local leftNum = jiFen-UIConfigData.jifenLevelInfo[i]
				local rightNum = UIConfigData.jifenLevelInfo[i+1]-jifenLvNum

				duanweijinduText:SetContent(leftNum .. "/" .. rightNum)
				duanweijindutiao:SetProgressValue(leftNum/rightNum)
				break
			end
		end
	end
	formSelf:GetWidgetProxyByName("jiFenLvText"):GetText():SetContent(jiFenLvText)

	if Timer1 then
		Timer1:EndTimer()
	end
	return true
end


function OnMainFormOpen(luaUIEvent)
	math.randomseed(tostring(os.time()):reverse():sub(1,7))
	math.random()
	formSelf = luaUIEvent.SrcForm;
	-- LuaCallCs_PersistentData.SetCustomizeDataStringArr({3}, {""})
	-- LuaCallCs_PersistentData.SetCustomizeDataStringArr({4}, {""})

	duanweijinduText = formSelf:GetWidgetProxyByName("duanweijinduText"):GetText()
	duanweijindutiao = formSelf:GetWidgetProxyByName("duanweijindutiao")



	renshuText = formSelf:GetWidgetProxyByName("renshuText"):GetText()
	renshuText:SetContent(UIConfigData.teamInfo[1][6])
	nanduText = formSelf:GetWidgetProxyByName("nanduText"):GetText()
	nanduText:SetContent(UIConfigData.nanduInfo[1][1])


	zhankaiText = formSelf:GetWidgetProxyByName("zhankaiText")
	shouqiText = formSelf:GetWidgetProxyByName("shouqiText")


	shensuoPanel = formSelf:GetWidgetProxyByName("shensuoPanel")
	shensuoPanel:SetActive(true)

	shensuoBtn = formSelf:GetWidgetProxyByName("Button(1151)")

	msxzBigPanel = formSelf:GetWidgetProxyByName("msxzBig")
	msxzBigPanel:SetActive(false)

	renshuChooseList = formSelf:GetWidgetProxyByName("renshuChooseList")
	renshuChooseList:SetElementAmount(4)

	duanweiNuberImg = formSelf:GetWidgetProxyByName("Image(1119)")
	duanweiNuberImg:SetActive(false)
	duanweiNuberText = formSelf:GetWidgetProxyByName("TextLabel(1137)")
	duanweiNuberText:SetActive(false)

	duanweiBigImg = formSelf:GetWidgetProxyByName("Image(1080)"):GetImage()

	wanfaPanel = formSelf:GetWidgetProxyByName("wanfaPanel")

	formSelf:GetWidgetProxyByName("wanfaText"):GetText():SetContent("    守卫60波，不让怪物数量<color=#FF0000>超过上限</color>并击杀最终首领即可获胜！\n\n    游戏内可召唤<color=#00FF00>粮草押运官</color>，<color=#FFFF00>魔袋长老</color>，<color=#FF0000>深渊魔王</color>，击杀后可获得金币，装备等资源！\n\n    可以在技能商店购买强大的<color=#00FF00>技能书</color>来武装自己！\n\n    可以使用金币进行<color=#00FF00>附魔</color>，强化自己的属性！\n\n    每5波会出现大型首领，击杀后会掉落技能书，装备等资源，小怪还会随机获得技能\n\n    每3波会出现随机事件强化敌人，请小心应对！")

	wanfaPanel:SetActive(false)

	nanduList = formSelf:GetWidgetProxyByName("nanduList")
	nanduList:SetElementAmount(#UIConfigData.nanduInfo)

	LuaCallCs_Task.ReqTaskInfo()

	Timer1 = formSelf:GetWidgetProxyByName("timer")
	getDuanwei()
	Timer1:StartTimer()

	local baoshiZhuangPeiInfo = {
		{
			{}, -- 槽1 {阶级， 属性， 属性值}
			{}, -- 槽2 {阶级， 属性， 属性值}
			{}, -- 槽3 {阶级， 属性， 属性值}
			-- 红宝石
			{}, -- 槽1 {阶级， 属性， 属性值}
			{}, -- 槽2 {阶级， 属性， 属性值}
			{}, -- 槽3 {阶级， 属性， 属性值}
			-- 蓝宝石
			{}, -- 槽1 {阶级， 属性， 属性值}
			{}, -- 槽2 {阶级， 属性， 属性值}
			{}, -- 槽3 {阶级， 属性， 属性值}
			-- 紫宝石
			{}, -- 槽1 {阶级， 属性， 属性值}
			{}, -- 槽2 {阶级， 属性， 属性值}
			{}, -- 槽3 {阶级， 属性， 属性值}
			-- 绿宝石
		},
		{
			{}, -- 槽1 {阶级， 属性， 属性值}
			{}, -- 槽2 {阶级， 属性， 属性值}
			{}, -- 槽3 {阶级， 属性， 属性值}
			-- 红宝石
			{}, -- 槽1 {阶级， 属性， 属性值}
			{}, -- 槽2 {阶级， 属性， 属性值}
			{}, -- 槽3 {阶级， 属性， 属性值}
			-- 蓝宝石
			{}, -- 槽1 {阶级， 属性， 属性值}
			{}, -- 槽2 {阶级， 属性， 属性值}
			{}, -- 槽3 {阶级， 属性， 属性值}
			-- 紫宝石
			{}, -- 槽1 {阶级， 属性， 属性值}
			{}, -- 槽2 {阶级， 属性， 属性值}
			{}, -- 槽3 {阶级， 属性， 属性值}
			-- 绿宝石
		},
		{
			{}, -- 槽1 {阶级， 属性， 属性值}
			{}, -- 槽2 {阶级， 属性， 属性值}
			{}, -- 槽3 {阶级， 属性， 属性值}
			-- 红宝石
			{}, -- 槽1 {阶级， 属性， 属性值}
			{}, -- 槽2 {阶级， 属性， 属性值}
			{}, -- 槽3 {阶级， 属性， 属性值}
			-- 蓝宝石
			{}, -- 槽1 {阶级， 属性， 属性值}
			{}, -- 槽2 {阶级， 属性， 属性值}
			{}, -- 槽3 {阶级， 属性， 属性值}
			-- 紫宝石
			{}, -- 槽1 {阶级， 属性， 属性值}
			{}, -- 槽2 {阶级， 属性， 属性值}
			{}, -- 槽3 {阶级， 属性， 属性值}
			-- 绿宝石
		},
	}

	local baoShiCangKu = {
		    -- {1, 1, 1, 1, 1}, -- 颜色，阶级，等级，属性， 属性值
		    -- {1, 1, 1, 1, 1},
		    -- {1, 1, 1, 1, 1},
		    -- {1, 1, 1, 1, 1},
		    -- {2, 1, 1, 1, 1},
		    -- {2, 1, 1, 1, 1},
		    -- {2, 1, 1, 1, 1},
		    -- {2, 1, 1, 1, 1},
		    -- {1, 1, 1, 1, 1}, -- 颜色，阶级，等级，属性， 属性值
		    -- {1, 1, 1, 1, 1},
		    -- {1, 1, 1, 1, 1},
		    -- {1, 1, 1, 1, 1},
		    -- {2, 1, 1, 1, 1},
		    -- {2, 1, 1, 1, 1},
		    -- {2, 1, 1, 1, 1},
		    -- {2, 1, 1, 1, 1},
		    -- {1, 4, 4, 2, 29.8},
		    -- {3, 4, 3, 2, 29.8},
		    -- {2, 6, 3, 2, 29.8},
		}

	-- local s = json.encode(baoshiZhuangPeiInfo)
	-- LuaCallCs_PersistentData.SetCustomizeDataStringArr({4}, {s})

	-- local s1 = json.encode(baoShiCangKu)
	-- LuaCallCs_PersistentData.SetCustomizeDataStringArr({3}, {s1})

	-- LuaCallCs_PersistentData.SetCustomizeDataIntArr({17}, {1000000})

	intArr = LuaCallCs_PersistentData.GetCustomizeDataIntArr()
	local stringArrData = LuaCallCs_PersistentData.GetCustomizeDataStringArr()



	if stringArrData then
		local baoshiZhuangPeiInfoString = stringArrData[5]
		if baoshiZhuangPeiInfoString == "" then
			UIDynamicData.baoshiZhuangPeiInfo = {
				{-- 第一页
					{},{},{}, -- 红
					{},{},{}, -- 蓝
					{},{},{}, -- 紫
					{},{},{}  -- 绿
				},
				{{},{},{},{},{},{},{},{},{},{},{},{}},
				{{},{},{},{},{},{},{},{},{},{},{},{}},
			}
			local s = json.encode(UIDynamicData.baoshiZhuangPeiInfo)
			LuaCallCs_PersistentData.SetCustomizeDataStringArr({4}, {s})
		else
			UIDynamicData.baoshiZhuangPeiInfo = json.decode(baoshiZhuangPeiInfoString)
		end

		local baoShiCangKuInfoString = stringArrData[4]
		if baoShiCangKuInfoString == "" then
			UIDynamicData.baoShiCangKuInfo = {}
			local s = json.encode(UIDynamicData.baoShiCangKuInfo)
			LuaCallCs_PersistentData.SetCustomizeDataStringArr({3}, {s})
		else
			UIDynamicData.baoShiCangKuInfo = json.decode(baoShiCangKuInfoString)
		end
	end


	if intArr then
		local lastLoginTime = intArr[10]

		LuaCallCs_Common.Log(lastLoginTime)

		local nowTime = os.time()
		local nowLoginTimeInfo = {}
		nowLoginTimeInfo.year = tonumber(os.date("%Y",nowTime))
		nowLoginTimeInfo.month =tonumber(os.date("%m",nowTime))
		nowLoginTimeInfo.day = tonumber(os.date("%d",nowTime))
		nowLoginTimeInfo.hour = tonumber(os.date("%H",nowTime))

		local fiveOclock = os.time({day=nowLoginTimeInfo.day, month=nowLoginTimeInfo.month, year=nowLoginTimeInfo.year, hour=5, minute=0, second=0})

		if lastLoginTime < fiveOclock then -- 昨天的数据，数据要刷新了
			LuaCallCs_PersistentData.SetCustomizeDataIntArr({9}, {nowTime}) -- index=10,时间戳

			LuaCallCs_PersistentData.SetCustomizeDataIntArr({10}, {0}) -- index=11,活跃度

			LuaCallCs_PersistentData.SetCustomizeDataIntArr({11}, {0}) -- index=11,活跃度
			LuaCallCs_PersistentData.SetCustomizeDataIntArr({12}, {0}) -- index=11,活跃度
			LuaCallCs_PersistentData.SetCustomizeDataIntArr({13}, {0}) -- index=11,活跃度
			LuaCallCs_PersistentData.SetCustomizeDataIntArr({14}, {0}) -- index=11,活跃度
			LuaCallCs_PersistentData.SetCustomizeDataIntArr({15}, {0}) -- index=11,活跃度
			LuaCallCs_PersistentData.SetCustomizeDataIntArr({16}, {0}) -- index=11,活跃度
			intArr[10] = nowTime
			intArr[11] = 0 -- 今日总活跃度

			intArr[12] = 0 -- 日活宝箱领奖状况
			intArr[13] = 0
			intArr[14] = 0
			intArr[15] = 0
			intArr[16] = 0
			intArr[17] = 0
		end
		LuaCallCs_Common.Log("done")
		if stringArrData then
			printTb("stringArrData")
			printTb(stringArrData)
			for  k = 1, #stringArrData do
				if stringArrData[k] ~= "" then
					LuaCallCs_Common.Log("第" .. k .. "条stringArr数据:  " .. stringArrData[k])
				end
			end
		end
		LuaCallCs_Common.Log("打印int   start")
		for  k = 1, #intArr do
			if k > 20 then
				LuaCallCs_Common.Log("第" .. 200 .. "条intArr数据:  " .. intArr[200])
				return
			end
			LuaCallCs_Common.Log("第" .. k .. "条intArr数据:  " .. intArr[k])
		end
		LuaCallCs_Common.Log("打印int   end")


		LuaCallCs_Common.Log("lastLoginTime:  ")
	end
end










function genxinDuanweiInfo(luaUIEvent)
	getDuanwei()
end


function nanduxuanzeClicked(luaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

	if ismoshiBigPanelShow == false then
		renshuChooseList:SetElementAmount(#UIConfigData.teamInfo)
		nanduList:SetElementAmount(#UIConfigData.nanduInfo)

		msxzBigPanel:SetActive(true)
		ismoshiBigPanelShow = true
	else
		msxzBigPanel:SetActive(false)
		ismoshiBigPanelShow = false
	end
	getDuanwei()
end


function moshiKongbaiClicked(luaUIEvent)
	msxzBigPanel:SetActive(false)
	ismoshiBigPanelShow = false
end



function nanduListOnEnable(luaUIEvent)
	local elem = luaUIEvent.SrcWidget
	local index = elem:GetIndexInBelongedList()
	local nanduBtn = elem:GetWidgetProxyByName("nanduBtn")

	renshuText:SetContent(UIConfigData.teamInfo[nowPlayerCntChoose+1][6])
	nanduText:SetContent(UIConfigData.nanduInfo[nowNandu+1][1])

	-- 难度开放
	nanduBtn:GetImage():SetRes(UIConfigData.nanduInfo[index+1][4])
	if intArr then
		if (intArr[3] == 0 and index == 0) or (index+1) <= (intArr[3]+1) then
			nanduBtn:GetImage():SetRes(UIConfigData.nanduInfo[index+1][3])
			nanduBtn:EnableInput(true)
		else
			nanduBtn:GetImage():SetRes(UIConfigData.nanduInfo[index+1][4])
			nanduBtn:EnableInput(false)
		end
	else
		intArr = LuaCallCs_PersistentData.GetCustomizeDataIntArr()
	end
end



function playerCntBtnClicked(luaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

	local index = renshuChooseList:GetSelectedIndex()
	nowPlayerCntChoose = index
	renshuChooseList:SetElementAmount(#UIConfigData.teamInfo)
	msxzBigPanel:SetActive(false)
	ismoshiBigPanelShow = false
end



function renshuChooseBtnClicked(luaUIEvent)
	jiantou = formSelf:GetWidgetProxyByName("Image(1112)"):GetImage()

	if ismoshiBigPanelShow == false then
		jiantou:SetRes("UGCResource/UGCUI/Sprite/General/widget/arrowDown.prefab")
		msxzBigPanel:SetActive(true)
		ismoshiBigPanelShow = true
	else
		jiantou:SetRes("UGCResource/UGCUI/Sprite/General/widget/arrow.prefab")
		msxzBigPanel:SetActive(false)
		ismoshiBigPanelShow = false
	end

	renshuChooseList:SetElementAmount(4)
end


function nanduBtnClicked(luaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

	local index = nanduList:GetSelectedIndex()
	nowNandu = index
	nanduList:SetElementAmount(#UIConfigData.nanduInfo)

	msxzBigPanel:SetActive(false)
	ismoshiBigPanelShow = false
end



function onPlayerCntPanelOnEnable(luaUIEvent)
	local elem = luaUIEvent.SrcWidget
	local index = elem:GetIndexInBelongedList()
	local playerCntBtn = elem:GetWidgetProxyByName("playerCntBtn")

	renshuText:SetContent(UIConfigData.teamInfo[nowPlayerCntChoose+1][6])
	nanduText:SetContent(UIConfigData.nanduInfo[nowNandu+1][1])

	if index == nowPlayerCntChoose then
		playerCntBtn:GetImage():SetRes(UIConfigData.teamInfo[index+1][3])
	else
		playerCntBtn:GetImage():SetRes(UIConfigData.teamInfo[index+1][4])
	end

end





function wanfaBtnClicked(luaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

	wanfaPanel:SetActive(true)
end

function wanfaCloseClicked(luaUIEvent)
	wanfaPanel:SetActive(false)
end







function OnMainFormClose(luaUIEvent)
	--销毁局外聊天黑盒
	LuaCallCs_InnerSystem.DestoryInnerLobbyChatForm()

	MainForm = {}
	formSelf = nil
	jiantou = nil
	nowPlayerCntChoose = 0 -- 索引从0开始
	ismoshiBigPanelShow = false
	nowNandu = 0
	nanduList = nil
	duanweijindutiao = nil
	duanweijinduText = nil
	Timer1 = nil
	msxzBigPanel = nil
	renshuChooseList = nil
	renshuText = nil
	nanduText = nil
	duanweiNuberImg = nil
	duanweiNuberText = nil
	duanweiBigImg = nil
	wanfaPanel = nil
	intArr = nil
	shensuoPanel = nil
	shensuoBtn = nil
	zhankaiText = nil
	shouqiText = nil
end


--单人匹配 
function OnQuicklyMatchOnePlayer(luaUIEvent)
	local stringArrData = LuaCallCs_PersistentData.GetCustomizeDataStringArr()
	checkHeroExFunc(stringArrData[1])

	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")
	--发送匹配申请
	local playerCnt = nowPlayerCntChoose+1
	LuaCallCs_Common.Log(playerCnt .. "人局")

	local modeName = "swwc_nandu_" .. nowNandu+1
	LuaCallCs_Common.Log("modeName:     " .. modeName)


	LuaCallCs_UGCStateDriver.StartSingleMatching(playerCnt, modeName)


	-- LuaCallCs_PersistentData.SetCustomizeDataIntArr({3}, {nowNandu+1}) -- 设置持久化数据的，索引是从0开始的
end

-- 组队点击事件
function zuduiBtnOnClicked(luaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

	local teamPlayerCnt = nowPlayerCntChoose+1
	LuaCallCs_Common.Log(teamPlayerCnt .. "人局")
	local teamModeName = "swwc_nandu_" .. nowNandu+1
	LuaCallCs_Common.Log("teamModeName:    " .. teamModeName)

	LuaCallCs_TeamMatch.CreateTeam(teamPlayerCnt, 1, teamPlayerCnt, UIConfigData.teamInfo[teamPlayerCnt][1], teamModeName)

	-- LuaCallCs_PersistentData.SetCustomizeDataIntArr({3}, {nowNandu+1})
end


--当开始匹配的时候
function MainForm.OnStartMatching()
	--显示顶部匹配时间UI黑盒
	LuaCallCs_TeamMatch.ShowOrUpdateMatchingStateTopTeamFuncForm()
end

--匹配成功，收到需要点击确认的要求
function MainForm.ReceiveNeedConfirmMatching(param)
	--显示匹配确认界面黑盒
	LuaCallCs_TeamMatch.ShowMatchingConfirmBox(enUGCMatchingConfirmBoxType.enConfirmBoxType_SingleSideAllCamp)
end


function OnCancelMatching()
	LuaCallCs_Common.Log("取消了匹配")
end




-- -- 聊天点击事件
-- function liaotianBtnOnClicked(luaUIEvent)
-- 	--创建局外聊天黑盒
-- 	LuaCallCs_InnerSystem.CreateInnerLobbyChatForm()
-- 	-- LuaCallCs_InnerSystem.CreateInnerLobbyChatForm()
-- 	LuaCallCs_InnerSystem.ShowInnerLobbyChatForm()

-- end

-- 任务点击事件
function renwuBtnOnClicked(luaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")
	-- LuaCallCs_InnerSystem.OpenInnerTaskForm()
	LuaCallCs_UI.OpenForm("UI/OnlineMode/RenWu.uixml")
end

-- -- 好友点击事件
function haoyouBtnOnClicked(luaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

	LuaCallCs_Friend.OpenFriendForm()
end


-- 排行榜点击事件
function paihangbangBtnOnClicked(luaUIEvent)
	local Ranking = {}
	table.insert(Ranking,{0,"击杀榜","击杀数"})
	table.insert(Ranking,{1,"金钱榜","金币数"})
	LuaCallCs_InnerSystem.OpenInnerRankingForm(Ranking)
end




function checkFightDataFunc(stringArrData)
	local fightData = stringArrData[2]
	LuaCallCs_Common.Log(fightData)
end


-- 战绩点击事件
function zhanjiBtnOnClicked(luaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

	LuaCallCs_History.RequestGameReportHistory()
	local stringArrData = LuaCallCs_PersistentData.GetCustomizeDataStringArr()

	checkFightDataFunc(stringArrData)

	checkHeroExFunc(stringArrData[1])
	LuaCallCs_UI.OpenForm("UI/OnlineMode/Zhanji.uixml")
end


-- 开房间点击事件
function kaifangjianBtnOnClicked(luaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

	LuaCallCs_Room.CreateFaceToFaceRoom(4, 1, 4, "111")
end


-- 商城点击事件
function shangchengBtnOnClicked(luaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

end


-- 道具包点击事件
function daojubaoBtnOnClicked(luaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

	LuaCallCs_InnerSystem.OpenInnerBagForm()
end


-- 道具点击事件
function daojuBtnOnClicked(luaUIEvent)
end



function Split(szFullString, szSeparator)
	local nFindStartIndex = 1
	local nSplitIndex = 1
	local nSplitArray = {}
	while true do
	   local nFindLastIndex = string.find(szFullString, szSeparator, nFindStartIndex)
	   if not nFindLastIndex then
			nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, string.len(szFullString))
			break
      end
      nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, nFindLastIndex - 1)
      nFindStartIndex = nFindLastIndex + string.len(szSeparator)
      nSplitIndex = nSplitIndex + 1
	end
	return nSplitArray
end




function OnReceivedReportHistoryData()
	UIDynamicData.zhanjiData = {}
	LuaCallCs_Common.Log("发送获取战绩请求")
	local HistArr = LuaCallCs_History.GetGameReportHistory()

	local zhanjilen = #HistArr
	if zhanjilen >= 10 then
		zhanjilen = 10
	end

	for i = 1, #HistArr do -- zdc 应该是 #HistArr
		table.insert(UIDynamicData.zhanjiData, json.decode(HistArr[i]))
	end
end


function shensuoClicked(luaUIEvent)
	local shensuoBtnX = shensuoBtn:GetScreenPosition().x
	local shensuoBtnY = shensuoBtn:GetScreenPosition().y
	local shensuoPanelY = shensuoPanel:GetScreenPosition().y

	local textX = formSelf:GetWidgetProxyByName("Button(1172)"):GetScreenPosition().x

	if shensuoPanel:IsActived() == true then
		shensuoBtn:GetImage():SetRes("Texture/Sprite/zhankai.sprite")

		shensuoPanel:SetActive(false)
    	LuaCallCs_Tween.MoveToScreenPosition(shensuoPanel, shensuoBtnX-300, shensuoPanelY, 0.1)

		shouqiText:SetActive(false)
		LuaCallCs_Tween.MoveToScreenPosition(shouqiText, textX, shensuoBtnY-91, 0.1)

		zhankaiText:SetActive(true)
    	LuaCallCs_Tween.MoveToScreenPosition(zhankaiText, textX, shensuoBtnY-91, 0.1)
	else
		shensuoBtn:GetImage():SetRes("Texture/Sprite/shouqi.sprite")

		shensuoPanel:SetActive(true)
    	LuaCallCs_Tween.MoveToScreenPosition(shensuoPanel, shensuoBtnX+68, shensuoPanelY, 0.1)

		zhankaiText:SetActive(false)
    	LuaCallCs_Tween.MoveToScreenPosition(zhankaiText, textX, shensuoBtnY-82, 0.1)

		shouqiText:SetActive(true)
		LuaCallCs_Tween.MoveToScreenPosition(shouqiText, textX, shensuoBtnY-82, 0.1)
	end
end



function tuJianBtnOnClicked(luaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")
	LuaCallCs_UI.OpenForm("UI/OnlineMode/TuJian.uixml")
end


function chengjiuBtnOnClicked(luaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

	LuaCallCs_UI.OpenForm("UI/OnlineMode/ChengJiu.uixml")
end




function BaoShiBtnOnClicked(luaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")
	LuaCallCs_UI.OpenForm("UI/OnlineMode/CunDangBaoShi.uixml")
end



return MainForm