G_GameDataMgr = require"OnlineMode/G_GameDataMgr.lua"

json = require "json.lua"

local formSelf
local monsterText
local nowCount = 0
local l_slider_time
local mainTimer
local checkEquipIndex = -1
local equipTipsPanel
local equipTipsText
local isbeibaoShow = false
local beiBaoPanel = false
local nowEquipClickedIndex = -1
local nowDaojuClickedIndex = -1
local wupingNameText
local wupingInfoText

local zhuangbeiChoosePanel
local beibaoBtn

local endTimer

local shiyongBtn

local GMGongGaoList

local shanghaiList


local shezhiPanel
local wfszPanel
local hmszPanel
local yxszPanel

math.randomseed(tostring(os.time()):reverse():sub(1,7))
math.random()


local function setSheZhi()
    shezhiPanel = formSelf:GetWidgetProxyByName("shezhiPanel")
    shezhiPanel:SetActive(false)
    wfszPanel = formSelf:GetWidgetProxyByName("wfszPanel")
    hmszPanel = formSelf:GetWidgetProxyByName("hmszPanel")
    yxszPanel = formSelf:GetWidgetProxyByName("yxszPanel")

    -- 玩法设置
    formSelf:GetWidgetProxyByName("Toggle(1260)"):SetChecked(false)


    -- 画面设置
    formSelf:GetWidgetProxyByName("Toggle(1254)"):SetChecked(false)
    formSelf:GetWidgetProxyByName("ToggleList(1256)"):SelectElement (2, false);
    formSelf:GetWidgetProxyByName("Toggle(1266)"):SetChecked(false)

    -- 音效设置
    local yinYueNum = LuaCallCs_Common.GetMusicSound() -- 音乐
    printTb("音乐大小： " .. yinYueNum)
    formSelf:GetWidgetProxyByName("Slider(1247)"):SetMinValue(0)
    formSelf:GetWidgetProxyByName("Slider(1247)"):SetMaxValue(100)
    formSelf:GetWidgetProxyByName("Slider(1247)"):SetCurrentValue(yinYueNum)

    local yinXiaoNum = LuaCallCs_Common.GetEffectSound() -- 音效
    printTb("音效大小： " .. yinXiaoNum)
    formSelf:GetWidgetProxyByName("Slider(1251)"):SetMinValue(0)
    formSelf:GetWidgetProxyByName("Slider(1251)"):SetMaxValue(100)
    formSelf:GetWidgetProxyByName("Slider(1251)"):SetCurrentValue(yinXiaoNum)

    -- 初始展示状态
    wfszPanel:SetActive(true)
    hmszPanel:SetActive(false)
    yxszPanel:SetActive(false)
end



function shezhiCloseClicked(LuaUIEvent)
    shezhiPanel:SetActive(false)
end

function wfszbtnClicked(LuaUIEvent)
    wfszPanel:SetActive(true)
    hmszPanel:SetActive(false)
    yxszPanel:SetActive(false)
end

function hmszbtnClicked(LuaUIEvent)
    hmszPanel:SetActive(true)
    wfszPanel:SetActive(false)
    yxszPanel:SetActive(false)
end


function yxszbtnClicked(LuaUIEvent)
    yxszPanel:SetActive(true)
    hmszPanel:SetActive(false)
    wfszPanel:SetActive(false)
end




function shezhiBtnClicked(LuaUIEvent)
    shezhiPanel:SetActive(true)
end



function gxdlChanged(LuaUIEvent)
    local msg = {}
    msg.cmd = "setShareBtn"
    LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg))
end


function gzlChanged(LuaUIEvent)
    if formSelf:GetWidgetProxyByName("Toggle(1254)"):IsChecked() then
        formSelf:GetWidgetProxyByName("Toggle(1254)"):SetChecked(true)
        LuaCallCs_Common.IsUse60FPS(true)
    else
        formSelf:GetWidgetProxyByName("Toggle(1254)"):SetChecked(false)
        LuaCallCs_Common.IsUse60FPS(false)
    end
end


function hmzlChanged(LuaUIEvent)
    local index = formSelf:GetWidgetProxyByName("ToggleList(1256)"):GetSelectedIndex()
    local lastIndex = formSelf:GetWidgetProxyByName("ToggleList(1256)"):GetLastSelectedIndex()
    if index ~= lastIndex then
        printTb("设置画质为： " .. index)
        LuaCallCs_Common.SetOnModeLOD(index)
    end
end



function hmfzChanged(LuaUIEvent)
    if formSelf:GetWidgetProxyByName("Toggle(1266)"):IsChecked() then  -- ischecked是有点潜规则的，这样反着用就完事儿了
        formSelf:GetWidgetProxyByName("Toggle(1266)"):SetChecked(true)
        LuaCallCs_Common.SetScreenRotation(true)
    else
        formSelf:GetWidgetProxyByName("Toggle(1266)"):SetChecked(false)
        LuaCallCs_Common.SetScreenRotation(false)
    end
end


function yxyyGouXuanChanged(LuaUIEvent)
    local jindutiao = formSelf:GetWidgetProxyByName("Slider(1247)")

    if formSelf:GetWidgetProxyByName("Toggle(1243)"):IsChecked() then  -- ischecked是有点潜规则的，这样反着用就完事儿了
        formSelf:GetWidgetProxyByName("Toggle(1243)"):SetChecked(true)
        jindutiao:EnableInput(true)

        ---获取滑动条背景Image组件Proxy
        jindutiao:GetSliderBGImage():SetGray(false)

        ---获取滑动条填充Image组件Proxy
        jindutiao:GetSliderFillImage():SetGray(false)

        ---获取滑块Image组件Proxy
        jindutiao:GetHandleImage():SetGray(false)
    else
        formSelf:GetWidgetProxyByName("Toggle(1243)"):SetChecked(false)
        jindutiao:EnableInput(false)

        ---获取滑动条背景Image组件Proxy
        jindutiao:GetSliderBGImage():SetGray(true)

        ---获取滑动条填充Image组件Proxy
        jindutiao:GetSliderFillImage():SetGray(true)

        ---获取滑块Image组件Proxy
        jindutiao:GetHandleImage():SetGray(true)
    end
end


function yxyxGouXuanChanged(LuaUIEvent)
    local jindutiao = formSelf:GetWidgetProxyByName("Slider(1251)")
    if formSelf:GetWidgetProxyByName("Toggle(1249)"):IsChecked() then  -- ischecked是有点潜规则的，这样反着用就完事儿了
        formSelf:GetWidgetProxyByName("Toggle(1249)"):SetChecked(true)
        jindutiao:EnableInput(true)

        ---获取滑动条背景Image组件Proxy
        jindutiao:GetSliderBGImage():SetGray(false)

        ---获取滑动条填充Image组件Proxy
        jindutiao:GetSliderFillImage():SetGray(false)

        ---获取滑块Image组件Proxy
        jindutiao:GetHandleImage():SetGray(false)
    else
        formSelf:GetWidgetProxyByName("Toggle(1249)"):SetChecked(false)
        jindutiao:EnableInput(false)

        ---获取滑动条背景Image组件Proxy
        jindutiao:GetSliderBGImage():SetGray(true)

        ---获取滑动条填充Image组件Proxy
        jindutiao:GetSliderFillImage():SetGray(true)

        ---获取滑块Image组件Proxy
        jindutiao:GetHandleImage():SetGray(true)
    end
end




function yxyyChanged(LuaUIEvent)
    local changedNum = formSelf:GetWidgetProxyByName("Slider(1247)"):GetCurrentValue()
    LuaCallCs_Common.SetMusicSound(changedNum)
end



function yxyxChanged(LuaUIEvent)
    local changedNum = formSelf:GetWidgetProxyByName("Slider(1251)"):GetCurrentValue()
    LuaCallCs_Common.SetEffectSound(changedNum)
end


function OnWinOrLoseOpen(LuaUIEvent)
    -- LuaCallCs_InnerSystem.OpenInnerBattleChatForm()
    -- local camp = LuaCallCs_Battle.GetHostPlayerCamp()
    -- LuaCallCs_InnerSystem.SetInnerBattleChatCamp(camp)
    nowCount = 0
    formSelf = LuaUIEvent.SrcForm
    formSelf:Hide()

    
    setSheZhi()

    beiBaoPanel = formSelf:GetWidgetProxyByName("beiBaoPanel")
    beiBaoPanel:SetActive(false)

    GMGongGaoList = formSelf:GetWidgetProxyByName("GMGongGaoList")
    GMGongGaoList:EnableInput(false)

    endTimer = formSelf:GetWidgetProxyByName("endTime")

    formSelf:GetWidgetProxyByName("Panel(1267)"):SetActive(false)

    monsterText = formSelf:GetWidgetProxyByName("msterCntText"):GetText()
    local nowCntText = nowCount
    if nowCount > #LuaCallCs_UGCStateDriver.GetAllPlayerInfos()*UIConfigData.loseCount then
        nowCntText = "<color=#FF0000>" .. nowCount .. "</color>"
    end
    monsterText:SetContent(nowCntText .. "/" .. #LuaCallCs_UGCStateDriver.GetAllPlayerInfos()*UIConfigData.loseCount)


    beibaoBtn = formSelf:GetWidgetProxyByName("beibaoBtn")


    wupingNameText = formSelf:GetWidgetProxyByName("wupingName"):GetText()
    wupingInfoText = formSelf:GetWidgetProxyByName("wupingInfo"):GetText()

    zhuangbeiChoosePanel = formSelf:GetWidgetProxyByName("zhuangbeiChoosePanel")
    zhuangbeiChoosePanel:SetActive(false)

    formSelf:GetWidgetProxyByName("Panel(1217)"):SetActive(false)

    shiyongBtn = formSelf:GetWidgetProxyByName("useDaojuBtn")




    l_slider_time = formSelf:GetWidgetProxyByName("Timer")
    mainTimer = formSelf:GetWidgetProxyByName("mainTimer")
end


function monsterCntAppear()
    formSelf:Appear()
end

function addGMGongGaoInfo(PlayerID, cmd, paramet1, paramet2, paramet3)
    local text
    local isBig = false
    local addType = 44 --44: 4个人发送，4个人存储     11: 1个人发送，一个人存储  14： 一个人发送，4个人存储

    if cmd == "actorDead" then
        addType = 44
        local playerName

        local playerinfo = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()
        for i=1, #playerinfo do
            if playerinfo[i].playerID == PlayerID then
                playerName = playerinfo[i].playerName
                break
            end
        end

        text = UIConfigData.GMGongGaoList[cmd][1] .. playerName .. UIConfigData.GMGongGaoList[cmd][2]
    elseif cmd == "symw" or cmd == "lcyyg" or cmd == "mdzl" or cmd == "wq" or cmd == "hj" or cmd == "jz" then
        local actorIDList = LuaCallCs_Battle.GetActorIDListByPlayerID(PlayerID)
        local actorID = actorIDList[1]
        addType = 14
        local playerName = LuaCallCs_Data.GetPlayerNameByActorId(actorID)
        text = playerName .. UIConfigData.GMGongGaoList[cmd]
    elseif cmd == "zbg" or cmd == "jyg" or cmd == "zzboss" or cmd == "sl" or cmd == "bbm" or cmd == "yxks3" or cmd == "jnsGMGG5" or cmd == "jnsGMGG15" or cmd == "jnsGMGG25" then
        text = UIConfigData.GMGongGaoList[cmd]
        if cmd == "zbg" or cmd == "jyg" or cmd == "jnsGMGG5" or cmd == "jnsGMGG15" or cmd == "jnsGMGG25" then
            isBig = true
            formSelf:GetWidgetProxyByName("TextLabel(1219)"):GetText():SetContent(text)
            formSelf:GetWidgetProxyByName("Panel(1217)"):SetActive(true)
        end
    elseif cmd == "md" then
        addType = 11
        text = UIConfigData.GMGongGaoList[cmd][1] .. paramet1 .. UIConfigData.GMGongGaoList[cmd][2] .. paramet2 .. UIConfigData.GMGongGaoList[cmd][3]
    elseif cmd == "yxks1" or cmd == "yxks2" or cmd == "bossdjs" then
        addType = 11
        text = UIConfigData.GMGongGaoList[cmd][1] .. paramet1 .. UIConfigData.GMGongGaoList[cmd][2]
    elseif cmd == "bossKilldjs" then
        addType = 11
        text = UIConfigData.GMGongGaoList[cmd][1] .. paramet1 .. UIConfigData.GMGongGaoList[cmd][2]
    elseif cmd == "wqfm" or cmd == "hjfm" or cmd == "jzfm" then
        addType = 11
        text = UIConfigData.GMGongGaoList[cmd]
        if paramet1 == false then
            text = text .. "<color=#FF0000>失败</color>"
        else
            text = text .. "<color=#00FF00>成功</color>"
        end
    elseif UIConfigData.GMGongGaoList[cmd] ~= nil then
        addType = 44
        text = UIConfigData.GMGongGaoList[cmd]
    end

    for key, value in pairs(UIDynamicData.GMGongGaoInfo) do
        if addType == 44 or addType == 14 then
            if #value == 0 then
                table.insert(value, "[" .. os.date("%M:%S", UIDynamicData.time) .. "]:" .. text)
            elseif #value == 1 then
                table.insert(value, "[" .. os.date("%M:%S", UIDynamicData.time) .. "]:" .. text)
            elseif #value == 2 then
                value[1] = value[2]
                value[2] = "[" .. os.date("%M:%S", UIDynamicData.time) .. "]:" .. text
            end
        elseif addType == 11 then
            if key == PlayerID then
                if #value == 0 then
                    table.insert(value, "[" .. os.date("%M:%S", UIDynamicData.time) .. "]:" .. text)
                elseif #value == 1 then
                    table.insert(value, "[" .. os.date("%M:%S", UIDynamicData.time) .. "]:" .. text)
                elseif #value == 2 then
                    value[1] = value[2]
                    value[2] = "[" .. os.date("%M:%S", UIDynamicData.time) .. "]:" .. text
                end
            end
        end

    end

    GMGongGaoList:SetAlpha(1)
    GMGongGaoList:SetElementAmount(2)
    local msg = {}
    msg.cmd = "GMGongGaoTime"
    LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg)) -- 发给蓝图

    if isBig == true then
        local msg1 = {}
        msg1.cmd = "BigGMGongGaoTime"
        LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg1)) -- 发给蓝图
    end
end



function closeBigGongGao(pid)
    if pid == LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
        LuaCallCs_Common.Log("关闭了")
        formSelf:GetWidgetProxyByName("Panel(1217)"):SetActive(false)
    end
end

function closeGongGao(playerID)
    LuaCallCs_Tween.WidgetAlpha(GMGongGaoList, 0, 0.5)
end



    -- local btn = formSelf:GetWidgetProxyByName("Button(1186)")

    -- local vint2 = LuaCallCs_FightUI.GetSkillBtnScreenPosition(5)
    -- LuaCallCs_Common.Log(vint2.x)
    -- LuaCallCs_Common.Log(vint2.y)

    -- LuaCallCs_Tween.MoveToScreenPosition(btn, vint2.x-10, vint2.y+77, 0.01)
function addCoinBtnClicked(LuaUIEvent)
    local msg = {}
    msg.cmd = "addCoin"
    LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg)) -- 发给蓝图
end




function jifenbanClicked(LuaUIEvent)
    openBtnClicked(LuaUIEvent)
end



function GMGongGaoOnEnable(LuaUIEvent)
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()
    if UIDynamicData.GMGongGaoInfo[selfPlayerID] then
        if #UIDynamicData.GMGongGaoInfo[selfPlayerID] >= (index+1) then
            if index == 0 then
                elem:GetWidgetProxyByName("GMGongGaoText"):SetAlpha(0.7)
                local text = "<color=#CDC9C9>" .. UIDynamicData.GMGongGaoInfo[selfPlayerID][index+1] .."</color>"
                elem:GetWidgetProxyByName("GMGongGaoText"):GetText():SetContent(UIDynamicData.GMGongGaoInfo[selfPlayerID][index+1])
            else
                elem:GetWidgetProxyByName("GMGongGaoText"):GetText():SetContent(UIDynamicData.GMGongGaoInfo[selfPlayerID][index+1])
            end
        end
    end
end


-- 刷新背包
function refreshEquipInfo(selfPlayerID)
    local selfpid = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    if UIDynamicData.itsOKInfo[selfpid] == false then
        return
    end

    if isbeibaoShow == false then
        return
    end

    if selfPlayerID ~= selfpid then
        return
    end

    formSelf:GetWidgetProxyByName("List(1088)"):SetElementAmount(6)
    formSelf:GetWidgetProxyByName("Cloned_List(1088)(1095)"):SetElementAmount(6)
    if nowEquipClickedIndex ~= -1 then
        local actorID = LuaCallCs_Battle.GetHostActorID();
        local cellInfo = LuaCallCs_Equip.GetEquipCellInfo(actorID, 0, nowEquipClickedIndex)
        local daojuName = LuaCallCs_Equip.GetEquipInfo(cellInfo.m_equipID).m_equipName
        local daojuInfo = LuaCallCs_Equip.GetEquipInfo(cellInfo.m_equipID).m_equipDesc

        local Cellinfo = LuaCallCs_Equip.GetEquipCellInfo(actorID, 0, nowEquipClickedIndex) -- 第三个参数索引从0开始
        local equipID = Cellinfo.m_equipID

        local equipLv = UIConfigData.equipImgInfo[equipID][2]

        local daojuNameColor
        if equipLv == 1 then
            daojuNameColor = "<color=#00FF00>" .. daojuName .. "</color>"
        elseif equipLv == 2 then
            daojuNameColor = "<color=#1E90FF>" .. daojuName .. "</color>"
        elseif equipLv == 3 then
            daojuNameColor = "<color=#8A2BE2>" .. daojuName .. "</color>"
        elseif equipLv == 4 then
            daojuNameColor = "<color=#EE7600>" .. daojuName .. "</color>"
        end


        wupingNameText:SetContent(daojuNameColor)
        wupingInfoText:SetContent(daojuInfo)

        shiyongBtn:EnableInput(false)
    end


    if nowDaojuClickedIndex ~= -1 then
        local daojuName = UIDynamicData.daojuInfo[selfPlayerID][nowDaojuClickedIndex+1][1]
        local daojuInfo = UIDynamicData.daojuInfo[selfPlayerID][nowDaojuClickedIndex+1][4]
        local daojuNameColor = UIConfigData.daojuNameColorInfo[daojuName]
        wupingNameText:SetContent(daojuNameColor)
        wupingInfoText:SetContent(daojuInfo)

        shiyongBtn:EnableInput(true)
    end

    if nowEquipClickedIndex == -1 and nowDaojuClickedIndex == -1 then
        zhuangbeiChoosePanel:SetActive(false)
    else
        zhuangbeiChoosePanel:SetActive(true)
    end
end


function equipImgHoldStart(LuaUIEvent)
    local index = LuaUIEvent.SrcWidget:GetIndexInBelongedList();
    local actorID = LuaCallCs_Battle.GetHostActorID();
    local CellInfo = LuaCallCs_Equip.GetEquipCellInfo(actorID, 0, index)
    if CellInfo then
        equipTipsText:SetContent(LuaCallCs_Equip.GetEquipInfo(CellInfo.m_equipID).m_equipDesc)
    end
    equipTipsPanel:SetActive(true)
end

function equipImgHoldEnd(LuaUIEvent)
    equipTipsPanel:SetActive(false)
end



function daojuImgHoldStart(LuaUIEvent)
    local index = LuaUIEvent.SrcWidget:GetIndexInBelongedList();
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    local desc = UIDynamicData.daojuInfo[selfPlayerID][index+1][4]
    if desc then
        equipTipsText:SetContent(desc)
    end
    equipTipsPanel:SetActive(true)
end

function daojuImgHoldEnd(LuaUIEvent)
    equipTipsPanel:SetActive(false)
end










function beibaoClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    formSelf:GetWidgetProxyByName("List(1088)"):SetElementAmount(6)
    formSelf:GetWidgetProxyByName("Cloned_List(1088)(1095)"):SetElementAmount(6)
    if isbeibaoShow == false then
        beiBaoPanel:SetActive(true)
        isbeibaoShow = true
    else
        beiBaoPanel:SetActive(false)
        isbeibaoShow = false
    end
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    refreshEquipInfo(selfPlayerID)
end


function beibaoPanelClose(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    beiBaoPanel:SetActive(false)
    isbeibaoShow = false
end




function daojuClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    local index = LuaUIEvent.SrcWidget:GetIndexInBelongedList()
    if UIDynamicData.daojuInfo[selfPlayerID][index+1] == nil then
        nowDaojuClickedIndex = -1
        nowEquipClickedIndex = -1
        refreshEquipInfo(selfPlayerID)
        return
    end

    nowDaojuClickedIndex = index
    nowEquipClickedIndex = -1

    refreshEquipInfo(selfPlayerID)
end









function chushoudaojuClicked(LuaUIEvent)
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    local daojuName = UIDynamicData.daojuInfo[selfPlayerID][nowDaojuClickedIndex+1][1]

    local msg = {}
    local num
    msg.cmd = "chushoudaoju"
    if daojuName == "D级被动技能书" then
        num = 1
        msg.sT = 0 -- skillType（是否主被动） 1是主动
    elseif daojuName == "D级主动技能书" then
        num = 5
        msg.sT = 1 -- skillType（是否主被动） 1是主动
    elseif daojuName == "C级被动技能书" then
        num = 2
        msg.sT = 0 -- skillType（是否主被动） 1是主动
    elseif daojuName == "C级主动技能书" then
        num = 6
        msg.sT = 1 -- skillType（是否主被动） 1是主动
    elseif daojuName == "B级被动技能书" then
        num = 3
        msg.sT = 0 -- skillType（是否主被动） 1是主动
    elseif daojuName == "B级主动技能书" then
        num = 7
        msg.sT = 1 -- skillType（是否主被动） 1是主动
    elseif daojuName == "A级被动技能书" then
        num = 4
        msg.sT = 0 -- skillType（是否主被动） 1是主动
    elseif daojuName == "A级主动技能书" then
        num = 8
        msg.sT = 1 -- skillType（是否主被动） 1是主动
    end
    msg.num = num
    LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg))

    if UIDynamicData.daojuInfo[selfPlayerID][nowDaojuClickedIndex+1][3] == 1 then
        nowDaojuClickedIndex = -1
    end

    formSelf:GetWidgetProxyByName("Cloned_List(1088)(1095)"):SetElementAmount(6)
end




function shiyongdaojuClicked(LuaUIEvent) --zdc使用技能书的 没同步呢
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID

    local daojuName = UIDynamicData.daojuInfo[selfPlayerID][nowDaojuClickedIndex+1][1]
    printTb("daojuName: " .. daojuName)
    local msg = {}
    local num
    msg.cmd = "useSkillBook"
    if daojuName == "D级被动技能书" then
        num = 1
        msg.sT = 0 -- skillType（是否主被动） 1是主动
    elseif daojuName == "D级主动技能书" then
        num = 5
        msg.sT = 1 -- skillType（是否主被动） 1是主动
    elseif daojuName == "C级被动技能书" then
        num = 2
        msg.sT = 0 -- skillType（是否主被动） 1是主动
    elseif daojuName == "C级主动技能书" then
        num = 6
        msg.sT = 1 -- skillType（是否主被动） 1是主动
    elseif daojuName == "B级被动技能书" then
        num = 3
        msg.sT = 0 -- skillType（是否主被动） 1是主动
    elseif daojuName == "B级主动技能书" then
        num = 7
        msg.sT = 1 -- skillType（是否主被动） 1是主动
    elseif daojuName == "A级被动技能书" then
        num = 4
        msg.sT = 0 -- skillType（是否主被动） 1是主动
    elseif daojuName == "A级主动技能书" then
        num = 8
        msg.sT = 1 -- skillType（是否主被动） 1是主动
    end
    msg.num = num
    LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg))

    if UIDynamicData.daojuInfo[selfPlayerID][nowDaojuClickedIndex+1][3] == 1 then
        nowDaojuClickedIndex = -1
    end
    formSelf:GetWidgetProxyByName("Cloned_List(1088)(1095)"):SetElementAmount(6)
end




function daojuOnEnable(LuaUIEvent)
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    if UIDynamicData.itsOKInfo[selfPlayerID] ~= true then
        return
    end

    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()

    if UIDynamicData.daojuInfo[selfPlayerID][index+1] ~= nil then

        elem:GetWidgetProxyByName("daojuImg"):GetImage():SetRes(UIDynamicData.daojuInfo[selfPlayerID][index+1][2])

        elem:GetWidgetProxyByName("cntText"):GetText():SetContent(UIDynamicData.daojuInfo[selfPlayerID][index+1][3])
        if nowDaojuClickedIndex == index then
            elem:GetWidgetProxyByName("djkuang"):GetImage():SetRes("Texture/Sprite/kuangHuang.sprite")
        else
            elem:GetWidgetProxyByName("djkuang"):GetImage():SetRes("Texture/Sprite/kuangHui.sprite")
        end

        elem:GetWidgetProxyByName("cntImg"):SetActive(true)
        elem:GetWidgetProxyByName("djkuang"):SetActive(true)
    else
        elem:GetWidgetProxyByName("daojuImg"):GetImage():SetRes("Texture/Sprite/kongcao.sprite")
        elem:GetWidgetProxyByName("cntImg"):SetActive(false)
        elem:GetWidgetProxyByName("djkuang"):SetActive(false)
    end
end



function chushouClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    if nowEquipClickedIndex ~= -1 then
        -- 当前选中的是装备
        local msg = {}
        msg.cmd = "sellEquip"
        msg.index = nowEquipClickedIndex
        LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg))

        nowEquipClickedIndex = -1
        nowDaojuClickedIndex = -1

    elseif nowDaojuClickedIndex ~= -1 then
        -- 当前选中的是道具
        local num = UIDynamicData.daojuInfo[selfPlayerID][nowDaojuClickedIndex+1][5]
        local msg = {}
        msg.cmd = "chushoudaoju"
        msg.num = num
        LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg))

        if UIDynamicData.daojuInfo[selfPlayerID][nowDaojuClickedIndex+1][3] == 1 then
            nowDaojuClickedIndex = -1
        end

        formSelf:GetWidgetProxyByName("Cloned_List(1088)(1095)"):SetElementAmount(6)
    end
    refreshEquipInfo(selfPlayerID)
end


function useDaojuBtnClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    if nowDaojuClickedIndex ~= -1 then
        shiyongdaojuClicked(LuaUIEvent)
    end
    refreshEquipInfo(selfPlayerID)
end

function diuQiEquipBtnClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID

    if nowEquipClickedIndex ~= -1 then
        local msg = {}
        msg.cmd = "dropEquip"
        msg.index = nowEquipClickedIndex
        LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg))
        nowEquipClickedIndex = -1
    end

    if nowDaojuClickedIndex ~= -1 then
        local daojuName = UIDynamicData.daojuInfo[selfPlayerID][nowDaojuClickedIndex+1][1]

        local msg = {}
        local num
        msg.cmd = "dropSkillBook"
        if daojuName == "D级被动技能书" then
            num = 1
            msg.sT = 0 -- skillType（是否主被动） 1是主动
        elseif daojuName == "D级主动技能书" then
            num = 5
            msg.sT = 1 -- skillType（是否主被动） 1是主动
        elseif daojuName == "C级被动技能书" then
            num = 2
            msg.sT = 0 -- skillType（是否主被动） 1是主动
        elseif daojuName == "C级主动技能书" then
            num = 6
            msg.sT = 1 -- skillType（是否主被动） 1是主动
        elseif daojuName == "B级被动技能书" then
            num = 3
            msg.sT = 0 -- skillType（是否主被动） 1是主动
        elseif daojuName == "B级主动技能书" then
            num = 7
            msg.sT = 1 -- skillType（是否主被动） 1是主动
        elseif daojuName == "A级被动技能书" then
            num = 4
            msg.sT = 0 -- skillType（是否主被动） 1是主动
        elseif daojuName == "A级主动技能书" then
            num = 8
            msg.sT = 1 -- skillType（是否主被动） 1是主动
        end
        msg.num = num
        LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg))

        if UIDynamicData.daojuInfo[selfPlayerID][nowDaojuClickedIndex+1][3] == 1 then
            nowDaojuClickedIndex = -1
        end
    end
    refreshEquipInfo(selfPlayerID)
end





function equipClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    formSelf:GetWidgetProxyByName("List(1088)"):SetElementAmount(6)
    local index = LuaUIEvent.SrcWidget:GetIndexInBelongedList()
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID

    local actorID = LuaCallCs_Battle.GetHostActorID();
    local cellInfo = LuaCallCs_Equip.GetEquipCellInfo(actorID, 0, index)
    if cellInfo.m_equipID == 0 then
        nowEquipClickedIndex = -1
        nowDaojuClickedIndex = -1
        refreshEquipInfo(selfPlayerID)
        return
    end


    nowEquipClickedIndex = index
    nowDaojuClickedIndex = -1
    refreshEquipInfo(selfPlayerID)
end



function chushouEquip(LuaUIEvent)
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID

    if nowEquipClickedIndex ~= -1 then
        local msg = {}
        msg.cmd = "sellEquip"
        msg.index = nowEquipClickedIndex
        LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg))
    end
    nowEquipClickedIndex = -1
    refreshEquipInfo(selfPlayerID)
end










function equipHoldStart(LuaUIEvent)
    local index = LuaUIEvent.SrcWidget:GetIndexInBelongedList();
end







function equipOnEnable(LuaUIEvent)
    if isbeibaoShow == false then
        return
    end

    local elem = LuaUIEvent.SrcWidget
    if elem:GetWidgetProxyByName("zbImg") then
        local index = elem:GetIndexInBelongedList()
        local actorID = LuaCallCs_Battle.GetHostActorID();
        local CellInfo = LuaCallCs_Equip.GetEquipCellInfo(actorID, 0, index)
        if CellInfo then
            if CellInfo.m_equipID ~= 0 then
                local iconPath = LuaCallCs_Equip.GetEquipInfo(CellInfo.m_equipID).m_equipIconPath
                elem:GetWidgetProxyByName("zbImg"):GetImage():SetRes(iconPath)
                if nowEquipClickedIndex == index then
                    elem:GetWidgetProxyByName("zbkuang"):GetImage():SetRes("Texture/Sprite/kuang.sprite")
                else
                    elem:GetWidgetProxyByName("zbkuang"):GetImage():SetRes("Texture/Sprite/kuangHui.sprite")
                end
                elem:GetWidgetProxyByName("zbkuang"):SetActive(true)
            else
                elem:GetWidgetProxyByName("zbImg"):GetImage():SetRes("Texture/Sprite/kongcao.sprite")
                elem:GetWidgetProxyByName("zbkuang"):SetActive(false)
            end
        end

        elem:GetWidgetProxyByName("zbImg"):SetOnClickedEvent("equipClicked", {})
    end
end




function equipImgClicked(LuaUIEvent)
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID

    local index = LuaUIEvent.SrcWidget:GetIndexInBelongedList();
    if checkEquipIndex == -1 then
        checkEquipIndex = index
    elseif index ~= checkEquipIndex then
        checkEquipIndex = index
    end
    refreshEquipInfo(selfPlayerID)
end



function bgClickEnd(LuaUIEvent)
    local shuxingShow = formSelf:GetWidgetProxyByName("Image(1020)")
    LuaCallCs_Tween.ScaleLocal(shuxingShow, 1, 1, 1)
end


function ChangeMonsterCountText(selfPlayerID, isAdd)
    if isAdd == true then
        nowCount = nowCount + 1
    else
        if nowCount > 0 then
            nowCount = nowCount - 1
        end
    end

    if nowCount > #LuaCallCs_UGCStateDriver.GetAllPlayerInfos()*UIConfigData.loseCount then
        monsterText:SetContent("<color=#FF0000>" .. nowCount .. "</color>/" .. #LuaCallCs_UGCStateDriver.GetAllPlayerInfos()*UIConfigData.loseCount)
    else
        monsterText:SetContent(nowCount .. "/" .. #LuaCallCs_UGCStateDriver.GetAllPlayerInfos()*UIConfigData.loseCount)
    end
end


function sendReportData(selfPid)
    local jsonTable = {
        -- {
        --     ['playName'] = "输诚一",
        --     ['heroicon'] = "xxxxxxaaaaa.png",
        --     ['killCount'] = 1111,
        --     ['boci'] = 59,
        --     ['equipiconPathInfo'] = {"XXSSADA.png", nil, "XXSSADA.png"},
        --     ['skillImg1'] = "XXSSADA.png",
        --     ['skillImg2'] = "XXSSADA.png",
        -- },
    }

    local playerArr = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()

    for i = 1, #playerArr do
        local playerInfo = {}
        local playerId = playerArr[i].playerID
        if playerId == selfPid then
            local actorIDList = LuaCallCs_Battle.GetActorIDListByPlayerID(playerId)
            local actorID = actorIDList[1]
            local playName = playerArr[i].playerName
            local heroInfo = LuaCallCs_Data.GetHeroInfoByActorID(actorID)
            local heroID = heroInfo.cfgID
            local killCount = G_GameDataMgr.PersistentData[playerId].SaasData.kill
            local boci = UIDynamicData.boci

            local zbList = {}

            for k = 1, 3 do
                if UIDynamicData.tianfuInfo[playerId][k] then
                    table.insert(zbList, UIDynamicData.tianfuInfo[playerId][k])
                else
                    table.insert(zbList, nil)
                end
            end

            local s1 = nil
            local s2 = nil


            local skill1 = LuaCallCs_Skill.GetSkillSlot(actorID, 8)
            if skill1 ~= nil then
                s1 = skill1.curSkillID
            end

            local skill2 = LuaCallCs_Skill.GetSkillSlot(actorID, 6)
            if skill2 ~= nil then
                s2 = skill2.curSkillID
            end

            playerInfo["M"] = playName
            playerInfo["HID"] = heroID
            playerInfo["K"] = killCount
            playerInfo["B"] = boci
            playerInfo["zb"] = zbList
            playerInfo["s1"] = s1
            playerInfo["s2"] = s2
            playerInfo["W"] = UIDynamicData.IsWin
            playerInfo["N"] = UIDynamicData.nowNanDu
            playerInfo["T"] = UIDynamicData.gameStartTimeInfo[playerId]

            table.insert(jsonTable, playerInfo)
        end
    end
    return jsonTable
end



function sendFixedFormatData()
    local allPlayerInfos = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()
    for k=1, #allPlayerInfos do
        local PID = allPlayerInfos[k].playerID
        if UIDynamicData.nowNanDu >= 6 then
            G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[4] = G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[4] + 10
            UIDynamicData.addJiFenNum[PID] = UIDynamicData.addJiFenNum[PID] + 10
        elseif UIDynamicData.nowNanDu == 5 then
            G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[4] = G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[4] + 9
            UIDynamicData.addJiFenNum[PID] = UIDynamicData.addJiFenNum[PID] + 9
        elseif UIDynamicData.nowNanDu == 4 then
            G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[4] = G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[4] + 8
            UIDynamicData.addJiFenNum[PID] = UIDynamicData.addJiFenNum[PID] + 8
        elseif UIDynamicData.nowNanDu == 3 then
            G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[4] = G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[4] + 7
            UIDynamicData.addJiFenNum[PID] = UIDynamicData.addJiFenNum[PID] + 7
        elseif UIDynamicData.nowNanDu == 2 then
            G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[4] = G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[4] + 6
            UIDynamicData.addJiFenNum[PID] = UIDynamicData.addJiFenNum[PID] + 6
        elseif UIDynamicData.nowNanDu == 1 then
            G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[4] = G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[4] + 5
            UIDynamicData.addJiFenNum[PID] = UIDynamicData.addJiFenNum[PID] + 5
        end

        if UIDynamicData.deadCntInfo[PID] == 0 then
            G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[4] = G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[4] + 1
            UIDynamicData.addJiFenNum[PID] = UIDynamicData.addJiFenNum[PID] + 1
        end
    end



    local mvpPid = UIDynamicData.shangHaiPaiXuList[1][1]

    G_GameDataMgr.PersistentData[mvpPid].CustomizeDataIntArr[4] = G_GameDataMgr.PersistentData[mvpPid].CustomizeDataIntArr[4] + 2
    UIDynamicData.addJiFenNum[mvpPid]  = UIDynamicData.addJiFenNum[mvpPid] + 2


    if #LuaCallCs_UGCStateDriver.GetAllPlayerInfos() == 4 then
        local maxkillCnt = 0
        local maxExpendMoney = 0
        local killMaxPidList = {}
        local maxExpendMoneyPidList = {}
        for i=1,#LuaCallCs_UGCStateDriver.GetAllPlayerInfos() do
            local pid = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()[i].playerID

            local killcnt = G_GameDataMgr.PersistentData[pid].SaasData.kill
            if killcnt > maxkillCnt then
                killMaxPidList = {}
                table.insert(killMaxPidList, pid)
            else
                if killcnt ==  maxkillCnt then
                    table.insert(killMaxPidList, pid)
                end
            end

        end

        -- 杀敌最多
        for i=1,#killMaxPidList do
            local pid = killMaxPidList[i]
            G_GameDataMgr.PersistentData[pid].CustomizeDataIntArr[4] = G_GameDataMgr.PersistentData[pid].CustomizeDataIntArr[4] + 1
            UIDynamicData.addJiFenNum[pid]  = UIDynamicData.addJiFenNum[pid] + 1
        end

    end
end


function addGameCnt()
    local allPlayerInfos = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()
    local mvpPid = UIDynamicData.shangHaiPaiXuList[1][1]

    -- 加mvp数
    UIDynamicData.fightDataInfo[mvpPid][1][3] = UIDynamicData.fightDataInfo[mvpPid][1][3] + 1
    UIDynamicData.fightDataInfo[mvpPid][UIDynamicData.nowNanDu+1][3] = UIDynamicData.fightDataInfo[mvpPid][UIDynamicData.nowNanDu+1][3] + 1


    for i=1, #allPlayerInfos do
        local PID = allPlayerInfos[i].playerID
        -- 加总场次数
        G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[1] = G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[1] + 1
        UIDynamicData.fightDataInfo[PID][1][1] = UIDynamicData.fightDataInfo[PID][1][1] + 1
        UIDynamicData.fightDataInfo[PID][UIDynamicData.nowNanDu+1][1] = UIDynamicData.fightDataInfo[PID][UIDynamicData.nowNanDu+1][1] + 1

        -- 加通关场次数
        if UIDynamicData.IsWin == true then
            -- 加胜利场次数
            G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[2] = G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[2] + 1
            UIDynamicData.fightDataInfo[PID][1][2] = UIDynamicData.fightDataInfo[PID][1][2] + 1
            UIDynamicData.fightDataInfo[PID][UIDynamicData.nowNanDu+1][2] = UIDynamicData.fightDataInfo[PID][UIDynamicData.nowNanDu+1][2] + 1
            -- 设定player的难度权限
            if UIDynamicData.nowNanDu > G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[3] then
                G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[3] = UIDynamicData.nowNanDu
            end
        end

        -- 加最高伤害值
        local shanghainum = UIDynamicData.hurtTotalInfo[PID]
        if shanghainum > UIDynamicData.fightDataInfo[PID][UIDynamicData.nowNanDu+1][4] then
            UIDynamicData.fightDataInfo[PID][UIDynamicData.nowNanDu+1][4] = shanghainum
            if shanghainum > UIDynamicData.fightDataInfo[PID][1][4] then
                UIDynamicData.fightDataInfo[PID][1][4] = shanghainum
            end
        end

        printTb("UIDynamicData.fightDataInfo[PID]")
        printTb(UIDynamicData.fightDataInfo[PID])
        G_GameDataMgr.PersistentData[PID].CustomizeDataStringArr[2] = json.encode(UIDynamicData.fightDataInfo[PID])
    end
end



function addHeroExFunc()
	local allPlayerInfos = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()
    for k=1, #allPlayerInfos do
        local PID = allPlayerInfos[k].playerID
        printTb("json.encode(UIDynamicData.sldTb[PID])")
        printTb(json.encode(UIDynamicData.sldTb[PID]))

        local actorIDList = LuaCallCs_Battle.GetActorIDListByPlayerID(PID)
        local actorID = actorIDList[1]

        local heroCfgID = LuaCallCs_Data.GetHeroInfoByActorID(actorID).cfgID
        local heroCfgIDString = tostring(heroCfgID)

        if UIDynamicData.boci >= 0 then
            UIDynamicData.sldTb[PID][heroCfgIDString] = UIDynamicData.sldTb[PID][heroCfgIDString] + math.ceil(UIConfigData.heroGetExLevelInfo[UIDynamicData.nowNanDu][1]*0.3)

            -- 通用经验
            G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[7] = G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[7] + math.ceil(UIConfigData.heroGetExLevelInfo[UIDynamicData.nowNanDu][2]*0.3)
            -- G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[7] = G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[7] + 11111



            UIDynamicData.addHreoSLDNum[PID] = UIDynamicData.addHreoSLDNum[PID] + math.ceil(UIConfigData.heroGetExLevelInfo[UIDynamicData.nowNanDu][1]*0.3)
            UIDynamicData.tyjyNum[PID] = UIDynamicData.tyjyNum[PID] + math.ceil(UIConfigData.heroGetExLevelInfo[UIDynamicData.nowNanDu][2]*0.3)
        end

        if UIDynamicData.IsWin == true then
            UIDynamicData.sldTb[PID][heroCfgIDString] = UIDynamicData.sldTb[PID][heroCfgIDString] + math.ceil(UIConfigData.heroGetExLevelInfo[UIDynamicData.nowNanDu][1]*0.7)

            G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[7] = G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[7] + math.ceil(UIConfigData.heroGetExLevelInfo[UIDynamicData.nowNanDu][2]*0.7)
            UIDynamicData.addHreoSLDNum[PID] = UIDynamicData.addHreoSLDNum[PID] + math.ceil(UIConfigData.heroGetExLevelInfo[UIDynamicData.nowNanDu][1]*0.7)
            UIDynamicData.tyjyNum[PID] = UIDynamicData.tyjyNum[PID] + math.ceil(UIConfigData.heroGetExLevelInfo[UIDynamicData.nowNanDu][2]*0.7)
        end


        G_GameDataMgr.PersistentData[PID].CustomizeDataStringArr[1] = json.encode(UIDynamicData.sldTb[PID])
    end
end



function saveGameTime()
    local gameEndTime = os.time()
    local allPlayerInfos = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()
    printTb("金币数： ")
    for k=1, #allPlayerInfos do
        local PID = allPlayerInfos[k].playerID

        local gameTimeNum = gameEndTime - UIDynamicData.gameStartTimeInfo[PID]
        local fen_int = math.ceil(gameTimeNum/60)
        G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[8] = G_GameDataMgr.PersistentData[PID].CustomizeDataIntArr[8] + fen_int
    end
end


function saveReportData()
    local allPlayerInfos = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()
    for k=1, #allPlayerInfos do
        local PID = allPlayerInfos[k].playerID
        local reportData = sendReportData(PID)
        G_GameDataMgr.PersistentData[PID].ReportData = json.encode(reportData)
    end
end

function saasFunc(PID)
    if UIDynamicData.IsWin == true then
        G_GameDataMgr.PersistentData[PID].SaasData.winCnt = 1 -- SaasData.winCnt 胜利场次
        if #LuaCallCs_UGCStateDriver.GetAllPlayerInfos() > 1 then
            G_GameDataMgr.PersistentData[PID].SaasData.zuDuiWinCnt = 1
        end
    end

    local mvpPid = UIDynamicData.shangHaiPaiXuList[1][1]
    if PID == mvpPid then
        G_GameDataMgr.PersistentData[PID].SaasData.mvpCnt = 1
    end
end






local nandujiejiInfo = {
    [1] = {101, },
    [2] = {101, 2.5},
    [3] = {101, 5},
    [4] = {101, 7.5},
    [5] = {101, 8.5, 1},
    [6] = {101, 12, 2},
    [7] = {101, 15.5, 3},
    [8] = {101, 20, 5},
    [9] = {101, 21, 6, 1},
    [10] = {101, 37, 9.5, 2},
    [11] = {101, 33, 13, 3},
    [12] = {101, 39, 16.5, 4},
    [13] = {101, 45, 20, 5},
    [14] = {101, 46, 21, 6, 1},
    [15] = {101, 48, 23, 8, 2},
    [16] = {101, 50, 25, 10, 3},
    [17] = {101, 52, 27, 12, 4},
    [18] = {101, 54, 29, 14, 5},
}



function makeJieJi()
    local nandu = UIDynamicData.nowNanDu
    local jieji = 1

    local num = math.random(1, 100)
    local num1 = math.random()
    num = num + num1

    for i=1,#nandujiejiInfo[nandu] do
        if num <= nandujiejiInfo[nandu][i] then
            jieji = i
        else
            break
        end
    end

    printTb("难度： " .. nandu)
    printTb("num： " .. num)

    return jieji
end

function makeShuXing(t1)
    local num = 1
    if t1 == 1 then
        num = math.random(1, 6)
    elseif t1 == 2 then
        num = math.random(1, 6)
    elseif t1 == 3 then
        num = math.random(1, 4)
    elseif t1 == 4 then
        num = math.random(1, 4)
    end
    return num
end


function makeShuXingZhi(t1, t2, t4)
    local tb = UIConfigData.baoShiInfo[t1]
    local tb1 = tb[t4]
    local tb2 = tb1[t2]
    local minNum = tb2[1]
    local maxNum = tb2[2]

    local nDecimal = 10 ^ 2
    local shuxingzhi = math.random(math.floor(minNum), math.floor(maxNum))
    print(shuxingzhi)

    local shuxingzhixiaoshu
    shuxingzhixiaoshu = math.floor(math.random() * nDecimal)/100
    print(shuxingzhixiaoshu)

    if shuxingzhi == math.floor(minNum) then
        if shuxingzhixiaoshu < math.floor((minNum-math.floor(minNum)) * nDecimal)/100 then
            print("shuxingzhixiaoshu1")
            print(shuxingzhixiaoshu)
            shuxingzhixiaoshu = math.floor((minNum-math.floor(minNum)) * nDecimal)/100
        end
    elseif shuxingzhi == math.floor(maxNum) then
        if shuxingzhixiaoshu > math.floor((maxNum-math.floor(maxNum)) * nDecimal)/100 then
            print("shuxingzhixiaoshu2")
            print(shuxingzhixiaoshu)
            shuxingzhixiaoshu = math.floor((maxNum-math.floor(maxNum)) * nDecimal)/100
        end
    end

    shuxingzhi = shuxingzhi + shuxingzhixiaoshu
    print(shuxingzhi)

    return shuxingzhi
end


function makeBaoShiFunc()
    local boci = UIDynamicData.boci
    local allPlayerInfos = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()
    for k=1, #allPlayerInfos do
        local PID = allPlayerInfos[k].playerID
        if boci >= 40 then
            for i=1, 4 do
                local t1 = math.random(1, 4) -- color

                local t2 = makeJieJi() -- jieji

                local t3 = 1 -- dengji

                local t4 =  makeShuXing(t1) -- shuxing

                local t5 = makeShuXingZhi(t1, t2, t4)-- shuxingzhi

                table.insert(UIDynamicData.giveBaoShiInfo[PID], {t1, t2, t3, t4, t5})
                table.insert(UIDynamicData.baoShiCangKuInfo[PID], {t1, t2, t3, t4, t5})

                -- printTb("{t1, t2, t3, t4, t5}")
                -- printTb({t1, t2, t3, t4, t5})
            end
        elseif boci >= 30 then
            for i=1, 3 do
                local t1 = math.random(1, 3) -- color

                local t2 = makeJieJi() -- jieji

                local t3 = 1 -- dengji

                local t4 =  makeShuXing(t1) -- shuxing

                local t5 = makeShuXingZhi(t1, t2, t4)-- shuxingzhi

                table.insert(UIDynamicData.giveBaoShiInfo[PID], {t1, t2, t3, t4, t5})
                table.insert(UIDynamicData.baoShiCangKuInfo[PID], {t1, t2, t3, t4, t5})
            end
        elseif boci >= 20 then
            for i=1, 2 do
                local t1 = math.random(1, 2) -- color

                local t2 = makeJieJi() -- jieji

                local t3 = 1 -- dengji

                local t4 =  makeShuXing(t1) -- shuxing

                local t5 = makeShuXingZhi(t1, t2, t4)-- shuxingzhi

                table.insert(UIDynamicData.giveBaoShiInfo[PID], {t1, t2, t3, t4, t5})
                table.insert(UIDynamicData.baoShiCangKuInfo[PID], {t1, t2, t3, t4, t5})
            end
        elseif boci >= 10 then
            for i=1, 1 do
                local t1 = math.random(1, 1) -- color

                local t2 = makeJieJi() -- jieji

                local t3 = 1 -- dengji

                local t4 =  makeShuXing(t1) -- shuxing

                local t5 = makeShuXingZhi(t1, t2, t4)-- shuxingzhi

                table.insert(UIDynamicData.giveBaoShiInfo[PID], {t1, t2, t3, t4, t5})
                table.insert(UIDynamicData.baoShiCangKuInfo[PID], {t1, t2, t3, t4, t5})
            end
        end


        G_GameDataMgr.PersistentData[PID].CustomizeDataStringArr[4] = json.encode(UIDynamicData.baoShiCangKuInfo[PID])
    end
end



function GameOver(PID) -- 要不要参数
    UIDynamicData.GameOver[PID] = true
    -- LuaCallCs_FightUI.Destroy3DUI(UIDynamicData.UIObject)
    local index = 1
    for playerID, allShangHai in pairs(UIDynamicData.hurtTotalInfo) do
        UIDynamicData.shangHaiPaiXuList[index] = {playerID, allShangHai}
        index = index + 1
    end

    table.sort(UIDynamicData.shangHaiPaiXuList, function (a,b)
        return (a[2] > b[2])
    end)

    if UIDynamicData.IsWin == true then
        LuaCallCs_GameFinish.ShowWinOrLose(true)
    else
        LuaCallCs_GameFinish.ShowWinOrLose(false)
    end

    saasFunc(PID) -- 存储saas数据

    saveGameTime() -- 为4个人存储游戏时长

    saveReportData() -- 为4个人存储历史战绩

    addGameCnt() -- 为4个人存储场次数

    sendFixedFormatData() -- 加积分

    addHeroExFunc()-- 加英雄熟练度

    makeBaoShiFunc()


    --发送指定玩家离开游戏申请, 在蓝图startup.gl中会接收事件并处理下一步
    if PID == LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
        -- LuaCallCs_FightUI.Destroy3DUI(UIDynamicData.UIObject)
        LuaCallCs_UGCStateDriver.RequestQuitBattle(PID)
        printTb(PID .. "离开游戏了")
    end

    endTimer:SetTotalTime(5)
    endTimer:ReStartTimer()

    formSelf:GetWidgetProxyByName("EndImg"):SetActive(true)

    LuaCallCs_Common.Log("run in gameover end")
end


function checkGameEnd(selfPlayerID)
    if nowCount > #LuaCallCs_UGCStateDriver.GetAllPlayerInfos()*UIConfigData.loseCount then
        GameOver(selfPlayerID)
    end
end


function openEndReport(LuaUIEvent)
    endTimer:EndTimer()
    LuaCallCs_UI.OpenForm("UI/OnlineMode/EndReport.uixml")
end

function endTimeDone()
    LuaCallCs_UI.OpenForm("UI/OnlineMode/EndReport.uixml")
end


function killBossEnough(pid)
    if UIDynamicData.timeDone == true then
        return
    end
    l_slider_time:PauseTimer()
    UIDynamicData.IsWin = true
    addGMGongGaoInfo(pid, "sl")
    GameOver(pid)
end




function Refreshboci(playerID, boci)
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    if UIDynamicData.itsOKInfo[selfPlayerID] ~= true then
        return
    end

    if playerID ~= selfPlayerID then
        return
    end

    UIDynamicData.boci = boci
    formSelf:GetWidgetProxyByName("jifenbanbociText"):GetText():SetContent(UIDynamicData.boci .. "/" .. UIConfigData.refreshMonsterLineCount)
    if boci >= 60 then
        formSelf:GetWidgetProxyByName("jifenbanbociText"):GetText():SetContent("60/60")
    end
end


function GameEnd(LuaUIEvent)
    shezhiPanel:SetActive(false)
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    GameOver(selfPlayerID)
end



function checkMonsterCountSafe(playerID)
    if nowCount > #LuaCallCs_UGCStateDriver.GetAllPlayerInfos()*UIConfigData.loseCount then
        -- 发公告
        l_slider_time:GetDisplayText():SetColor(BluePrint.UGC.UI.Core.Color.Red);
        addGMGongGaoInfo(playerID, "csx")
    end
    l_slider_time:GetDisplayText():SetColor(BluePrint.UGC.UI.Core.Color.White);
end






function revolveCamera(LuaUIEvent)
    local msg = {
        cmd = "revolveCamera"
    }
    LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg))
end



-- 兵线间隔计时
function time_change(LuaUIEvent)
    local time = l_slider_time:GetCurrentTime()
    local pid = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID

    local numTime = math.floor(time)
    if numTime <= 10 then
        addGMGongGaoInfo(pid, "bossKilldjs", numTime)
    end
end


-- 总时间计时
function mainTimeChanged(LuaUIEvent)
    -- local time = mainTimer:GetCurrentTime()
    -- UIDynamicData.time = time
end


function SetDaojishi(playerID)
    if playerID ~= LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
        return
    end
    l_slider_time:SetTotalTime(UIConfigData.refreshMonsterLineTime/1000)
    l_slider_time:ReStartTimer()
end



function bigBossDaoJiShiTimeEnd(pid)
    UIDynamicData.timeDone = true
    UIDynamicData.IsWin = false
    GameOver(LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID)
end




function setBigBossDaojishi(playerID)
    if playerID ~= LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
        return
    end

    l_slider_time:SetTotalTime(120)
    l_slider_time:SetOnTimeupEvent("bigBossDaoJiShiTimeEnd", {})
    l_slider_time:SetOnTimeChangedEvent("time_change", {})
    l_slider_time:StartTimer()
end


function addBigBossCount(playerID)
    UIDynamicData.bigBossNeedKillCount = UIDynamicData.bigBossNeedKillCount + 1
end





-- function time_end(LuaUIEvent)
--     -- 游戏结束逻辑

--     local PID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
--     --发送指定玩家离开游戏申请, 在蓝图startup.gl中会接收事件并处理下一步
--     LuaCallCs_UGCStateDriver.RequestQuitBattle(PID)
-- end





function OnInnerChartBtnClick(LuaUIEvent)
    LuaCallCs_InnerSystem.ShowInnerBattleChatInput()
end

function yidong()
    -- local tagdian2DPos = formSelf:GetWidgetProxyByName("Button(1117)"):GetScreenPosition() -- 中间点的屏幕坐标
    -- local tagdian3DPos = LuaCallCs_UI.ScreenToWorldPos(tagdian2DPos, 0);


    -- local UIObject = LuaCallCs_FightUI.Create3DUI("Prefab_Skill_Effects/tongyong_effects/tongyong_hurt/Hunqiu_01.prefab", 0, 0)
    -- -- Prefab_Skill_Effects/tongyong_effects/tongyong_hurt/Hunqiu_01.prefab
    -- LuaCallCs_FightUI.Set3DUIPos(UIObject, tagdian3DPos)

    -- zzz:SetActive(true)
    -- local pos2D = formSelf:GetWidgetProxyByName("beibaoBtn"):GetScreenPosition()
    -- local pos3D = LuaCallCs_UI.ScreenToWorldPos(pos2D, 0);
    -- LuaCallCs_Tween.MoveToScreenPosition(zzz, pos.x+100, pos.y, 1)

end

function inTimeSkill(killBossPlayerID, iswin)
    UIDynamicData.bigBossKillCount = UIDynamicData.bigBossKillCount + 1
end



function createNewTable(tab, equipID)
    local newTable = {}
    for _, v in pairs(tab) do
        if v ~= equipID then
            table.insert(newTable, v)
        end
    end
    return newTable
end



function addCZChuiCnt(PlayerID)
    if PlayerID ~= LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
        return
    end
    UIDynamicData.CZChuiCnt[PlayerID] = UIDynamicData.CZChuiCnt[PlayerID] + 1
end



function czBtnClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    if nowEquipClickedIndex == -1 then
        return
    end

    local actorID = LuaCallCs_Battle.GetHostActorID();
    local Cellinfo = LuaCallCs_Equip.GetEquipCellInfo(actorID, 0, nowEquipClickedIndex) -- 第三个参数索引从0开始
    local equipID = Cellinfo.m_equipID
    local equipLv = UIConfigData.equipImgInfo[equipID][2]


    if equipLv then
        local newTab
        if equipLv == 1 then
            newTab = createNewTable(UIConfigData.oneLvEquipCZInfo, equipID)
        elseif equipLv == 2 then
            newTab = createNewTable(UIConfigData.twoLvEquipCZInfo, equipID)
        elseif equipLv == 3 then
            newTab = createNewTable(UIConfigData.threeLvEquipCZInfo, equipID)
        elseif equipLv == 4 then
            newTab = createNewTable(UIConfigData.fourLvEquipCZInfo, equipID)
        end
        local newEquipID = newTab[math.random(1,#newTab)]

        local msg = {}
        msg.cmd = "czEquip"
        msg.index = nowEquipClickedIndex
        msg.newEquipID = newEquipID
        LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg))
    else
        return
    end
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    refreshEquipInfo(selfPlayerID)
end


local shaubingInfo = {
    {"近战小兵", 162101},
    {"远程小兵", 162102},
    {"金币怪", 1032601},
    {"金矿怪", 52005},
    {"深渊魔王", 162502},
    {"武器怪", 162103},
    {"护甲怪", 52002},
    {"精英怪1", 162113},
    {"精英怪2", 162111},
    {"精英怪3", 162402},
    {"精英怪4", 162401},
    {"首领怪1", 162108},
    {"首领怪2", 162301},
    {"首领怪3", 162106},
    {"最终boss", 161802},
    -- 射线
}



function shuabinOnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()
    elem:GetWidgetProxyByName("Button(1192)"):GetText():SetContent(shaubingInfo[index+1][1])
end


function shuabingClicked(LuaUIEvent)
    local shuabinpanel = formSelf:GetWidgetProxyByName("Panel(1193)")

    if shuabinpanel:IsActived() == false then
        shuabinpanel:SetActive(true)
        formSelf:GetWidgetProxyByName("List(1190)"):SetElementAmount(#shaubingInfo)
    else
        shuabinpanel:SetActive(false)
    end
end


function shuabinBtnClicked(LuaUIEvent)
    local index = formSelf:GetWidgetProxyByName("List(1190)"):GetSelectedIndex()


    local msg = {}
    msg.cmd = "callMonster"
    msg.cfg = shaubingInfo[index+1][2]
    LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg))

    LuaCallCs_FightUI.CreateFloatText(index, "-10000", 6, LuaCallCs_Battle.GetHostActorWorldPos(), true, 0, 3000, -0.3)

end


function shanghaiOnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()
    local pid = UIDynamicData.shangHaiPaiXuList[index+1][1]
    local num = UIDynamicData.shangHaiPaiXuList[index+1][2]
    local allNum = UIDynamicData.shangHaiPaiXuList[1][2]

    local actorIDList = LuaCallCs_Battle.GetActorIDListByPlayerID(pid)
    local actorID = actorIDList[1]
    local heroInfo = LuaCallCs_Data.GetHeroInfoByActorID(actorID)
    local icon = "UGUI/Sprite/Dynamic/BustCircle/" .. heroInfo.imagePath

    elem:GetWidgetProxyByName("Image(1202)"):GetImage():SetRes(icon)

    elem:GetWidgetProxyByName("TextLabel(1206)"):GetText():SetContent(index+1)

    local playerName = LuaCallCs_Data.GetPlayerNameByActorId(actorID)
    elem:GetWidgetProxyByName("TextLabel(1204)"):GetText():SetContent(playerName)
    local shanghaiText
    if num > 10000 then
        shanghaiText = math.floor((num/10000)) .. "万"
    else
        shanghaiText = num
    end
    elem:GetWidgetProxyByName("TextLabel(1205)"):GetText():SetContent(shanghaiText)

    elem:GetWidgetProxyByName("ProgressBar(1203)"):SetProgressValue(num/allNum)

    if pid == LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
        formSelf:GetWidgetProxyByName("TextLabel(1211)"):GetText():SetContent(index+1)
        formSelf:GetWidgetProxyByName("Image(1212)"):GetImage():SetRes(icon)
        formSelf:GetWidgetProxyByName("TextLabel(1213)"):GetText():SetContent(playerName)
        formSelf:GetWidgetProxyByName("TextLabel(1215)"):GetText():SetContent(shanghaiText)
        formSelf:GetWidgetProxyByName("ProgressBar(1209)"):SetProgressValue(num/allNum)
    end
end

function shuaxinshanghai()
    formSelf:GetWidgetProxyByName("List(1197)"):SetElementAmount(#UIDynamicData.shangHaiPaiXuList)
end

--[[
function shareBtnClicked(LuaUIEvent)
    local msg = {}
    msg.cmd = "setShareBtn"
    LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg))
end


function setShareBtn_callBack(pid, num)
    if pid ~= LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
        return
    end

    if num == 0 then
        shareBtn:GetText():SetContent("共享：关")
        formSelf:GetWidgetProxyByName("Toggle(1260)"):SetChecked(false)
    else
        shareBtn:GetText():SetContent("共享：开")
        formSelf:GetWidgetProxyByName("Toggle(1260)"):SetChecked(true)
    end
end
]]

function shanghaiBtnClicked(LuaUIEvent)
    if shanghaiList:IsActived() == false then
        shanghaiList:SetActive(true)
        formSelf:GetWidgetProxyByName("Panel(1210)"):SetActive(false)
        formSelf:GetWidgetProxyByName("TextLabel(1199)"):SetActive(true)
    else
        shanghaiList:SetActive(false)
        formSelf:GetWidgetProxyByName("Panel(1210)"):SetActive(true)
        formSelf:GetWidgetProxyByName("TextLabel(1199)"):SetActive(false)
    end
end


function jygShow(pid, actor)
    if pid == LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
        formSelf:GetWidgetProxyByName("Timer(1269)"):GetDisplayText():SetColor(BluePrint.UGC.UI.Core.Color.White)
        formSelf:GetWidgetProxyByName("Timer(1269)"):SetTotalTime(30)
        formSelf:GetWidgetProxyByName("Timer(1269)"):ReStartTimer()

        formSelf:GetWidgetProxyByName("Panel(1267)"):SetActive(true)
    end
end




function jygTimeEnd(LuaUIEvent)
    formSelf:GetWidgetProxyByName("Panel(1267)"):SetActive(false)
    formSelf:GetWidgetProxyByName("Timer(1269)"):GetDisplayText():SetColor(BluePrint.UGC.UI.Core.Color.White)
end




function jygTimeChanged(LuaUIEvent)
    local time = formSelf:GetWidgetProxyByName("Timer(1269)"):GetCurrentTime()

    local numTime = math.floor(time)
    if numTime <= 10 then
        formSelf:GetWidgetProxyByName("Timer(1269)"):GetDisplayText():SetColor(BluePrint.UGC.UI.Core.Color.Red)
    end
end



function XSJCNext1(LuaUIEvent)
    formSelf:GetWidgetProxyByName("MCXSJCPanel(1)"):SetActive(false)
    formSelf:GetWidgetProxyByName("MCXSJCPanel(2)"):SetActive(true)
end

function tiaoguo1()
    formSelf:GetWidgetProxyByName("MCXSJCPanel(1)"):SetActive(false)
end



function XSJCNext2(LuaUIEvent)
    formSelf:GetWidgetProxyByName("MCXSJCPanel(2)"):SetActive(false)
    formSelf:GetWidgetProxyByName("MCXSJCPanel(3)"):SetActive(true)
end

function tiaoguo2()
    formSelf:GetWidgetProxyByName("MCXSJCPanel(2)"):SetActive(false)
end


function XSJCNext3(LuaUIEvent)
    formSelf:GetWidgetProxyByName("MCXSJCPanel(3)"):SetActive(false)
    formSelf:GetWidgetProxyByName("MCXSJCPanel(4)"):SetActive(true)
end

function tiaoguo3()
    formSelf:GetWidgetProxyByName("MCXSJCPanel(3)"):SetActive(false)
end

function XSJCNext4(LuaUIEvent)
    formSelf:GetWidgetProxyByName("MCXSJCPanel(4)"):SetActive(false)
    formSelf:GetWidgetProxyByName("MCXSJCPanel(5)"):SetActive(true)
end


function tiaoguo4()
    formSelf:GetWidgetProxyByName("MCXSJCPanel(4)"):SetActive(false)
end

function XSJCNext5(LuaUIEvent)
    formSelf:GetWidgetProxyByName("MCXSJCPanel(5)"):SetActive(false)
    formSelf:GetWidgetProxyByName("MCXSJCPanel(6)"):SetActive(true)
end

function tiaoguo5()
    formSelf:GetWidgetProxyByName("MCXSJCPanel(5)"):SetActive(false)
end


function XSJCNext6(LuaUIEvent)
    formSelf:GetWidgetProxyByName("MCXSJCPanel(6)"):SetActive(false)

    shopXSJCShow()
end


function tiaoguo6()
    formSelf:GetWidgetProxyByName("MCXSJCPanel(6)"):SetActive(false)
end


function XSJCFunc(num)
    local btnName = "MCXSJCBtn(" .. num .. ")"
    local eventName = "XSJCNext" .. num
    local panelName = "MCXSJCPanel(" .. num .. ")"


    formSelf:GetWidgetProxyByName(btnName):SetOnClickedEvent(eventName, {})

    formSelf:GetWidgetProxyByName(panelName):SetActive(true)
end


function OnWinOrLoseClose()
    formSelf = nil
    monsterText = nil
    nowCount = 0
    l_slider_time = nil
    mainTimer = nil
    checkEquipIndex = -1
    equipTipsPanel = nil
    equipTipsText = nil
    isbeibaoShow = false
    beiBaoPanel = false
    nowEquipClickedIndex = -1
    nowDaojuClickedIndex = -1
    wupingNameText = nil
    wupingInfoText = nil
    zhuangbeiChoosePanel = nil
    beibaoBtn = nil
    endTimer = nil
    shiyongBtn = nil
    GMGongGaoList = nil
    shanghaiList = nil
    shezhiPanel = nil
    wfszPanel = nil
    hmszPanel = nil
    yxszPanel = nil
end
