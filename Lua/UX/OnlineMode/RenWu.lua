G_GameDataMgr = require"OnlineMode/G_GameDataMgr.lua"
json = require "json.lua"

local formSelf


local allRenWuZuTB
local renWuList
local renWuZuTBLen
local allRenWuZuTBUserData
local huoyueduNum
local hydjindutiao
local tyjy

local RiChangJiangLiInfo = {
    [6833] = {{"活", 5, "Texture/Sprite/90010.sprite", "活info"},{"积", 3, "Texture/Sprite/90916.sprite", "积info"}}, -- 获胜1局
    [6834] = {{"活", 10, "Texture/Sprite/90010.sprite", "活info"},{"积", 2, "Texture/Sprite/90916.sprite", "积info"}}, -- 参与防守1局
    [6835] = {{"活", 20, "Texture/Sprite/90010.sprite", "活info"},{"积", 4, "Texture/Sprite/90916.sprite", "积info"}}, -- 获胜2局
    [6836] = {{"活", 15, "Texture/Sprite/90010.sprite", "活info"},{"积", 3, "Texture/Sprite/90916.sprite", "积info"}}, -- 参与防守3局
    [6837] = {{"活", 20, "Texture/Sprite/90010.sprite", "活info"},{"积", 4, "Texture/Sprite/90916.sprite", "积info"}}, -- mvp1次
    [6838] = {{"活", 10, "Texture/Sprite/90010.sprite", "活info"},{"积", 2, "Texture/Sprite/90916.sprite", "积info"}}, -- 组队防守1局
    [6839] = {{"活", 15, "Texture/Sprite/90010.sprite", "活info"},{"积", 3, "Texture/Sprite/90916.sprite", "积info"}}, -- 组队胜利1局
    [6840] = {{"活", 10, "Texture/Sprite/90010.sprite", "活info"},{"积", 2, "Texture/Sprite/90916.sprite", "积info"}}, -- 粮草15次
    [6841] = {{"活", 15, "Texture/Sprite/90010.sprite", "活info"},{"积", 3, "Texture/Sprite/90916.sprite", "积info"}}, -- 魔袋8次
    [6842] = {{"活", 15, "Texture/Sprite/90010.sprite", "活info"},{"积", 3, "Texture/Sprite/90916.sprite", "积info"}}, -- 魔王10次

}


local rihuoJiangLiInfo = {
    {"通", 100, "imgPath", "活info", 0},
    {"通", 200, "imgPath", "活info", 0},
    {"通", 300, "imgPath", "活info", 0},
    {"通", 400, "imgPath", "活info", 0},
    {"通", 500, "imgPath", "活info", 0},
    {"通", 800, "imgPath", "活info", 0},
}

function refreshRiChangRenWuInfo()
    -- LuaCallCs_Task.RefreshTaskList()
    -- LuaCallCs_Task.ReqTaskInfo()
    allRenWuZuTBUserData = LuaCallCs_Task.GetAllTaskGroupListData()


    allRenWuZuTB = {}
    for i=1,#UIConfigData.renWuZuInfo do
        allRenWuZuTB[UIConfigData.renWuZuInfo[i]] = {}
    end

    printTb(#UIConfigData.renWuZuInfo)
    for i1=0, (#UIConfigData.renWuZuInfo-1) do
        local renWuZuTB = allRenWuZuTBUserData[i1]
	    local renWuZuTBName = renWuZuTB.m_name
        local renWuZuID = renWuZuTB.m_groupId

        renWuZuTBLen = renWuZuTB.m_taskNum -- 任务组长度

        local renWuInfo = renWuZuTB.outTaskList

        for k1=0, renWuZuTBLen-1 do
            local renWuName = renWuInfo[k1].m_name
            local renWuID = renWuInfo[k1].m_taskId

            local renWuTargetNum = renWuInfo[k1].m_target
            local renWuProgressNum = renWuInfo[k1].m_progress

            local renWuisFinished = renWuInfo[k1].m_isFinished
            local renWuisAwarded = renWuInfo[k1].m_isAwarded

            if renWuisFinished then
                if not renWuisAwarded then
                    table.insert(allRenWuZuTB[renWuZuTBName], {
                        renWuName,
                        renWuID,
                        renWuProgressNum,
                        renWuTargetNum,
                        renWuisFinished,
                        renWuisAwarded,
                        renWuZuID
                    })
                end
            end
        end

        for k1=0, renWuZuTBLen-1 do
            local renWuName = renWuInfo[k1].m_name
            local renWuID = renWuInfo[k1].m_taskId

            local renWuTargetNum = renWuInfo[k1].m_target
            local renWuProgressNum = renWuInfo[k1].m_progress

            local renWuisFinished = renWuInfo[k1].m_isFinished
            local renWuisAwarded = renWuInfo[k1].m_isAwarded

            if not renWuisFinished then
                table.insert(allRenWuZuTB[renWuZuTBName], {
                    renWuName,
                    renWuID,
                    renWuProgressNum,
                    renWuTargetNum,
                    renWuisFinished,
                    renWuisAwarded,
                    renWuZuID
                })
            end
        end

        for k1=0, renWuZuTBLen-1 do
            local renWuName = renWuInfo[k1].m_name
            local renWuID = renWuInfo[k1].m_taskId

            local renWuTargetNum = renWuInfo[k1].m_target
            local renWuProgressNum = renWuInfo[k1].m_progress

            local renWuisFinished = renWuInfo[k1].m_isFinished
            local renWuisAwarded = renWuInfo[k1].m_isAwarded

            if renWuisFinished then
                if renWuisAwarded then
                    table.insert(allRenWuZuTB[renWuZuTBName], {
                        renWuName,
                        renWuID,
                        renWuProgressNum,
                        renWuTargetNum,
                        renWuisFinished,
                        renWuisAwarded,
                        renWuZuID
                    })
                end
            end
        end
    end

    for i1=0, (#UIConfigData.renWuZuInfo-1) do
        local renWuZuTB = allRenWuZuTBUserData[i1]
	    local renWuZuTBName = renWuZuTB.m_name
        local renWuZuTBLen1 = renWuZuTB.m_taskNum -- 任务组长度
        if renWuZuTBName == "每日任务" then
            renWuList:SetElementAmount(renWuZuTBLen1)
            printTb("每日任务111")
        elseif renWuZuTBName == "每周任务" then
            printTb("每周任务111")
        end
    end

    printTb("allRenWuZuTB: 111")
    printTb(allRenWuZuTB)
end



function linJiangRefreshRenWuInfo(linJiangRenWuID)
    -- LuaCallCs_Task.RefreshTaskList()
    -- LuaCallCs_Task.ReqTaskInfo()

    for i1=0, (#UIConfigData.renWuZuInfo-1) do
        local renWuZuTB = allRenWuZuTBUserData[i1]
	    local renWuZuTBName = renWuZuTB.m_name
        local renWuZuTBLen1 = renWuZuTB.m_taskNum -- 任务组长度
        if renWuZuTBName == "每日任务" then

            for i=1, #allRenWuZuTB["每日任务"] do
                -- 每日任务
                if allRenWuZuTB["每日任务"][i][2] == linJiangRenWuID then
                    local t = table.remove(allRenWuZuTB["每日任务"], i)
                    t[6] = true
                    table.insert(allRenWuZuTB["每日任务"], t)
                    renWuList:SetElementAmount(renWuZuTBLen1)
                    return
                end
            end
        end
    end
end





local rihuoPanelNameInfo = {
    [1] = {"Button(1018)", "TextLabel(1035)", "Image(1036)", "Image(1052)"},
    [2] = {"Button(1019)", "TextLabel(1038)", "Image(1039)", "Image(1053)"},
    [3] = {"Button(1020)", "TextLabel(1041)", "Image(1042)", "Image(1054)"},
    [4] = {"Button(1021)", "TextLabel(1044)", "Image(1045)", "Image(1055)"},
    [5] = {"Button(1022)", "TextLabel(1047)", "Image(1048)", "Image(1056)"},
    [6] = {"Button(1023)", "TextLabel(1050)", "Image(1051)", "Image(1057)"},
}


function refreshrihuoRenWuInfo(num)
    for i=1, #rihuoJiangLiInfo do
        local textName = rihuoPanelNameInfo[i][2]
        local gouName = rihuoPanelNameInfo[i][3]
        local clickedName = rihuoPanelNameInfo[i][4]

        if num >= (20*i) then
            if rihuoJiangLiInfo[i][5] == 1 then
                formSelf:GetWidgetProxyByName(clickedName):SetActive(false)
                formSelf:GetWidgetProxyByName(textName):SetActive(false)
                formSelf:GetWidgetProxyByName(gouName):SetActive(true)
            else
                formSelf:GetWidgetProxyByName(clickedName):SetActive(true)
                formSelf:GetWidgetProxyByName(gouName):SetActive(false)

                formSelf:GetWidgetProxyByName(textName):GetText():SetContent(20*i)
                formSelf:GetWidgetProxyByName(textName):SetActive(true)
            end
        else
            formSelf:GetWidgetProxyByName(clickedName):SetActive(false)
            formSelf:GetWidgetProxyByName(gouName):SetActive(false)
            formSelf:GetWidgetProxyByName(textName):GetText():SetContent(20*i)
            formSelf:GetWidgetProxyByName(textName):SetActive(true)
        end




    end

	hydjindutiao:SetProgressValue(num/120)
end


function showJiangLi(index)
    local jiangliNum = rihuoJiangLiInfo[index][2]
    formSelf:GetWidgetProxyByName("TextLabel(1030)"):GetText():SetContent("通用经验*" .. jiangliNum)
    formSelf:GetWidgetProxyByName("Panel(1024)"):SetActive(true)
end


function rihuoClosed()
    formSelf:GetWidgetProxyByName("Panel(1024)"):SetActive(false)
end

function rihuoClicked1(LuaUIEvent)
    rihuoJiangLiInfo[1][5] = 1
    LuaCallCs_PersistentData.SetCustomizeDataIntArr({11}, {1})

    tyjy = tyjy + rihuoJiangLiInfo[1][2]
    LuaCallCs_PersistentData.SetCustomizeDataIntArr({6}, {tyjy})

    refreshrihuoRenWuInfo(huoyueduNum)

    showJiangLi(1)
end

function rihuoClicked2(LuaUIEvent)
    rihuoJiangLiInfo[2][5] = 1
    LuaCallCs_PersistentData.SetCustomizeDataIntArr({12}, {1})

    tyjy = tyjy + rihuoJiangLiInfo[2][2]
    LuaCallCs_PersistentData.SetCustomizeDataIntArr({6}, {tyjy})

    refreshrihuoRenWuInfo(huoyueduNum)
    showJiangLi(2)
end

function rihuoClicked3(LuaUIEvent)
    rihuoJiangLiInfo[3][5] = 1
    LuaCallCs_PersistentData.SetCustomizeDataIntArr({13}, {1})

    tyjy = tyjy + rihuoJiangLiInfo[3][2]
    LuaCallCs_PersistentData.SetCustomizeDataIntArr({6}, {tyjy})

    refreshrihuoRenWuInfo(huoyueduNum)
    showJiangLi(3)
end

function rihuoClicked4(LuaUIEvent)
    rihuoJiangLiInfo[4][5] = 1
    LuaCallCs_PersistentData.SetCustomizeDataIntArr({14}, {1})

    tyjy = tyjy + rihuoJiangLiInfo[4][2]
    LuaCallCs_PersistentData.SetCustomizeDataIntArr({6}, {tyjy})

    refreshrihuoRenWuInfo(huoyueduNum)
    showJiangLi(4)
end

function rihuoClicked5(LuaUIEvent)
    rihuoJiangLiInfo[5][5] = 1
    LuaCallCs_PersistentData.SetCustomizeDataIntArr({15}, {1})

    tyjy = tyjy + rihuoJiangLiInfo[5][2]
    LuaCallCs_PersistentData.SetCustomizeDataIntArr({6}, {tyjy})

    refreshrihuoRenWuInfo(huoyueduNum)
    showJiangLi(5)
end

function rihuoClicked6(LuaUIEvent)
    rihuoJiangLiInfo[6][5] = 1
    LuaCallCs_PersistentData.SetCustomizeDataIntArr({16}, {1})

    tyjy = tyjy + rihuoJiangLiInfo[6][2]
    LuaCallCs_PersistentData.SetCustomizeDataIntArr({6}, {tyjy})

    refreshrihuoRenWuInfo(huoyueduNum)
    showJiangLi(6)
end




function renWuFormOnOpen(LuaUIEvent)
    allRenWuZuTBUserData = nil
    huoyueduNum = nil
    tyjy = nil
    local intArr = LuaCallCs_PersistentData.GetCustomizeDataIntArr()
    LuaCallCs_Task.RefreshTaskList()
    math.randomseed(tostring(os.time()):reverse():sub(1,7))
    math.random()
    LuaCallCs_Task.ReqTaskInfo()

    huoyueduNum = intArr[11]
    rihuoJiangLiInfo[1][5] = intArr[12]
    rihuoJiangLiInfo[2][5] = intArr[13]
    rihuoJiangLiInfo[3][5] = intArr[14]
    rihuoJiangLiInfo[4][5] = intArr[15]
    rihuoJiangLiInfo[5][5] = intArr[16]
    rihuoJiangLiInfo[6][5] = intArr[17]

    tyjy = intArr[7]

    printTb("huoyueduNum")
    printTb(huoyueduNum)
    formSelf = LuaUIEvent.SrcForm
    renWuList = formSelf:GetWidgetProxyByName("List(1005)")
    hydjindutiao = formSelf:GetWidgetProxyByName("ProgressBar(1017)")
    formSelf:GetWidgetProxyByName("TextLabel(1058)"):GetText():SetContent(huoyueduNum)

    refreshRiChangRenWuInfo()
    refreshrihuoRenWuInfo(huoyueduNum)

end










function renWuClosed(LuaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    LuaCallCs_UI.CloseForm("UI/OnlineMode/RenWu.uixml")
end



-- renWuList
function renWuOnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()

    local tb = allRenWuZuTB["每日任务"][index+1]

    local renWuName = tb[1]
    local renWuID = tb[2]
    local renWuProgressNum = tb[3]
    local renWuTargetNum = tb[4]
    local renWuisFinished = tb[5]
    local renWuisAwarded = tb[6]

    elem:GetWidgetProxyByName("TextLabel(1007)"):GetText():SetContent(renWuName .. "  " .. renWuProgressNum .. "/" .. renWuTargetNum)

    elem:GetWidgetProxyByName("Image(1008)"):GetImage():SetRes(RiChangJiangLiInfo[renWuID][1][3])
    elem:GetWidgetProxyByName("Image(1011)"):GetImage():SetRes(RiChangJiangLiInfo[renWuID][2][3])

    elem:GetWidgetProxyByName("TextLabel(1009)"):GetText():SetContent(RiChangJiangLiInfo[renWuID][1][2])

    elem:GetWidgetProxyByName("TextLabel(1013)"):GetText():SetContent(RiChangJiangLiInfo[renWuID][2][2])

    if renWuisFinished then
        if renWuisAwarded then
            elem:GetWidgetProxyByName("Button(1014)"):EnableInput(false)
            elem:GetWidgetProxyByName("Button(1014)"):GetImage():SetRes("Texture/Sprite/toumingtupian.sprite")
            elem:GetWidgetProxyByName("Button(1014)"):GetText():SetContent("已完成")
        else
            elem:GetWidgetProxyByName("Button(1014)"):GetText():SetContent("领取")
            elem:GetWidgetProxyByName("Button(1014)"):GetImage():SetRes("Texture/Sprite/yellowBtn.sprite")
            elem:GetWidgetProxyByName("Button(1014)"):EnableInput(true)
        end
    else
        elem:GetWidgetProxyByName("Button(1014)"):GetText():SetContent("前往")
        elem:GetWidgetProxyByName("Button(1014)"):GetImage():SetRes("Texture/Sprite/blueBtn.sprite")
        elem:GetWidgetProxyByName("Button(1014)"):EnableInput(true)
    end
end




function renWuClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")
    local index = renWuList:GetSelectedIndex()
    local tb = allRenWuZuTB["每日任务"][index+1]
    local renWuID = tb[2]
    local renWuZuID = tb[7]
    local renWuisFinished = tb[5]
    local renWuisAwarded = tb[6]


    if renWuisFinished then
        if not renWuisAwarded then
            allRenWuZuTB["每日任务"][index+1][6] = true
            huoyueduNum = huoyueduNum + RiChangJiangLiInfo[renWuID][1][2]
            linJiangRefreshRenWuInfo(renWuID)
            refreshrihuoRenWuInfo(huoyueduNum)
            formSelf:GetWidgetProxyByName("TextLabel(1058)"):GetText():SetContent(huoyueduNum)

            LuaCallCs_InnerSystem.GetTaskAward(renWuZuID, renWuID)

            -- LuaCallCs_Task.RefreshTaskList()
            -- LuaCallCs_Task.ReqTaskInfo()

        end
    end
end


function renWuFormOnClose()
    formSelf = nil
    allRenWuZuTB = nil
    renWuList = nil
    renWuZuTBLen = nil
    allRenWuZuTBUserData = nil
    huoyueduNum = nil
    hydjindutiao = nil
    tyjy = nil
end