require "OnlineMode/rtti.lua"

EventLoop = EventLoop or rtti.class("EventLoop")
EventLoop.m_listeners = nil

function EventLoop:ctor(eventLoopName)
	self.m_name = eventLoopName
	self.m_listeners = {}
end

function EventLoop:dtor()
	for k,v in pairs(self.m_listeners) do
		self.m_listeners[k] = nil
	end

	self.m_listeners = nil
end

function EventLoop:IsEventListenerExist(eventName, listener)
	if eventName == nil or #eventName == 0 or listener == nil then
		return false
	end

	local listeners = self.m_listeners
	if listeners == nil then 
		return false
	end

	for k,v in pairs(listeners) do
		if v.m_eventName == eventName and v.m_listener == listener then
			return true, i
		end
	end

	return false
end


function EventLoop:AddEventListener(eventName, listener, call_once)
	if not call_once then
		local exist, index = self:IsEventListenerExist(eventName, listener)
		if exist then
			return false, index
		end
	end

	local listeners = self.m_listeners
	if listeners == nil then 
		return false, 0
	end

	listeners[#listeners+1] = {m_eventName=eventName, m_listener=listener, m_callOnce=call_once}

	return true
end

function EventLoop:RemoveListner(eventName, listener)
	local exist, index = self:IsEventListenerExist(eventName, listener)
	if exist then
		table.remove(self.m_listeners, index)
	end

	return exist
end

function EventLoop:RemoveAllListeners(eventName)
	local listeners = self.m_listeners

	if listeners == nil then
		return false
	end

	if eventName == nil then
		local n = #self.m_listeners
		self.m_listeners = {}
		return true, n
	end
	
	local exist = false
	local n = 0

	for k,v in pairs(listeners) do
		if v.m_eventName == eventName then				
			listeners[k] = nil
			exist = true
			n = n + 1
		end	
	end

	return exist, n	
end

function EventLoop:DispatchEvent(event, ...)
	if event == nil then
		return false
	end

	if type(event) == "table" then
		if event.name == nil or type(event.name) ~= "string" or #event.name == 0 then
			return false
		end
	elseif type(event) == "string" then
		if #event == 0 then
			return false	
		end
		event = {name=event}
	end

	local listeners = self.m_listeners
	if listeners == nil then 
		return false 
	end

	local dispatched = false

	for k,v in pairs(listeners) do
		if v.m_listener ~= nil and v.m_eventName == event.name then
			event.m_target = v.m_listener
			event.m_source = self

			if type(v.m_listener) == "function" then
				v.m_listener(event, ...)
				if v.m_callOnce then 
					self:RemoveEventListener(event.name, v.m_listener) 
				end
				dispatched = true
			elseif type(v.m_listener) == "table" then
				local f = v.m_listener[event.name]
				if f ~= nil then
					f(event, ...)
					if v.m_callOnce then 
						self:RemoveEventListener(event.name, v.m_listener) 
					end
					dispatched = true
				end
			end
		end
	end

	return dispatched
end

function EventLoop:PrintListeners()
	for k,v in pairs(m_listeners) do
		-- print(k, v.m_eventName, v.m_listener, v.m_callOnce)
	end
end
