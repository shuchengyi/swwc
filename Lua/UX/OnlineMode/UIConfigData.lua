UIConfigData = UIConfigData or {}



-- 杂项 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
UIConfigData.actorCamp = 1 -- 阵营

UIConfigData.loseCount = 50 -- 失败单数

UIConfigData.levelName = "ZCX03"

UIConfigData.GMGongGaoList = {
    ["actorDead"] = {"<color=#FF0000>", " 已经死亡！</color>"},
    ["symw"] = " 召唤了深渊魔王，请及时击杀！", -- 深渊魔王
    ["lcyyg"] = " 召唤了粮草押运官，请及时击杀！", -- 粮草押运官
    ["mdzl"] = " 召唤了魔袋长老，请及时击杀！", -- 魔袋长老
    ["wq"] = " 召唤了武器附魔挑战怪，请小心应对！", -- 武器附魔挑战怪
    ["hj"] = " 召唤了护甲附魔挑战怪，请小心应对！", -- 护甲附魔挑战怪
    ["jz"] = " 召唤了戒指附魔挑战怪，请小心应对！", -- 戒指附魔挑战怪
    ["zbg"] = "敌军统帅已经出现，请及时击杀！", -- 装备怪
    ["jyg"] = "敌军精英已经出现，请及时击杀！", -- 精英怪
    ["zzboss"] = "<color=#FF0000>敌军首领已经出现，请及时击杀！</color>", -- 最终boss
    ["sl"] = "<color=#00FF00>恭喜您！成功击杀敌军首领，获得胜利！</color>", -- 胜利
    ["bbm"] = "<color=#FF0000>背包已满</color>", -- 背包已满
    ["csx"] = "<color=#FF0000>怪物超过上限，请在下一波怪物刷新前及时清理，否则将失败！</color>", -- 超上限
    ["md"] = {"成功击败魔袋长老，恭喜获得", "级黄金魔袋，击败", "个敌人即可获得魔袋奖励"}, -- 获得魔袋
    ["yxks1"] = {"距离游戏开始还有", "秒"}, -- 游戏开始倒计时类型1
    ["yxks2"] = {"<color=#FF0000>距离游戏开始还有", "秒</color>"}, -- 游戏开始倒计时类型2
    ["yxks3"] = "<color=#00EE00>游戏开始，祝您好运</color>", -- 游戏开始倒计时类型3
    ["bossdjs"] = {"<color=#FF0000>敌军首领还有", "秒到达战场！</color>"}, -- boss倒计时
    ["bossKilldjs"] = {"<color=#FF0000>击杀最终Boss剩余时间不足", "秒，请尽快击败最终Boss</color>"}, -- boss倒计时
    ["1"] = "随机事件: 部分怪物获得自身血量20%护盾！",
    ["2"] = "随机事件: 部分怪物获得5秒无敌效果！",
    ["3"] = "随机事件: 部分怪物拥有一次复活效果！",
    ["4"] = "随机事件: 部分怪物获得50%的闪避！",
    ["5"] = "随机事件: 部分怪物获得10秒50%的攻速提升！",
    ["6"] = "随机事件: 部分怪物获得血量翻倍！",
    ["7"] = "随机事件: 部分怪物获得10秒100%的攻击力提升！",
    ["8"] = "随机事件: 部分怪物获得攻击减少英雄移动速度效果！",
    ["9"] = "随机事件: 部分怪物获得攻击减少英雄攻击速度效果！",
    ["10"] = "随机事件: 部分怪物获得攻击减少英雄能量效果！",
    ["11"] = "随机事件: 部分怪物获得攻击减少英雄攻击力效果！",
    ["wqfm"] = "武器附魔",
    ["hjfm"] = "护甲附魔",
    ["jzfm"] = "戒指附魔",
    ["jnsGMGG5"] = "C级技能书已开放购买",
    ["jnsGMGG15"] = "B级技能书已开放购买",
    ["jnsGMGG25"] = "A级技能书已开放购买",
}




UIConfigData.yxxxDicString = "英雄熟练度达到"
-- 杂项 end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<










-- 附魔 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
UIConfigData.addGJLBuffList = {
    ["lcyyg"] = 2010261,
    ["mdzl"] = 2010262,
    ["symw"] = 2010263,

    ["wpLv1"] = 2010264,
    ["wpLv2"] = 2010265,
    ["wpLv3"] = 2010266,
    ["wpLv4"] = 2010267,
    ["wpLv5"] = 2010268,
    ["hjLv1"] = 2010269,
    ["hjLv2"] = 2010270,
    ["hjLv3"] = 2010271,
    ["hjLv4"] = 2010272,
    ["hjLv5"] = 2010273,
    ["jzLv1"] = 2010274,
    ["jzLv2"] = 2010275,
    ["jzLv3"] = 2010276,
    ["jzLv4"] = 2010277,
    ["jzLv5"] = 2010278,
}

UIConfigData.equipBossCfgIDList = {
    162103,
    52002,
    52003
}
-- 附魔 end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<












-- 英雄 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
UIConfigData.heroList = { -- 这个地方跟UIConfigData.heroEXInfo是同步的
	537, -- 1
	511, -- 2
	198, -- 3
	144, -- 4
	120, -- 5
	105, -- 6
	171, -- 7
	518, -- 8
	522, -- 9
	507, -- 10
	503, -- 11
	502, -- 12
	154, -- 13
	140, -- 14
	128, -- 15
	527, -- 16
	-- 536, -- 17
	312, -- 18
	121, -- 19
	515, -- 20
	505, -- 21
	524, -- 22
	508, -- 23
	132, -- 24
	182, -- 25
	175, -- 26
	146, -- 27
	108, -- 28
	189, -- 29
	194, -- 30
	135, -- 31
	134, -- 32
	117, -- 33
	114, -- 34
	113, -- 35
	168, -- 36
	510, -- 37
	195, -- 38
	193, -- 39
	183, -- 40
	178, -- 41
	170, -- 42
	167, -- 43
	150, -- 44
	131, -- 45
	129, -- 46
	123, -- 47
	107, -- 48
	531, -- 49
	528, -- 50
	137, -- 51
	-- 504, -- 52
	179, -- 53
	190, -- 54
	148, -- 55
	124, -- 56
	119, -- 57
	110, -- 58
	106, -- 59
	109, -- 60
	115, -- 61
	118, -- 62
	127, -- 63
	141, -- 64
	142, -- 65
	152, -- 66
	184, -- 67
	501, -- 68
	176, -- 69
	197, -- 70
	513, -- 71
	523, -- 72
	525, -- 73
	199, -- 74
	174, -- 75
	173, -- 76
	111, -- 77
	133, -- 78
	192, -- 79
}


UIConfigData.heroExLevelInfo = {
    [0] = 0,
    [1] = 100,
    [2] = 600,
    [3] = 1600,
    [4] = 3600,
    [5] = 6600,
}

UIConfigData.tanKeInfo = {
    [511] = 1,
    [198] = 1,
    [194] = 1,
    [144] = 1,
    [135] = 1,
    [134] = 1,
    [120] = 1,
    [117] = 1,
    [114] = 1,
    [113] = 1,
    [105] = 1,
    [168] = 1,
    [171] = 1,
}

UIConfigData.zhanShiInfo = {
    [518] = 1,
    [522] = 1,
    [507] = 1,
    [510] = 1,
    [503] = 1,
    [502] = 1,
    [195] = 1,
    [193] = 1,
    [183] = 1,
    [178] = 1,
    [170] = 1,
    [167] = 1,
    [154] = 1,
    [150] = 1,
    [140] = 1,
    [131] = 1,
    [129] = 1,
    [128] = 1,
    [123] = 1,
    [107] = 1,
    [531] = 1,
    [527] = 1,
    -- [536] = 1,
    [528] = 1,
}

UIConfigData.sheShouInfo = {
    [524] = 1,
    [508] = 1,
    [199] = 1,
    [174] = 1,
    [173] = 1,
    [132] = 1,
    [111] = 1,
    [133] = 1,
    [192] = 1,
}


UIConfigData.faShiInfo = {
    [312] = 1,
    [137] = 1,
    [504] = 1,
    [179] = 1,
    [182] = 1,
    [190] = 1,
    [175] = 1,
    [148] = 1,
    [146] = 1,
    [124] = 1,
    [121] = 1,
    [119] = 1,
    [110] = 1,
    [106] = 1,
    [108] = 1,
    [109] = 1,
    [115] = 1,
    [118] = 1,
    [127] = 1,
    [141] = 1,
    [142] = 1,
    [152] = 1,
    [184] = 1,
    [189] = 1,
    [501] = 1,
    [176] = 1,
    [197] = 1,
    [513] = 1,
    [515] = 1,
    [505] = 1,
    [523] = 1,
    [525] = 1,
    [537] = 1,
}


UIConfigData.jinZhanInfo = {
    [501] = 1,
    [182] = 1,
    [175] = 1,
    [146] = 1,
    [108] = 1,
    [537] = 1,
    [511] = 1,
    [198] = 1,
    [194] = 1,
    [144] = 1,
    [135] = 1,
    [134] = 1,
    [120] = 1,
    [117] = 1,
    [114] = 1,
    [113] = 1,
    [105] = 1,
    [168] = 1,
    [171] = 1,
    [518] = 1,
    [522] = 1,
    [507] = 1,
    [510] = 1,
    [503] = 1,
    [502] = 1,
    [195] = 1,
    [193] = 1,
    [183] = 1,
    [178] = 1,
    [170] = 1,
    [167] = 1,
    [154] = 1,
    [150] = 1,
    [140] = 1,
    [131] = 1,
    [129] = 1,
    [128] = 1,
    [123] = 1,
    [107] = 1,
    [531] = 1,
    [527] = 1,
    -- [536] = 1,
    [528] = 1,
}


UIConfigData.yuanChenInfo = {
    [189] = 1,
    [312] = 1,
    [137] = 1,
    [504] = 1,
    [179] = 1,
    [190] = 1,
    [148] = 1,
    [124] = 1,
    [121] = 1,
    [119] = 1,
    [110] = 1,
    [106] = 1,
    [109] = 1,
    [115] = 1,
    [118] = 1,
    [127] = 1,
    [141] = 1,
    [142] = 1,
    [152] = 1,
    [184] = 1,
    [176] = 1,
    [197] = 1,
    [513] = 1,
    [515] = 1,
    [505] = 1,
    [523] = 1,
    [525] = 1,
    [524] = 1,
    [508] = 1,
    [199] = 1,
    [174] = 1,
    [173] = 1,
    [132] = 1,
    [111] = 1,
    [133] = 1,
    [192] = 1,
}

UIConfigData.heroEXInfo = { -- 这个地方跟UIDynamicData.stringArrData是同步的
    [537] = 1,
    [511] = 2,
    [198] = 3,
    [144] = 4,
    [120] = 5,
    [105] = 6,
    [171] = 7,
    [518] = 8,
    [522] = 9,
    [507] = 10,
    [503] = 11,
    [502] = 12,
    [154] = 13,
    [140] = 14,
    [128] = 15,
    [527] = 16,
    [536] = 17,
    [312] = 18,
    [121] = 19,
    [515] = 20,
    [505] = 21,
    [524] = 22,
    [508] = 23,
    [132] = 24,
    [182] = 25,
    [175] = 26,
    [146] = 27,
    [108] = 28,
    [189] = 29,
    [194] = 30,
    [135] = 31,
    [134] = 32,
    [117] = 33,
    [114] = 34,
    [113] = 35,
    [168] = 36,
    [510] = 37,
    [195] = 38,
    [193] = 39,
    [183] = 40,
    [178] = 41,
    [170] = 42,
    [167] = 43,
    [150] = 44,
    [131] = 45,
    [129] = 46,
    [123] = 47,
    [107] = 48,
    [531] = 49,
    [528] = 50,
    [137] = 51,
    [504] = 52,
    [179] = 53,
    [190] = 54,
    [148] = 55,
    [124] = 56,
    [119] = 57,
    [110] = 58,
    [106] = 59,
    [109] = 60,
    [115] = 61,
    [118] = 62,
    [127] = 63,
    [141] = 64,
    [142] = 65,
    [152] = 66,
    [184] = 67,
    [501] = 68,
    [176] = 69,
    [197] = 70,
    [513] = 71,
    [523] = 72,
    [525] = 73,
    [199] = 74,
    [174] = 75,
    [173] = 76,
    [111] = 77,
    [133] = 78,
    [192] = 79,
}


-- 英雄 end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<























-- 技能 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
local activeSkillSlot = 6
UIConfigData.activeSkillTable = {
    [1] = {
        [1] = {12803,activeSkillSlot}, -- D级多重箭 √
        [2] = {12806,activeSkillSlot}, -- D级无光之盾 √
        [3] = {12815,activeSkillSlot}, -- D级圣光领域 √
        [4] = {128551,activeSkillSlot}, -- D级牺牲 √
        [5] = {128091,activeSkillSlot}, -- D级剑舞 √
        [6] = {128521,activeSkillSlot}, -- D级无敌术 √
        [7] = {1282011,activeSkillSlot}, -- D级点金术 √
        [8] = {128411,activeSkillSlot}, -- D级离歌 √
        [9] = {1284131,activeSkillSlot}, -- D级野性释放 √
        [10] = {128611,activeSkillSlot}, -- D级剑燃 √
        [11] = {128181,activeSkillSlot}, -- D级泔油 √
        [12] = {1283611,activeSkillSlot}, -- D级蛊虫术 √
    },
    [2] = {
        [1] = {12816,activeSkillSlot}, -- C级野性释放 √
        [2] = {128201,activeSkillSlot}, -- C级点金术 √
        [3] = {12861,activeSkillSlot}, -- C级剑燃 √
        [4] = {128031,activeSkillSlot}, -- C级多重箭 √
        [5] = {128061,activeSkillSlot}, -- C级无光之盾 √
        [6] = {128151,activeSkillSlot}, -- C级圣光领域 √
        [7] = {128552,activeSkillSlot}, -- C级牺牲 √
        [8] = {128092,activeSkillSlot}, -- C级剑舞 √
        [9] = {128522,activeSkillSlot}, -- C级无敌术 √
        [10] = {128412,activeSkillSlot}, -- C级离歌 √
        [11] = {128182,activeSkillSlot}, -- C级泔油 √
        [12] = {1283612,activeSkillSlot}, -- C级蛊虫术 √
    },
    [3] = {
        [1] = {12809,activeSkillSlot}, -- B级剑舞 √
        [2] = {12841,activeSkillSlot}, -- B级离歌 √
        [3] = {12852,activeSkillSlot}, -- B级无敌术 √
        [4] = {128032,activeSkillSlot}, -- B级多重箭 √
        [5] = {128062,activeSkillSlot}, -- B级无光之盾 √
        [6] = {128152,activeSkillSlot}, -- B级圣光领域 √
        [7] = {128553,activeSkillSlot}, -- B级牺牲 √
        [8] = {1282012,activeSkillSlot}, -- B级点金术 √
        [9] = {1284132,activeSkillSlot}, -- B级野性释放 √
        [10] = {128612,activeSkillSlot}, -- B级剑燃 √
        [11] = {128183,activeSkillSlot}, -- B级泔油 √
        [12] = {1283613,activeSkillSlot}, -- B级蛊虫术 √
    },
    [4] = {
        [1] = {12818,activeSkillSlot}, -- A级泔油 √
        [2] = {12836,activeSkillSlot}, -- A级蛊虫术
        [3] = {12855,activeSkillSlot}, -- A级牺牲 √
        [4] = {128033,activeSkillSlot}, -- A级多重箭 √
        [5] = {128063,activeSkillSlot}, -- A级无光之盾 √
        [6] = {128153,activeSkillSlot}, -- A级圣光领域 √
        [7] = {128093,activeSkillSlot}, -- A级剑舞 √
        [8] = {128523,activeSkillSlot}, -- A级无敌术 √
        [9] = {1282013,activeSkillSlot}, -- A级点金术 √
        [10] = {128413,activeSkillSlot}, -- A级离歌 √
        [11] = {1284133,activeSkillSlot}, -- A级野性释放 √
        [12] = {128613,activeSkillSlot}, -- A级剑燃 √
    },
}


local passSkillSlot = 8 -- 9
UIConfigData.passSkillTable = {
    [1] = {
        [1] = {12318, passSkillSlot}, -- D级重击 √
        [2] = {1230401, passSkillSlot}, -- D级活性护甲 √
        [3] = {12366, passSkillSlot}, -- D级狂怒 √
        [4] = {12339, passSkillSlot}, -- D级清算 √
        [5] = {1230201, passSkillSlot}, -- D级闪避 √
        [6] = {12314, passSkillSlot}, -- D级武器附魔 √
        [7] = {123201, passSkillSlot}, -- D级闪电过载 √
        [8] = {12309, passSkillSlot}, -- D级理财高手 √
        [9] = {12312, passSkillSlot}, -- D级刀扇装甲 √
        [10] = {123010, passSkillSlot}, -- D级属性加强 √
        [11] = {112401, passSkillSlot}, -- D级重生 √
        [12] = {12372, passSkillSlot}, -- D级苦难光环  √
    },
    [2] = {
        [1] = {1230202, passSkillSlot}, -- C级闪避 √
        [2] = {123391, passSkillSlot}, -- C级清算 √
        [3] = {123141, passSkillSlot}, -- C级武器附魔 √
        [4] = {123181, passSkillSlot}, -- C级重击 √
        [5] = {12304011, passSkillSlot}, -- C级活性护甲 √
        [6] = {123661, passSkillSlot}, -- C级狂怒 √
        [7] = {123202, passSkillSlot}, -- C级闪电过载 √
        [8] = {123091, passSkillSlot}, -- C级理财高手 √
        [9] = {123121, passSkillSlot}, -- C级刀扇装甲 √
        [10] = {123011, passSkillSlot}, -- C级属性加强 √
        [11] = {112402, passSkillSlot}, -- C级重生 √
        [12] = {123721, passSkillSlot}, -- C级苦难光环 √
    },
    [3] = {
        [1] = {123092, passSkillSlot}, -- D级理财高手 √
        [2] = {123203, passSkillSlot}, -- B级闪电过载 √
        [3] = {123122, passSkillSlot}, -- B级刀扇装甲 √
        [4] = {123392, passSkillSlot}, -- B级清算 √
        [5] = {123182, passSkillSlot}, -- B级重击 √
        [6] = {12304012, passSkillSlot}, -- B级活性护甲 √
        [7] = {123392, passSkillSlot}, -- B级清算 √
        [8] = {1230203, passSkillSlot}, -- B级闪避 √
        [9] = {123142, passSkillSlot}, -- B级武器附魔 √
        [10] = {123012, passSkillSlot}, -- B级属性加强 √
        [11] = {112403, passSkillSlot}, -- B级重生 √
        [12] = {123722, passSkillSlot}, -- B级苦难光环 √
    },
    [4] = {
        [1] = {123013,passSkillSlot}, -- A级属性加强 √
        [2] = {112404,passSkillSlot}, -- A级重生 √
        [3] = {123723,passSkillSlot}, -- A级苦难光环 √
        [4] = {123393,passSkillSlot}, -- A级清算 √
        [5] = {123183,passSkillSlot}, -- A级重击 √
        [6] = {12304013,passSkillSlot}, -- A级活性护甲 √
        [7] = {123393,passSkillSlot}, -- A级清算 √
        [8] = {1230204,passSkillSlot}, -- A级闪避 √
        [9] = {123143,passSkillSlot}, -- A级武器附魔 √
        [10] = {123204,passSkillSlot}, -- A级闪电过载 √
        [11] = {123093,passSkillSlot}, -- A级理财高手 √
        [12] = {123723,passSkillSlot}, -- A级刀扇装甲 √
    },
}


UIConfigData.daojuNameColorInfo = {
    ["D级被动技能书"] = "D 级 被 动 技 能 书",
    ["D级主动技能书"] = "D 级 主 动 技 能 书",
    ["C级被动技能书"] = "<color=#1E90FF>C 级 被 动 技 能 书</color>",
    ["C级主动技能书"] = "<color=#1E90FF>C 级 主 动 技 能 书</color>",
    ["B级被动技能书"] = "<color=#8A2BE2>B 级 被 动 技 能 书</color>",
    ["B级主动技能书"] = "<color=#8A2BE2>B 级 主 动 技 能 书</color>",
    ["A级被动技能书"] = "<color=#FFA500>A 级 被 动 技 能 书</color>",
    ["A级主动技能书"] = "<color=#FFA500>A 级 主 动 技 能 书</color>"
}


UIConfigData.equipLevelImgInfo = {
    [0] = "Texture/Sprite/lingFanTi.sprite",
    [1] = "Texture/Sprite/yiFanTi.sprite",
    [2] = "Texture/Sprite/erFanTi.sprite",
    [3] = "Texture/Sprite/sanFanTi.sprite",
    [4] = "Texture/Sprite/siFanTi.sprite",
    [5] = "Texture/Sprite/wuFanTi.sprite",
    [6] = "Texture/Sprite/liuFanTi.sprite",
    [7] = "Texture/Sprite/qiFanTi.sprite",
    [8] = "Texture/Sprite/baFanTi.sprite",
}


UIConfigData.shopHanziInfo = {
    ["零"] = "零",
    ["壹"] = "<color=#1E90FF>壹</color>",
    ["贰"] = "<color=#8A2BE2>贰</color>",
    ["叁"] = "<color=#FFA500>叁</color>",
    ["肆"] = "<color=#FF0000>肆</color>",
    ["伍"] = "<color=#00FF00>伍</color>",
}


UIConfigData.equipLevelImgInfoOld = {
    [0] = "Texture/Sprite/0.sprite",
    [1] = "Texture/Sprite/1.sprite",
    [2] = "Texture/Sprite/2.sprite",
    [3] = "Texture/Sprite/3.sprite",
    [4] = "Texture/Sprite/4.sprite",
    [5] = "Texture/Sprite/5.sprite",
}


UIConfigData.skillBookOpenBoci = {
    0,5,15,25,40
}


UIConfigData.erxuanyiInfo = {
    ["残废"] = {"普通攻击会降低3000码范围内敌人20%移动速度，持续2秒，冷却2秒", "UGUI/Sprite/System/BattleEquip/1128", "Texture/Sprite/yiFanTi.sprite"},
    ["暗幕"] = {"受到致命伤害时，进入短暂无敌状态，持续3秒，冷却时间：40秒", "UGUI/Sprite/System/BattleEquip/1127", "Texture/Sprite/yiFanTi.sprite"},
    ["暴风"] = {"暴击后提升自身30%攻击速度和10%移动速度，持续5秒", "UGUI/Sprite/System/BattleEquip/1137", "Texture/Sprite/yiFanTi.sprite"},
    ["破军"] = {"对生命值低于30%的敌人造成额外30%伤害", "UGUI/Sprite/System/BattleEquip/1138", "Texture/Sprite/yiFanTi.sprite"},
    ["破败"] = {"普通攻击会造成敌人当前生命值1%的伤害，对BOSS无效", "UGUI/Sprite/System/BattleEquip/1126", "Texture/Sprite/yiFanTi.sprite"},
    ["审判"] = {"造成伤害有10%的概率直接秒杀敌人，有1%的概率令BOSS损失当前20%生命", "UGUI/Sprite/System/BattleEquip/1728", "Texture/Sprite/yiFanTi.sprite"},
    ["回响"] = {"技能命中会触发小范围爆炸，造成【100+200%法术攻击】点真实伤害，冷却时间：5秒", "UGUI/Sprite/System/BattleEquip/1233", "Texture/Sprite/yiFanTi.sprite"},
    ["强击"] = {"使用技能后，5秒内的下一次普通攻击附加【60%物理攻击+150%法术攻击】的真实伤害，冷却时间：2秒", "UGUI/Sprite/System/BattleEquip/1134", "Texture/Sprite/yiFanTi.sprite"},
    ["荆棘"] = {"受到物理伤害时，会将伤害量的50%以法术伤害的形式回敬给对方", "UGUI/Sprite/System/BattleEquip/1327", "Texture/Sprite/yiFanTi.sprite"},
    ["献祭"] = {"每秒对身边3000码范围内的敌人造成使用者自身最大生命值10%的伤害", "UGUI/Sprite/System/BattleEquip/1331", "Texture/Sprite/yiFanTi.sprite"},
    ["冰心"] = {"受到单次伤害超过当前生命值10%时触发寒冰冲击，对周围敌人造成【80%物理攻击+80%法术攻击】点伤害，并降低其30%攻击，持续2秒，冷却时间：2秒", "UGUI/Sprite/System/BattleEquip/1336", "Texture/Sprite/yiFanTi.sprite"},
    ["无畏"] = {"每次受到伤害后，自身造成的所有伤害提升5%并增加1%的移速，这个效果最高可以叠加10层，持续3秒", "UGUI/Sprite/System/BattleEquip/1338", "Texture/Sprite/yiFanTi.sprite"},
    ["重组"] = {"减少40%英雄复活时间", "UGUI/Sprite/System/BattleEquip/1349", "Texture/Sprite/yiFanTi.sprite"},
}


UIConfigData.ciTiaoInfo = {
    ["饮血"] = {"饮血：物理吸血+", 10, 12.5, 15},
    ["闪电"] = {"闪电：攻击速度+", 12.5, 18.8, 25},
    ["隐追"] = {"隐追：暴击率+", 10, 12.5, 15},
    ["凝神"] = {"凝神：冷却缩短+", 12.5, 18.8, 25},
    ["无尽"] = {"无尽：暴击效果+", 12.5, 16.3, 20},
    ["强攻"] = {"强攻：物理攻击力+", 12.5, 18.8, 25},
    ["虚无"] = {"虚无：法术穿透+", 12.5, 18.8, 25},
    ["弑神"] = {"弑神：法术吸血+", 12.5, 18.8, 25},
    ["毁灭"] = {"毁灭：法术攻击+", 12.5, 18.8, 25},
    ["破甲"] = {"破甲：物理穿透+", 12.5, 18.8, 25},
    ["神行"] = {"神行：移动速度+", 5, 6.3, 7.5},
    ["刻印"] = {"刻印：最大生命值+", 60, 80, 100},
    ["学识"] = {"学识：最大法力+", 60, 80, 100},
    ["源泉"] = {"源泉：回蓝+", 0.6, 0.8, 1},
    ["支配"] = {"支配：回血+", 0.6, 0.8, 1},
    ["统御"] = {"统御：防御+", 10, 12.5, 15},
    ["慈善"] = {"慈善：返还附魔金币的", 7.5, 10, 12.5},
    ["暴怒"] = {"暴怒：最终伤害+", 10, 12.5, 15},
    ["坚毅"] = {"坚毅：伤害减免+", 10, 12.5, 15},
    ["馈赠"] = {"馈赠：金币收益+", 7.5, 8.1, 12.5},
}




-- 技能 end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



-- 宝石 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
UIConfigData.redShuXingInfo = {
    [1] = {
        [1] = 172101
    }
}


UIConfigData.redshuxingStringInfo = {
    [1] = "物理攻击力",
    [2] = "物理穿透",
    [3] = "法术攻击",
    [4] = "法术穿透",
    [5] = "暴击率",
    [6] = "暴击效果",
}


UIConfigData.blueshuxingStringInfo = {
    [1] = "每秒回血",
    [2] = "每秒回蓝",
    [3] = "生命值上限",
    [4] = "魔法值上限",
    [5] = "法术防御",
    [6] = "物理防御",
}

UIConfigData.purpleshuxingStringInfo = {
    [1] = "物理吸血",
    [2] = "法术吸血",
    [3] = "攻击速度",
    [4] = "移动速度",
}

UIConfigData.greenshuxingStringInfo = {
    [1] = "冷却缩短",
    [2] = "每秒金币",
    [3] = "杀敌金币",
    [4] = "金币获取",
}


UIConfigData.redshuxingImgInfo = {
    [1] = {"Texture/Sprite/wlgjlBS1.sprite","Texture/Sprite/wlgjlBS2.sprite","Texture/Sprite/wlgjlBS3.sprite","Texture/Sprite/wlgjlBS4.sprite","Texture/Sprite/wlgjlBS5.sprite"},
    [2] = {"Texture/Sprite/wlctBS1.sprite","Texture/Sprite/wlctBS2.sprite","Texture/Sprite/wlctBS3.sprite","Texture/Sprite/wlctBS4.sprite","Texture/Sprite/wlctBS5.sprite"},
    [3] = {"Texture/Sprite/fsgjlBS1.sprite","Texture/Sprite/fsgjlBS2.sprite","Texture/Sprite/fsgjlBS3.sprite","Texture/Sprite/fsgjlBS4.sprite","Texture/Sprite/fsgjlBS5.sprite"},
    [4] = {"Texture/Sprite/fsctBS1.sprite","Texture/Sprite/fsctBS2.sprite","Texture/Sprite/fsctBS3.sprite","Texture/Sprite/fsctBS4.sprite","Texture/Sprite/fsctBS5.sprite"},
    [5] = {"Texture/Sprite/bjlBS1.sprite","Texture/Sprite/bjlBS2.sprite","Texture/Sprite/bjlBS3.sprite","Texture/Sprite/bjlBS4.sprite","Texture/Sprite/bjlBS5.sprite"},
    [6] = {"Texture/Sprite/bjxgBS1.sprite","Texture/Sprite/bjxgBS2.sprite","Texture/Sprite/bjxgBS3.sprite","Texture/Sprite/bjxgBS4.sprite","Texture/Sprite/bjxgBS5.sprite"},
}

UIConfigData.blueshuxingImgInfo = {
    [1] = {"Texture/Sprite/mmhxBS1.sprite","Texture/Sprite/mmhxBS2.sprite","Texture/Sprite/mmhxBS3.sprite","Texture/Sprite/mmhxBS4.sprite","Texture/Sprite/mmhxBS5.sprite"},
    [2] = {"Texture/Sprite/mmhlBS1.sprite","Texture/Sprite/mmhlBS2.sprite","Texture/Sprite/mmhlBS3.sprite","Texture/Sprite/mmhlBS4.sprite","Texture/Sprite/mmhlBS5.sprite"},
    [3] = {"Texture/Sprite/smzsxBS1.sprite","Texture/Sprite/smzsxBS2.sprite","Texture/Sprite/smzsxBS3.sprite","Texture/Sprite/smzsxBS4.sprite","Texture/Sprite/smzsxBS5.sprite"},
    [4] = {"Texture/Sprite/mfzsxBS1.sprite","Texture/Sprite/mfzsxBS2.sprite","Texture/Sprite/mfzsxBS3.sprite","Texture/Sprite/mfzsxBS4.sprite","Texture/Sprite/mfzsxBS5.sprite"},
    [5] = {"Texture/Sprite/fsfyBS1.sprite","Texture/Sprite/fsfyBS2.sprite","Texture/Sprite/fsfyBS3.sprite","Texture/Sprite/fsfyBS4.sprite","Texture/Sprite/fsfyBS5.sprite"},
    [6] = {"Texture/Sprite/wlfyBS1.sprite","Texture/Sprite/wlfyBS2.sprite","Texture/Sprite/wlfyBS3.sprite","Texture/Sprite/wlfyBS4.sprite","Texture/Sprite/wlfyBS5.sprite"},
}


UIConfigData.purpleshuxingImgInfo = {
    [1] = {"Texture/Sprite/wlxxBS1.sprite","Texture/Sprite/wlxxBS2.sprite","Texture/Sprite/wlxxBS3.sprite","Texture/Sprite/wlxxBS4.sprite","Texture/Sprite/wlxxBS5.sprite"},
    [2] = {"Texture/Sprite/fsxxBS1.sprite","Texture/Sprite/fsxxBS2.sprite","Texture/Sprite/fsxxBS3.sprite","Texture/Sprite/fsxxBS4.sprite","Texture/Sprite/fsxxBS5.sprite"},
    [3] = {"Texture/Sprite/gjsdBS1.sprite","Texture/Sprite/gjsdBS2.sprite","Texture/Sprite/gjsdBS3.sprite","Texture/Sprite/gjsdBS4.sprite","Texture/Sprite/gjsdBS5.sprite"},
    [4] = {"Texture/Sprite/ydsdBS1.sprite","Texture/Sprite/ydsdBS2.sprite","Texture/Sprite/ydsdBS3.sprite","Texture/Sprite/ydsdBS4.sprite","Texture/Sprite/ydsdBS5.sprite"},
}

UIConfigData.greenshuxingImgInfo = {
    [1] = {"Texture/Sprite/lqsjBS1.sprite","Texture/Sprite/lqsjBS2.sprite","Texture/Sprite/lqsjBS3.sprite","Texture/Sprite/lqsjBS4.sprite","Texture/Sprite/lqsjBS5.sprite"},
    [2] = {"Texture/Sprite/mmjbBS1.sprite","Texture/Sprite/mmjbBS2.sprite","Texture/Sprite/mmjbBS3.sprite","Texture/Sprite/mmjbBS4.sprite","Texture/Sprite/mmjbBS5.sprite"},
    [3] = {"Texture/Sprite/sdjbBS1.sprite","Texture/Sprite/sdjbBS2.sprite","Texture/Sprite/sdjbBS3.sprite","Texture/Sprite/sdjbBS4.sprite","Texture/Sprite/sdjbBS5.sprite"},
    [4] = {"Texture/Sprite/jbhqBS1.sprite","Texture/Sprite/jbhqBS2.sprite","Texture/Sprite/jbhqBS3.sprite","Texture/Sprite/jbhqBS4.sprite","Texture/Sprite/jbhqBS5.sprite"},
}






UIConfigData.baoShiInfo = {
    { -- 红宝石
        {
            {8.5, 10}, -- 1阶
            {17, 20}, -- 2阶
            {25.5, 30}, -- 3阶
            {34, 40}, -- 4阶
            {42.5, 50}, -- 5阶
        }, -- 物理攻击力
        {
            {10, 12.5},
            {20, 25},
            {30, 37.5},
            {40, 50},
            {50, 57.5},
        }, -- 物理穿透
        {
            {10, 11.5},
            {20, 23},
            {30, 34.5},
            {40, 46},
            {50, 57.5},
        }, -- 法术攻击
        {
            {8.5, 10},
            {17, 20},
            {25.5, 30},
            {34, 40},
            {42.5, 50},
        }, -- 法术穿透
        {
            {1.25, 1.5},
            {2.5, 3},
            {3.75, 4.5},
            {5, 6},
            {6.25, 7.5},
        }, -- 暴击率
        {
            {10, 12.5},
            {20, 25},
            {30, 37.5},
            {40, 50},
            {50, 62.5},
        }, -- 暴击效果
    },
    { -- 蓝宝石
        {
            {0.55, 0.65},
            {1.1, 1.3},
            {1.65, 1.95},
            {2.2, 2.6},
            {2.75, 3.25},
        }, -- 每秒回血
        {
            {0.55, 0.65},
            {1.1, 1.3},
            {1.65, 1.95},
            {2.2, 2.6},
            {2.75, 3.25},
        }, -- 每秒回蓝
        {
            {25, 27.5},
            {50, 55},
            {75, 82.5},
            {100, 110},
            {125, 137},
        }, -- 生命值上限
        {
            {25, 27.5},
            {50, 55},
            {75, 82.5},
            {100, 110},
            {125, 137},
        }, -- 魔法值上限
        {
            {4.5, 5},
            {9, 10},
            {13.5, 15},
            {18, 20},
            {22.5, 25},
        }, -- 法术防御
        {
            {4.5, 5},
            {9, 10},
            {13.5, 15},
            {18, 20},
            {22.5, 25},
        }, -- 物理防御
    },
    { -- 紫宝石
        {
            {3.25, 3.5},
            {6.5, 7},
            {9.75, 10.5},
            {13, 14},
            {16.25, 17.5},
        }, -- 物理吸血
        {
            {3, 3.25},
            {6, 6.5},
            {9, 9.75},
            {12, 13},
            {15, 16.25},
        }, -- 法术吸血
        {
            {4.5, 4.75},
            {9, 9.5},
            {13.5, 14.25},
            {18, 19},
            {22.5, 23.75},
        }, -- 攻击速度
        {
            {1.5, 1.75},
            {3, 3.5},
            {4.5, 5.25},
            {6, 7},
            {7.5, 8.75},
        }, -- 移动速度
    },
    { -- 绿宝石
        {
            {1.5, 1.75},
            {3, 3.5},
            {4.5, 5.25},
            {6, 7},
            {7.5, 8.75},
        }, -- 冷却缩短
        {
            {20, 22},
            {40, 44},
            {60, 66},
            {80, 88},
            {100, 110},
        }, -- 每秒金币增加
        {
            {50, 55},
            {100, 110},
            {150, 165},
            {200, 220},
            {250, 175},
        }, -- 杀敌金币增加
        {
            {22.5, 25},
            {45, 50},
            {67.5, 75},
            {90, 100},
            {112.5, 125},
        }, -- 金币获取增加
    },
}







-- 宝石 end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


-- 图鉴 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
UIConfigData.tuJianMonsterInfo = {
    {"近战怪", "Texture/Sprite/duochongjian.sprite" ,1, "怪物描述：\n近战怪"},
    {"远程怪", "Texture/Sprite/wuguangzhidun.sprite" ,1, "怪物描述：\n远程怪"},
    {"经验怪", "Texture/Sprite/shengguanglingyu.sprite" ,1, "怪物描述：\n经验怪"},
    {"首领怪", "Texture/Sprite/xianji.sprite" ,1, "怪物描述：\n首领怪"},
    {"粮草押运官", "Texture/Sprite/jianwu.sprite" ,1, "怪物描述：\n粮草押运官"},
    {"魔袋长老", "Texture/Sprite/wudishu.sprite" ,1, "怪物描述：\n魔袋长老"},
    {"深渊魔王", "Texture/Sprite/dianjinshu.sprite" ,1, "怪物描述：\n深渊魔王"},
    {"武器突破怪", "Texture/Sprite/duopo.sprite" ,1, "怪物描述：\n武器突破怪"},
    {"护甲突破怪", "Texture/Sprite/yexingshifang.sprite" ,1, "怪物描述：\n护甲突破怪"},
    {"最终Boss", "Texture/Sprite/jianran.sprite" ,1, "怪物描述：\n最终Boss"},
}

UIConfigData.tuJianSkillInfo = {
    {"多重箭", "Texture/Sprite/duochongjian.sprite" ,1, "技能描述：\n向前方扇形区域射出7只箭，每一支箭造成真实伤害"},
    {"无光之盾", "Texture/Sprite/wuguangzhidun.sprite" ,1, "技能描述：\n获得一个可以吸收伤害的护盾，持续10秒，护盾被击破时，可对周围单位造成真实伤害"},
    {"圣光领域", "Texture/Sprite/shengguanglingyu.sprite" ,1, "技能描述：\n以圣光之力持续审判周围5000码内所有敌人6秒，对领域内的敌方单位造成伤害，同时治疗领域内的友军生命值"},
    {"牺牲", "Texture/Sprite/xianji.sprite" ,1, "技能描述：\n开启后，每秒消耗自身生命值，对身边6000码内所有敌人造成伤害，可手动关闭"},
    {"剑舞", "Texture/Sprite/jianwu.sprite" ,1, "技能描述：\n人剑合一，每次对周围的一个敌方单位造成斩击，每一斩造成真实伤害，最多造成10次斩击，每个单位最多被斩击2次"},
    {"无敌术", "Texture/Sprite/wudishu.sprite" ,1, "技能描述：\n使英雄免疫伤害，持续一定时间"},
    {"点金术", "Texture/Sprite/dianjinshu.sprite" ,1, "技能描述：\n对目标3000码范围内的敌人造成真实伤害，并给予金币奖励"},
    {"离歌", "Texture/Sprite/duopo.sprite" ,1, "技能描述：\n对自身周围5000码内所有敌人释放离歌，造成真实伤害，每击中一个单位，最多5个单位，立刻恢复自身生命值"},
    {"野性释放", "Texture/Sprite/yexingshifang.sprite" ,1, "技能描述：\n立即增加攻击速度和移动速度，持续5秒"},
    {"剑燃", "Texture/Sprite/jianran.sprite" ,1, "技能描述：\n使攻击带有溅射的力量，持续8s，对目标造成额外真实伤害，随后引爆，并对周围3000码的范围造成真实伤害，同时造成眩晕"},
    {"黑洞", "Texture/Sprite/ganyou.sprite" ,1, "技能描述：\n释放一个黑洞，对处于漩涡中的敌人立即造成真实伤害，并在2秒内拉拽敌人2次，每次拉拽对目标造成0.1s的眩晕并造成真实伤害"},
    {"虚弱术", "Texture/Sprite/duopo.sprite" ,1, "技能描述：\n对自身周围5000码内所有敌人释放虚弱，造成真实伤害，并使其攻击降低，持续5秒"},
    {"重击", "Texture/Sprite/zhongjibd.sprite", 0, "技能描述：\n造成伤害时有25%的几率重击当前目标，对其造成额外真实伤害并使其晕迷2秒钟"},
    {"活性护甲", "Texture/Sprite/huoxinghujia.sprite", 0, "技能描述：\n每次受到普通攻击时，生命回复得到加成，持续6秒最多叠加5层"},
    {"狂怒", "Texture/Sprite/kuangnv.sprite", 0, "技能描述：\n英雄获得额外的物理攻击力，每降低1%的生命值便会提高0.5%的物理吸血"},
    {"清算", "Texture/Sprite/qingsuan.sprite", 0, "技能描述：\n英雄击败敌方单位时会在其位置5000码内造成范围伤害"},
    {"闪避", "Texture/Sprite/shanbi.sprite", 0, "技能描述：\n拥有几率闪避敌人的物理攻击"},
    {"武器附魔", "Texture/Sprite/wuqifumo.sprite", 0, "技能描述：\n攻击有几率对目标额外造成真实伤害同时立即恢复自身法力值"},
    {"闪电过载", "Texture/Sprite/shandianguozai.sprite", 0, "技能描述：\n攻击时有20%的几率施放闪电伤害目标，攻击时有10%的几率向目标施放闪电链伤害多个敌人,每道闪电造成真实伤害"},
    {"理财高手", "Texture/Sprite/licaigaoshou.sprite", 0, "技能描述：\n击败敌人获得的金钱奖励增加"},
    {"刀扇装甲", "Texture/Sprite/daoshanzhuangjia.sprite", 0, "技能描述：\n受到伤害时，20%对周围的敌人发出锋利的气刃进行伤害，造成真实伤害"},
    {"属性加强", "Texture/Sprite/shuxingjiaqiang.sprite", 0, "技能描述：\n攻击、法强、护甲加成"},
    {"苦难光环", "Texture/Sprite/kunanguanghuan.sprite", 0, "技能描述：\n半径5000码内的所有敌人持续受到伤害，每1秒对所有敌人造成真实伤害"},
}


UIConfigData.tuJianWuPinInfo = {
    {"技能书", "Texture/Sprite/Db.sprite" ,1, "物品描述：\n游戏内商店可以购买，击败怪物也会随机掉落，使用可获得额外的主动/被动技能,但是会替代原有的主动/被动技能"},
    {"攻击属性", "Texture/Sprite/gongjidrop.sprite" ,1, "物品描述：\n击败精英怪随机掉落，拾取获得攻击力和法术强度加成"},
    {"生命属性", "Texture/Sprite/shengmingdrop.sprite" ,1, "物品描述：\n击败精英怪随机掉落，拾取获得增加最大生命值加成"},
    {"防御属性", "Texture/Sprite/fangyudrop.sprite" ,1, "物品描述：\n击败精英怪随机掉落，拾取获得护甲加成"},
    {"武器附魔", "Texture/Sprite/wuqi.sprite" ,1, "物品描述：\n武器附魔后获得攻击力和法术强度加成，达到指定等级需要击败武器附魔挑战怪才可以进行下一阶段的武器附魔"},
    {"护甲附魔", "Texture/Sprite/hujia.sprite" ,1, "物品描述：\n护甲附魔后获得护甲和魔抗加成，达到指定等级需要击败护甲附魔挑战怪才可以进行下一阶段的护甲附魔"},
    {"粮草押运官", "Texture/Sprite/jinbiguai.sprite" ,1, "物品描述：\n召唤10个粮草押运官，击败可以获得金币奖励和属性加成"},
    {"魔袋长老", "Texture/Sprite/jinkuang.sprite" ,1, "物品描述：\n召唤魔袋长老，魔袋长老会给周围敌军提供强力光环效果，请尽快击杀，击杀后可获得黄金魔袋和属性加成，魔袋等级随着召唤次数的增加而提高"},
    {"深渊魔王", "Texture/Sprite/diyuhuo.sprite" ,1, "物品描述：\n召唤深渊魔王，深渊魔王非常恐怖，请慎重召唤，击杀后可升级王城圣物和属性加成，品质随着召唤次数的增加而提升"},
    {"王城圣物", "Texture/Sprite/shengWu.sprite" ,1, "物品描述：\n可通过击败深渊魔王来升级王城圣物，获得属性加成和词条加成"},
}


UIConfigData.tuJianCiTiaoInfo = {
    {"饮血", "Texture/Sprite/duochongjian.sprite" ,1, "词条描述：\n饮血：+物理吸血"},
    {"闪电", "Texture/Sprite/wuguangzhidun.sprite" ,1, "词条描述：\n闪电：+攻击速度"},
    {"隐追", "Texture/Sprite/shengguanglingyu.sprite" ,1, "词条描述：\n隐追：+暴击率"},
    {"凝神", "Texture/Sprite/xianji.sprite" ,1, "词条描述：\n凝神：+冷却缩短"},
    {"无尽", "Texture/Sprite/jianwu.sprite" ,1, "词条描述：\n无尽：+暴击效果"},
    {"强攻", "Texture/Sprite/wudishu.sprite" ,1, "词条描述：\n强攻：+物理攻击力"},
    {"虚无", "Texture/Sprite/dianjinshu.sprite" ,1, "词条描述：\n虚无：+法术穿透"},
    {"弑神", "Texture/Sprite/duopo.sprite" ,1, "词条描述：\n弑神：+法术吸血"},
    {"毁灭", "Texture/Sprite/duopo.sprite" ,1, "词条描述：\n毁灭：+法术攻击"},
    {"破甲", "Texture/Sprite/duopo.sprite" ,1, "词条描述：\n破甲：+物理穿透"},
    {"神行", "Texture/Sprite/duochongjian.sprite" ,1, "词条描述：\n神行：+移动速度"},
    {"刻印", "Texture/Sprite/wuguangzhidun.sprite" ,1, "词条描述：\n刻印：+最大生命值"},
    {"学识", "Texture/Sprite/shengguanglingyu.sprite" ,1, "词条描述：\n学识：+最大法力"},
    {"源泉", "Texture/Sprite/xianji.sprite" ,1, "词条描述：\n源泉：+回蓝"},
    {"支配", "Texture/Sprite/jianwu.sprite" ,1, "词条描述：\n支配：+回血"},
    {"统御", "Texture/Sprite/wudishu.sprite" ,1, "词条描述：\n统御：+防御"},
    {"慈善", "Texture/Sprite/dianjinshu.sprite" ,1, "词条描述：\n慈善：返还附魔金币"},
    {"暴怒", "Texture/Sprite/duopo.sprite" ,1, "词条描述：\n暴怒：+最终伤害"},
    {"坚毅", "Texture/Sprite/duopo.sprite" ,1, "词条描述：\n坚毅：+伤害减免"},
    {"馈赠", "Texture/Sprite/duopo.sprite" ,1, "词条描述：\n馈赠：+金币收益"},
}



UIConfigData.tuJianTianFuInfo = {
    {"残废", "UGUI/Sprite/System/BattleEquip/1128" ,1, "天赋描述：\n普通攻击会降低3000码范围内敌人20%移动速度，持续2秒，冷却2秒"},
    {"暗幕", "UGUI/Sprite/System/BattleEquip/1127" ,1, "天赋描述：\n受到致命伤害时，进入短暂无敌状态，持续3秒，冷却时间：40秒"},
    {"暴风", "UGUI/Sprite/System/BattleEquip/1137" ,1, "天赋描述：\n暴击后提升自身30%攻击速度和10%移动速度，持续5秒"},
    {"破军", "UGUI/Sprite/System/BattleEquip/1138" ,1, "天赋描述：\n对生命值低于30%的敌人造成额外30%伤害"},
    {"破败", "UGUI/Sprite/System/BattleEquip/1126" ,1, "天赋描述：\n普通攻击会造成敌人当前生命值1%的伤害，对BOSS无效"},
    {"审判", "UGUI/Sprite/System/BattleEquip/1228" ,1, "天赋描述：\n造成伤害有10%的概率直接秒杀敌人，有1%的概率令BOSS损失当前20%生命"},
    {"回响", "UGUI/Sprite/System/BattleEquip/1233" ,1, "天赋描述：\n技能命中会触发小范围爆炸，造成【100+200%法术攻击】点真实伤害，冷却时间：5秒"},
    {"强击", "UGUI/Sprite/System/BattleEquip/1134" ,1, "天赋描述：\n使用技能后，5秒内的下一次普通攻击附加【60%物理攻击+150%法术攻击】的真实伤害，冷却时间：2秒"},
    {"荆棘", "UGUI/Sprite/System/BattleEquip/1327" ,1, "天赋描述：\n受到物理伤害时，会将伤害量的50%以法术伤害的形式回敬给对方"},
    {"献祭", "UGUI/Sprite/System/BattleEquip/1331" ,1, "天赋描述：\n每秒对身边3000码范围内的敌人造成使用者自身最大生命值10%的伤害"},
    {"冰心", "UGUI/Sprite/System/BattleEquip/1336" ,1, "天赋描述：\n受到单次伤害超过当前生命值10%时触发寒冰冲击，对周围敌人造成【80%物理攻击+80%法术攻击】点伤害，并降低其30%攻击，持续2秒，冷却时间：2秒"},
    {"无畏", "UGUI/Sprite/System/BattleEquip/1338" ,1, "天赋描述：\n每次受到伤害后，自身造成的所有伤害提升5%并增加1%的移速，这个效果最高可以叠加10层，持续3秒"},
    {"重组", "UGUI/Sprite/System/BattleEquip/1349" ,1, "天赋描述：\n减少40%英雄复活时间"},
}






UIConfigData.tuJianZhiYeInfo = {
    {"坦克", "UGUI/Sprite/System/BattleEquip/1128" ,1, "职业描述：\n伤害减免5%"},
    {"战士", "UGUI/Sprite/System/BattleEquip/1127" ,1, "职业描述：\n物理攻击力提升5%"},
    {"射手", "UGUI/Sprite/System/BattleEquip/1137" ,1, "职业描述：\n攻击速度提升5%"},
    {"法师", "UGUI/Sprite/System/BattleEquip/1138" ,1, "职业描述：\n法强提升5%"},
    {"近战", "UGUI/Sprite/System/BattleEquip/1126" ,1, "职业描述：\n防御提升5%"},
    {"远程", "UGUI/Sprite/System/BattleEquip/1228" ,1, "职业描述：\n普攻造成范围伤害"},
    {"有蓝条", "UGUI/Sprite/System/BattleEquip/1233" ,1, "职业描述：\n回蓝/每秒0.8%"},
    {"无蓝条", "UGUI/Sprite/System/BattleEquip/1134" ,1, "职业描述：\n回血/每秒0.8%"},
}


UIConfigData.tuJianSuiJiShiJianInfo = {
    {"血量盾", "UGUI/Sprite/System/BattleEquip/1128" ,1, "职业描述：\n伤害减免5%"},
    {"无敌盾", "UGUI/Sprite/System/BattleEquip/1127" ,1, "职业描述：\n物理攻击力提升5%"},
    {"死亡复活", "UGUI/Sprite/System/BattleEquip/1137" ,1, "职业描述：\n攻击速度提升5%"},
    {"50%闪避", "UGUI/Sprite/System/BattleEquip/1138" ,1, "职业描述：\n法强提升5%"},
    {"10s攻速提升50%", "UGUI/Sprite/System/BattleEquip/1126" ,1, "职业描述：\n防御提升5%"},
    {"血量翻倍", "UGUI/Sprite/System/BattleEquip/1228" ,1, "职业描述：\n普攻造成范围伤害"},
    {"10s攻击力提升100%", "UGUI/Sprite/System/BattleEquip/1233" ,1, "职业描述：\n回蓝/每秒0.8%"},
    {"减少英雄蓝量", "UGUI/Sprite/System/BattleEquip/1134" ,1, "职业描述：\n回血/每秒0.8%"},
    {"减少英雄移速", "UGUI/Sprite/System/BattleEquip/1134" ,1, "职业描述：\n回血/每秒0.8%"},
    {"减少英雄攻速", "UGUI/Sprite/System/BattleEquip/1134" ,1, "职业描述：\n回血/每秒0.8%"},
    {"减少英雄攻击力", "UGUI/Sprite/System/BattleEquip/1134" ,1, "职业描述：\n回血/每秒0.8%"},
}



UIConfigData.tuJianAllInfo = {
    UIConfigData.tuJianMonsterInfo,
    UIConfigData.tuJianSkillInfo,
    UIConfigData.tuJianWuPinInfo,
    UIConfigData.tuJianCiTiaoInfo,
    UIConfigData.tuJianTianFuInfo,
    UIConfigData.tuJianZhiYeInfo,
    UIConfigData.tuJianSuiJiShiJianInfo,
}

-- 图鉴 end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<













-- 组队 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
UIConfigData.nanduInfo = { -- zdc   可以设置每日获取上限，然后卖上限道具
    {"初级",10,"Texture/Sprite/chuji.sprite", "Texture/Sprite/chujiHui.sprite", "Texture/Sprite/chujiText.sprite", "D级"},
    {"中级",15,"Texture/Sprite/zhongji1.sprite", "Texture/Sprite/zhongjiHui.sprite", "Texture/Sprite/zhongjiText.sprite", "B级"},
    {"高级",20,"Texture/Sprite/gaoji.sprite", "Texture/Sprite/gaojiHui.sprite", "Texture/Sprite/gaojiText.sprite", "A级"},
    {"超级",25,"Texture/Sprite/chaoji.sprite", "Texture/Sprite/chaojiHui.sprite", "Texture/Sprite/chaojiText.sprite", "超级"},
    {"王者",30,"Texture/Sprite/wangzhe.sprite", "Texture/Sprite/wangzheHui.sprite", "Texture/Sprite/wangzheText.sprite", "王者"},
    {"死亡",35,"Texture/Sprite/siwang.sprite", "Texture/Sprite/siwangHui.sprite", "Texture/Sprite/siwangText.sprite", "死亡"},
    {"梦魇",40,"Texture/Sprite/mengyan.sprite", "Texture/Sprite/mengyanHui.sprite", "Texture/Sprite/mengyanText.sprite", "梦魇"},
    {"超脱",45,"Texture/Sprite/chaotuo.sprite", "Texture/Sprite/chaotuoHui.sprite", "Texture/Sprite/chaotuoText.sprite", "超脱"},
}


UIConfigData.teamInfo = {
	{"单人游戏", "1", "Texture/Sprite/danren.sprite", "Texture/Sprite/danrenHui.sprite", "Texture/Sprite/danrenText.sprite", "单人"},
	{"双人组队", "2", "Texture/Sprite/shuangren.sprite", "Texture/Sprite/shuangrenHui.sprite", "Texture/Sprite/shuangrenText.sprite", "双人"},
	{"三人组队", "3", "Texture/Sprite/sanren.sprite", "Texture/Sprite/sanrenHui.sprite", "Texture/Sprite/sanrenText.sprite", "三人"},
	{"四人组队", "4", "Texture/Sprite/siren.sprite", "Texture/Sprite/sirenHui.sprite", "Texture/Sprite/sirenText.sprite", "四人"},
}


-- 组队 end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<













-- 段位 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
UIConfigData.duanweizengfu = {
    ["青铜"] = {0,20},
    ["白银"] = {60,50},
    ["黄金"] = {210,75},
    ["铂金"] = {510,100},
    ["钻石"] = {1010,200},
    ["星耀"] = {2010,400},
    ["最强王者"] = {4010,600},
    ["荣耀王者"] = {34010,800},
}


UIConfigData.jifenLevelInfo = {
    [0] = 0,
    [1] = 15,
    [2] = 80,
    [3] = 140,
    [4] = 280,
    [5] = 460,
    [6] = 700,
    [7] = 1000,
    [8] = 1320,
    [9] = 1640,
    [10] = 2000,
}



UIConfigData.duanweizengfu = {
    ["青铜"] = {0,20},
    ["白银"] = {60,50},
    ["黄金"] = {210,75},
    ["铂金"] = {510,100},
    ["钻石"] = {1010,200},
    ["星耀"] = {2010,400},
    ["最强王者"] = {4010,600},
    ["荣耀王者"] = {34010,800},
}




UIConfigData.daunweiNuberImgInfo = {
    ["青铜"] = {"Texture/Sprite/jjqt1.sprite","Texture/Sprite/jjqt2.sprite","Texture/Sprite/jjqt3.sprite"},
    ["白银"] = {"Texture/Sprite/zxby1.sprite","Texture/Sprite/zxby2.sprite","Texture/Sprite/zxby3.sprite",},
    ["黄金"] = {"Texture/Sprite/ryhj1.sprite","Texture/Sprite/ryhj2.sprite","Texture/Sprite/ryhj3.sprite","Texture/Sprite/ryhj4.sprite"},
    ["铂金"] = {"Texture/Sprite/zgbj1.sprite","Texture/Sprite/zgbj2.sprite","Texture/Sprite/zgbj3.sprite","Texture/Sprite/zgbj4.sprite","Texture/Sprite/zgbj5.sprite"},
    ["钻石"] = {"Texture/Sprite/yhzs1.sprite","Texture/Sprite/yhzs2.sprite","Texture/Sprite/yhzs3.sprite","Texture/Sprite/yhzs4.sprite","Texture/Sprite/yhzs5.sprite"},
    ["星耀"] = {"Texture/Sprite/zzhy1.sprite","Texture/Sprite/zzhy2.sprite","Texture/Sprite/zzhy3.sprite","Texture/Sprite/zzhy4.sprite","Texture/Sprite/zzhy5.sprite",},
}


-- 段位 end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


















-- 掉落物/装备 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
UIConfigData.dropSKillIDLevelInfo = {
    [1] = {12301, 12302, 12303, 12304},
    [2] = {12303, 12304, 12305, 12306},
    [3] = {12305, 12306, 12307, 12308}
}

UIConfigData.jingyingGuaiDropEquipInfoLvSe = { -- 绿色装备
    901, -- 铁剑
    921, -- 匕首
    1461, -- 搏击拳套
    1471, -- 吸血之镰
    2221, -- 雷鸣刃                    -- 这里的东西在掉落物里找！！！
    1501, -- 咒术典籍
    1511, -- 蓝宝石
    1531, -- 圣者法典
    2241, -- 元素杖
    1571, -- 红玛瑙
    1581, -- 布甲
}


UIConfigData.jingyingGuaiDropEquipInfoLanSe = { -- 蓝色装备
    911, -- 风暴巨剑
    1481, -- 日冕
    2211, -- 狂暴双刃
    931, -- 陨星
    1491, -- 速击之枪
    2271, -- 穿云弓
    941, -- 大棒
    1541, -- 血族之书
    1551, -- 光辉之剑
    951, -- 魅影面罩
    2281, -- 进化水晶
    1561, -- 破碎圣杯
    961, -- 力量腰带
    1781, -- 熔炼之心
    1791, -- 雪山圆盾
    971, -- 守护者之铠
}


UIConfigData.jingyingGuaiDropEquipInfoZiSe = { -- 紫色装备
    991, -- 末世
    1051, -- 宗师之力
    1061, -- 闪电匕首
    1081, -- 暗影战斧
    1001, -- 名刀
    1141, -- 回响之杖
    1441, -- 圣杯
    1121, -- 虚无法杖
    1221, -- 反伤刺甲
    1151, -- 冰霜法杖
    1291, -- 极寒风暴
    1171, -- 巫术法杖
}


UIConfigData.jingyingGuaiDropEquipInfoChengSe = { -- 橙色装备
    1131, -- 博学者之怒（帽子）
    1041, -- 无尽战刃
    811, -- 破军
    1181, -- 贤者之书
    1251, -- 霸者重装
    1311, -- 暴烈之甲
    1241, -- 红莲斗篷
    -- 1031, -- 泣血之刃
    1071, -- 影刃
    1161, -- 痛苦面具
}


UIConfigData.bossGuaiDropEquipInfoLvSeAndLanSe = { -- 绿+蓝色装备
    901, -- 铁剑
    921, -- 匕首
    1461, -- 搏击拳套
    1471, -- 吸血之镰
    2221, -- 雷鸣刃                    -- 这里的东西在掉落物里找！！！
    1501, -- 咒术典籍
    1511, -- 蓝宝石
    1531, -- 圣者法典
    2241, -- 元素杖
    1571, -- 红玛瑙
    1581, -- 布甲
    911, -- 风暴巨剑
    1481, -- 日冕
    2211, -- 狂暴双刃
    931, -- 陨星
    1491, -- 速击之枪
    2271, -- 穿云弓
    941, -- 大棒
    1541, -- 血族之书
    1551, -- 光辉之剑
    951, -- 魅影面罩
    2281, -- 进化水晶
    1561, -- 破碎圣杯
    961, -- 力量腰带
    1781, -- 熔炼之心
    1791, -- 雪山圆盾
    971, -- 守护者之铠
}

UIConfigData.bossGuaiDropEquipInfoLanSeAndZiSe = { -- 绿+蓝色装备
    911, -- 风暴巨剑
    1481, -- 日冕
    2211, -- 狂暴双刃
    931, -- 陨星
    1491, -- 速击之枪
    2271, -- 穿云弓
    941, -- 大棒
    1541, -- 血族之书
    1551, -- 光辉之剑
    951, -- 魅影面罩
    2281, -- 进化水晶
    1561, -- 破碎圣杯
    961, -- 力量腰带
    1781, -- 熔炼之心
    1791, -- 雪山圆盾
    971, -- 守护者之铠
    991, -- 末世
    1051, -- 宗师之力
    1061, -- 闪电匕首
    1081, -- 暗影战斧
    1001, -- 名刀
    1141, -- 回响之杖
    1441, -- 圣杯
    1121, -- 虚无法杖
    1221, -- 反伤刺甲
    1151, -- 冰霜法杖
    1291, -- 极寒风暴
    1171, -- 巫术法杖
}

UIConfigData.bossGuaiDropEquipInfoZiSeAndChengSe = { -- 绿+蓝色装备
    991, -- 末世
    1051, -- 宗师之力
    1061, -- 闪电匕首
    1081, -- 暗影战斧
    1001, -- 名刀
    1141, -- 回响之杖
    1441, -- 圣杯
    1121, -- 虚无法杖
    1221, -- 反伤刺甲
    1151, -- 冰霜法杖
    1291, -- 极寒风暴
    1171, -- 巫术法杖
    1131, -- 博学者之怒（帽子）
    1041, -- 无尽战刃
    811, -- 破军
    1181, -- 贤者之书
    1251, -- 霸者重装
    1311, -- 暴烈之甲
    1241, -- 红莲斗篷
    -- 1031, -- 泣血之刃
    1071, -- 影刃
    1161, -- 痛苦面具
}


UIConfigData.oneLvEquipCZInfo = {
    1111,
    1112,
    1113,
    1114,
    1116,
    1211,
    1212,
    1214,
    1216,
    1311,
    1312,
}

UIConfigData.twoLvEquipCZInfo = {
    1121,
    1122,
    1123,
    1124,
    1129,
    1154,
    1221,
    1222,
    1223,
    1224,
    1225,
    1229,
    1321,
    1322,
    1324,
    1325,
}


UIConfigData.threeLvEquipCZInfo = {
    1126,
    1134,
    1135,
    1137,
    1127,
    1233,
    1226,
    1231,
    1327,
    1234,
    1336,
    1236,
}


UIConfigData.fourLvEquipCZInfo = {
    1232,
    1133,
    1138,
    1238,
    1332,
    1338,
    1331,
    1132,
    1136,
    1235,
}


UIConfigData.equipImgInfo = {
    [1111] = {"UGUI/Sprite/System/BattleEquip/1111", 1},
    [1112] = {"UGUI/Sprite/System/BattleEquip/1112", 1},
    [1113] = {"UGUI/Sprite/System/BattleEquip/1113", 1},
    [1114] = {"UGUI/Sprite/System/BattleEquip/1114", 1},
    [1116] = {"UGUI/Sprite/System/BattleEquip/1116", 1},
    [1211] = {"UGUI/Sprite/System/BattleEquip/1211", 1},
    [1212] = {"UGUI/Sprite/System/BattleEquip/1212", 1},
    [1214] = {"UGUI/Sprite/System/BattleEquip/1214", 1},
    [1216] = {"UGUI/Sprite/System/BattleEquip/1216", 1},
    [1311] = {"UGUI/Sprite/System/BattleEquip/1311", 1},
    [1312] = {"UGUI/Sprite/System/BattleEquip/1312", 1},
    [1121] = {"UGUI/Sprite/System/BattleEquip/1121", 2},
    [1122] = {"UGUI/Sprite/System/BattleEquip/1122", 2},
    [1123] = {"UGUI/Sprite/System/BattleEquip/1123", 2},
    [1124] = {"UGUI/Sprite/System/BattleEquip/1124", 2},
    [1129] = {"UGUI/Sprite/System/BattleEquip/1129", 2},
    [1154] = {"UGUI/Sprite/System/BattleEquip/1154", 2},
    [1221] = {"UGUI/Sprite/System/BattleEquip/1221", 2},
    [1222] = {"UGUI/Sprite/System/BattleEquip/1222", 2},
    [1223] = {"UGUI/Sprite/System/BattleEquip/1223", 2},
    [1224] = {"UGUI/Sprite/System/BattleEquip/1224", 2},
    [1225] = {"UGUI/Sprite/System/BattleEquip/1225", 2},
    [1229] = {"UGUI/Sprite/System/BattleEquip/1229", 2},
    [1321] = {"UGUI/Sprite/System/BattleEquip/1321", 2},
    [1322] = {"UGUI/Sprite/System/BattleEquip/1322", 2},
    [1324] = {"UGUI/Sprite/System/BattleEquip/1324", 2},
    [1325] = {"UGUI/Sprite/System/BattleEquip/1325", 2},
    [1126] = {"UGUI/Sprite/System/BattleEquip/1126", 3},
    [1134] = {"UGUI/Sprite/System/BattleEquip/1134", 3},
    [1135] = {"UGUI/Sprite/System/BattleEquip/1135", 3},
    [1137] = {"UGUI/Sprite/System/BattleEquip/1137", 3},
    [1127] = {"UGUI/Sprite/System/BattleEquip/1127", 3},
    [1233] = {"UGUI/Sprite/System/BattleEquip/1233", 3},
    [1226] = {"UGUI/Sprite/System/BattleEquip/1226", 3},
    [1231] = {"UGUI/Sprite/System/BattleEquip/1231", 3},
    [1327] = {"UGUI/Sprite/System/BattleEquip/1327", 3},
    [1234] = {"UGUI/Sprite/System/BattleEquip/1234", 3},
    [1336] = {"UGUI/Sprite/System/BattleEquip/1336", 3},
    [1236] = {"UGUI/Sprite/System/BattleEquip/1236", 3},
    [1232] = {"UGUI/Sprite/System/BattleEquip/1232", 4},
    [1133] = {"UGUI/Sprite/System/BattleEquip/1133", 4},
    [1138] = {"UGUI/Sprite/System/BattleEquip/1138", 4},
    [1238] = {"UGUI/Sprite/System/BattleEquip/1238", 4},
    [1332] = {"UGUI/Sprite/System/BattleEquip/1332", 4},
    [1338] = {"UGUI/Sprite/System/BattleEquip/1338", 4},
    [1331] = {"UGUI/Sprite/System/BattleEquip/1331", 4},
    [1132] = {"UGUI/Sprite/System/BattleEquip/1132", 4},
    [1136] = {"UGUI/Sprite/System/BattleEquip/1136", 4},
    [1235] = {"UGUI/Sprite/System/BattleEquip/1235", 4},
}


UIConfigData.jingyingGuaiDropEquipInfo = {
    [1] = UIConfigData.jingyingGuaiDropEquipInfoLvSe,
    [2] = UIConfigData.jingyingGuaiDropEquipInfoLanSe,
    [3] = UIConfigData.jingyingGuaiDropEquipInfoZiSe,
    [4] = UIConfigData.jingyingGuaiDropEquipInfoChengSe,
}

--[[
-- 掉落物相关数据 Start +++++++++++
UIConfigData.levelInfo = {
    [901] = 1, -- 铁剑
    [921] = 1, -- 匕首
    [1461] = 1, -- 搏击拳套
    [1471] = 1, -- 吸血之镰
    [2221] = 1, -- 雷鸣刃                    -- 这里的东西在掉落物里找！！！
    [1501] = 1, -- 咒术典籍
    [1511] = 1, -- 蓝宝石
    [1531] = 1, -- 圣者法典
    [2241] = 1, -- 元素杖
    [1571] = 1, -- 红玛瑙
    [1581] = 1, -- 布甲
    [911] = 2, -- 风暴巨剑
    [1481] = 2, -- 日冕
    [2211] = 2, -- 狂暴双刃
    [931] = 2, -- 陨星
    [1491] = 2, -- 速击之枪
    [2271] = 2, -- 穿云弓
    [941] = 2, -- 大棒
    [1541] = 2, -- 血族之书
    [1551] = 2, -- 光辉之剑
    [951] = 2, -- 魅影面罩
    [2281] = 2, -- 进化水晶
    [1561] = 2, -- 破碎圣杯
    [961] = 2, -- 力量腰带
    [1781] = 2, -- 熔炼之心
    [1791] = 2, -- 雪山圆盾
    [971] = 2, -- 守护者之铠
    [991] = 3, -- 末世
    [1051] = 3, -- 宗师之力
    [1061] = 3, -- 闪电匕首
    [1081] = 3, -- 暗影战斧
    [1001] = 3, -- 名刀
    [1141] = 3, -- 回响之杖
    [1441] = 3, -- 圣杯
    [1121] = 3, -- 虚无法杖
    [1221] = 3, -- 反伤刺甲
    [1151] = 3, -- 冰霜法杖
    [1291] = 3, -- 极寒风暴
    [1171] = 3, -- 巫术法杖
    [1131] = 4, -- 博学者之怒（帽子）
    [1041] = 4, -- 无尽战刃
    [811] = 4, -- 破军
    [1181] = 4, -- 贤者之书
    [1251] = 4, -- 霸者重装
    [1311] = 4, -- 暴烈之甲
    [1241] = 4, -- 红莲斗篷
    -- [1031] = 4, -- 泣血之刃
    [1071] = 4, -- 影刃
    [1161] = 4, -- 痛苦面具
}
]]
-- 掉落物 end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
























-- 兵线/怪物 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
UIConfigData.refreshMonsterLineTime = 24000  -- 每波兵间隔
UIConfigData.refreshMonsterTime = 800  -- 每个兵间隔
UIConfigData.refreshMonsterLineCount = 40 -- 总共要刷多少波兵
UIConfigData.refreshMonsterCount = 24 -- 每波刷几个兵

-- 随机事件波次
UIConfigData.sjsjBociInfo = {
    [3] = 1,
    [6] = 1,
    [9] = 1,
    [12] = 1,
    [15] = 1,
    [18] = 1,
    [21] = 1,
    [24] = 1,
    [27] = 1,
    [30] = 1,
    [33] = 1,
    [36] = 1,
    [39] = 1,
    [42] = 1,
    [45] = 1,
    [48] = 1,
    [51] = 1,
    [54] = 1,
    [57] = 1,
    [60] = 1,
}

-- 随机事件效果
-- 小怪随机buff：减伤20%盾永久，无敌盾5s，死亡复活，闪避50%永久，攻击速度提升50%持续10s，血量提升100%永久，攻击力提升100%持续10s
UIConfigData.sjsjInfo = {
    [1] = "血量盾", -- √
    [2] = "无敌盾", -- √
    [3] = "死亡复活", -- √
    [4] = "50%闪避", -- √
    [5] = "10s攻速提升50%", -- √
    [6] = "血量翻倍", -- √
    [7] = "10s攻击力提升100%", -- √
}




-- 所有会出生的怪物的CfgID
UIConfigData.MonsterCfgList = {
    [20921] = true,
    [162102] = true,
    [162101] = true,
    [162108] = true,
    [162301] = true,
    [162106] = true,
    [20790] = true,
    [20791] = true,
    [20792] = true,
    [20781] = true,
    [162111] = true,
    [162402] = true,
    [162109] = true,
    [20788] = true,
    [20789] = true,
    [52004] = true,
    [1032601] = true,
    [52005] = true,
    [162103] = true,
    [52002] = true,
    [52003] = true,
}


-- 最终boss CfgID
UIConfigData.bigBossCfgID = 161802

-- 小兵库CfgID
UIConfigData.bingXianMonsterIDInfo = {
    [162102] = 162102,
    [162101] = 162101,
}

-- 装备怪--> 敌军统帅
-- 精英怪--> 敌军精英
-- boss怪刷新波次  掉技能和装备的
UIConfigData.bossGuaiRoundIndexs = {
    [9] = 162108, -- 兵线索引 = boss怪CfgID
    [19] = 162301,
    [29] = 162106,
    [39] = 20790,
    [49] = 20791,
    [59] = 20792,
}

UIConfigData.bossGuaiRoundIndexsBack = {
    [162108] = {9,1,1},  -- 波次 技能等级 装备等级
    [162301] = {19,1,1},
    [162106] = {29,2,2},
    [20790] = {39,2,2},
    [20791] = {49,3,3},
    [20792] = {59,3,3},
}



-- 精英怪刷新波次
UIConfigData.jingYingGuaiRoundIndexs = { -- 哪几波兵线刷新精英怪
    [5] = 162113, -- 兵线索引 = 精英怪CfgID
    [15] = 162111,
    [25] = 162402,
    [35] = 162401,
    [45] = 20788,
    [55] = 20789,
}

UIConfigData.jingYingGuaiRoundIndexsBack = {
    [162113] = {5, 1},  -- 波次 装备等级
    [162111] = {15, 2},
    [162402] = {25, 3},
    [162401] = {35, 3},
    [20788] = {45, 4},
    [20789] = {55, 4},
}




UIConfigData.jinKuangPrice = 1000
UIConfigData.diyuhuoPrice = 2000
UIConfigData.jinbiguaiPrice = 1000


-- 小兵数值
UIConfigData.bingXianData = {
    -- 1英雄估算生命 2英雄估算攻击 3英雄估算物理防御 4小兵血量 5小兵攻击 6小怪掉落金币
    [-2] = {0,0,0,0,0,0},
    [1] = {3500,170,90,500,8,10},
    [2] = {3500,170,90,550,9,11},
    [3] = {3750,182,109,605,10,13},
    [4] = {3750,182,109,666,11,14},
    [5] = {4000,194,128,732,12,16},
    [6] = {4000,194,128,805,13,18},
    [7] = {4250,206,147,886,14,20},
    [8] = {4250,206,147,974,16,22},
    [9] = {4500,218,166,1072,17,25},
    [10] = {4750,230,185,1179,19,28},
    [11] = {4750,230,185,1297,21,31},
    [12] = {4750,230,185,1427,23,35},
    [13] = {5000,242,204,1569,25,39},
    [14] = {5000,242,204,1726,28,44},
    [15] = {5250,254,223,1899,30,49},
    [16] = {5250,254,223,2089,33,55},
    [17] = {5500,266,242,2297,37,61},
    [18] = {5500,266,242,2527,40,69},
    [19] = {5500,266,242,2780,44,77},
    [20] = {5750,278,261,3058,49,86},
    [21] = {5750,278,261,3364,54,96},
    [22] = {6000,290,280,3700,59,108},
    [23] = {6000,290,280,4070,65,121},
    [24] = {6000,290,280,4477,72,136},
    [25] = {6250,302,299,4925,79,152},
    [26] = {6250,302,299,5417,87,170},
    [27] = {6250,302,299,5959,95,190},
    [28] = {6500,314,318,6555,105,213},
    [29] = {6500,314,318,7210,115,239},
    [30] = {6500,314,318,7932,127,267},
    [31] = {6750,326,337,8725,140,300},
    [32] = {6750,326,337,9597,154,336},
    [33] = {6750,326,337,10557,169,376},
    [34] = {6750,326,337,11613,186,421},
    [35] = {7000,338,356,12774,204,471},
    [36] = {7000,338,356,14051,225,500},
    [37] = {7000,338,356,15456,247,500},
    [38] = {7000,338,356,17002,272,500},
    [39] = {7000,338,356,18702,299,500},
    [40] = {7000,338,356,20572,329,500},
    [41] = {7000,338,356,22630,362,500},
    [42] = {7000,338,356,24893,398,500},
    [43] = {7000,338,356,27382,438,500},
    [44] = {7000,338,356,30120,482,500},
    [45] = {7000,338,356,33132,530,500},
    [46] = {7000,338,356,36445,583,500},
    [47] = {7000,338,356,40090,641,500},
    [48] = {7000,338,356,44099,706,500},
    [49] = {7000,338,356,48509,776,500},
    [50] = {7000,338,356,53359,854,500},
    [51] = {7000,338,356,58695,939,500},
    [52] = {7000,338,356,64565,1033,500},
    [53] = {7000,338,356,71021,1136,500},
    [54] = {7000,338,356,78124,1250,500},
    [55] = {7000,338,356,85936,1375,500},
    [56] = {7000,338,356,94530,1512,500},
    [57] = {7000,338,356,103983,1664,500},
    [58] = {7000,338,356,114381,1830,500},
    [59] = {7000,338,356,125819,2013,500},
    [60] = {7000,338,356,138401,2214,500},
}

-- 精英怪数值
UIConfigData.jingYingGuaiData = {
    -- 1英雄估算生命 2英雄估算攻击 3英雄估算物理防御 4精英怪血量 5精英怪攻击 6精英怪怪掉落金币
    [5] = {4000,194,128,2196,18,0},
    [15] = {5250,254,223,5696,46,0},
    [25] = {6250,302,299,14775,118,0},
    [35] = {7000,338,356,38322,307,0},
    [45] = {7000,338,356,99369,795,0},
    [55] = {7000,338,356,257808,2062,0},
}

-- boss怪数值
UIConfigData.bossGuaiData = {
    [9] = {4500,218,166,5359,43,0}, -- 兵线索引 = boss怪CfgID
    [19] = {5500,266,242,13900,111,0},
    [29] = {6500,314,318,36052,288,0},
    [39] = {7000,338,356,93511,748,0},
    [49] = {7000,338,356,242543,1940,0},
    [59] = {7000,338,356,629094,5033,0},
}


UIConfigData.jinyanzhi = 40

-- 附魔BossCfgID
UIConfigData.wuQiBossCfgID = 162103
UIConfigData.huJiaBossCfgID = 52002
UIConfigData.jieZhiBossCfgID = 52003


UIConfigData.diyuhuoCfgID = 162502
UIConfigData.jinbiCfgID = 1032601
UIConfigData.jinkuangCfgID = 52005


UIConfigData.jinKuangShuXing = {
    -- 1生命值   2攻击  3物理防御 4魔抗
    {10000,0,150,150},
    {12000,0,180,180},
    {14400,0,216,216},
    {17280,0,259,259},
    {20736,0,311,311},
    {24883,0,373,373},
    {29860,0,448,448},
    {35832,0,537,537},
    {42998,0,645,645},
    {51598,0,774,774},
    {51598,0,774,774},
    {51598,0,774,774},
    {51598,0,774,774},
}


UIConfigData.diyuhuoShuXing = {
    -- 1生命值   2攻击  3物理防御 4魔抗
    {20000,200,20,10},
    {24000,240,24,12},
    {28800,288,29,14},
    {34560,346,35,17},
    {41472,415,41,21},
    {49766,498,50,25},
    {59720,597,60,30},
    {71664,717,72,36},
    {85996,860,86,43},
    {103196,1032,103,52},
    {103196,1032,103,52},
    {103196,1032,103,52},
    {103196,1032,103,52},
}

UIConfigData.jinbiguaiShuXing = {
    -- 1生命值   2攻击  3物理防御 4魔抗 5每个怪的金币数
    {2500,50,0,0,100},
    {4250,65,0,0,150},
    {7225,85,0,0,225},
    {12283,110,0,0,338},
    {20880,143,0,0,506},
    {35496,186,0,0,759},
    {60344,241,0,0,1139},
    {102585,314,0,0,1709},
    {174394,408,0,0,2563},
    {174394,408,0,0,2563},
    {174394,408,0,0,2563},
    {174394,408,0,0,2563},
    {174394,408,0,0,2563},
    {174394,408,0,0,2563},
    {174394,408,0,0,2563},
    {174394,408,0,0,2563},
}

-- 附魔boss属性
UIConfigData.fuMoBossShuXing = {
    -- 1血量 2攻击 3物法防御
    {5000,300,50},
    {15000,600,100},
    {50000,1200,200},
    {150000,1800,300},
    {250000,2500,500},
}

UIConfigData.huJiaBossShuXing = {
    {5000,300,50},
    {15000,600,100},
    {50000,1200,200},
    {150000,1800,300},
    {250000,2500,500},
}

UIConfigData.jieZhiBossShuXing = {
    {5000,300,50},
    {15000,600,100},
    {50000,1200,200},
    {150000,1800,300},
    {250000,2500,500},
}

-- 兵线/怪物 end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



-- 成就 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
UIConfigData.chengjiuInfo = {
    ["积分"] = {
        {30, 1, nil},
        {160, 2, nil},
        {280, 3, nil},
        {560, 4, nil},
        {920, 5, nil},
        {1400, 6, nil},
        {2000, 7, nil},
        {2640, 8, nil},
        {3280, 9, nil},
        {4000, 10 ,nil},
    },
    ["通关难度"] = {
        {1,nil},
        {2,nil},
        {3,nil},
        {4,nil},
        {5,nil},
        {6,nil},
        {7,nil},
        {8,nil},
    },
    ["英雄等级"] = {
        {100, 1, nil},
        {600, 2, nil},
        {1600, 3, nil},
        {3600, 4, nil},
        {6600, 5, nil},
    },
    ["5星英雄数量"] = {
        {10, 1, nil},
        {20, 2, nil},
        {30, 3, nil},
        {40, 4, nil},
        {50, 5, nil},
        {60, 6, nil},
    },
    ["英雄总等级"] = {
        {30, 1, nil},
        {60, 2, nil},
        {120, 3, nil},
        {180, 4, nil},
        {210, 5, nil},
        {240, 6, nil},
        {270, 7, nil},
        {300, 8, nil},
    },

}


UIConfigData.heroGetExLevelInfo = {
    [1] = {100, 500}, --  [难度] = {当前使用的yx经验，通用经验}
    [2] = {120, 600},
    [3] = {140, 700},
    [4] = {160, 800},
    [5] = {180, 900},
    [6] = {200, 1000},
    [7] = {200, 1000},
    [8] = {200, 1000},
}



UIConfigData.chengjiuTextInfo = {
    {"积分达到[30]: 初始金币*1000", 0},
    {"积分达到[160]: 随机D级技能书*1", 0},
    {"积分达到[280]: 初始金币*4000", 0},
    {"积分达到[560]: 随机C级技能书*1", 0},
    {"积分达到[920]: 随机英雄次数*1", 0},
    {"积分达到[1400]: 初始金币*8000", 0},
    {"积分达到[2000]: 随机英雄次数*1", 0},
    {"积分达到[2640]: 随机A级技能书*1", 0},
    {"积分达到[3280]: 伤害减免*20%", 0},
    {"积分达到[4000]: 伤害提升*65%", 1}, -- 10
    {"通关难度达到[N1]: 金币收益*2%", 0},
    {"通关难度达到[N2]: 伤害提升*2%", 0},
    {"通关难度达到[N3]: 伤害减免*2%", 0},
    {"通关难度达到[N4]: 金币收益*5%", 0},
    {"通关难度达到[N5]: 伤害提升*5%", 0},
    {"通关难度达到[N6]: 伤害减免*5%", 0},
    {"通关难度达到[N7]: 金币收益*8%", 0},
    {"通关难度达到[N8]: 伤害减免*8%", 1}, -- 8
    {"英雄熟练度等级达到[1]: 攻击力、法强、护甲各+5%", 0},
    {"英雄熟练度等级达到[2]: 攻击力、法强、护甲各+10%", 0},
    {"英雄熟练度等级达到[3]: 攻击力、法强、护甲各+20%", 0},
    {"英雄熟练度等级达到[4]: 攻击力、法强、护甲各+35%", 0},
    {"英雄熟练度等级达到[5]: 攻击力、法强、护甲各+50%", 1}, -- 5
    {"英雄熟练度满级英雄个数等级达到[10]: 金币收益*10%", 0},
    {"英雄熟练度满级英雄个数等级达到[20]: 伤害提升*10%", 0},
    {"英雄熟练度满级英雄个数等级达到[30]: 金币收益*15%", 0},
    {"英雄熟练度满级英雄个数等级达到[40]: 伤害提升*25%", 0},
    {"英雄熟练度满级英雄个数等级达到[50]: 金币收益*30%", 0},
    {"英雄熟练度满级英雄个数等级达到[60]: 伤害提升*50%", 1}, -- 6
    {"所有英雄熟练度总等级达到[30]: 伤害提升*6%", 0},
    {"所有英雄熟练度总等级达到[60]: 金币收益*8%", 0},
    {"所有英雄熟练度总等级达到[120]: 伤害提升*12%", 0},
    {"所有英雄熟练度总等级达到[180]: 金币收益*18%", 0},
    {"所有英雄熟练度总等级达到[210]: 伤害提升*20%", 0},
    {"所有英雄熟练度总等级达到[240]: 金币收益*25%", 0},
    {"所有英雄熟练度总等级达到[270]: 伤害提升*35%", 0},
    {"所有英雄熟练度总等级达到[300]: 金币收益*50%", 0}, -- 8
}





UIConfigData.chengJiuJiFenInfo = {
    {"积分达到[30]: 初始金币*1000", 0},
    {"积分达到[160]: 随机D级技能书*1", 0},
    {"积分达到[280]: 初始金币*4000", 0},
    {"积分达到[560]: 随机C级技能书*1", 0},
    {"积分达到[920]: 随机英雄次数*1", 0},
    {"积分达到[1400]: 初始金币*8000", 0},
    {"积分达到[2000]: 随机英雄次数*1", 0},
    {"积分达到[2640]: 随机A级技能书*1", 0},
    {"积分达到[3280]: 伤害减免*20%", 0},
    {"积分达到[4000]: 伤害提升*65%", 0},
}

UIConfigData.chengJiuTongGuanNanDuInfo = {
    {"通关难度达到[N1]: 金币收益*2%", 0},
    {"通关难度达到[N2]: 伤害提升*2%", 0},
    {"通关难度达到[N3]: 伤害减免*2%", 0},
    {"通关难度达到[N4]: 金币收益*5%", 0},
    {"通关难度达到[N5]: 伤害提升*5%", 0},
    {"通关难度达到[N6]: 伤害减免*5%", 0},
    {"通关难度达到[N7]: 金币收益*8%", 0},
    {"通关难度达到[N8]: 伤害减免*8%", 0},
}


UIConfigData.chengJiuHeroDengJiInfo = {
    {"英雄熟练度等级达到[1]: 攻击力、法强、护甲各+5%", 0},
    {"英雄熟练度等级达到[2]: 攻击力、法强、护甲各+10%", 0},
    {"英雄熟练度等级达到[3]: 攻击力、法强、护甲各+20%", 0},
    {"英雄熟练度等级达到[4]: 攻击力、法强、护甲各+35%", 0},
    {"英雄熟练度等级达到[5]: 攻击力、法强、护甲各+50%", 0},
}


UIConfigData.chengJiuAllHeroExLvInfo = {
    {"所有英雄熟练度总等级达到[30]: 伤害提升*6%", 0},
    {"所有英雄熟练度总等级达到[60]: 金币收益*8%", 0},
    {"所有英雄熟练度总等级达到[120]: 伤害提升*12%", 0},
    {"所有英雄熟练度总等级达到[180]: 金币收益*18%", 0},
    {"所有英雄熟练度总等级达到[210]: 伤害提升*20%", 0},
    {"所有英雄熟练度总等级达到[240]: 金币收益*25%", 0},
    {"所有英雄熟练度总等级达到[270]: 伤害提升*35%", 0},
    {"所有英雄熟练度总等级达到[300]: 金币收益*50%", 0},
}


UIConfigData.chengJiuHeroManJiCntInfo = {
    {"英雄熟练度满级英雄个数等级达到[10]: 金币收益*10%", 0},
    {"英雄熟练度满级英雄个数等级达到[20]: 伤害提升*10%", 0},
    {"英雄熟练度满级英雄个数等级达到[30]: 金币收益*15%", 0},
    {"英雄熟练度满级英雄个数等级达到[40]: 伤害提升*25%", 0},
    {"英雄熟练度满级英雄个数等级达到[50]: 金币收益*30%", 0},
    {"英雄熟练度满级英雄个数等级达到[60]: 伤害提升*50%", 0}
}






UIConfigData.chengJiuAllInfo = {
    UIConfigData.chengJiuJiFenInfo,
    UIConfigData.chengJiuTongGuanNanDuInfo,
    UIConfigData.chengJiuHeroDengJiInfo,
    UIConfigData.chengJiuAllHeroExLvInfo,
    UIConfigData.chengJiuHeroManJiCntInfo,
}




-- 成就 end   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<






-- 任务 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
UIConfigData.renWuZuInfo = {
    "每日任务",
    "每周任务",
}



-- 任务 end   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


-- intArr索引 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
--[[
    intArrInfo = {
        [1] = "总场次"
        [2] = "通关场次"
        [3] = "通关难度"
        [4] = "积分"
        [5] = "满星经验英雄个数"
        [6] = "英雄经验总等级"
        [7] = "通用经验值"
    }
]]
-- intArr索引 end   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


-- 一些方法
local insert = table.insert
local format = string.format
local tostring = tostring
local type = type

function serialize(t)
    local mark={}
    local assign={}

    local tb_cache = {}
    local function tb(len)
        if tb_cache[len] then 
            return tb_cache[len]
        end
        local ret = ''
        while len > 1 do
            ret = ret .. '       '
            len = len - 1
        end
        if len >= 1 then
            ret = ret .. '    '
        end
        tb_cache[len] = ret
        return ret
    end

    local function table2str(t, parent, deep)
        if type(t) == "table" and t.__tostring then 
            return tostring(t)
        end

        deep = deep or 0
        mark[t] = parent
        local ret = {}
        table.foreach(t, function(f, v)
            local k = type(f)=="number" and "["..f.."]" or tostring(f)
            local t = type(v)
            if t == "userdata" or t == "function" or t == "thread" or t == "proto" or t == "upval" then
                insert(ret, format("%s=%q", k, tostring(v)))
            elseif t == "table" then
                local dotkey = parent..(type(f)=="number" and k or "."..k)
                if mark[v] then
                    insert(assign, dotkey.."="..mark[v])
                else
                    insert(ret, format("%s=%s", k, table2str(v, dotkey, deep + 1)))
                end
            elseif t == "string" then
                insert(ret, format("%s=%q", k, v))
            elseif t == "number" then
                if v == math.huge then
                    insert(ret, format("%s=%s", k, "math.huge"))
                elseif v == -math.huge then
                    insert(ret, format("%s=%s", k, "-math.huge"))
                else
                    insert(ret, format("%s=%s", k, tostring(v)))
                end
            else
                insert(ret, format("%s=%s", k, tostring(v)))
            end
        end)
        return "{\n" .. tb(deep + 2) .. table.concat(ret,",\n" .. tb(deep + 2)) .. '\n' .. tb(deep+1) .."}"
    end

    if type(t) == "table" then
        if t.__tostring then 
            return tostring(t)
        end
        local str = format("%s%s",  table2str(t,"_"), table.concat(assign," "))
        return "\n<<UI_table>>" .. str
    else
        return tostring(t)
    end
end


function printTb(...)
    local n = select('#', ...)

    local tb = {}

    table.insert(tb, '[' .. os.date() .. ']')
    for i = 1, n do
        local v = select(i, ...)
        local str = serialize(v)
        table.insert(tb, str)
    end

    local ret = table.concat(tb, '  ')

    LuaCallCs_Common.Log(ret)

    return ret
end
