G_GameDataMgr = require"OnlineMode/G_GameDataMgr.lua"
json = require "json.lua"

local formSelf

local biaoQianCJList
local biaoQianCJInfo = {
    "积分", -- √
    "通关难度", -- √
    "英雄等级",
    "英雄总等级", -- √
    "满级英雄数量", -- √
}

local jiFen_chengJiu


local chengJiuList



function setChengJiuJiFen()
    local num = 0

	while jiFen_chengJiu == nil do
		num  = num + 1
		jiFen_chengJiu = LuaCallCs_PersistentData.GetCustomizeDataIntArr()[4]
		if jiFen_chengJiu < 0 then
			jiFen_chengJiu = 0
		end

		if jiFen_chengJiu then
			break
		end
		if num == 10 then
			return false
		end
	end
	LuaCallCs_Common.Log("积分_成就: " .. jiFen_chengJiu)



    local jiFenLv
	for i=#UIConfigData.jifenLevelInfo, 0 ,-1 do
		if i <= 0 then
			jiFenLv = i

			break
		elseif i >= #UIConfigData.jifenLevelInfo then -- 最大的索引
			jiFenLv = #UIConfigData.jifenLevelInfo
			local jifenLvNum = UIConfigData.jifenLevelInfo[i]
			if jiFen_chengJiu >= jifenLvNum then
				break
			end
		else
			jiFenLv = i
			local jifenLvNum = UIConfigData.jifenLevelInfo[i]
			if jiFen_chengJiu >= jifenLvNum then
				break
			end
		end
	end

    for i=1, #UIConfigData.chengJiuJiFenInfo do
        if i <= jiFenLv then
            UIConfigData.chengJiuJiFenInfo[i][2] = 1
        else
            UIConfigData.chengJiuJiFenInfo[i][2] = 0
        end
    end
end



function setChengJiuTongGuanNanDu()
    local intArr = LuaCallCs_PersistentData.GetCustomizeDataIntArr()

    local nanDuNum = intArr[3]

    for i=1, #UIConfigData.chengJiuTongGuanNanDuInfo do
        if i <= nanDuNum then
            UIConfigData.chengJiuTongGuanNanDuInfo[i][2] = 1
        else
            UIConfigData.chengJiuTongGuanNanDuInfo[i][2] = 0
        end
    end
end



function setChengJiuHeroAllLv()
    -- [5] = "满星经验英雄个数"
    local intArr = LuaCallCs_PersistentData.GetCustomizeDataIntArr()
    local cntNum = intArr[6]

    local allLvCntLv
    local t = UIConfigData.chengjiuInfo["英雄总等级"]
	for i=#t, 0 ,-1 do
		if i <= 0 then
			allLvCntLv = i

			break
		elseif i >= #t then -- 最大的索引
			allLvCntLv = #t
			local allLvNum = t[i][1]
			if cntNum >= allLvNum then
				break
			end
		else
			allLvCntLv = i
			local allLvNum = t[i][1]
			if cntNum >= allLvNum then
				break
			end
		end
	end

    for i=1, #UIConfigData.chengJiuAllHeroExLvInfo do
        if i <= allLvCntLv then
            UIConfigData.chengJiuAllHeroExLvInfo[i][2] = 1
        else
            UIConfigData.chengJiuAllHeroExLvInfo[i][2] = 0
        end
    end

end


function setChengJiuHeroMaxCnt()
    local intArr = LuaCallCs_PersistentData.GetCustomizeDataIntArr()
    local cntNum = intArr[5]

    local allLvCntLv
    local t = UIConfigData.chengjiuInfo["5星英雄数量"]
	for i=#t, 0 ,-1 do
		if i <= 0 then
			allLvCntLv = i

			break
		elseif i >= #t then -- 最大的索引
			allLvCntLv = #t
			local allLvNum = t[i][1]
			if cntNum >= allLvNum then
				break
			end
		else
			allLvCntLv = i
			local allLvNum = t[i][1]
			if cntNum >= allLvNum then
				break
			end
		end
	end

    for i=1, #UIConfigData.chengJiuHeroManJiCntInfo do
        if i <= allLvCntLv then
            UIConfigData.chengJiuHeroManJiCntInfo[i][2] = 1
        else
            UIConfigData.chengJiuHeroManJiCntInfo[i][2] = 0
        end
    end

end


function chengJiuFormOnOpen(LuaUIEvent)
    math.randomseed(tostring(os.time()):reverse():sub(1,7))
    math.random()

    formSelf = LuaUIEvent.SrcForm

    setChengJiuJiFen()

    setChengJiuTongGuanNanDu()


    setChengJiuHeroAllLv() -- 英雄总等级

    setChengJiuHeroMaxCnt() -- 满级英雄


    -- [6] = "英雄经验总等级"




    biaoQianCJList = formSelf:GetWidgetProxyByName("List(1026)")
    biaoQianCJList:SetElementAmount(#biaoQianCJInfo)
    biaoQianCJList:SelectElement(0, false)
    biaoQianCJList:SetElementAmount(#biaoQianCJInfo)
    chengJiuList = formSelf:GetWidgetProxyByName("List(1031)")
    chengJiuList:SelectElement(0, false)
    chengJiuList:SetElementAmount(#UIConfigData.chengJiuAllInfo[1])
end




function chengjiuClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    local selectIndex = chengJiuList:GetSelectedIndex()

    local index = biaoQianCJList:GetSelectedIndex()
end








function biaoQianCJOnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()
    local selectIndex = biaoQianCJList:GetSelectedIndex()
    elem:GetWidgetProxyByName("Button(1029)"):GetText():SetContent(biaoQianCJInfo[index+1])
    if index == selectIndex then
        elem:GetWidgetProxyByName("Button(1029)"):GetImage():SetRes("Texture/xuanzhong.sprite")
    else
        elem:GetWidgetProxyByName("Button(1029)"):GetImage():SetRes("Texture/Sprite/toumingtupian.sprite")
    end
end




function chengJiuClosed(LuaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    LuaCallCs_UI.CloseForm("UI/OnlineMode/ChengJiu.uixml")
end




function biaoQianCJClicked(LuaUIEvent)
	LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")
    local index = biaoQianCJList:GetSelectedIndex()

    biaoQianCJList:SetElementAmount(#biaoQianCJInfo)
    chengJiuList:SetElementAmount(#UIConfigData.chengJiuAllInfo[index+1])
end



function chengJiuOnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()
    local selectIndex = biaoQianCJList:GetSelectedIndex()

    elem:GetWidgetProxyByName("TextLabel(1034)"):GetText():SetContent(UIConfigData.chengJiuAllInfo[selectIndex+1][index+1][1])
    if UIConfigData.chengJiuAllInfo[selectIndex+1][index+1][2] == 0 then
        elem:GetWidgetProxyByName("Button(1033)"):GetImage():SetRes("Texture/pay_che_icon_false.sprite")
    else
        elem:GetWidgetProxyByName("Button(1033)"):GetImage():SetRes("Texture/pay_che_icon_true.sprite")
    end
end


function chengJiuFormOnClose()
    formSelf = nil

    biaoQianCJList = nil

    jiFen_chengJiu = nil

    chengJiuList = nil
end