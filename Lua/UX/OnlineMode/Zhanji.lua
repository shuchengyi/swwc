G_GameDataMgr = require"OnlineMode/G_GameDataMgr.lua"
json = require "json.lua"

local formSelf

local zhanjiPanel
local zhuyePanel

local jiantouImg
local zhankaiPanel
local listNanDuText
local nowduList
local nownandu = 0
local rightNanDuText
local yxxxPanel
local yxList
local mxCnt
local tongyongjingyan
local yxjsPanel
local tyjy = 0
local allheroExLv

local isCanAddEx = false
local fightData = {}


local useExBtn

local listInfo
local zuigaoNanduNum

local biaoQianGrzyInfo = {
    "个人主页",
    "历史战绩",
    "英雄信息",
}

local grzyBiaoQianList


local fightInfo = {
    -- {
    --     {"总场次", 0},
    --     {"通关场次", 0},
    --     {"通关率", 0},
    --     {"MVP场次", 0},
    --     {"MVP率", 0},
    --     {"最高伤害量", 0},
    -- },
}






-- json.decode
function savefightInfo()
    local fightDataString = LuaCallCs_PersistentData.GetCustomizeDataStringArr()[2]

	local listlen = #UIConfigData.nanduInfo + 1 -- 索引[1]是全局的fightData
	if #fightDataString <= 0 then
		for i=1,listlen do
			table.insert(fightData, {0,0,0,0})
		end
	else
		fightData = json.decode(fightDataString)
		LuaCallCs_Common.Log("fightDataList:  ")
	end

    for i=1,#fightData do
        local allCnt = fightData[i][1]
        local winCnt = fightData[i][2]

        local shenglvnum = winCnt/allCnt*100
        if allCnt ~= 0 then
            shenglvnum = math.floor(winCnt/allCnt*100)
        else
            shenglvnum = 0
        end
        local shenglvText = shenglvnum .. "%"

        local mvpCnt = fightData[i][3]
        local mvpCntlvnum = fightData[i][3]/allCnt*100
        if allCnt ~= 0 then
            mvpCntlvnum = math.floor(fightData[i][3]/allCnt*100)
        else
            mvpCntlvnum = 0
        end
        local mvpCntlvText = mvpCntlvnum .. "%"

        local shanghai = fightData[i][4]
        local shanghaiText

        if shanghai > 10000 then
            shanghaiText = string.format("%.1f", (shanghai/10000)) .. "w"
        else
            shanghaiText = shanghai
        end

        fightInfo[i] = {
            {"总场次", allCnt},
            {"通关场次", winCnt},
            {"通关率", shenglvText},
            {"MVP场次", mvpCnt},
            {"MVP率", mvpCntlvText},
            {"最高伤害量", shanghaiText},
        }
    end
end




function setjindutiao()
    local jiFen
	local num = 0

	while jiFen == nil do
		num  = num + 1
		jiFen = LuaCallCs_PersistentData.GetCustomizeDataIntArr()[4]
		if jiFen < 0 then
			jiFen = 0
		end

		if jiFen then
			break
		end
		if num == 10 then
			return false
		end
	end

	local jiFenLvText
	for i=#UIConfigData.jifenLevelInfo, 0 ,-1 do
		if i <= 0 then
			jiFenLvText = "Lv" .. i

			local leftNum = jiFen
			local rightNum = UIConfigData.jifenLevelInfo[1]

			formSelf:GetWidgetProxyByName("gameLvJinduText"):GetText():SetContent(leftNum .. "/" .. rightNum)
			formSelf:GetWidgetProxyByName("gameLvJindutiao"):SetProgressValue(leftNum/rightNum)
			break
		elseif i >= #UIConfigData.jifenLevelInfo then -- 最大的索引
			jiFenLvText = "Lv" .. i
			local jifenLvNum = UIConfigData.jifenLevelInfo[i]
			if jiFen >= jifenLvNum then

				local leftNum = jiFen - jifenLvNum
				local rightNum = 99999

				if leftNum > rightNum then
					leftNum = rightNum
				end
                formSelf:GetWidgetProxyByName("gameLvJinduText"):GetText():SetContent(leftNum .. "/" .. rightNum)
				formSelf:GetWidgetProxyByName("gameLvJindutiao"):SetProgressValue(leftNum/rightNum)
				break
			end
		else
			jiFenLvText = "Lv" .. i
			local jifenLvNum = UIConfigData.jifenLevelInfo[i]
			if jiFen >= jifenLvNum then

				local leftNum = jiFen-UIConfigData.jifenLevelInfo[i]
				local rightNum = UIConfigData.jifenLevelInfo[i+1]-jifenLvNum

				formSelf:GetWidgetProxyByName("gameLvJinduText"):GetText():SetContent(leftNum .. "/" .. rightNum)
				formSelf:GetWidgetProxyByName("gameLvJindutiao"):SetProgressValue(leftNum/rightNum)
				break
			end
		end
	end
    formSelf:GetWidgetProxyByName("TextLabel(1162)"):GetText():SetContent(jiFenLvText)
end

function zhanjiOnOpen(LuaUIEvent)
    math.randomseed(tostring(os.time()):reverse():sub(1,7))
    math.random()
    formSelf = LuaUIEvent.SrcForm

    setjindutiao()

	local playInfo = LuaCallCs_Data.GetHostCampPlayerInfo()

	local playerName = playInfo.name
    formSelf:GetWidgetProxyByName("playerName"):GetText():SetContent(playerName)

    useExBtn = formSelf:GetWidgetProxyByName("Button(1136)")

    local urlText = playInfo.headIconUrl
    formSelf:GetWidgetProxyByName("HttpImage(1178)"):SetImageUrl(urlText, false, true)

    UIDynamicData.intArrData = LuaCallCs_PersistentData.GetCustomizeDataIntArr()
    isCanAddEx = false
    tyjy = UIDynamicData.intArrData[7]

    zuigaoNanduNum = UIDynamicData.intArrData[3]
    local zuigaonanduText
    if zuigaoNanduNum <= 1 then
        zuigaonanduText= UIConfigData.nanduInfo[1][1]
    else
        zuigaonanduText= UIConfigData.nanduInfo[zuigaoNanduNum][1]
    end
    formSelf:GetWidgetProxyByName("TextLabel(1158)"):GetText():SetContent("最高通关难度：" .. zuigaonanduText .. " 难度")

    local shichangFenZhongNum = UIDynamicData.intArrData[8]
    if shichangFenZhongNum < 60 then
        formSelf:GetWidgetProxyByName("shichangText"):GetText():SetContent("时长：" .. shichangFenZhongNum .. "分钟")
    else
        local hour_int = math.ceil(shichangFenZhongNum/60)
        formSelf:GetWidgetProxyByName("shichangText"):GetText():SetContent("时长：" .. hour_int .. "小时")
    end

    savefightInfo()



    allheroExLv = formSelf:GetWidgetProxyByName("allheroExLv")

    zhuyePanel = formSelf:GetWidgetProxyByName("zhuyePanel")
    zhuyePanel:SetActive(true)

    zhanjiPanel = formSelf:GetWidgetProxyByName("zhanjiPanel")
    zhanjiPanel:SetActive(false)

    yxxxPanel = formSelf:GetWidgetProxyByName("yxxxPanel")
    yxxxPanel:SetActive(false)
    yxList = formSelf:GetWidgetProxyByName("yxList")

    nowduList = formSelf:GetWidgetProxyByName("List(1089)")

    jiantouImg = formSelf:GetWidgetProxyByName("Image(1087)"):GetImage()
    jiantouImg:SetRes("Texture/Sprite/downJianTou.sprite")

    rightNanDuText = formSelf:GetWidgetProxyByName("TextLabel(1131)")
    rightNanDuText:GetText():SetContent("英雄熟练度")

    listNanDuText = formSelf:GetWidgetProxyByName("Button(1130)")
    listNanDuText:GetText():SetContent("初级难度")

    formSelf:GetWidgetProxyByName("TextLabel(1129)"):GetText():SetContent("初级难度")



    zhankaiPanel = formSelf:GetWidgetProxyByName("zhankaiPanel")
    zhankaiPanel:SetActive(false)

    mxCnt = formSelf:GetWidgetProxyByName("mxCnt")
    tongyongjingyan = formSelf:GetWidgetProxyByName("tongyongjingyan")

    yxjsPanel = formSelf:GetWidgetProxyByName("yxjsPanel")
    yxjsPanel:SetActive(false)
    formSelf:GetWidgetProxyByName("TextLabel(1114)"):GetText():SetContent(UIConfigData.yxxxDicString)


    formSelf:GetWidgetProxyByName("List(1146)"):SetElementAmount(#fightInfo[1])
    formSelf:GetWidgetProxyByName("List(1144)"):SetElementAmount(#fightInfo[1])
    formSelf:GetWidgetProxyByName("List(1151)"):SetElementAmount(5)
    nowduList:SetElementAmount(#UIConfigData.nanduInfo)

    grzyBiaoQianList = formSelf:GetWidgetProxyByName("List(1168)")
    grzyBiaoQianList:SelectElement(0, false)
    grzyBiaoQianList:SetElementAmount(#biaoQianGrzyInfo)


    listInfo = UIDynamicData.stringArrDataNew

end


function yxjsCloseBtn(LuaUIEvent)
    yxjsPanel:SetActive(false)
end


function wenhaoBtnClicked(LuaUIEvent)
    yxjsPanel:SetActive(true)
end

function zhiyeChanged(LuaUIEvent)
    local checkIndex = formSelf:GetWidgetProxyByName("DropList(1105)"):GetSelectedIndex()
    if checkIndex == 0 then
        listInfo = UIDynamicData.stringArrDataNew
    elseif checkIndex == 1 then
        listInfo = UIDynamicData.tankeStringDataInfo
    elseif checkIndex == 2 then
        listInfo = UIDynamicData.zhanshiStringDataInfo
    elseif checkIndex == 3 then
        listInfo = UIDynamicData.sheshouStringDataInfo
    elseif checkIndex == 4 then
        listInfo = UIDynamicData.fashiStringDataInfo
    end
    yxList:SetElementAmount(#listInfo)
end



function yxListOnEnable(LuaUIEvent)
    if listInfo == nil then
        return
    end


    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()

    local imgPath = LuaCallCs_Resource.GetHeroIcon(2, listInfo[index+1][1])
    elem:GetWidgetProxyByName("yxIcon"):GetImage():SetRes(imgPath)

    local heroExNum = listInfo[index+1][2]

    local heroLevel
    if heroExNum >= UIConfigData.heroExLevelInfo[5] then
        heroLevel = 5
    elseif heroExNum >= UIConfigData.heroExLevelInfo[4] then
        heroLevel = 4
    elseif heroExNum >= UIConfigData.heroExLevelInfo[3] then
        heroLevel = 3
    elseif heroExNum >= UIConfigData.heroExLevelInfo[2] then
        heroLevel = 2
    elseif heroExNum >= UIConfigData.heroExLevelInfo[1] then
        heroLevel = 1
    elseif heroExNum >= 0 then
        heroLevel = 0
    end


    if heroLevel >= 5 then
        elem:GetWidgetProxyByName("TextLabel(1135)"):GetText():SetContent(UIConfigData.heroExLevelInfo[5] .. "/" .. UIConfigData.heroExLevelInfo[5])
        elem:GetWidgetProxyByName("shuliandujindutiao"):SetProgressValue(1)
        elem:GetWidgetProxyByName("TextLabel(1104)"):GetText():SetContent(heroLevel .. "级")

    else
        local nowLvEx = heroExNum - UIConfigData.heroExLevelInfo[heroLevel]
        local nowLvMax = UIConfigData.heroExLevelInfo[heroLevel+1] - UIConfigData.heroExLevelInfo[heroLevel]


        elem:GetWidgetProxyByName("TextLabel(1135)"):GetText():SetContent(nowLvEx .. "/" .. nowLvMax)
        elem:GetWidgetProxyByName("shuliandujindutiao"):SetProgressValue(nowLvEx/nowLvMax)
        elem:GetWidgetProxyByName("TextLabel(1104)"):GetText():SetContent(heroLevel .. "级")

    end


    if isCanAddEx == true then
        elem:GetWidgetProxyByName("Image(1139)"):SetActive(true)

        if heroLevel >= 5 then
            elem:GetWidgetProxyByName("TextLabel(1104)"):GetText():SetContent("5级")
            elem:GetWidgetProxyByName("Image(1139)"):EnableInput(false)
            -- elem:GetWidgetProxyByName("Image(1139)"):SetAlpha(0.4)
        else
            elem:GetWidgetProxyByName("TextLabel(1104)"):GetText():SetContent(heroLevel .. "级 - " .. (heroLevel+1) .. "级")
            if tyjy >= (UIConfigData.heroExLevelInfo[heroLevel+1]-heroExNum) then
                elem:GetWidgetProxyByName("Image(1139)"):EnableInput(true)
                elem:GetWidgetProxyByName("Image(1139)"):SetAlpha(1)
            else
                elem:GetWidgetProxyByName("Image(1139)"):EnableInput(false)
                -- elem:GetWidgetProxyByName("Image(1139)"):SetAlpha(0.4)
            end
        end
    else
        elem:GetWidgetProxyByName("Image(1139)"):SetActive(false)
        elem:GetWidgetProxyByName("Image(1139)"):EnableInput(false)
    end
end



function tyjyBtnClicked(LuaUIEvent)
    if isCanAddEx == true then
        isCanAddEx = false
        useExBtn:GetText():SetContent("使用")
        useExBtn:GetImage():SetRes("Texture/Sprite/xran2.sprite")
    else
        isCanAddEx = true
        useExBtn:GetText():SetContent("退出")
        useExBtn:GetImage():SetRes("Texture/Sprite/xran1.sprite")
    end
    yxList:SetElementAmount(#listInfo)
end


function addHeroExClicked(LuaUIEvent)
    local index = yxList:GetSelectedIndex()
    local heroExNum = listInfo[index+1][2]
    local heroID = listInfo[index+1][1]
    local heroLevel
    if heroExNum >= UIConfigData.heroExLevelInfo[5] then
        heroLevel = 5
    elseif heroExNum >= UIConfigData.heroExLevelInfo[4] then
        heroLevel = 4
    elseif heroExNum >= UIConfigData.heroExLevelInfo[3] then
        heroLevel = 3
    elseif heroExNum >= UIConfigData.heroExLevelInfo[2] then
        heroLevel = 2
    elseif heroExNum >= UIConfigData.heroExLevelInfo[1] then
        heroLevel = 1
    elseif heroExNum >= 0 then
        heroLevel = 0
    end
    local needEx = UIConfigData.heroExLevelInfo[heroLevel+1]-heroExNum

    tyjy = tyjy - needEx

    tongyongjingyan:GetText():SetContent("通用经验： " .. tyjy)

    UIDynamicData.mainFormsldTb[tostring(heroID)] = UIDynamicData.mainFormsldTb[tostring(heroID)] + needEx
    local ExStringJson = json.encode(UIDynamicData.mainFormsldTb)


    LuaCallCs_PersistentData.SetCustomizeDataStringArr({0}, {ExStringJson})
    LuaCallCs_PersistentData.SetCustomizeDataIntArr({6}, {tyjy}) -- 设置持久化数据的，索引是从0开始的

    local canshu = ExStringJson
    checkHeroExFunc(canshu)
    mxCnt:GetText():SetContent("满级英雄数： " .. UIDynamicData.manjiHeroCnt)
    allheroExLv:GetText():SetContent("英雄总等级： " .. UIDynamicData.allheroExLv)
    zhiyeChanged(LuaUIEvent)
end




function jiantouClicked(LuaUIEvent)
    if zhankaiPanel:IsActived() == false then
        nowduList:SetElementAmount(#UIConfigData.nanduInfo)
        jiantouImg:SetRes("Texture/Sprite/upJianTou.sprite")
        zhankaiPanel:SetActive(true)
    else
        jiantouImg:SetRes("Texture/Sprite/downJianTou.sprite")
        zhankaiPanel:SetActive(false)
    end
end



function nanduClicked(LuaUIEvent) --zhiyeChanged
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")
    local checkIndex = formSelf:GetWidgetProxyByName("DropList(1182)"):GetSelectedIndex()
    formSelf:GetWidgetProxyByName("TextLabel(1129)"):GetText():SetContent(UIConfigData.nanduInfo[checkIndex+1][1] .. "难度")

    formSelf:GetWidgetProxyByName("List(1146)"):SetElementAmount(#fightInfo[1])
end



function nanDuChanged(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    formSelf:GetWidgetProxyByName("List(1146)"):SetElementAmount(#fightInfo[1])
end





function formClosed(LuaUIEvent)
    LuaCallCs_UI.CloseForm("UI/OnlineMode/Zhanji.uixml");
end



function nanduChooseOnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()

    local text = UIConfigData.nanduInfo[index+1][1] .. "  难度"

    if (index+1) <= zuigaoNanduNum then
        elem:GetWidgetProxyByName("Button(1094)"):EnableInput(true)
    else
        elem:GetWidgetProxyByName("Button(1094)"):EnableInput(false)

    end

    elem:GetWidgetProxyByName("Button(1094)"):GetText():SetContent(text)

    if nownandu == index then
        elem:GetWidgetProxyByName("Image(1095)"):SetActive(true)
    else
        elem:GetWidgetProxyByName("Image(1095)"):SetActive(false)
    end


end


function grzybiaoQianOnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()
    local selectIndex = grzyBiaoQianList:GetSelectedIndex()

    elem:GetWidgetProxyByName("Button(1169)"):GetText():SetContent(biaoQianGrzyInfo[index+1])
    if index == selectIndex then
        elem:GetWidgetProxyByName("Button(1169)"):GetImage():SetRes("Texture/Sprite/xuanzhong2.sprite")
        elem:GetWidgetProxyByName("Button(1169)"):GetText():SetColor(BluePrint.UGC.UI.Core.Color.White);
    else
        elem:GetWidgetProxyByName("Button(1169)"):GetImage():SetRes("Texture/Sprite/toumingtupian.sprite")
        elem:GetWidgetProxyByName("Button(1169)"):GetText():SetColor(BluePrint.UGC.UI.Core.Color(157/255,194/255,239/255,1))
    end
end













function gerenzhuyeBtnClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    formSelf:GetWidgetProxyByName("List(1146)"):SetElementAmount(#fightInfo[1])
    formSelf:GetWidgetProxyByName("List(1144)"):SetElementAmount(#fightInfo[1])
    formSelf:GetWidgetProxyByName("List(1151)"):SetElementAmount(5)
    zhanjiPanel:SetActive(false)
    yxxxPanel:SetActive(false)
    zhuyePanel:SetActive(true)
end



function lishizhanjiBtnClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")
    formSelf:GetWidgetProxyByName("List(1005)"):SetElementAmount(#UIDynamicData.zhanjiData)

    zhuyePanel:SetActive(false)
    yxxxPanel:SetActive(false)
    zhanjiPanel:SetActive(true)
end



function yxxxBtnClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    mxCnt:GetText():SetContent("满级英雄数： " .. UIDynamicData.manjiHeroCnt)
    tongyongjingyan:GetText():SetContent("通用经验： " .. tyjy)
    allheroExLv:GetText():SetContent("英雄总等级： " .. UIDynamicData.allheroExLv)


    listInfo = UIDynamicData.stringArrDataNew
    yxList:SetElementAmount(#listInfo)
    zhuyePanel:SetActive(false)
    zhanjiPanel:SetActive(false)
    yxxxPanel:SetActive(true)
end



local grzyBiaoQianClickedFuncInfo = {
    gerenzhuyeBtnClicked,
    lishizhanjiBtnClicked,
    yxxxBtnClicked
}


function grzyBiaoQianBtnClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")
    local index = grzyBiaoQianList:GetSelectedIndex()
    local lastIndex = grzyBiaoQianList:GetLastSelectedIndex()

    grzyBiaoQianList:SetElementAmount(#biaoQianGrzyInfo)

    if lastIndex ~= index then
        grzyBiaoQianClickedFuncInfo[index+1]()
    end
end

















function zhanjiOnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()

    local danjuZhanjiData = UIDynamicData.zhanjiData[index+1] -- 这里是有4个人的数据的
    for i = 1, #danjuZhanjiData do
        if true then
            local selfData = danjuZhanjiData[1]
            local playName = selfData.M
            local heroID = selfData.HID
            local killCount = selfData.K
            local boci = selfData.B
            local zbList = selfData.zb
            local skillID1 = selfData.s1
            local skillID2 = selfData.s2
            local IsWin = selfData.W
            local nandu = selfData.N
            local gameStartTime = selfData.T
            if gameStartTime then
                elem:GetWidgetProxyByName("TextLabel(1030)"):GetText():SetContent(os.date("%Y/%m/%d %H:%M", gameStartTime))
            else
                elem:GetWidgetProxyByName("TextLabel(1030)"):GetText():SetContent("")
            end

            elem:GetWidgetProxyByName("Image(1007)"):GetImage():SetRes(LuaCallCs_Resource.GetHeroIcon(0, heroID))
            if IsWin == true then
                elem:GetWidgetProxyByName("TextLabel(1015)"):GetText():SetContent("<color=#32CD32>胜利</color>")
            else
                elem:GetWidgetProxyByName("TextLabel(1015)"):GetText():SetContent("<color=#F08080>失败</color>")
            end
            elem:GetWidgetProxyByName("TextLabel(1016)"):GetText():SetContent(killCount)
            elem:GetWidgetProxyByName("TextLabel(1017)"):GetText():SetContent(boci)

            for k = 1, 3 do
                local widgetName = "zhanjitianfu" .. k
                if zbList[k] ~= nil then
                    local tianfuName = zbList[k]
                    elem:GetWidgetProxyByName(widgetName):GetImage():SetRes(UIConfigData.erxuanyiInfo[tianfuName][2])
                else
                    elem:GetWidgetProxyByName(widgetName):GetImage():SetRes("Texture/Sprite/kongcao.sprite")
                end
            end

            if skillID1 ~= nil then
                elem:GetWidgetProxyByName("skill1"):GetImage():SetRes(LuaCallCs_Skill.GetSkillInfo(skillID1).iconPath)
            else
                elem:GetWidgetProxyByName("skill1"):GetImage():SetRes("Texture/Sprite/kongcao.sprite")
            end

            if skillID2 ~= nil then
                elem:GetWidgetProxyByName("skill2"):GetImage():SetRes(LuaCallCs_Skill.GetSkillInfo(skillID2).iconPath)
            else
                elem:GetWidgetProxyByName("skill2"):GetImage():SetRes("Texture/Sprite/kongcao.sprite")
            end
            elem:GetWidgetProxyByName("TextLabel(1029)"):GetText():SetContent(UIConfigData.nanduInfo[nandu][1] .. " 难度")

        end
    end
end









function nanduFightDataOnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()

    local checkIndex = formSelf:GetWidgetProxyByName("DropList(1182)"):GetSelectedIndex()

    LuaCallCs_Common.Log("索引： " .. checkIndex)
    -- 索引[1]是全局的，索引是  index+1+1
    local text1 = fightInfo[checkIndex+1][index+1][1]
    local text2 = fightInfo[checkIndex+1][index+1][2]
    printTb(text1 .. text2)

    elem:GetWidgetProxyByName("TextLabe(1149)"):GetText():SetContent(text1)
    elem:GetWidgetProxyByName("nanduCntText(1148)"):GetText():SetContent(text2)
end


function allFightDataOnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()

    -- 索引[1]是全局的，索引是  index+1+1
    local text1 = fightInfo[1][index+1][1]
    local text2 = fightInfo[1][index+1][2]

    elem:GetWidgetProxyByName("TextLabel(1125)"):GetText():SetContent(text1)
    elem:GetWidgetProxyByName("allCntText"):GetText():SetContent(text2)
end



function heroExListOnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()

    local NoNum = index + 1
    local heroCfgID = UIDynamicData.stringArrDataNew[index+1][1]


    local heroImg = LuaCallCs_Resource.GetHeroIcon(0, heroCfgID)
    local heroExNum = UIDynamicData.stringArrDataNew[index+1][2]



    elem:GetWidgetProxyByName("Image(1153)"):GetImage():SetRes(heroImg)
    elem:GetWidgetProxyByName("TextLabel(1154)"):GetText():SetContent("No." .. NoNum)
    elem:GetWidgetProxyByName("TextLabel(1155)"):GetText():SetContent("英雄熟练度：" .. heroExNum)
end


function zhanjiOnClose()
    formSelf = nil
    zhanjiPanel = nil
    zhuyePanel = nil
    jiantouImg = nil
    zhankaiPanel = nil
    listNanDuText = nil
    nowduList = nil
    nownandu = 0
    rightNanDuText = nil
    yxxxPanel = nil
    yxList = nil
    mxCnt = nil
    tongyongjingyan = nil
    yxjsPanel = nil
    tyjy = 0
    allheroExLv = nil
    isCanAddEx = false
    fightData = {}
    useExBtn = nil
    listInfo = nil
    zuigaoNanduNum = nil
    grzyBiaoQianList = nil
    fightInfo = {
        -- {
        --     {"总场次", 0},
        --     {"通关场次", 0},
        --     {"通关率", 0},
        --     {"MVP场次", 0},
        --     {"MVP率", 0},
        --     {"最高伤害量", 0},
        -- },
    }
end