G_GameDataMgr = require"OnlineMode/G_GameDataMgr.lua"


local formSelf
local mainPanel
local scoreList
local isMainPanelOpen = false
local duizhenPanel
local heroPanel
local heroInfoTextLeft
local heroInfoTextRight


local nowchoose = "对阵属性"  -- 对阵属性，英雄属性
local duizhenBtn
local heroBtn
local chengjiuPanel
local chengjiuList
local chengjiuBtn

local sxxxList


function ScoreboardOnOpen(LuaUIEvent)
    formSelf = LuaUIEvent.SrcForm

    mainPanel = formSelf:GetWidgetProxyByName("Panel(1065)")
    formSelf:GetWidgetProxyByName("Panel(1067)"):SetActive(false)
    formSelf:GetWidgetProxyByName("Button(1115)"):SetActive(false)
    formSelf:GetWidgetProxyByName("Panel(1066)"):SetActive(false)
    mainPanel:SetActive(false)

    duizhenBtn = formSelf:GetWidgetProxyByName("Button(1126)")
    duizhenBtn:GetImage():SetRes("Texture/Sprite/cebian.sprite")

    heroBtn = formSelf:GetWidgetProxyByName("Button(1127)")
    heroBtn:GetImage():SetRes("Texture/Sprite/toumingtupian.sprite")

    chengjiuBtn = formSelf:GetWidgetProxyByName("Button(1133)")
    chengjiuBtn:GetImage():SetRes("Texture/Sprite/toumingtupian.sprite")


    duizhenPanel = formSelf:GetWidgetProxyByName("duizhenPanel")
    duizhenPanel:SetActive(true)

    heroPanel = formSelf:GetWidgetProxyByName("heroPanel")
    heroPanel:SetActive(false)

    sxxxList = formSelf:GetWidgetProxyByName("List(1151)")

    heroInfoTextLeft = formSelf:GetWidgetProxyByName("heroInfoTextLeft"):GetText()
    heroInfoTextRight = formSelf:GetWidgetProxyByName("heroInfoTextRight"):GetText()

    chengjiuPanel = formSelf:GetWidgetProxyByName("chengjiuPanel")
    chengjiuPanel:SetActive(false)

    chengjiuList = formSelf:GetWidgetProxyByName("chengjiuList")

    scoreList = formSelf:GetWidgetProxyByName("scoreList")
end



function scoreboardOnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()

    -- 拿到该listElement的actorID
    local playerArr = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()

    local playerId = playerArr[index+1].playerID
    local actorIDList = LuaCallCs_Battle.GetActorIDListByPlayerID(playerId)
    local actorID = actorIDList[1]

    for i = 1, 3 do
        local widgetName = "tianfu" .. (i)
        if UIDynamicData.tianfuInfo[playerId][i] ~= nil then
            elem:GetWidgetProxyByName(widgetName):GetImage():SetRes(UIConfigData.erxuanyiInfo[UIDynamicData.tianfuInfo[playerId][i]][2])
        else
            elem:GetWidgetProxyByName(widgetName):GetImage():SetRes("Texture/Sprite/kongcao.sprite")
        end
    end

    local allJinBi = UIDynamicData.allJinBi[playerId]
    local allJinBiText
    if allJinBi > 10000 then
        allJinBiText = math.floor((allJinBi/10000)) .. "万"
    else
        allJinBiText = allJinBi
    end
    elem:GetWidgetProxyByName("TextLabel(1147)"):GetText():SetContent(allJinBiText)

    local shanghainum = UIDynamicData.hurtTotalInfo[playerId]
    local shanghaiText
    if shanghainum > 10000 then
        shanghaiText = math.floor((shanghainum/10000)) .. "万"
    else
        shanghaiText = shanghainum
    end

    elem:GetWidgetProxyByName("TextLabel(1144)"):GetText():SetContent(shanghaiText)



    local heroInfo = LuaCallCs_Data.GetHeroInfoByActorID(actorID)
    local icon = "UGUI/Sprite/Dynamic/BustCircle/" .. heroInfo.imagePath
    elem:GetWidgetProxyByName("scoreheroImg"):GetImage():SetRes(icon)

    elem:GetWidgetProxyByName("scoreplayerNameText"):GetText():SetContent(playerArr[index+1].playerName)

    -- 击败数
    elem:GetWidgetProxyByName("scorekillCount"):GetText():SetContent(G_GameDataMgr.PersistentData[playerId].SaasData.kill)

    elem:GetWidgetProxyByName("scorezhanli"):GetText():SetContent(UIDynamicData.zhanli[playerId])


    --技能
    local skill1 = LuaCallCs_Skill.GetSkillSlot(actorID, 8)
    if skill1 ~= nil then
        local SkillInfo1 = LuaCallCs_Skill.GetSkillInfo(skill1.curSkillID)
        local skillImg1 = SkillInfo1.iconPath
        elem:GetWidgetProxyByName("beidongskill"):GetImage():SetRes(skillImg1)
    else
        elem:GetWidgetProxyByName("beidongskill"):GetImage():SetRes("Texture/Sprite/kongcao.sprite")
    end

    local skill2 = LuaCallCs_Skill.GetSkillSlot(actorID, 6)
    if skill2 ~= nil then
        local SkillInfo2 = LuaCallCs_Skill.GetSkillInfo(skill2.curSkillID)
        local skillImg2 = SkillInfo2.iconPath
        elem:GetWidgetProxyByName("zhudongskill"):GetImage():SetRes(skillImg2)
    else
        elem:GetWidgetProxyByName("zhudongskill"):GetImage():SetRes("Texture/Sprite/kongcao.sprite")
    end
end


function chengjiuListElementOnEnable(LuaUIEvent)
    local selfpid = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    if UIDynamicData.itsOKInfo[selfpid] == false then
        return
    end

    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()

    if UIDynamicData.chengjiuUIList[selfpid] then
        if UIDynamicData.chengjiuUIList[selfpid][index+1] == 0 then
            elem:GetWidgetProxyByName("Image(1131)"):GetImage():SetRes("Texture/pay_che_icon_false.sprite")
            elem:GetWidgetProxyByName("TextLabel(1132)"):GetText():SetContent(UIConfigData.chengjiuTextInfo[index+1][1])
        else
            elem:GetWidgetProxyByName("Image(1131)"):GetImage():SetRes("Texture/pay_che_icon_true.sprite")
            elem:GetWidgetProxyByName("TextLabel(1132)"):GetText():SetContent(UIConfigData.chengjiuTextInfo[index+1][1])
        end
    end

    if UIConfigData.chengjiuTextInfo[index+1][2] == 1 then
        elem:GetWidgetProxyByName("Image(1136)"):SetActive(true)
    else
        elem:GetWidgetProxyByName("Image(1136)"):SetActive(false)
    end
end




local sxInfo = {
    {"物理攻击", "法术攻击", "", "",},
    {"最大生命", "冷却缩减", "", "",},
    {"物理防御", "法术防御", "", "",},
    {"攻击速度", "移动速度", "", "",},
    {"暴击几率", "暴击伤害", "", "",},
    {"生命恢复", "能量恢复", "", "",},
    {"物理穿透", "法术穿透", "", "",},
    {"物理吸血", "法术吸血", "", "",},
    {"伤害加成", "伤害减免", "", "",},
}




function refreshScoreBoard()
    local playerArr = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()
    scoreList:SetElementAmount(#playerArr)
end


function refreshShuXing(selfPlayerID,shjc,shmy, wlgj,zdsm,wlfy,gjsd,bjjl,smhf,hjct,wlxx,fsgj,lqsj,fsfy,ydsd,bjsh,nlhf,fsct,fsxx)
    math.randomseed(tostring(os.time()):reverse():sub(1,7))
    math.random()
    local zhanli = math.modf(0.2*zdsm+0.5*wlgj+2*fsgj+1.2*wlfy+1.2*fsfy+5000*lqsj/10000+2000*bjjl/10000+4000*bjsh/10000+1000*gjsd/10000+2000*wlxx/10000+800*fsxx/10000+13*fsct/10000+8*hjct/10000)

    UIDynamicData.zhanli[selfPlayerID] = zhanli


    if isMainPanelOpen == false then
        return
    end

    local gjsd1 = math.modf(gjsd/10000*100) .. "%"
    local bjjl1 = math.modf(bjjl/10000*100) .. "%"
    local hjct1 = math.modf(hjct/10000*100) .. "%"
    local wlxx1 = math.modf(wlxx/10000*100) .. "%"
    local lqsj1 = math.modf(lqsj/10000*100) .. "%"
    local bjsh1 = math.modf(bjsh/10000*100) .. "%"
    local fsct1 = math.modf(fsct/10000*100) .. "%"
    local fsxx1 = math.modf(fsxx/10000*100) .. "%"

    local shjc1 =  math.modf(shjc) .. "%"
    local shmy1 =  math.modf(shmy) .. "%"

    sxInfo[1][3] = wlgj
    sxInfo[1][4] = fsgj
    sxInfo[2][3] = zdsm
    sxInfo[2][4] = lqsj1
    sxInfo[3][3] = wlfy
    sxInfo[3][4] = fsfy
    sxInfo[4][3] = gjsd1
    sxInfo[4][4] = gjsd
    sxInfo[5][3] = bjjl1
    sxInfo[5][4] = bjsh1
    sxInfo[6][3] = smhf
    sxInfo[6][4] = nlhf
    sxInfo[7][3] = hjct1
    sxInfo[7][4] = fsct1
    sxInfo[8][3] = wlxx1
    sxInfo[8][4] = fsxx1
    sxInfo[9][3] = shjc1
    sxInfo[9][4] = shmy1

    if selfPlayerID == LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
        local msgLeft = wlgj .. "\n" .. zdsm .. "\n" .. wlfy .. "\n" .. gjsd1 .. "\n" .. bjjl1 .. "\n" .. smhf .. "\n" .. hjct1 .. "\n" .. wlxx1 .. "\n" .. shjc1

        local msgRight = fsgj .. "\n" .. lqsj1 .. "\n" .. fsfy .. "\n" .. ydsd .. "\n" .. bjsh1 .. "\n" .. nlhf .. "\n" .. fsct1 .. "\n" .. fsxx1 .. "\n" .. shmy1

        heroInfoTextLeft:SetContent(msgLeft)
        heroInfoTextRight:SetContent(msgRight)
        sxxxList:SetElementAmount(#sxInfo)
    end

    -- playerArr = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()
    -- playerArrLenght = #playerArr
    -- scoreList:SetElementAmount(playerArrLenght)
end





function sxOnEnabled(LuaUIEvent)
    local selfpid = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    if UIDynamicData.itsOKInfo[selfpid] == false then
        return
    end

    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()

    local _,num2=math.modf(index/2)
    printTb(num2)
    if  num2 == 0 then
        elem:GetWidgetProxyByName("Image(1158)"):GetImage():SetRes("Texture/Sprite/toumingtupian.sprite") -- zdc 把panel换成img。这里是拿不到img，可以再研究一下
    else
        elem:GetWidgetProxyByName("Image(1158)"):GetImage():SetRes("Texture/jifenbantiao.sprite")
    end

    local leftText1 = elem:GetWidgetProxyByName("TextLabel(1154)"):GetText()
    local leftText2 = elem:GetWidgetProxyByName("TextLabel(1155)"):GetText()

    local rightText1 = elem:GetWidgetProxyByName("TextLabel(1156)"):GetText()
    local rightText2 = elem:GetWidgetProxyByName("TextLabel(1157)"):GetText()


    leftText1:SetContent(sxInfo[index+1][1])
    leftText2:SetContent(sxInfo[index+1][3])
    rightText1:SetContent(sxInfo[index+1][2])
    rightText2:SetContent(sxInfo[index+1][4])
end











function openBtnClicked(LuaUIEvent)
    if isMainPanelOpen then
        formSelf:GetWidgetProxyByName("Panel(1067)"):SetActive(false)
        formSelf:GetWidgetProxyByName("Button(1115)"):SetActive(false)
        formSelf:GetWidgetProxyByName("Panel(1066)"):SetActive(false)
        mainPanel:SetActive(false)
        isMainPanelOpen = false
    else
        formSelf:GetWidgetProxyByName("Panel(1067)"):SetActive(true)
        formSelf:GetWidgetProxyByName("Button(1115)"):SetActive(true)
        formSelf:GetWidgetProxyByName("Panel(1066)"):SetActive(true)
        mainPanel:SetActive(true)
        isMainPanelOpen = true
        -- 刷新UI
        refreshScoreBoard()
    end
end


function scoreClosed(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    formSelf:GetWidgetProxyByName("Panel(1067)"):SetActive(false)
    formSelf:GetWidgetProxyByName("Button(1115)"):SetActive(false)
    formSelf:GetWidgetProxyByName("Panel(1066)"):SetActive(false)
    mainPanel:SetActive(false)
    isMainPanelOpen = false
end


function fightBtnClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")


    if nowchoose ~= "对阵属性" then
        nowchoose = "对阵属性"
        duizhenBtn:GetImage():SetRes("Texture/Sprite/cebian.sprite")
        chengjiuBtn:GetImage():SetRes("Texture/Sprite/toumingtupian.sprite")
        heroBtn:GetImage():SetRes("Texture/Sprite/toumingtupian.sprite")

        chengjiuPanel:SetActive(false)
        heroPanel:SetActive(false)
        duizhenPanel:SetActive(true)
        playerArr = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()
        playerArrLenght = #playerArr
        scoreList:SetElementAmount(playerArrLenght)
    end
    playerArr = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()
    playerArrLenght = #playerArr
    scoreList:SetElementAmount(playerArrLenght)
end


function heroBtnClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")

    if nowchoose ~= "英雄属性" then
        nowchoose = "英雄属性"
        heroBtn:GetImage():SetRes("Texture/Sprite/cebian.sprite")
        duizhenBtn:GetImage():SetRes("Texture/Sprite/toumingtupian.sprite")
        chengjiuBtn:GetImage():SetRes("Texture/Sprite/toumingtupian.sprite")

        duizhenPanel:SetActive(false)
        chengjiuPanel:SetActive(false)
        heroPanel:SetActive(true)
    end
end

function chengjiuClicked(LuaUIEvent)
    LuaCallCs_Sound.PlaySoundEvent("Play_Chess_UI_Click_01")
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID

    if UIDynamicData.itsOKInfo[selfPlayerID] == false then
        return
    end

    if nowchoose ~= "成就信息" then
        nowchoose = "成就信息"
        chengjiuBtn:GetImage():SetRes("Texture/Sprite/cebian.sprite")
        heroBtn:GetImage():SetRes("Texture/Sprite/toumingtupian.sprite")
        duizhenBtn:GetImage():SetRes("Texture/Sprite/toumingtupian.sprite")

        duizhenPanel:SetActive(false)
        heroPanel:SetActive(false)
        chengjiuPanel:SetActive(true)

        chengjiuList:SetElementAmount(#UIDynamicData.chengjiuUIList[selfPlayerID])
    end
end

function ScoreboardOnClose()
    formSelf = nil
    mainPanel = nil
    scoreList = nil
    isMainPanelOpen = false
    duizhenPanel = nil
    heroPanel = nil
    heroInfoTextLeft = nil
    heroInfoTextRight = nil
    nowchoose = "对阵属性"  -- 对阵属性，英雄属性
    duizhenBtn = nil
    heroBtn = nil
    chengjiuPanel = nil
    chengjiuList = nil
    chengjiuBtn = nil
    sxxxList = nil
end
