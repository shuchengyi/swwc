require "OnlineMode/EventLoop.lua"
json = require "json.lua"


local G_GameDataMgr = {}
local GameData = {}
local AllPlayerPersistentData = {}
local G_EventLoop = {}

--EventLoop
G_EventLoop = EventLoop.new("1")
G_GameDataMgr.GameData = GameData
G_GameDataMgr.PersistentData = AllPlayerPersistentData
G_GameDataMgr.EventLoop = G_EventLoop

local heroIDList = {
    -- 105,106,107,108,109,110,
    105,
}



function Split(szFullString, szSeparator)
	local nFindStartIndex = 1
	local nSplitIndex = 1
	local nSplitArray = {}
	while true do
	   local nFindLastIndex = string.find(szFullString, szSeparator, nFindStartIndex)
	   if not nFindLastIndex then
			nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, string.len(szFullString))
			break
      end
      nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, nFindLastIndex - 1)
      nFindStartIndex = nFindLastIndex + string.len(szSeparator)
      nSplitIndex = nSplitIndex + 1
	end
	return nSplitArray
end


local modeNameInfo = {
    ["swwc_nandu_1"] = 1,
    ["swwc_nandu_2"] = 2,
    ["swwc_nandu_3"] = 3,
    ["swwc_nandu_4"] = 4,
    ["swwc_nandu_5"] = 5,
    ["swwc_nandu_6"] = 6,
    ["swwc_nandu_7"] = 7,
    ["swwc_nandu_8"] = 8,
}

function G_GameDataMgr.OnStartOperation(customOperation, initFlag)
    local selfPlayer = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo()
    LuaCallCs_Common.Log("initFlag :")
    LuaCallCs_Common.Log(initFlag)
    LuaCallCs_Common.Log("customOperation: ")
    LuaCallCs_Common.Log(customOperation)
    if initFlag == 1 then
        --发送默认数据到服务器中
        LuaCallCs_Common.Log("第一次进入： " .. customOperation)
        local modeName = customOperation
        local nandu = modeNameInfo[modeName]
        SendDefaultCustomOperation(nandu)

        LuaCallCs_UI.CloseForm("UI/OnlineMode/MainForm.uixml")
        LuaCallCs_UI.OpenForm("UI/OnlineMode/OperationForm.uixml")

    else
        --如果是重连进入
        LuaCallCs_Common.Log("重连进入： ")
        local newData = json.decode(customOperation)  -- 倒计时秒数要上传的，不然重连之后秒数不同步，啥锁定状态也要研究一下
        printTb(newData)
        --还原最新数据
        for i = 1, #newData.playerInfos do
            GameData.playerInfos[i] = newData.playerInfos[i]
            -- if selfPlayerID == GameData.playerInfos[i].playerID then
            --     --如果用户已经确认过,停止UI监听
            --     if GameData.playerInfos[i].confirmed == 1 then
            --         sendEvent = true
            --     end
            -- end
        end
        GameData.nandu = newData.nandu
        UIDynamicData.nowNanDu = GameData.nandu
        GameData.time = newData.time
        LuaCallCs_Common.Log("重连进入： " .. UIConfigData.levelName)
        LuaCallCs_Common.Log("重连进入： " .. GameData.time)
        LuaCallCs_Common.Log("重连进入： " .. GameData.nandu)

        LuaCallCs_UI.CloseForm("UI/OnlineMode/MainForm.uixml")
        LuaCallCs_UI.OpenForm("UI/OnlineMode/OperationForm.uixml") --zdc
        -- LuaCallCs_UGCStateDriver.SendCustomOperationCompleted("main_Level")
    end
end


function OnReconnect(playerID) -- 游戏重连
    LuaCallCs_Common.Log("OnReconnect重连进入： " .. playerID)
end


function OnDisconnect(palyerID)
    LuaCallCs_Common.Log("断线玩家PID： " .. palyerID)
end



--开始loading之后,开始准备游戏内使用的玩家自定义数据
function G_GameDataMgr.OnPersistentDataInBattleIsReady()                                     -- 这个地方重连的时候很重要
    LuaCallCs_Common.Log("开始loading，开始准备游戏内使用的玩家自定义数据")
    --获取所有玩家的PlayerInfo
    local playerArr = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()
    local playerArrLenght = #playerArr
    --循环
    for i = 1,playerArrLenght do
        local PID = playerArr[i].playerID

        local PlayerPersistentData = {}
        PlayerPersistentData.FixedFormatData  = LuaCallCs_PersistentDataInBattle.GetFixedFormatDataInBattle(PID)
        PlayerPersistentData.CustomizeDataIntArr = LuaCallCs_PersistentDataInBattle.GetCustomizeDataIntArrInBattle(PID)
        PlayerPersistentData.CustomizeDataStringArr = LuaCallCs_PersistentDataInBattle.GetCustomizeDataStringArrInBattle(PID)

        PlayerPersistentData.ReportData = ""
        PlayerPersistentData.SaasData = {}
        PlayerPersistentData.SaasData.kill = 0 -- 杀敌数(实加了)
        PlayerPersistentData.SaasData.iswin = 0 -- 是否胜利 zdc可删
        PlayerPersistentData.SaasData.pickitemcnt = 0 -- 拾取数量 zdc可删
        PlayerPersistentData.SaasData.jifen = 0 -- 积分 zdc可删

        PlayerPersistentData.SaasData.winCnt = 0 -- 胜利场次 √
        PlayerPersistentData.SaasData.playCnt = 0 -- 游戏场次 √

        PlayerPersistentData.SaasData.mvpCnt = 0 -- MVP次数 √
        PlayerPersistentData.SaasData.zuDuiCnt = 0 -- 组队场次
        PlayerPersistentData.SaasData.zuDuiWinCnt = 0 -- 组队胜利场次
        PlayerPersistentData.SaasData.lcyygCnt = 0 -- 挑战粮草押运官次数 √
        PlayerPersistentData.SaasData.mdzlCnt = 0 -- 挑战魔袋长老次数 √
        PlayerPersistentData.SaasData.symwCnt = 0 -- 挑战深渊魔王次数 √

        LuaCallCs_Common.Log("开始loading之后,开始准备游戏内使用的 玩家自定义数据，saas数据同步到本地了++++")
        AllPlayerPersistentData[PID] = PlayerPersistentData
    end
    G_GameDataMgr.PersistentData = AllPlayerPersistentData
end



-- 在saas数据中更新杀敌数
function addKillCount(playerID)
    --获取所有玩家的PlayerInfo
    local playerArr = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()
    local playerArrLenght = #playerArr
    --循环
    for i = 1,playerArrLenght do
        local PID = playerArr[i].playerID
        if PID == playerID then
            G_GameDataMgr.PersistentData[PID].SaasData.kill = G_GameDataMgr.PersistentData[PID].SaasData.kill + 1
        end
    end
end


--发送默认的自定义操作数据(也是开局数据)
function SendDefaultCustomOperation(nandu)
    --一定要先发送默认数据, 这样玩家不进行任何操作也能开始游戏
    ----初始化数据
	local playerArr = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()
    local playerArrLenght = #playerArr
    --需要准备的数据
    for i = 1, playerArrLenght do
        local playerBattleInfo = {}
        playerBattleInfo.playerID = playerArr[i].playerID
        math.randomseed(tostring(os.time()):reverse():sub(1,7))
        math.random()
        playerBattleInfo.heroID = heroIDList[math.random(1,#heroIDList)];
        playerBattleInfo.SelectedHero = nil
        playerBattleInfo.confirmed = false
        playerBattleInfo.bs = 1
        -- playerBattleInfo.playerName = playerArr[i].playerName
        -- LuaCallCs_Common.Log("playerName:zdc :    ===")
        -- LuaCallCs_Common.Log(playerBattleInfo.playerName)
        GameData.playerInfos[i] = playerBattleInfo
    end
    --初始化地图数据
    GameData.nandu = nandu
    UIDynamicData.nowNanDu = nandu
    GameData.time = 60.0000000000
	--对数据序列化为json格式
	operatData = json.encode(GameData)
	--发送数据到服务器
    LuaCallCs_UGCStateDriver.SendFullDataBuf(operatData)
    G_GameDataMgr.GameData = GameData
end


local qd = 0
function G_GameDataMgr.OnReceiveOperateCmd(cmdInfo)
    LuaCallCs_Common.Log("run in G_GameDataMgr.OnReceiveOperateCmd")
    --切换英雄
    local playerArr = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()
    local playerArrLenght = #playerArr
    if cmdInfo.cmdType == 1 then
        for i = 1, playerArrLenght do
            if GameData.playerInfos[i].playerID == cmdInfo.playerID then
                GameData.playerInfos[i].heroID = cmdInfo.heroID
                GameData.playerInfos[i].SelectedHero = cmdInfo.heroID
                GameData.playerInfos[i].confirmed = cmdInfo.confirmed
                GameData.playerInfos[i].index = cmdInfo.index
            end
        end
        --发送刷新UI事件
        G_EventLoop:DispatchEvent("RefreshUI")
    --切换地图
    elseif (cmdInfo.cmdType == 2) then
        GameData.levelName = cmdInfo.levelName
    elseif cmdInfo.cmdType == 3 then
        for i = 1, playerArrLenght do
            if GameData.playerInfos[i].playerID == cmdInfo.playerID then
                GameData.playerInfos[i].heroID = cmdInfo.heroID
                GameData.playerInfos[i].SelectedHero = cmdInfo.heroID
                GameData.playerInfos[i].confirmed = cmdInfo.confirmed

                qd =qd+1
                if qd >= playerArrLenght then
                    SetTime()
                end
            end
        end
        G_EventLoop:DispatchEvent("Confirmed")
    elseif cmdInfo.cmdType == 4 then
        GameData.time = cmdInfo.time
    elseif cmdInfo.cmdType == 5 then
        local h1 = cmdInfo.hero1
        local h2 = cmdInfo.hero2
        local h3 = cmdInfo.hero3
        local SelectedHero = cmdInfo.SelectedHero
        local suijiCnt = cmdInfo.suijiCnt

        for i = 1, playerArrLenght do
            if GameData.playerInfos[i].playerID == cmdInfo.playerID then
                GameData.playerInfos[i].h1 = h1
                GameData.playerInfos[i].h2 = h2
                GameData.playerInfos[i].h3 = h3
                GameData.playerInfos[i].SelectedHero = SelectedHero

                if suijiCnt then
                    GameData.playerInfos[i].suijiCnt = suijiCnt
                end
            end
        end
    elseif cmdInfo.cmdType == 6 then
        local bsy = cmdInfo.bsy
        for i = 1, playerArrLenght do
            if GameData.playerInfos[i].playerID == cmdInfo.playerID then
                GameData.playerInfos[i].bs = bsy
            end
        end
    end
    G_GameDataMgr.GameData = GameData
    printTb("G_GameDataMgr.GameData")
    printTb(cmdInfo.cmdType)
    printTb(G_GameDataMgr.GameData)
end


function G_GameDataMgr.OnOperateCmdReceiveDone() -- 每次运行结束之后自动上传开局 数据
    LuaCallCs_Common.Log("run in G_GameDataMgr.OnOperateCmdReceiveDone()")
    local OpData = json.encode(GameData)
    --上传全量数据
    LuaCallCs_UGCStateDriver.SendFullDataBuf(OpData)
    G_GameDataMgr.GameData = GameData
end


--当数据发生变化时(自动调用)
function OnFixedFormatDataChanged()
    --获取用户自身的数据
    local fixedData = LuaCallCs_PersistentData.GetFixedFormatData()
    --获取匹配分数(内置固定数据,用来决定玩家之间的匹配优先级)
end


--当数据发生变化时(自动调用)
function OnCustomizeIntAllDataChanged()
    --获取用户自身的数据 
   local customIntArr = LuaCallCs_PersistentData.GetCustomizeDataIntArr()
   local v = customIntArr[200]
end

--当数据发生变化时(自动调用)
function OnCustomizeStringAllDataChanged()
    --获取用户自身自定义数据
    local StringArr = LuaCallCs_PersistentData.GetCustomizeDataStringArr()
    local s = StringArr[5]
end





-- ========== 退出游戏后Saas数据处理/持久数据上报处理 Start ==========
--上传Saas的指标数据(自动调用)
function UploadSaasString(PlayerID)
    --只需要上传单局的数据, 指标数据会根据在后台配置的规则自动累加
    LuaCallCs_Common.Log("上传SaasData")
	local SaasString = json.encode(AllPlayerPersistentData[PlayerID].SaasData)
    printTb(SaasString)
    printTb(AllPlayerPersistentData[PlayerID].SaasData)

    return SaasString
end


function UploadFixedFormatData(playerID)
    return G_GameDataMgr.PersistentData[playerID].FixedFormatData
end

function UploadCustomizeIntArr(playerID)
    LuaCallCs_Common.Log("print intArrData start")

    local pid = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    LuaCallCs_Common.Log("本机pid： " .. pid)
    -- LuaCallCs_Common.Log(AllPlayerPersistentData[playerID].CustomizeDataIntArr[1])
    -- LuaCallCs_Common.Log(AllPlayerPersistentData[playerID].CustomizeDataIntArr[2])
    -- LuaCallCs_Common.Log(AllPlayerPersistentData[playerID].CustomizeDataIntArr[3])
    -- LuaCallCs_Common.Log(AllPlayerPersistentData[playerID].CustomizeDataIntArr[4])
    -- LuaCallCs_Common.Log(AllPlayerPersistentData[playerID].CustomizeDataIntArr[200])
    -- LuaCallCs_Common.Log("print intArrData end")
    return AllPlayerPersistentData[playerID].CustomizeDataIntArr
end


function UploadCustomizeStringArr(playerID)
    LuaCallCs_Common.Log("上传string+++++++++++++++")
    LuaCallCs_Common.Log(UIDynamicData.heroExString01Info[playerID])

    AllPlayerPersistentData[playerID].CustomizeDataStringArr[1] = G_GameDataMgr.PersistentData[playerID].CustomizeDataStringArr[1]
    AllPlayerPersistentData[playerID].CustomizeDataStringArr[2] = G_GameDataMgr.PersistentData[playerID].CustomizeDataStringArr[2]
    printTb("AllPlayerPersistentData[playerID].CustomizeDataStringArr[2]")
    printTb(AllPlayerPersistentData[playerID].CustomizeDataStringArr[2])

    return AllPlayerPersistentData[playerID].CustomizeDataStringArr
end

--上传单局战绩, 自动保存到战绩历史记录里面(服务器会自动比对,下发可靠数据)
function UploadGameReport(PlayerID)
    LuaCallCs_Common.Log("上传单局战绩")
    return G_GameDataMgr.PersistentData[PlayerID].ReportData
end






return G_GameDataMgr