UIDynamicData = UIDynamicData or {}
G_GameDataMgr = require"OnlineMode/G_GameDataMgr.lua"



-- 不需要跟GamePlay同步的数据 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
UIDynamicData.gameStartTimeInfo = {}
UIDynamicData.heroExString01Info = {}
UIDynamicData.jinbijiacheng = {}
UIDynamicData.allheroExLv = 0
UIDynamicData.timeDone = false
UIDynamicData.stringArrDataNew =  {}
UIDynamicData.tankeStringDataInfo = {}
UIDynamicData.zhanshiStringDataInfo = {}
UIDynamicData.fashiStringDataInfo = {}
UIDynamicData.sheshouStringDataInfo = {}
UIDynamicData.manjiHeroCnt = 0
UIDynamicData.GameOver = {}

UIDynamicData.addHreoSLDNum = {}
UIDynamicData.tyjyNum = {}
UIDynamicData.tianfuInfo = {}
UIDynamicData.intArrData = {}

UIDynamicData.GMGongGaoInfo = {
    --[[
        [playerID] = {
            ["第一条公告"]，
            ["第二条公告"]
        }
    ]]
}

UIDynamicData.itsOK = false
UIDynamicData.itsOKInfo = {}
UIDynamicData.playerLiveInfo = {}

UIDynamicData.fightDataInfo = {
	--[[
		[pid] = {
			{99, 10, 2, 9898989}, -- 1  -- 总场次，通关场次，MVP场次，最高伤害量
			{}, -- 2
			{}, -- 3
			{}, -- 4
			{}, -- 5
			{}, -- 6
			{}, -- 7
			{}, -- 8
			{}, -- 9
			{}, -- 10
		}
	]]
}

UIDynamicData.shangHaiPaiXuList = {}

UIDynamicData.addJiFenNum = {}

UIDynamicData.jidiActorID = 0
UIDynamicData.UIObject = nil

UIDynamicData.ciTiaoInfo = {}

UIDynamicData.shengWuShuXingInfo = {}

UIDynamicData.haveCiTiaoInfo = {}


UIDynamicData.sldTb = {}

UIDynamicData.mainFormsldTb = {}

UIDynamicData.allJinBi = {}

UIDynamicData.giveBaoShiInfo = {}







-- 不需要跟GamePlay同步的数据 end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


UIDynamicData.hurtTotalInfo = {}


UIDynamicData.playerShareInfo = {}

UIDynamicData.IsWin = false
UIDynamicData.boci = 0
-- UIDynamicData.PlayerInfo = {}


UIDynamicData.test = ""



UIDynamicData.time = 0


UIDynamicData.bigBossKillCount = 0
UIDynamicData.bigBossNeedKillCount = 0

UIDynamicData.zhanjiData = {}

UIDynamicData.nowNanDu = -1

UIDynamicData.CZUseInfo = {0, 5000} -- 重铸次数， 需求金币
UIDynamicData.CZChuiCnt = {}

UIDynamicData.grandListInfo = {}


UIDynamicData.modaiHaveInfo = {
    -- [playerID] = {1, false, 0, 50} -- 魔袋等级，魔袋是否存在，魔袋击杀数, 魔袋需要击杀数
}

-- UIDynamicData.shopBossBuyTimeInfo = {
--     ["jinbiguai"] = {0, -1}, -- 召唤次数， 召唤时间
--     ["diyuhuo"] = {0, -1},
--     ["jinkuang"] = {0, -1}
-- }

UIDynamicData.shopBossBuyTimeInfoNew = {
	-- [pid1] = {{0, -1},{0, -1},{0, -1}},
	-- [pid2] = {{0, -1},{0, -1},{0, -1}},
	-- [pid3] = {{0, -1},{0, -1},{0, -1}},
	-- [pid4] = {{0, -1},{0, -1},{0, -1}},
	-- ["jinbiguai"] = {0, -1}, -- 召唤次数， 召唤时间
    -- ["jinkuang"] = {0, -1},
    -- ["diyuhuo"] = {0, -1},
}

UIDynamicData.yaoshuiTimeInfo = {}


UIDynamicData.modaiLevel = 1
UIDynamicData.diyuhuoLevel = 1
UIDynamicData.jinbiguaiLevel = 1

UIDynamicData.challengeBossInfo = {
    --[[
        [playerID] = {{1,false},{1,false},{1,false}} -- 按索引分别是 金币怪，金矿怪，地狱火
    ]]
}


UIDynamicData.money = 0
UIDynamicData.fulldata = ""

UIDynamicData.deadCntInfo = {}


UIDynamicData.daojuInfo = {
    -- { 物品名, 图标路径，数量, tips }
    -- [PID] = {
    --     {"D级被动技能书", "Texture/Sprite/jjbd.sprite", 1, "随机获得一个D级被动技能"},
    --     {"D级主动技能书", "Texture/Sprite/jjzd.sprite", 1, "随机获得一个D级主动技能"}
    -- }

}


UIDynamicData.skillBookBuyCountList = {
    -- 0,0,0,0,0,0,0,0
}


UIDynamicData.chengjiuUIList = {}



UIDynamicData.heroEXstring = ""


UIDynamicData.isXSJCInfo = {}

UIDynamicData.zhanli = {}
UIDynamicData.challengeZhaungTaiInfo = {}
UIDynamicData.fumoZhuangTaiInfo = {}
UIDynamicData.baoshiZhuangPeiInfo = {}
UIDynamicData.baoShiCangKuInfo = {}

UIDynamicData.baoShiYeInfo = {}


function saveHeroExInfoFunc(PID)
	local sldJson = G_GameDataMgr.PersistentData[PID].CustomizeDataStringArr[1]
		if sldJson ~= "" then -- 不是初始化
			UIDynamicData.sldTb[PID] = json.decode(sldJson)

			for i=1, #UIConfigData.heroList do
				if UIDynamicData.sldTb[PID][tostring(UIConfigData.heroList[i])] == nil then
					UIDynamicData.sldTb[PID][tostring(UIConfigData.heroList[i])] = 0
				end
			end
		else
			for i=1, #UIConfigData.heroList do
				UIDynamicData.sldTb[PID][tostring(UIConfigData.heroList[i])] = 0
			end
		end
	-- end
end



function saveFightDataInfo(PID)
	local fightDataString = G_GameDataMgr.PersistentData[PID].CustomizeDataStringArr[2]
	LuaCallCs_Common.Log("fightDataString:  ")
	LuaCallCs_Common.Log(fightDataString)

	local listlen = #UIConfigData.nanduInfo + 1 -- 索引[1]是全局的fightData
	if #fightDataString <= 0 then
		for i=1,listlen do
			table.insert(UIDynamicData.fightDataInfo[PID], {0,0,0,0})
		end
	else
		local fightDataList = json.decode(fightDataString)
		LuaCallCs_Common.Log("fightDataList:  ")
		UIDynamicData.fightDataInfo[PID] = fightDataList
	end
end

function sendBaoShiTzsx(playerID)
	local allJiejiNum = 0
    local allDengjiNum = 0
	local bsyIndex = UIDynamicData.baoShiYeInfo[playerID]

    local baoshizhuangpeiinfo = UIDynamicData.baoshiZhuangPeiInfo[playerID][bsyIndex]
    for i=1, #baoshizhuangpeiinfo do
        if #baoshizhuangpeiinfo[i] ~= 0 then
            local jieji = baoshizhuangpeiinfo[i][1]
            local dengji = baoshizhuangpeiinfo[i][2]

            allJiejiNum = allJiejiNum + jieji
            allDengjiNum = allDengjiNum + dengji
        end
    end

	local msg = {}
	msg.cmd = "baoshiTZSX"
	msg.a = allJiejiNum
	msg.b = allDengjiNum
	LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg))
end

function sendRedBaoShiInfo(playerID)
	-- red1
	for i=1, 3 do
		local bsyIndex = UIDynamicData.baoShiYeInfo[playerID]
		printTb("UIDynamicData.baoshiZhuangPeiInfo[playerID][bsyIndex]")
		printTb(bsyIndex)

		printTb(UIDynamicData.baoshiZhuangPeiInfo[playerID][bsyIndex])
		if #UIDynamicData.baoshiZhuangPeiInfo[playerID][bsyIndex][i] == 0 then
			break
		end

		local r1shuxing = UIDynamicData.baoshiZhuangPeiInfo[playerID][bsyIndex][i][3]
		local r1shuxingzhi = UIDynamicData.baoshiZhuangPeiInfo[playerID][bsyIndex][i][4]

		local msg = {}
		msg.cmd = "baoshi"
		msg.a = 1
		msg.b = r1shuxing
		msg.c = r1shuxingzhi
		LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg))
	end
end



function sendBlueBaoShiInfo(playerID)
	for i=4, 6 do
		local bsyIndex = UIDynamicData.baoShiYeInfo[playerID]
		if #UIDynamicData.baoshiZhuangPeiInfo[playerID][bsyIndex][i] == 0 then
			break
		end

		local r1shuxing = UIDynamicData.baoshiZhuangPeiInfo[playerID][bsyIndex][i][3]
		local r1shuxingzhi = UIDynamicData.baoshiZhuangPeiInfo[playerID][bsyIndex][i][4]


		local msg = {}
		msg.cmd = "baoshi"
		msg.a = 2
		msg.b = r1shuxing
		msg.c = r1shuxingzhi
		LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg))
	end
end



function sendpurpleBaoShiInfo(playerID)
	for i=7, 9 do
		local bsyIndex = UIDynamicData.baoShiYeInfo[playerID]
		if #UIDynamicData.baoshiZhuangPeiInfo[playerID][bsyIndex][i] == 0 then
			break
		end

		local r1shuxing = UIDynamicData.baoshiZhuangPeiInfo[playerID][bsyIndex][i][3]
		local r1shuxingzhi = UIDynamicData.baoshiZhuangPeiInfo[playerID][bsyIndex][i][4]


		local msg = {}
		msg.cmd = "baoshi"
		msg.a = 3
		msg.b = r1shuxing
		msg.c = r1shuxingzhi
		LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg))
	end
end


local GreenBaoShishuxingtypeInfo = {
	[2] = 2,
	[3] = 3,
	[4] = 4,
}


function sendGreenBaoShiInfo(playerID)
	for i=10, 12 do
		local bsyIndex = UIDynamicData.baoShiYeInfo[playerID]
		if #UIDynamicData.baoshiZhuangPeiInfo[playerID][bsyIndex][i] == 0 then
			break
		end

		local r1shuxing = UIDynamicData.baoshiZhuangPeiInfo[playerID][bsyIndex][i][3]
		local r1shuxingzhi = UIDynamicData.baoshiZhuangPeiInfo[playerID][bsyIndex][i][4]


		local msg = {}
		msg.cmd = "baoshi"
		msg.a = 4
		msg.b = r1shuxing
		msg.c = r1shuxingzhi
		LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg))
	end
end





function UIInit(playerID, nandu, cfgID, baoShiYe)
	UIDynamicData.itsOKInfo[playerID] = false
	UIDynamicData.nowNanDu = nandu

	UIDynamicData.gameStartTimeInfo[playerID] = os.time()

	UIDynamicData.daojuInfo[playerID] = {}
    UIDynamicData.grandListInfo[playerID] = {{1,1,false},{1,1,false},{1,1,false}} -- {1级， 零阶， boss是否存活}
    UIDynamicData.challengeBossInfo[playerID] = {{1,false},{1,false},{1,false}}
    UIDynamicData.GMGongGaoInfo[playerID] = {}
	UIDynamicData.modaiHaveInfo[playerID] = {1, false, 0, 50} -- 魔袋等级，魔袋是否存在，魔袋击杀数, 魔袋需要击杀数
	UIDynamicData.daojuInfo[playerID] = {}
	UIDynamicData.shopBossBuyTimeInfoNew[playerID] = {{0, -1},{1, 0},{1, 0}} -- {{金币怪召唤次数， 召唤时间}, {金矿怪召唤次数，召唤时间}，{地狱火召唤次数，召唤时间}}
	UIDynamicData.yaoshuiTimeInfo[playerID] = {{0, -1},{0, -1}} -- {{回蓝药水使用次数，使用时间}, {恢复药水使用次数，使用时间}}
	UIDynamicData.skillBookBuyCountList[playerID] = {0,0,0,0,0,0,0,0}
	UIDynamicData.CZChuiCnt[playerID] = 0
	UIDynamicData.playerLiveInfo[playerID] = true
	UIDynamicData.deadCntInfo[playerID] = 0
	UIDynamicData.jinbijiacheng[playerID] = 0
	UIDynamicData.heroExString01Info[playerID] = ""
	UIDynamicData.chengjiuUIList[playerID] = {}
	UIDynamicData.fightDataInfo[playerID] = {}
	UIDynamicData.hurtTotalInfo[playerID] = 0
	UIDynamicData.addJiFenNum[playerID] = 0
	UIDynamicData.ciTiaoInfo[playerID] = {}
	UIDynamicData.playerShareInfo[playerID] = 0
	UIDynamicData.shengWuShuXingInfo[playerID] = {0,0,0} -- 暴击 吸血 移速
	UIDynamicData.challengeZhaungTaiInfo[playerID] = {0,0,0} -- 金币，金矿，地狱火  状态
	UIDynamicData.fumoZhuangTaiInfo[playerID] = {0,0}
	UIDynamicData.addHreoSLDNum[playerID] = 0
	UIDynamicData.tyjyNum[playerID] = 0
	UIDynamicData.tianfuInfo[playerID] = {}
	UIDynamicData.haveCiTiaoInfo[playerID] = {}
	UIDynamicData.sldTb[playerID] = {}
	UIDynamicData.allJinBi[playerID] = 0
	UIDynamicData.isXSJCInfo[playerID] = 1
	UIDynamicData.baoShiYeInfo[playerID] = baoShiYe
	UIDynamicData.giveBaoShiInfo[playerID] = {}

	UIDynamicData.GameOver[playerID] = false

	local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID

	saveFightDataInfo(playerID)

	saveHeroExInfoFunc(playerID)


	-- 绑定一下金币变化事件
	LuaCallCs_UI.RegisterGameEventListenerForLua(enGameEventForLua.Event_ActorGoldCoinChange, LuaCallCs_Battle.GetHostActorID(), actorGoldCoinChange)


	if G_GameDataMgr.PersistentData[playerID].CustomizeDataIntArr[1] <= 2 then
		UIDynamicData.isXSJCInfo[playerID] = 1
	else
		UIDynamicData.isXSJCInfo[playerID] = 0
	end



	local baoshiZhuangPeiInfoString = G_GameDataMgr.PersistentData[playerID].CustomizeDataStringArr[5]
	if baoshiZhuangPeiInfoString == "" then
		UIDynamicData.baoshiZhuangPeiInfo[playerID] = {
			{-- 第一页
				{},{},{}, -- 红
				{},{},{}, -- 蓝
				{},{},{}, -- 紫
				{},{},{}  -- 绿
			},
			{{},{},{},{},{},{},{},{},{},{},{},{}},
			{{},{},{},{},{},{},{},{},{},{},{},{}},
		}
		-- local s = json.encode(UIDynamicData.baoshiZhuangPeiInfo[playerID])
		-- LuaCallCs_PersistentData.SetCustomizeDataStringArr({4}, {s})
	else
		UIDynamicData.baoshiZhuangPeiInfo[playerID] = json.decode(baoshiZhuangPeiInfoString)
	end

	local baoShiCangKuInfoString = G_GameDataMgr.PersistentData[playerID].CustomizeDataStringArr[4]
	if baoShiCangKuInfoString == "" then
		UIDynamicData.baoShiCangKuInfo[playerID] = {}
		-- local s = json.encode(UIDynamicData.baoShiCangKuInfo[playerID])
		-- LuaCallCs_PersistentData.SetCustomizeDataStringArr({3}, {s})
	else
		UIDynamicData.baoShiCangKuInfo[playerID] = json.decode(baoShiCangKuInfoString)
	end

	if playerID == selfPlayerID then
		local msg = {}
		msg.cmd = "nL"
		msg.cfgID = cfgID
		msg.type = LuaCallCs_Data.GetHeroInfo(cfgID).energyType
		LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg))

		local jifen = G_GameDataMgr.PersistentData[playerID].CustomizeDataIntArr[4]
		local nanduLv = G_GameDataMgr.PersistentData[playerID].CustomizeDataIntArr[3]
		local manjiHeroCnt = G_GameDataMgr.PersistentData[playerID].CustomizeDataIntArr[5]
		local allheroExCnt = G_GameDataMgr.PersistentData[playerID].CustomizeDataIntArr[6]

		local nowHeroExNum = UIDynamicData.sldTb[playerID][tostring(cfgID)]
		printTb("UIDynamicData.sldTb")
		printTb(UIDynamicData.sldTb)
		-- LuaCallCs_Common.Log(nowHeroExNum)

		local nowHeroLevel
		if nowHeroExNum >= UIConfigData.heroExLevelInfo[5] then
			nowHeroLevel = 5
		elseif nowHeroExNum >= UIConfigData.heroExLevelInfo[4] then
			nowHeroLevel = 4
		elseif nowHeroExNum >= UIConfigData.heroExLevelInfo[3] then
			nowHeroLevel = 3
		elseif nowHeroExNum >= UIConfigData.heroExLevelInfo[2] then
			nowHeroLevel = 2
		elseif nowHeroExNum >= UIConfigData.heroExLevelInfo[1] then
			nowHeroLevel = 1
		elseif nowHeroExNum >= 0 then
			nowHeroLevel = 0
		end

		local allheroExLv = 0

		for i = 1, #UIConfigData.chengjiuInfo["英雄总等级"] do
			if allheroExCnt >= UIConfigData.chengjiuInfo["英雄总等级"][i][1] then
				allheroExLv = UIConfigData.chengjiuInfo["英雄总等级"][i][2]
			else
				break
			end
		end

		local msg1 = {}
		msg1.cmd = "chengjiu"
		msg1.a = jifen
		msg1.b = nanduLv
		msg1.c = manjiHeroCnt
		msg1.d = allheroExLv
		msg1.e = nowHeroLevel
		LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg1))

		-- --------------------------------------------
		sendRedBaoShiInfo(playerID)
		sendBlueBaoShiInfo(playerID)
		sendpurpleBaoShiInfo(playerID)
		sendGreenBaoShiInfo(playerID)

		sendBaoShiTzsx(playerID)

		setchengjiuUIList(playerID,jifen,nanduLv,manjiHeroCnt,allheroExLv,nowHeroLevel)
	end



	UIDynamicData.itsOKInfo[playerID] = true
	if playerID == selfPlayerID then
		shopAppear()
		monsterCntAppear()

		if UIDynamicData.isXSJCInfo[playerID] == 1 then
			XSJCFunc(1)
		end
	end



	LuaCallCs_Common.Log("uicallback_UIInit")
end


--[[
UIDynamicData.chengjiuUIList = {
	[pid] = {0,1,0,0,1,0,1,0,1,1,1,1,0,0,0,0,1,1......}
}
]]

function setchengjiuUIList(playerID,jifen,nanduLv,manjiHeroCnt,allheroExLv,nowHeroLv)
	local jifenIndex = 1
	local nanduIndex = jifenIndex + #UIConfigData.chengjiuInfo["积分"]
	local nowHeroLevelIndex = nanduIndex + #UIConfigData.chengjiuInfo["通关难度"]
	local manjiHeroCntIndex = nowHeroLevelIndex + #UIConfigData.chengjiuInfo["英雄等级"]
	local allheroExLvIndex = manjiHeroCntIndex + #UIConfigData.chengjiuInfo["5星英雄数量"]


	local jifenLv = 0
	local manjiHeroCntLv = 0

    for i = 1, #UIConfigData.chengjiuInfo["积分"] do
        if jifen >= UIConfigData.chengjiuInfo["积分"][i][1] then
            jifenLv = UIConfigData.chengjiuInfo["积分"][i][2]
        else
            break
        end
    end

	for i = 1, #UIConfigData.chengjiuInfo["5星英雄数量"] do
		if manjiHeroCnt >= UIConfigData.chengjiuInfo["5星英雄数量"][i][1] then
			manjiHeroCntLv = UIConfigData.chengjiuInfo["5星英雄数量"][i][2]
		else
			break
		end
	end



	local Index = 0
	-- 开始制作成就列表信息
	for i=jifenIndex, (jifenIndex+#UIConfigData.chengjiuInfo["积分"]-1) do
		Index = Index + 1
		if jifenLv >= Index then
			UIDynamicData.chengjiuUIList[playerID][i] = 1
		else
			UIDynamicData.chengjiuUIList[playerID][i] = 0
		end
	end
	Index = 0

	for i=nanduIndex, (nanduIndex + #UIConfigData.chengjiuInfo["通关难度"]-1) do
		Index = Index + 1
		if nanduLv >= Index then -- zdc 触发成就的难度下限等级
			UIDynamicData.chengjiuUIList[playerID][i] = 1
		else
			UIDynamicData.chengjiuUIList[playerID][i] = 0
		end
	end
	Index = 0
	for i=nowHeroLevelIndex, (nowHeroLevelIndex + #UIConfigData.chengjiuInfo["英雄等级"]-1) do
		Index = Index + 1
		if nowHeroLv >= Index then
			UIDynamicData.chengjiuUIList[playerID][i] = 1
		else
			UIDynamicData.chengjiuUIList[playerID][i] = 0
		end
	end
	Index = 0
	for i=manjiHeroCntIndex, (manjiHeroCntIndex + #UIConfigData.chengjiuInfo["5星英雄数量"]-1) do
		Index = Index + 1
		if manjiHeroCntLv >= Index then
			UIDynamicData.chengjiuUIList[playerID][i] = 1
		else
			UIDynamicData.chengjiuUIList[playerID][i] = 0
		end
	end
	Index = 0
	for i=allheroExLvIndex, (allheroExLvIndex + #UIConfigData.chengjiuInfo["英雄总等级"]-1) do
		Index = Index + 1
		if allheroExLv >= Index then
			UIDynamicData.chengjiuUIList[playerID][i] = 1
		else
			UIDynamicData.chengjiuUIList[playerID][i] = 0
		end
	end
end


-- UIDynamicData.allJinBi = {}

function actorGoldCoinChange(evtParam)
	if LuaCallCs_Battle.GetHostActorID() ~= evtParam.ActorId then
		return
	end
	local actorID = evtParam.ActorId  -- ActorID
	local nowJinBi = evtParam.ParamInt1  -- 当前金币数量
	local oldJinBi = evtParam.ParamInt2 -- 变化前的金币数量
	UIDynamicData.money = nowJinBi
	refresh_shop_item(-1)

	-- if oldJinBi <= nowJinBi then
	-- 	UIDynamicData.allJinBi[actorID] = UIDynamicData.allJinBi[actorID] + nowJinBi - oldJinBi
	-- end
end



function wp_CallBack(pid,isSucc)
	if isSucc == true then
		if pid == LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
			LuaCallCs_FightUI.CreateFloatText(6, "武器附魔成功", 6, LuaCallCs_Battle.GetHostActorWorldPos(), true, 0, 3000, -0.3)
		end

		UIDynamicData.grandListInfo[pid][1][1] = UIDynamicData.grandListInfo[pid][1][1] + 1
		addGMGongGaoInfo(pid, "wqfm", true)
	else
		if pid == LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
			LuaCallCs_FightUI.CreateFloatText(0, "武器附魔失败", 6, LuaCallCs_Battle.GetHostActorWorldPos(), true, 0, 3000, -0.3)
		end

		addGMGongGaoInfo(pid, "wqfm", false)
	end

	if pid == LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
		shuaxinfumo(pid)
	end
end



function hj_CallBack(pid,isSucc)
	if isSucc == true then
		if pid == LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
			LuaCallCs_FightUI.CreateFloatText(6, "护甲附魔成功", 6, LuaCallCs_Battle.GetHostActorWorldPos(), true, 0, 3000, -0.3)
		end

		UIDynamicData.grandListInfo[pid][2][1] = UIDynamicData.grandListInfo[pid][2][1] + 1
		addGMGongGaoInfo(pid, "hjfm", true)
	else
		if pid == LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
			LuaCallCs_FightUI.CreateFloatText(0, "护甲附魔失败", 6, LuaCallCs_Battle.GetHostActorWorldPos(), true, 0, 3000, -0.3)
		end

		addGMGongGaoInfo(pid, "hjfm", false)
	end
	shuaxinfumo(pid)
end



function jz_CallBack(pid,isSucc)
	if isSucc == true then
		UIDynamicData.grandListInfo[pid][3][1] = UIDynamicData.grandListInfo[pid][3][1] + 1
		addGMGongGaoInfo(pid, "jzfm", true)
	else
		addGMGongGaoInfo(pid, "jzfm", false)
	end
end



function weapoBoss_CallBack(pid)
	UIDynamicData.grandListInfo[pid][1][3] = true
	LuaCallCs_Common.Log("weapoBoss_CallBack")
end



function hujiaBoss_CallBack(pid)
	UIDynamicData.grandListInfo[pid][2][3] = true
end



function jiezhiBoss_CallBack(pid)
	UIDynamicData.grandListInfo[pid][3][3] = true
end


function buySkillBook_callBack(pid, iscanBuy, isHave, num)
	if iscanBuy == false then
		addGMGongGaoInfo(pid, "bbm")
		return
	end

	UIDynamicData.skillBookBuyCountList[pid][num] = UIDynamicData.skillBookBuyCountList[pid][num] + 1
	refreshEquipInfo(pid)

	if num == 1 then
		if isHave == false then
			table.insert(UIDynamicData.daojuInfo[pid], {"D级被动技能书", "Texture/Sprite/Db.sprite", 1, "随机获得一个D级被动技能", 1}) -- 最后一个参数是num，前面一个是cnt
		else
			for index = 1, #UIDynamicData.daojuInfo[pid] do
				if UIDynamicData.daojuInfo[pid][index][1] == "D级被动技能书" then
					UIDynamicData.daojuInfo[pid][index][3] = UIDynamicData.daojuInfo[pid][index][3] + 1
				end
			end
		end
    elseif num == 5 then
        if isHave == false then
			table.insert(UIDynamicData.daojuInfo[pid], {"D级主动技能书", "Texture/Sprite/Dz.sprite", 1, "随机获得一个D级主动技能", 5})
		else
			for index = 1, #UIDynamicData.daojuInfo[pid] do
				if UIDynamicData.daojuInfo[pid][index][1] == "D级主动技能书" then
					UIDynamicData.daojuInfo[pid][index][3] = UIDynamicData.daojuInfo[pid][index][3] + 1
				end
			end
        end
    elseif num == 2 then
        if isHave == false then
			table.insert(UIDynamicData.daojuInfo[pid], {"C级被动技能书", "Texture/Sprite/Cb.sprite", 1, "随机获得一个C级被动技能", 2})
		else
			for index = 1, #UIDynamicData.daojuInfo[pid] do
				if UIDynamicData.daojuInfo[pid][index][1] == "C级被动技能书" then
					UIDynamicData.daojuInfo[pid][index][3] = UIDynamicData.daojuInfo[pid][index][3] + 1
				end
			end
        end
    elseif num == 6 then
        if isHave == false then
			table.insert(UIDynamicData.daojuInfo[pid], {"C级主动技能书", "Texture/Sprite/Cz.sprite", 1, "随机获得一个C级主动技能", 6})
		else
			for index = 1, #UIDynamicData.daojuInfo[pid] do
				if UIDynamicData.daojuInfo[pid][index][1] == "C级主动技能书" then
					UIDynamicData.daojuInfo[pid][index][3] = UIDynamicData.daojuInfo[pid][index][3] + 1
				end
			end
        end
    elseif num == 3 then
        if isHave == false then
			table.insert(UIDynamicData.daojuInfo[pid], {"B级被动技能书", "Texture/Sprite/Bb.sprite", 1, "随机获得一个B级被动技能", 3})
		else
			for index = 1, #UIDynamicData.daojuInfo[pid] do
				if UIDynamicData.daojuInfo[pid][index][1] == "B级被动技能书" then
					UIDynamicData.daojuInfo[pid][index][3] = UIDynamicData.daojuInfo[pid][index][3] + 1
				end
			end
        end
    elseif num == 7 then
        if isHave == false then
			table.insert(UIDynamicData.daojuInfo[pid], {"B级主动技能书", "Texture/Sprite/Bz.sprite", 1, "随机获得一个B级主动技能", 7})
		else
			for index = 1, #UIDynamicData.daojuInfo[pid] do
				if UIDynamicData.daojuInfo[pid][index][1] == "B级主动技能书" then
					UIDynamicData.daojuInfo[pid][index][3] = UIDynamicData.daojuInfo[pid][index][3] + 1
				end
			end
        end
    elseif num == 4 then
        if isHave == false then
			table.insert(UIDynamicData.daojuInfo[pid], {"A级被动技能书", "Texture/Sprite/Ab.sprite", 1, "随机获得一个A级被动技能", 4})
		else
			for index = 1, #UIDynamicData.daojuInfo[pid] do
				if UIDynamicData.daojuInfo[pid][index][1] == "A级被动技能书" then
					UIDynamicData.daojuInfo[pid][index][3] = UIDynamicData.daojuInfo[pid][index][3] + 1
				end
			end
        end
    elseif num == 8 then
        if isHave == false then
			table.insert(UIDynamicData.daojuInfo[pid], {"A级主动技能书", "Texture/Sprite/Az.sprite", 1, "随机获得一个A级主动技能", 8})
		else
			for index = 1, #UIDynamicData.daojuInfo[pid] do
				if UIDynamicData.daojuInfo[pid][index][1] == "A级主动技能书" then
					UIDynamicData.daojuInfo[pid][index][3] = UIDynamicData.daojuInfo[pid][index][3] + 1
				end
			end
		end
	end
	refreshEquipInfo(pid)
end


function touchSkillBook_callBack(pid, iscanBuy, isHave, num)
	if iscanBuy == false then
		addGMGongGaoInfo(pid, "bbm")
		return
	end

	if num == 1 then
		if isHave == false then
			table.insert(UIDynamicData.daojuInfo[pid], {"D级被动技能书", "Texture/Sprite/Db.sprite", 1, "随机获得一个D级被动技能", 1}) -- 最后一个参数是num，前面一个是cnt
		else
			for index = 1, #UIDynamicData.daojuInfo[pid] do
				if UIDynamicData.daojuInfo[pid][index][1] == "D级被动技能书" then
					UIDynamicData.daojuInfo[pid][index][3] = UIDynamicData.daojuInfo[pid][index][3] + 1
				end
			end
		end
    elseif num == 5 then
        if isHave == false then
			table.insert(UIDynamicData.daojuInfo[pid], {"D级主动技能书", "Texture/Sprite/Dz.sprite", 1, "随机获得一个D级主动技能", 5})
		else
			for index = 1, #UIDynamicData.daojuInfo[pid] do
				if UIDynamicData.daojuInfo[pid][index][1] == "D级主动技能书" then
					UIDynamicData.daojuInfo[pid][index][3] = UIDynamicData.daojuInfo[pid][index][3] + 1
				end
			end
        end
    elseif num == 2 then
        if isHave == false then
			table.insert(UIDynamicData.daojuInfo[pid], {"C级被动技能书", "Texture/Sprite/Cb.sprite", 1, "随机获得一个C级被动技能", 2})
		else
			for index = 1, #UIDynamicData.daojuInfo[pid] do
				if UIDynamicData.daojuInfo[pid][index][1] == "C级被动技能书" then
					UIDynamicData.daojuInfo[pid][index][3] = UIDynamicData.daojuInfo[pid][index][3] + 1
				end
			end
        end
    elseif num == 6 then
        if isHave == false then
			table.insert(UIDynamicData.daojuInfo[pid], {"C级主动技能书", "Texture/Sprite/Cz.sprite", 1, "随机获得一个C级主动技能", 6})
		else
			for index = 1, #UIDynamicData.daojuInfo[pid] do
				if UIDynamicData.daojuInfo[pid][index][1] == "C级主动技能书" then
					UIDynamicData.daojuInfo[pid][index][3] = UIDynamicData.daojuInfo[pid][index][3] + 1
				end
			end
        end
    elseif num == 3 then
        if isHave == false then
			table.insert(UIDynamicData.daojuInfo[pid], {"B级被动技能书", "Texture/Sprite/Bb.sprite", 1, "随机获得一个B级被动技能", 3})
		else
			for index = 1, #UIDynamicData.daojuInfo[pid] do
				if UIDynamicData.daojuInfo[pid][index][1] == "B级被动技能书" then
					UIDynamicData.daojuInfo[pid][index][3] = UIDynamicData.daojuInfo[pid][index][3] + 1
				end
			end
        end
    elseif num == 7 then
        if isHave == false then
			table.insert(UIDynamicData.daojuInfo[pid], {"B级主动技能书", "Texture/Sprite/Bz.sprite", 1, "随机获得一个B级主动技能", 7})
		else
			for index = 1, #UIDynamicData.daojuInfo[pid] do
				if UIDynamicData.daojuInfo[pid][index][1] == "B级主动技能书" then
					UIDynamicData.daojuInfo[pid][index][3] = UIDynamicData.daojuInfo[pid][index][3] + 1
				end
			end
        end
    elseif num == 4 then
        if isHave == false then
			table.insert(UIDynamicData.daojuInfo[pid], {"A级被动技能书", "Texture/Sprite/Ab.sprite", 1, "随机获得一个A级被动技能", 4})
		else
			for index = 1, #UIDynamicData.daojuInfo[pid] do
				if UIDynamicData.daojuInfo[pid][index][1] == "A级被动技能书" then
					UIDynamicData.daojuInfo[pid][index][3] = UIDynamicData.daojuInfo[pid][index][3] + 1
				end
			end
        end
    elseif num == 8 then
        if isHave == false then
			table.insert(UIDynamicData.daojuInfo[pid], {"A级主动技能书", "Texture/Sprite/Az.sprite", 1, "随机获得一个A级主动技能", 8})
		else
			for index = 1, #UIDynamicData.daojuInfo[pid] do
				if UIDynamicData.daojuInfo[pid][index][1] == "A级主动技能书" then
					UIDynamicData.daojuInfo[pid][index][3] = UIDynamicData.daojuInfo[pid][index][3] + 1
				end
			end
		end
	end
	refreshEquipInfo(pid)
end




function sellSkillBook_callBack(pid, isRemove, num)
	local skillBookName
    if num == 1 then
        skillBookName = "D级被动技能书"
    elseif num == 5 then
        skillBookName = "D级主动技能书"
    elseif num == 2 then
        skillBookName = "C级被动技能书"
    elseif num == 6 then
        skillBookName = "C级主动技能书"
    elseif num == 3 then
        skillBookName = "B级被动技能书"
    elseif num == 7 then
        skillBookName = "B级主动技能书"
    elseif num == 4 then
        skillBookName = "A级被动技能书"
    elseif num == 8 then
        skillBookName = "A级主动技能书"
	end

	for index = 1, #UIDynamicData.daojuInfo[pid] do
        if UIDynamicData.daojuInfo[pid][index][1] == skillBookName then
            if isRemove == false then
                UIDynamicData.daojuInfo[pid][index][3] = UIDynamicData.daojuInfo[pid][index][3] - 1
            else
                table.remove(UIDynamicData.daojuInfo[pid], index)
			end
			break
        end
	end
	refreshEquipInfo(pid)
end



function dellSkillBook_callBack(pid, isRemove, num)
	local skillBookName
    if num == 1 then
        skillBookName = "D级被动技能书"
    elseif num == 5 then
        skillBookName = "D级主动技能书"
    elseif num == 2 then
        skillBookName = "C级被动技能书"
    elseif num == 6 then
        skillBookName = "C级主动技能书"
    elseif num == 3 then
        skillBookName = "B级被动技能书"
    elseif num == 7 then
        skillBookName = "B级主动技能书"
    elseif num == 4 then
        skillBookName = "A级被动技能书"
    elseif num == 8 then
        skillBookName = "A级主动技能书"
	end

	for index = 1, #UIDynamicData.daojuInfo[pid] do
        if UIDynamicData.daojuInfo[pid][index][1] == skillBookName then
            if isRemove == false then
                UIDynamicData.daojuInfo[pid][index][3] = UIDynamicData.daojuInfo[pid][index][3] - 1
            else
                table.remove(UIDynamicData.daojuInfo[pid], index)
			end
			break
		end
	end
	refreshEquipInfo(pid)
end


function callHellFire_callBack(pid, callCnt) -- 地狱火
	UIDynamicData.challengeBossInfo[pid][3][1] = callCnt
    UIDynamicData.challengeBossInfo[pid][3][2] = true

	UIDynamicData.shopBossBuyTimeInfoNew[pid][3][1] = callCnt
	UIDynamicData.shopBossBuyTimeInfoNew[pid][3][2] = UIDynamicData.time

	G_GameDataMgr.PersistentData[pid].SaasData.symwCnt = G_GameDataMgr.PersistentData[pid].SaasData.symwCnt + 1
end


function callMoneyBoss_callBack(pid, callCnt) -- 金币
	UIDynamicData.challengeBossInfo[pid][1][1] = callCnt
    UIDynamicData.challengeBossInfo[pid][1][2] = true

	UIDynamicData.shopBossBuyTimeInfoNew[pid][1][1] = callCnt
	UIDynamicData.shopBossBuyTimeInfoNew[pid][1][2] = UIDynamicData.time

	G_GameDataMgr.PersistentData[pid].SaasData.lcyygCnt = G_GameDataMgr.PersistentData[pid].SaasData.lcyygCnt + 1
end



function callGoldOre_callBack(pid, callCnt) -- 金矿
	UIDynamicData.challengeBossInfo[pid][2][1] = callCnt
    UIDynamicData.challengeBossInfo[pid][2][2] = true

	UIDynamicData.shopBossBuyTimeInfoNew[pid][2][1] = callCnt
	UIDynamicData.shopBossBuyTimeInfoNew[pid][2][2] = UIDynamicData.time

	G_GameDataMgr.PersistentData[pid].SaasData.mdzlCnt = G_GameDataMgr.PersistentData[pid].SaasData.mdzlCnt + 1
end




function czEquip_callBack(pid)
	UIDynamicData.CZChuiCnt[pid] = UIDynamicData.CZChuiCnt[pid] - 1
end


function jiluActorDead_callBack(pid, isLive)
	UIDynamicData.playerLiveInfo[pid] = isLive
	UIDynamicData.deadCntInfo[pid] = UIDynamicData.deadCntInfo[pid] + 1
	refresh_shop_item()
end


function addCZC_callBack(pid, addCZCCnt)
	UIDynamicData.CZChuiCnt[pid] = UIDynamicData.CZChuiCnt[pid] + addCZCCnt
end


function timeTick_callBack()
	UIDynamicData.time = UIDynamicData.time + 1
end


function addExpendMoney(pid, num)
	if pid == LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
		LuaCallCs_FightUI.CreateFloatText(11, (0-num) .. "g", 6, LuaCallCs_Battle.GetHostActorWorldPos(), true, 0, 1000, -0.3)
	end
end




function shangHaiTongJi_callBack(pid, num)
	UIDynamicData.hurtTotalInfo[pid] = num

	-- local index = 1
	-- for playerID, allShangHai in pairs(UIDynamicData.hurtTotalInfo) do
	-- 	UIDynamicData.shangHaiPaiXuList[index] = {playerID, allShangHai}
	-- 	index = index + 1
	-- end



	-- table.sort(UIDynamicData.shangHaiPaiXuList, function (a,b)
	-- 	return (a[2] > b[2])
	-- end)
	if UIDynamicData.itsOKInfo[pid] == false then
		return
	end

	shuaxinshanghai()
end










function setJiTaActor(pid,actorID)
	if pid ~= LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
		return
	end
	UIDynamicData.jidiActorID = actorID
	UIDynamicData.UIObject = LuaCallCs_FightUI.Create3DText(
		"0/40波(0%)\n<color=#1E90FF>▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆▆</color>",
		BluePrint.UGC.UI.Core.Color.White,
		20,
		BluePrint.UGC.UI.Core.enFontType.GameFont,
		BluePrint.UGC.UI.Core.enFontStyle.Normal,
		BluePrint.UGC.UI.Core.enTextAlignment.MiddleCenter,
		1
	)

	local offset = BluePrint.UGC.UI.Core.Vector3(0, 4, 0)
	LuaCallCs_FightUI.SetFollowActor(UIDynamicData.UIObject, actorID , offset)
end


function SetJidiJiDuTiao(pid, boci)
	if pid ~= LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
		return
	end
	LuaCallCs_FightUI.Destroy3DUI(UIDynamicData.UIObject)
	local jindunum
	if boci ~= 0 then
		jindunum = math.floor(boci/40*100)
	else
		jindunum = 0
	end
	local text1 = boci .. "/40波(" .. jindunum .. "%)\n"
	local kuai1 = ""
	for i=1, boci do
		kuai1 = kuai1 .. "▆"
	end

	local kuai2 = ""
	for k=1,(40-boci) do
		kuai2 = kuai2 .. "▆"
	end
	local text2 = "<color=#00FF00>" .. kuai1 .. "</color><color=#1E90FF>" .. kuai2 .. "</color>"

	local allText = text1 .. text2


	UIDynamicData.UIObject = LuaCallCs_FightUI.Create3DText(
		allText,
		BluePrint.UGC.UI.Core.Color.White,
		20,
		BluePrint.UGC.UI.Core.enFontType.GameFont,
		BluePrint.UGC.UI.Core.enFontStyle.Normal,
		BluePrint.UGC.UI.Core.enTextAlignment.MiddleCenter,
		1
	)

	local offset = BluePrint.UGC.UI.Core.Vector3(0, 4, 0)
	LuaCallCs_FightUI.SetFollowActor(UIDynamicData.UIObject, UIDynamicData.jidiActorID , offset)
end





function setjyg3DdjsText(pid, actor)
	actor1 = actor


	if pid ~= LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
		return
	end
	ddd = LuaCallCs_FightUI.Create3DText(
		"30",
		BluePrint.UGC.UI.Core.Color.White,
		20,
		BluePrint.UGC.UI.Core.enFontType.GameFont,
		BluePrint.UGC.UI.Core.enFontStyle.Normal,
		BluePrint.UGC.UI.Core.enTextAlignment.MiddleCenter,
		1
	)

	local offset = BluePrint.UGC.UI.Core.Vector3(0, 4, 0)
	LuaCallCs_FightUI.SetFollowActor(UIDynamicData.UIObject, actorID , offset)

end







function addCiTiao_callBack(pid, ciTiaoText, ciTiaoLv, num1, num2)
	UIDynamicData.shengWuShuXingInfo[pid][1] = UIDynamicData.shengWuShuXingInfo[pid][1] + num1
	UIDynamicData.shengWuShuXingInfo[pid][2] = UIDynamicData.shengWuShuXingInfo[pid][2] + num2
	UIDynamicData.shengWuShuXingInfo[pid][3] = UIDynamicData.shengWuShuXingInfo[pid][3] + 15

	UIDynamicData.ciTiaoInfo[pid][ciTiaoText] = UIConfigData.ciTiaoInfo[ciTiaoText][ciTiaoLv+1]
	table.insert(UIDynamicData.haveCiTiaoInfo[pid], {ciTiaoText, UIConfigData.ciTiaoInfo[ciTiaoText][ciTiaoLv+1], ciTiaoLv})


	local text = "王城圣物Lv"
	text = text .. (UIDynamicData.challengeBossInfo[pid][3][1]-1) .. "\n\n"

	text = text .. "暴击+" .. UIDynamicData.shengWuShuXingInfo[pid][1] .. "%\n"
	text = text .. "吸血+" .. UIDynamicData.shengWuShuXingInfo[pid][2] .. "%\n"
	text = text .. "移速+" .. UIDynamicData.shengWuShuXingInfo[pid][3] .. "\n\n"

	for i=1, #UIDynamicData.haveCiTiaoInfo[pid] do
		local haveCiTiaoText = UIDynamicData.haveCiTiaoInfo[pid][i][1]
		local num = UIDynamicData.haveCiTiaoInfo[pid][i][2]
		local lv = UIDynamicData.haveCiTiaoInfo[pid][i][3]
		local citiaotext111 = UIConfigData.ciTiaoInfo[haveCiTiaoText][1] .. num .. "%"
		if lv == 1 then
			citiaotext111 = "<color=#00FF00>" .. citiaotext111 .. "</color>"
		elseif lv == 2 then
			citiaotext111 = "<color=#FFFF00>" .. citiaotext111 .. "</color>"
		elseif lv == 3 then
			citiaotext111 = "<color=#FF8C00>" .. citiaotext111 .. "</color>"
		end

		text = text .. citiaotext111 .. '\n'
	end

	if pid == LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
		LuaCallCs_FightUI.CreateFloatText(0, "+词条： " .. ciTiaoText, 6, LuaCallCs_Battle.GetHostActorWorldPos(), true, 0, 3000, -0.3)
		shuaxinUICiTiao(pid, text)
	end
end


function useTianfuFunc_callBack(pid, tianfuText)
	if pid == LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
		LuaCallCs_FightUI.CreateFloatText(0, "+天赋:" .. tianfuText, 6, LuaCallCs_Battle.GetHostActorWorldPos(), true, 0, 3000, -0.3)
	end
	table.insert(UIDynamicData.tianfuInfo[pid], tianfuText)
end


function piaozi1(pid, skillType, skillID)
	if pid == LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID then
		local skillTypeText
		if skillType == 0 then
			skillTypeText = "获得被动："
		else
			skillTypeText = "获得主动："
		end

		local skillName = LuaCallCs_Skill.GetSkillInfo(skillID).skillName


		LuaCallCs_FightUI.CreateFloatText(2, skillTypeText .. skillName, 6, LuaCallCs_Battle.GetHostActorWorldPos(), true, 0, 3000, -0.3)
	end
end

function piaozi2(pid, text)
	if pid == LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID and UIDynamicData.GameOver[pid] == false then
		LuaCallCs_FightUI.CreateFloatText(2, text, 6, LuaCallCs_Battle.GetHostActorWorldPos(), true, 0, 3000, -0.3)
	end
end


function allJinBi_callBack(pid, num)
	UIDynamicData.allJinBi[pid] = num
end


function init_callBack()
	local playerIDInfo = {}
	local allPlayerInfos = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()
    for i=1, #allPlayerInfos do
        local PID = allPlayerInfos[i].playerID
		printTb(allPlayerInfos[i].playerName)
		table.insert(playerIDInfo, {PID, allPlayerInfos[i].playerName})

		local t = {
			["pName"] = allPlayerInfos[i].playerName
		}

		local msg1 = {}
		msg1.cmd = "setName"
		msg1.pid = PID
		msg1.name = json.encode(t)
		LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg1))

		G_GameDataMgr.PersistentData[PID].SaasData.playCnt = 1 -- SaasData.playCnt 游戏场次
		if #allPlayerInfos > 1 then
			G_GameDataMgr.PersistentData[PID].SaasData.zuDuiCnt = 1
		end
    end

	local msg = {}
	msg.cmd = "init"
	-- msg.tb = playerIDInfo
	LuaCallCs_UGCStateDriver.SendCustomizeFrameCmd(json.encode(msg))



end


function UIDyDataFormClose()
	UIDynamicData.gameStartTimeInfo = {}
	UIDynamicData.heroExString01Info = {}
	UIDynamicData.jinbijiacheng = {}
	UIDynamicData.allheroExLv = 0
	UIDynamicData.timeDone = false
	UIDynamicData.stringArrDataNew = {}
	UIDynamicData.tankeStringDataInfo = {}
	UIDynamicData.zhanshiStringDataInfo = {}
	UIDynamicData.fashiStringDataInfo = {}
	UIDynamicData.sheshouStringDataInfo = {}
	UIDynamicData.manjiHeroCnt = 0
	UIDynamicData.GameOver = {}
	UIDynamicData.addHreoSLDNum = {}
	UIDynamicData.tyjyNum = {}
	UIDynamicData.tianfuInfo = {}
	UIDynamicData.intArrData = {}
	UIDynamicData.GMGongGaoInfo = {}
	UIDynamicData.itsOK = false
	UIDynamicData.itsOKInfo = {}
	UIDynamicData.playerLiveInfo = {}
	UIDynamicData.fightDataInfo = {}
	UIDynamicData.shangHaiPaiXuList = {}
	UIDynamicData.addJiFenNum = {}
	UIDynamicData.jidiActorID = 0
	UIDynamicData.UIObject = nil
	UIDynamicData.ciTiaoInfo = {}
	UIDynamicData.shengWuShuXingInfo = {}
	UIDynamicData.haveCiTiaoInfo = {}
	UIDynamicData.sldTb = {}
	UIDynamicData.mainFormsldTb = {}
	UIDynamicData.allJinBi = {}
	UIDynamicData.hurtTotalInfo = {}
	UIDynamicData.playerShareInfo = {}
	UIDynamicData.IsWin = false
	UIDynamicData.boci = 0
	UIDynamicData.test = ""
	UIDynamicData.time = 0
	UIDynamicData.bigBossKillCount = 0
	UIDynamicData.bigBossNeedKillCount = 0
	UIDynamicData.zhanjiData = {}
	UIDynamicData.nowNanDu = -1
	UIDynamicData.CZUseInfo = {0, 5000} -- 重铸次数， 需求金币
	UIDynamicData.CZChuiCnt = {}
	UIDynamicData.grandListInfo = {}
	UIDynamicData.modaiHaveInfo = {}
	UIDynamicData.shopBossBuyTimeInfoNew = {}
	UIDynamicData.yaoshuiTimeInfo = {}
	UIDynamicData.modaiLevel = 1
	UIDynamicData.diyuhuoLevel = 1
	UIDynamicData.jinbiguaiLevel = 1
	UIDynamicData.challengeBossInfo = {}
	UIDynamicData.money = 0
	UIDynamicData.fulldata = ""
	UIDynamicData.deadCntInfo = {}
	UIDynamicData.daojuInfo = {}
	UIDynamicData.skillBookBuyCountList = {}
	UIDynamicData.chengjiuUIList = {}
	UIDynamicData.heroEXstring = ""
	UIDynamicData.isXSJCInfo = {}
	UIDynamicData.zhanli = {}
	UIDynamicData.challengeZhaungTaiInfo = {}
	UIDynamicData.fumoZhuangTaiInfo = {}
	UIDynamicData.baoshiZhuangPeiInfo = {}
	UIDynamicData.baoShiCangKuInfo = {}
	UIDynamicData.baoShiYeInfo = {}
end