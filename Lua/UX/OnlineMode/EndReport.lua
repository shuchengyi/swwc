G_GameDataMgr = require"OnlineMode/G_GameDataMgr.lua"


local formSelf



function EndReportOnOpen(LuaUIEvent)
    formSelf = LuaUIEvent.SrcForm
    -- formSelf:Hide()

    formSelf:GetWidgetProxyByName("Panel(1076)"):SetActive(true)
    formSelf:GetWidgetProxyByName("Panel(1005)"):SetActive(false)


    local iconUrl = LuaCallCs_Data.GetHostCampPlayerInfo().headIconUrl
    formSelf:GetWidgetProxyByName("HttpImage(1134)"):SetImageUrl(iconUrl, false, true)


    local index = 1
    for playerID, allShangHai in pairs(UIDynamicData.hurtTotalInfo) do
        UIDynamicData.shangHaiPaiXuList[index] = {playerID, allShangHai}
        index = index + 1
    end

    table.sort(UIDynamicData.shangHaiPaiXuList, function (a,b)
        return (a[2] > b[2])
    end)


    local addHeroExNum = "+"

    if UIDynamicData.IsWin == true then
        addHeroExNum = addHeroExNum .. UIConfigData.heroGetExLevelInfo[UIDynamicData.nowNanDu][2]
    else
        addHeroExNum = addHeroExNum .. math.ceil(UIConfigData.heroGetExLevelInfo[UIDynamicData.nowNanDu][2]*0.3)
    end

    formSelf:GetWidgetProxyByName("addshulianduNum"):GetText():SetContent(addHeroExNum)
    local heroLevel
    local heroCfgID = LuaCallCs_Data.GetHeroInfoByActorID(LuaCallCs_Battle.GetHostActorID()).cfgID
    local PID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    local heroExNum = UIDynamicData.sldTb[PID][tostring(heroCfgID)]

    if heroExNum >= UIConfigData.heroExLevelInfo[5] then
        heroLevel = 5
    elseif heroExNum >= UIConfigData.heroExLevelInfo[4] then
        heroLevel = 4
    elseif heroExNum >= UIConfigData.heroExLevelInfo[3] then
        heroLevel = 3
    elseif heroExNum >= UIConfigData.heroExLevelInfo[2] then
        heroLevel = 2
    elseif heroExNum >= UIConfigData.heroExLevelInfo[1] then
        heroLevel = 1
    elseif heroExNum >= 0 then
        heroLevel = 0
    end

    formSelf:GetWidgetProxyByName("shulianduLv"):GetText():SetContent("Lv." .. heroLevel)

    local playeInfoList = formSelf:GetWidgetProxyByName("List(1024)")

    if UIDynamicData.IsWin == false then
        formSelf:GetWidgetProxyByName("Image(1004)"):GetImage():SetRes("Texture/loseImg.sprite")
    else
        formSelf:GetWidgetProxyByName("Image(1004)"):GetImage():SetRes("Texture/winImg.sprite")
    end

    formSelf:GetWidgetProxyByName("nandu"):GetText():SetContent("守卫王城  " .. UIConfigData.nanduInfo[UIDynamicData.nowNanDu][1] .. "难度")

    playerArr = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()
    playerArrLenght = #playerArr
    playeInfoList:SetElementAmount(playerArrLenght)


    LuaCallCs_GameFinish.CloseBattleScene()
    LuaCallCs_UI.CloseForm("UI/OnlineMode/MainForm.uixml")
	LuaCallCs_UI.CloseForm("UI/OnlineMode/MonsterCount.uixml");
	LuaCallCs_UI.CloseForm("UI/OnlineMode/OperationForm.uixml");
	LuaCallCs_UI.CloseForm("UI/OnlineMode/Shop.uixml");
    LuaCallCs_UI.CloseForm("UI/OnlineMode/Scoreboard.uixml");
end

function openEndReport()
    formSelf:Appear()
end


function endReportJiXuClicked(LuaUIEvent)
    formSelf:GetWidgetProxyByName("Panel(1076)"):SetActive(false)
    formSelf:GetWidgetProxyByName("Panel(1005)"):SetActive(true)
end





function playerInfoOnEnable(LuaUIEvent)
    local selfPlayerID = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    if UIDynamicData.itsOKInfo[selfPlayerID] ~= true then
        return
    end
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()


    -- 拿到该listElement的actorID
    local playerArr = LuaCallCs_UGCStateDriver.GetAllPlayerInfos()
    local playerId = playerArr[index+1].playerID

    local actorIDList = LuaCallCs_Battle.GetActorIDListByPlayerID(playerId)
    local actorID = actorIDList[1]

    local heroInfo = LuaCallCs_Data.GetHeroInfoByActorID(actorID)
    local heroCfgID = heroInfo.cfgID
    local yuanIcon = LuaCallCs_Resource.GetHeroIcon(0, heroCfgID)
    local fangIcon = LuaCallCs_Resource.GetHeroIcon(1, heroCfgID)

    local icon = "UGUI/Sprite/Dynamic/BustCircle/" .. heroInfo.imagePath

    if selfPlayerID == playerId then
        -- playerInfo
        formSelf:GetWidgetProxyByName("TextLabel(1084)"):GetText():SetContent(playerArr[index+1].playerName)
        formSelf:GetWidgetProxyByName("Image(1085)"):GetImage():SetRes(fangIcon)
        local jiFen = G_GameDataMgr.PersistentData[playerId].CustomizeDataIntArr[4]

        local jiFenLvText
        for i=#UIConfigData.jifenLevelInfo, 0 ,-1 do
            if i <= 0 then
                jiFenLvText = "积分等级 Lv" .. i

                local leftNum = jiFen
                local rightNum = UIConfigData.jifenLevelInfo[1]

                formSelf:GetWidgetProxyByName("TextLabel(1097)"):GetText():SetContent(leftNum .. "/" .. rightNum)
                formSelf:GetWidgetProxyByName("ProgressBar(1096)"):SetProgressValue(leftNum/rightNum)
                break
            elseif i >= #UIConfigData.jifenLevelInfo then -- 最大的索引
                jiFenLvText = "积分等级 Lv" .. i
                local jifenLvNum = UIConfigData.jifenLevelInfo[i]
                if jiFen >= jifenLvNum then

                    local leftNum = jiFen - jifenLvNum
                    local rightNum = 99999

                    if leftNum > rightNum then
                        leftNum = rightNum
                    end

                    formSelf:GetWidgetProxyByName("TextLabel(1097)"):GetText():SetContent(leftNum .. "/" .. rightNum)
                    formSelf:GetWidgetProxyByName("ProgressBar(1096)"):SetProgressValue(leftNum/rightNum)
                    break
                end
            else
                jiFenLvText = "积分等级 Lv" .. i
                local jifenLvNum = UIConfigData.jifenLevelInfo[i]
                if jiFen >= jifenLvNum then

                    local leftNum = jiFen-UIConfigData.jifenLevelInfo[i]
                    local rightNum = UIConfigData.jifenLevelInfo[i+1]-jifenLvNum

                    formSelf:GetWidgetProxyByName("TextLabel(1097)"):GetText():SetContent(leftNum .. "/" .. rightNum)
                    formSelf:GetWidgetProxyByName("ProgressBar(1096)"):SetProgressValue(leftNum/rightNum)
                    break
                end
            end
        end

        formSelf:GetWidgetProxyByName("TextLabel(1095)"):GetText():SetContent(jiFenLvText)
        formSelf:GetWidgetProxyByName("TextLabel(1094)"):GetText():SetContent("+" .. UIDynamicData.addJiFenNum[playerId])


        -- 英雄熟练度
        formSelf:GetWidgetProxyByName("Image(1105)"):GetImage():SetRes(yuanIcon)

        local nowHeroExNum = UIDynamicData.sldTb[playerId][tostring(heroCfgID)]

        local nowHeroLevelText
        for i=#UIConfigData.heroExLevelInfo, 0 ,-1 do
            if i <= 0 then
                nowHeroLevelText = "Lv" .. i

                local leftNum = nowHeroExNum
                local rightNum = UIConfigData.heroExLevelInfo[1]

                formSelf:GetWidgetProxyByName("shulianduJinduNum"):GetText():SetContent(leftNum .. "/" .. rightNum)
                formSelf:GetWidgetProxyByName("shulianduJindu"):SetProgressValue(leftNum/rightNum)
                break
            elseif i >= #UIConfigData.heroExLevelInfo then -- 最大的索引
                nowHeroLevelText = "Lv" .. i
                local HeroExLvNum = UIConfigData.heroExLevelInfo[i]
                if nowHeroExNum >= HeroExLvNum then

                    local leftNum = nowHeroExNum - HeroExLvNum
                    local rightNum = 99999

                    if leftNum > rightNum then
                        leftNum = rightNum
                    end

                    formSelf:GetWidgetProxyByName("shulianduJinduNum"):GetText():SetContent(leftNum .. "/" .. rightNum)
                    formSelf:GetWidgetProxyByName("shulianduJindu"):SetProgressValue(leftNum/rightNum)
                    break
                end
            else
                nowHeroLevelText = "Lv" .. i
                local HeroExLvNum = UIConfigData.heroExLevelInfo[i]
                if nowHeroExNum >= HeroExLvNum then

                    local leftNum = nowHeroExNum-UIConfigData.heroExLevelInfo[i]
                    local rightNum = UIConfigData.heroExLevelInfo[i+1]-HeroExLvNum

                    formSelf:GetWidgetProxyByName("shulianduJinduNum"):GetText():SetContent(leftNum .. "/" .. rightNum)
                    formSelf:GetWidgetProxyByName("shulianduJindu"):SetProgressValue(leftNum/rightNum)
                    break
                end
            end
        end

        formSelf:GetWidgetProxyByName("shulianduLv"):GetText():SetContent(nowHeroLevelText)
        formSelf:GetWidgetProxyByName("addshulianduNum"):GetText():SetContent("+" .. UIDynamicData.addHreoSLDNum[playerId])



        -- 通用经验
        formSelf:GetWidgetProxyByName("TextLabel(1116)"):GetText():SetContent("+" .. UIDynamicData.tyjyNum[playerId])
        formSelf:GetWidgetProxyByName("TextLabel(1118)"):GetText():SetContent(G_GameDataMgr.PersistentData[playerId].CustomizeDataIntArr[7])

        -- 宝石
        formSelf:GetWidgetProxyByName("List(1152)"):SetElementAmount(#UIDynamicData.giveBaoShiInfo[playerId])

        printTb("UIDynamicData.giveBaoShiInfo")
        printTb(UIDynamicData.giveBaoShiInfo)
    end




    local mvpPid = UIDynamicData.shangHaiPaiXuList[1][1]
    if mvpPid == playerId then
        elem:GetWidgetProxyByName("Image(1129)"):SetActive(true)
    else
        elem:GetWidgetProxyByName("Image(1129)"):SetActive(false)
    end


    if playerId == selfPlayerID then
        elem:GetWidgetProxyByName("Image(1129)"):GetImage():SetRes("Texture/Sprite/playerDiBan1024.sprite")
    else
        elem:GetWidgetProxyByName("Image(1129)"):GetImage():SetRes("Texture/Sprite/playerDiBan21024.sprite")
    end


    printTb(UIDynamicData.tianfuInfo)

    for i = 1, 3 do
        local widgetName = "tianfu" .. (i)
        if UIDynamicData.tianfuInfo[playerId][i] ~= nil then
            elem:GetWidgetProxyByName(widgetName):GetImage():SetRes(UIConfigData.erxuanyiInfo[UIDynamicData.tianfuInfo[playerId][i]][2])
        else
            elem:GetWidgetProxyByName(widgetName):GetImage():SetRes("Texture/Sprite/kongcao.sprite")
        end
    end

    elem:GetWidgetProxyByName("Image(1026)"):GetImage():SetRes(icon)
    elem:GetWidgetProxyByName("TextLabel(1027)"):GetText():SetContent(UIDynamicData.boci)

    elem:GetWidgetProxyByName("playerNameText"):GetText():SetContent(playerArr[index+1].playerName)

    elem:GetWidgetProxyByName("tongguancishu"):GetText():SetContent(G_GameDataMgr.PersistentData[playerId].CustomizeDataIntArr[2])

    -- 击败数
    elem:GetWidgetProxyByName("killCount"):GetText():SetContent(G_GameDataMgr.PersistentData[playerId].SaasData.kill)

    local oldJiFen = G_GameDataMgr.PersistentData[playerId].CustomizeDataIntArr[4]-UIDynamicData.addJiFenNum[playerId]
    elem:GetWidgetProxyByName("jifen"):GetText():SetContent(oldJiFen .. "(+" .. UIDynamicData.addJiFenNum[playerId] .. ")")

    elem:GetWidgetProxyByName("yxEX"):GetText():SetContent(UIDynamicData.sldTb[playerId][tostring(heroCfgID)])

    elem:GetWidgetProxyByName("zhanli"):GetText():SetContent(UIDynamicData.zhanli[playerId])

    --技能
    local skill1 = LuaCallCs_Skill.GetSkillSlot(actorID, 8)
    if skill1 ~= nil then
        local SkillInfo1 = LuaCallCs_Skill.GetSkillInfo(skill1.curSkillID)
        local skillImg1 = SkillInfo1.iconPath
        elem:GetWidgetProxyByName("Image(1034)"):GetImage():SetRes(skillImg1)
    else
        elem:GetWidgetProxyByName("Image(1034)"):GetImage():SetRes("Texture/Sprite/kongcao.sprite")
    end

    local skill2 = LuaCallCs_Skill.GetSkillSlot(actorID, 6)
    if skill2 ~= nil then
        local SkillInfo2 = LuaCallCs_Skill.GetSkillInfo(skill2.curSkillID)
        local skillImg2 = SkillInfo2.iconPath
        elem:GetWidgetProxyByName("Image(1035)"):GetImage():SetRes(skillImg2)
    else
        elem:GetWidgetProxyByName("Image(1035)"):GetImage():SetRes("Texture/Sprite/kongcao.sprite")
    end

    if index == #LuaCallCs_UGCStateDriver.GetAllPlayerInfos()-1 then
        LuaCallCs_UI.CloseForm("UI/OnlineMode/UIConfigData.uixml");
        LuaCallCs_UI.CloseForm("UI/OnlineMode/UIDynamicData.uixml");
    end
end




function endBaoshiOnEnable(LuaUIEvent)
    local elem = LuaUIEvent.SrcWidget
    local index = elem:GetIndexInBelongedList()

    local playerId = LuaCallCs_UGCStateDriver.GetSelfPlayerInfo().playerID
    local color = UIDynamicData.giveBaoShiInfo[playerId][index+1][1]
    local jieji = UIDynamicData.giveBaoShiInfo[playerId][index+1][2]
    local shuxing = UIDynamicData.giveBaoShiInfo[playerId][index+1][4]
    local shuxingzhi = UIDynamicData.giveBaoShiInfo[playerId][index+1][5]

    local colorNum
    local shuxingInfo

    local text1 = "<color=#FFFF00>" .. jieji .. "阶</color>1级"
    local text3 = "+" .. shuxingzhi .. "%"

    local imgInfo
    if color == 1 then
        shuxingInfo = UIConfigData.redshuxingStringInfo
        imgInfo = UIConfigData.redshuxingImgInfo
    elseif color == 2 then
        shuxingInfo = UIConfigData.blueshuxingStringInfo
        imgInfo = UIConfigData.blueshuxingImgInfo
    elseif color == 3 then
        shuxingInfo = UIConfigData.purpleshuxingStringInfo
        imgInfo = UIConfigData.purpleshuxingImgInfo
    elseif color == 4 then
        shuxingInfo = UIConfigData.greenshuxingStringInfo
        imgInfo = UIConfigData.greenshuxingImgInfo
        if shuxing == 2 or shuxing == 3 then
            text3 = "+" .. shuxingzhi
        end
    end
    local text2 = shuxingInfo[shuxing]

    local imgPath = imgInfo[shuxing][jieji]

    elem:GetWidgetProxyByName("Image(1146)"):GetImage():SetRes(imgPath)
    elem:GetWidgetProxyByName("TextLabel(1145)"):GetText():SetContent(text1)
    elem:GetWidgetProxyByName("TextLabel(1150)"):GetText():SetContent(text2)
    elem:GetWidgetProxyByName("TextLabel(1151)"):GetText():SetContent(text3)

end

function jiesuanBSguizeClosed(LuaUIEvent)
    formSelf:GetWidgetProxyByName("Panel(1155)"):SetActive(false)
end


function jiesuanBSguizeClicked(LuaUIEvent)
    formSelf:GetWidgetProxyByName("Panel(1155)"):SetActive(true)
end


--[[
每10关获得1个宝石，每局最多获得4个宝石\n宝石分为红，蓝，紫，绿四种分类，每种都有其不同的属性\n宝石品质跟难度挂钩，难度越高，获得高阶级宝石的概率就越高\n宝石在产出时会在宝石阶级的属性值范围中随机确定其最终属性值
]]




function GoHomeForm(LuaUIEvent)
    -- 应该：1、回到大厅页面2、彻底关闭游戏
    -- LuaCallCs_GameFinish.CloseBattleScene() -- 彻底关闭游戏
    LuaCallCs_UI.CloseForm("UI/OnlineMode/UIConfigData.uixml");
	LuaCallCs_UI.CloseForm("UI/OnlineMode/UIDynamicData.uixml");
    LuaCallCs_UI.CloseForm("UI/OnlineMode/EndReport.uixml")
    LuaCallCs_UI.CloseForm("UI/OnlineMode/MainForm.uixml")
	LuaCallCs_UI.CloseForm("UI/OnlineMode/MonsterCount.uixml");
	LuaCallCs_UI.CloseForm("UI/OnlineMode/OperationForm.uixml");
	LuaCallCs_UI.CloseForm("UI/OnlineMode/Shop.uixml");
	LuaCallCs_UI.CloseForm("UI/OnlineMode/Scoreboard.uixml");
    LuaCallCs_UI.OpenForm("UI/OnlineMode/MainForm.uixml")
    LuaCallCs_GameFinish.CloseBattleScene()
end

function GoRoomForm(LuaUIEvent)
    LuaCallCs_UI.CloseForm("UI/OnlineMode/UIConfigData.uixml");
	LuaCallCs_UI.CloseForm("UI/OnlineMode/UIDynamicData.uixml");
    LuaCallCs_UI.CloseForm("UI/OnlineMode/EndReport.uixml")
    LuaCallCs_UI.CloseForm("UI/OnlineMode/MainForm.uixml")
	LuaCallCs_UI.CloseForm("UI/OnlineMode/MonsterCount.uixml");
	LuaCallCs_UI.CloseForm("UI/OnlineMode/OperationForm.uixml");
	LuaCallCs_UI.CloseForm("UI/OnlineMode/Shop.uixml");
	LuaCallCs_UI.CloseForm("UI/OnlineMode/Scoreboard.uixml");
    LuaCallCs_UI.OpenForm("UI/OnlineMode/MainForm.uixml")
    LuaCallCs_GameFinish.CloseBattleScene()
end



function EndReportOnClose()
    formSelf = nil
end