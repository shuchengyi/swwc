sc = sc or {}
local cjson = require "cjson"

function sc.CreateInstanceByClassName(classname)
	local class = rtti.get_class(classname)
	if class ~= nil then
		local inst = class.new()
		return inst
	else
		print("Class [" .. classname .. "] is not exist")
	end

	return nil
end

function sc.CreateTable()
	return {}
end






sc.BattleEventID = sc.BattleEventID or {}
sc.BattleEventID.UpdateLogic 				= 113
sc.BattleEventID.FightPrepare 				= 2
sc.BattleEventID.FightStart 				= 3
sc.BattleEventID.FightOver 					= 1
sc.BattleEventID.ActorStartFight 			= 20
sc.BattleEventID.PlayerQuitBattle 			= 222
sc.BattleEventID.ActorDead 					= 9
sc.BattleEventID.ActorPrepared 				= 19
sc.BattleEventID.ActorRevive 				= 12
sc.BattleEventID.ActorDamage 				= 26
sc.BattleEventID.HeroEquipInBattleChanged 	= 85
sc.BattleEventID.SendUGCLua 				= 234
sc.BattleEventID.StartLevel 				= 232
sc.BattleEventID.ShapeTriggerEvent 			= 116
sc.BattleEventID.TailsmanTouch 				= 54

sc.GameSkillEvent = sc.GameSkillEvent or {}
sc.GameSkillEvent.UseSkill 					= 243


function mainentry(startContent)
	local t = cjson.decode(startContent)
	-- print("startContent.nandu")
	sc.nowNanDu = t.nandu
	local playerInfo = {}
	local playerInfos = t.playerInfos
	for i=1, #playerInfos do
		local pid = playerInfos[i].playerID
		local bs = playerInfos[i].bs
		-- print("global playerName:   ")
		-- print(playerName)
		playerInfo[pid] = bs
	end
	sc.playerInfo = playerInfo
	print(t.nandu)

	-- package.path=package.path .. ';' .. 'Lua/'
	-- package.path=package.path .. ';' .. 'Lua/GamePlay/'
end