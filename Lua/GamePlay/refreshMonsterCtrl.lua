local cjson = require "cjson"

RefreshMonsterCtrl = RefreshMonsterCtrl or rtti.class("RefreshMonsterCtrl")
RefreshMonsterCtrl.m_binderObj = nil


GamePlayDynamicData.boci = 0



local normalMonsterCfgIDs = {
    162102,
    162101,
}


local abnormalMonsterCfgIDs = {
    162102,
    162101,
}



function RefreshMonsterCtrl:ctor()
end


function RefreshMonsterCtrl:OnEnable()
    sc.AddEventListener(self, sc.enBattleEventID.RecvUGCMsgLua)
    sc.AddEventListener(self, sc.enBattleEventID.FightStart)

    sc.AddEventListener(self, sc.enBattleEventID.RecvUGCMsg)

    sc.AddEventListener(self, sc.enBattleEventID.UGCCustomizeFrameCmd)
end




function SpawnActorFunctionTest(actorID, position)
    local actor = sc.SpawnActor(
            sc.GameObject_Nil,
            position,
            VInt3.new(-1,0,-1),
            actorID,
            1, -- 野怪
            2, -- 阵营
            false,
            false,
            0
        )
    -- sc.SetActorMeshScale(actor, 1500, true)

    local actorGongJiLi = 300
    sc.SetActorSystemProperty(actor,1,actorGongJiLi)

    local actorHP = 10000
    sc.SetActorSystemProperty(actor,5,actorHP)
    sc.SetActorSystemProperty(actor,1005,sc.GetActorSystemProperty(actor, 5))

    local wulifangyu = 100
    sc.SetActorSystemProperty(actor,3,wulifangyu)

    local fashufangyu = 1
    sc.SetActorSystemProperty(actor,4,fashufangyu)

    return actor
end



function SpawnActorFunction(actorID, position, bossLevel)
    local actor = sc.SpawnActor(
            sc.GameObject_Nil,
            position,
            VInt3.new(-1,0,-1),
            actorID,
            1, -- 野怪
            2, -- 阵营
            false,
            false,
            0
        )

    if actorID == GamePlayConfigData.jinbiCfgID then
        local actorGongJiLi = math.ceil(GamePlayConfigData.jinbiguaiShuXing[bossLevel][2]*(1+(sc.nowNanDu-1)*0.25))
        sc.SetActorSystemProperty(actor,1,actorGongJiLi)

        local actorHP = math.ceil(GamePlayConfigData.jinbiguaiShuXing[bossLevel][1]*(1+(sc.nowNanDu-1)*0.25))
        sc.SetActorSystemProperty(actor,5,actorHP)
        sc.SetActorSystemProperty(actor,1005,sc.GetActorSystemProperty(actor, 5))

        local wulifangyu = GamePlayConfigData.jinbiguaiShuXing[bossLevel][3]
        sc.SetActorSystemProperty(actor,3,wulifangyu)

        local fashufangyu = GamePlayConfigData.jinbiguaiShuXing[bossLevel][4]
        sc.SetActorSystemProperty(actor,4,fashufangyu)
    end


    if actorID == GamePlayConfigData.jinkuangCfgID then
        local actorHP = math.ceil(GamePlayConfigData.jinKuangShuXing[bossLevel][1]*(1+(sc.nowNanDu-1)*0.25))
        sc.SetActorSystemProperty(actor,5,actorHP)
        sc.SetActorSystemProperty(actor,1005,sc.GetActorSystemProperty(actor, 5))

        local wulifangyu = GamePlayConfigData.jinKuangShuXing[bossLevel][3]
        sc.SetActorSystemProperty(actor,3,wulifangyu)

        local fashufangyu = GamePlayConfigData.jinKuangShuXing[bossLevel][4]
        sc.SetActorSystemProperty(actor,4,fashufangyu)
    end


    if actorID == GamePlayConfigData.diyuhuoCfgID then
        local actorGongJiLi = math.ceil(GamePlayConfigData.diyuhuoShuXing[bossLevel][2]*(1+(sc.nowNanDu-1)*0.25))
        sc.SetActorSystemProperty(actor,1,actorGongJiLi)

        local actorHP = math.ceil(GamePlayConfigData.diyuhuoShuXing[bossLevel][1]*(1+(sc.nowNanDu-1)*0.25))
        sc.SetActorSystemProperty(actor,5,actorHP)
        sc.SetActorSystemProperty(actor,1005,sc.GetActorSystemProperty(actor, 5))

        local wulifangyu = GamePlayConfigData.diyuhuoShuXing[bossLevel][3]
        sc.SetActorSystemProperty(actor,3,wulifangyu)

        local fashufangyu = GamePlayConfigData.diyuhuoShuXing[bossLevel][4]
        sc.SetActorSystemProperty(actor,4,fashufangyu)
    end

    -- 装备附魔
    if actorID == GamePlayConfigData.wuQiBossCfgID then
        local actorGongJiLi = math.ceil(GamePlayConfigData.fuMoBossShuXing[bossLevel][2]*(1+(sc.nowNanDu-1)*0.25))
        sc.SetActorSystemProperty(actor,1,actorGongJiLi)

        local actorHP = math.ceil(GamePlayConfigData.fuMoBossShuXing[bossLevel][1]*(1+(sc.nowNanDu-1)*0.25))
        sc.SetActorSystemProperty(actor,5,actorHP)
        sc.SetActorSystemProperty(actor,1005,sc.GetActorSystemProperty(actor, 5))

        local actorFangYu = GamePlayConfigData.fuMoBossShuXing[bossLevel][3]
        sc.SetActorSystemProperty(actor,3,actorFangYu)
        sc.SetActorSystemProperty(actor,4,actorFangYu)
    end

    if actorID == GamePlayConfigData.huJiaBossCfgID then
        local actorHP = math.ceil(GamePlayConfigData.huJiaBossShuXing[bossLevel][1]*(1+(sc.nowNanDu-1)*0.25))
        sc.SetActorSystemProperty(actor,5,actorHP)
        sc.SetActorSystemProperty(actor,1005,sc.GetActorSystemProperty(actor, 5))

        local actorGongJiLi = math.ceil(GamePlayConfigData.huJiaBossShuXing[bossLevel][2]*(1+(sc.nowNanDu-1)*0.25))
        sc.SetActorSystemProperty(actor,1,actorGongJiLi)

        local actorFangYu = GamePlayConfigData.huJiaBossShuXing[bossLevel][3]
        sc.SetActorSystemProperty(actor,3,actorFangYu)
        sc.SetActorSystemProperty(actor,4,actorFangYu)
    end

    local selfPlayerID = sc.GetHostPlayerID()
    sc.CallUILuaFunction({selfPlayerID, true}, StringId.new("ChangeMonsterCountText"))
    return actor
end




local function sjsjOne()

    
end






local function dropShuXingShu1(data)
    local position = data[1]
    local pid = data[2]

    local isShare = GamePlayDynamicData.playerShareInfo[pid]
    local index

    if isShare == 1 then
        index = 5
    else
        index = GamePlayDynamicData.playerList[pid]["roomIndex"]
    end

    local dropItemInfo = GamePlayConfigData.shuxingshuDropItemInfo[index]
    local dropItemID = dropItemInfo[sc.RangedRand(1, #dropItemInfo)]
    local dropItem = sc.SpawnDropItem(dropItemID, GamePlayConfigData.actorCamp, true, position)

    sc.SetDropItemBubbleScale(dropItem, 550)  -- 缩放泡泡
    sc.SetDropItemIconScale(dropItem, 1700) -- 缩放图片

    local itemName = GamePlayConfigData.dropSXNameInfo[dropItemID][1]
    local itemImg = GamePlayConfigData.dropSXNameInfo[dropItemID][2]
    if isShare == 0 then
        local playerName = GamePlayDynamicData.playerList[pid]["playerName"]
        -- itemName = playerName .. "的" .. itemName
        itemName = itemName
    end

    sc.SetDropItemName(dropItem, StringId.new(itemName))

    sc.SetDropItemIconPath(dropItem, StringId.new(itemImg))
end



local function dropShuXingShu(data)
    local pid = data[1]
    local monsterActor = GamePlayDynamicData.jingyingguaiHPInfo[pid][2]
    if sc.IsAlive(monsterActor) == false then
        return
    end

    local cnt = data[2]/4
    local hp = GamePlayDynamicData.jingyingguaiHPInfo[pid][1]

    local nowHp = sc.GetActorSystemProperty(monsterActor, 1005)

    local newCnt = math.ceil(cnt * ((hp-nowHp) / hp))

    if newCnt > 0 then
        local position, _, _ = sc.GetActorLogicPos(monsterActor)
        local newPositon = VInt3.new(position.x, 0, position.z)

        sc.SetTimer(150, 0, newCnt , dropShuXingShu1, {newPositon,pid})
    end

    sc.BuffAction(monsterActor, monsterActor, true, false, 204150, 0, 0)
end




function spawnMonster(data)
    local index = data.i
    local monsterID = data.monsterID
    local position = GamePlayDynamicData.playerList[GamePlayDynamicData.playerIndexInfo[index]]["spawnPosition"]
    local playerID = GamePlayDynamicData.playerList[GamePlayDynamicData.playerIndexInfo[index]]["playerID"]

    local actor = SpawnActorFunction(monsterID, position)

    -- local isHighRankMonster = data.isHighRankMonster
    local monsterBoCi = data.monsterBoCi
    sc.SetCustomProperty(StringId.new("boci"), sc.enCustomType.Int, actor, monsterBoCi)
    sc.SetCustomProperty(StringId.new("callPlayerID"), sc.enCustomType.Int, actor, playerID)

    if GamePlayConfigData.bingXianMonsterIDInfo[monsterID] ~= nil then
        local actorHP = math.ceil(GamePlayConfigData.bingXianData[monsterBoCi][1]*(1+(sc.nowNanDu-1)*0.25))
        sc.SetActorSystemProperty(actor,5,actorHP)
        sc.SetActorSystemProperty(actor,1005,sc.GetActorSystemProperty(actor, 5))

        local actorGongJiLi = math.ceil(GamePlayConfigData.bingXianData[monsterBoCi][2]*(1+(sc.nowNanDu-1)*0.25))
        sc.SetActorSystemProperty(actor,1,actorGongJiLi)

        local wufang = GamePlayConfigData.bingXianData[monsterBoCi][4]
        sc.SetActorSystemProperty(actor,3,wufang)
        local fafang = GamePlayConfigData.bingXianData[monsterBoCi][5]
        sc.SetActorSystemProperty(actor,4,fafang)
    end

    if GamePlayConfigData.jingYingGuaiRoundIndexsBack[monsterID] ~= nil then
        local t1 = sc.SetTimer(GamePlayConfigData.jingyingguaiLiveTime, 0, 1 , dropShuXingShu, {playerID, GamePlayConfigData.jingYingGuaiData[monsterBoCi][6]})
        sc.CallUILuaFunction({playerID, actor}, StringId.new("jygShow"))


        local actorHP = math.ceil(GamePlayConfigData.jingYingGuaiData[monsterBoCi][1]*(1+(sc.nowNanDu-1)*0.25))
        sc.SetActorSystemProperty(actor,5,actorHP)
        sc.SetActorSystemProperty(actor,1005,sc.GetActorSystemProperty(actor, 5))

        GamePlayDynamicData.jingyingguaiHPInfo[playerID] = {actorHP,actor}

        local actorGongJiLi = math.ceil(GamePlayConfigData.jingYingGuaiData[monsterBoCi][2]*(1+(sc.nowNanDu-1)*0.25))
        sc.SetActorSystemProperty(actor,1,actorGongJiLi)

        local wufang = GamePlayConfigData.jingYingGuaiData[monsterBoCi][4]
        sc.SetActorSystemProperty(actor,3,wufang)
        local fafang = GamePlayConfigData.jingYingGuaiData[monsterBoCi][5]
        sc.SetActorSystemProperty(actor,4,fafang)
    end

    if GamePlayConfigData.bossGuaiRoundIndexsBack[monsterID] ~= nil then
        local actorHP = math.ceil(GamePlayConfigData.bossGuaiData[monsterBoCi][1]*(1+(sc.nowNanDu-1)*0.25))
        sc.SetActorSystemProperty(actor,5,actorHP)
        sc.SetActorSystemProperty(actor,1005,sc.GetActorSystemProperty(actor, 5))

        local actorGongJiLi = math.ceil(GamePlayConfigData.bossGuaiData[monsterBoCi][2]*(1+(sc.nowNanDu-1)*0.25))
        sc.SetActorSystemProperty(actor,1,actorGongJiLi)

        local wufang = GamePlayConfigData.bossGuaiData[monsterBoCi][4]
        sc.SetActorSystemProperty(actor,3,wufang)
        local fafang = GamePlayConfigData.bossGuaiData[monsterBoCi][5]
        sc.SetActorSystemProperty(actor,4,fafang)
    end


    if monsterID == GamePlayConfigData.bigBossCfgID then
        local actorHP = math.ceil(GamePlayConfigData.bigBossData[1]*(1+(sc.nowNanDu-1)*0.25))
        sc.SetActorSystemProperty(actor,5,actorHP)
        sc.SetActorSystemProperty(actor,1005,sc.GetActorSystemProperty(actor, 5))

        local actorGongJiLi = math.ceil(GamePlayConfigData.bigBossData[2]*(1+(sc.nowNanDu-1)*0.25))
        sc.SetActorSystemProperty(actor,1,actorGongJiLi)

        local wufang = GamePlayConfigData.bigBossData[4]
        sc.SetActorSystemProperty(actor,3,wufang)
        local fafang = GamePlayConfigData.bigBossData[5]
        sc.SetActorSystemProperty(actor,4,fafang)
    end



    if sc.RangedRand(1, 2) == 1 then
        -- 给随机事件的buff
        if GamePlayDynamicData.suijishijian ~= -1 then
            if GamePlayDynamicData.suijishijian == 1 then
                sc.AddPassiveSkill(actor, 120201, 1, 9)
            elseif GamePlayDynamicData.suijishijian == 2 then
                sc.AddPassiveSkill(actor, 120301, 1, 9)
            elseif GamePlayDynamicData.suijishijian == 3 then
                sc.AddPassiveSkill(actor, 120302, 1, 9) -- 失效
                sc.AddPassiveSkill(actor, 121005, 1, 9) -- 失效
            elseif GamePlayDynamicData.suijishijian == 4 then
                sc.AddPassiveSkill(actor, 120303, 1, 9)
            elseif GamePlayDynamicData.suijishijian == 5 then
                sc.AddPassiveSkill(actor, 120305, 1, 9)
            elseif GamePlayDynamicData.suijishijian == 6 then
                sc.AddPassiveSkill(actor, 120401, 1, 9)
                local hpMax =  sc.GetActorSystemProperty(actor,5)
                sc.SetActorSystemProperty(actor,5,hpMax*2)
                sc.SetActorSystemProperty(actor,1005,hpMax*2)
            elseif GamePlayDynamicData.suijishijian == 7 then
                sc.AddPassiveSkill(actor, 121101, 1, 9)
                local gjl = sc.GetActorSystemProperty(actor,1)
                sc.SetActorSystemProperty(actor,1,gjl*2)
            elseif GamePlayDynamicData.suijishijian == 8 then
                sc.AddPassiveSkill(actor, 21011101, 1, 9)
                sc.AddPassiveSkill(actor, 21011108, 1, 9)
            elseif GamePlayDynamicData.suijishijian == 9 then
                sc.AddPassiveSkill(actor, 21011102, 1, 9)
                sc.AddPassiveSkill(actor, 21011109, 1, 9)
            elseif GamePlayDynamicData.suijishijian == 10 then
                sc.AddPassiveSkill(actor, 21011103, 1, 9)
                sc.AddPassiveSkill(actor, 21011105, 1, 9)
            elseif GamePlayDynamicData.suijishijian == 11 then
                sc.AddPassiveSkill(actor, 21011104, 1, 9)
                sc.AddPassiveSkill(actor, 21011106, 1, 9)
            end
        end
    end
end




function OnRecvUGCMsgLua(eventName, data)
    if eventName == "怪物属性信息" then
        local monsterLineCount = GamePlayDynamicData.playerCount
        local spawnMonsterData
        for i = 1, monsterLineCount do
            spawnMonsterData = data
            spawnMonsterData.i = i
            spawnMonster(spawnMonsterData)
        end
    elseif eventName == "callBoss" then
        local bossLevel = data.bossLevel -- bossLevel
        local bossID = data.BossID
        local position = data.position
        local callPlayerID = data.playerID
        local actor = SpawnActorFunction(bossID, position, bossLevel) -- 要把召唤点传进去
        sc.SetCustomProperty(StringId.new("boci"), sc.enCustomType.Int, actor, -2)
        sc.SetCustomProperty(StringId.new("callPlayerID"), sc.enCustomType.Int, actor, callPlayerID)
    elseif eventName == "callbing" then
        local bossID = data.BossID
        local position = data.position
        local callPlayerID = data.playerID
        local actor = SpawnActorFunctionTest(bossID, position) -- 要把召唤点传进去

        sc.SetCustomProperty(StringId.new("boci"), sc.enCustomType.Int, actor, -2)
        sc.SetCustomProperty(StringId.new("callPlayerID"), sc.enCustomType.Int, actor, callPlayerID)
    end
end









-- 给生成怪逻辑图发自定义事件
function SendMsgOfRefreshMosnter(data)
    local selfPlayerID = sc.GetHostPlayerID()
    -- local self = data.this
    local num
    local monsterCfgID
    local index
    local isHighRankMonster = sc.RangedRand(0, 1)
    if data.isNormal == 0 then
        num = #normalMonsterCfgIDs
        index = sc.RangedRand(1, num)
        monsterCfgID = normalMonsterCfgIDs[index]
    elseif data.isNormal == 1 then
        num = #abnormalMonsterCfgIDs -- zdc
        index = sc.RangedRand(1, num)
        monsterCfgID = abnormalMonsterCfgIDs[index]
    elseif data.isNormal == 2 then -- boss怪，掉技能或/和装备
        local boci = data.boci
        monsterCfgID = GamePlayConfigData.bossGuaiRoundIndexs[boci]
        isHighRankMonster = 0
        sc.CallUILuaFunction({selfPlayerID, "zbg"}, StringId.new("addGMGongGaoInfo"))
    elseif data.isNormal == 3 then -- 精英怪，掉装备
        local boci = data.boci
        monsterCfgID = GamePlayConfigData.jingYingGuaiRoundIndexs[boci]
        isHighRankMonster = 0
        sc.CallUILuaFunction({selfPlayerID, "jyg"}, StringId.new("addGMGongGaoInfo"))
    elseif data.isNormal == 4 then
        isHighRankMonster = 0
        -- 刷最终boss
        monsterCfgID = GamePlayConfigData.bigBossCfgID
        sc.CallUILuaFunction({selfPlayerID, "zzboss"}, StringId.new("addGMGongGaoInfo"))
    end

    -- sc.UGCSendMsgLua(
    --     "怪物属性信息",
    --     {
    --         monsterID = monsterCfgID,
    --         isHighRankMonster = isHighRankMonster,
    --         monsterBoCi = GamePlayDynamicData.boci
    --     }
    -- )

    local monsterLineCount = GamePlayDynamicData.playerCount -- 有几条兵线
    for i = 1, monsterLineCount do -- 在有的几条兵线里刷兵
        local msg = {}
        msg.monsterID = monsterCfgID
        msg.isHighRankMonster = isHighRankMonster
        msg.monsterBoCi = GamePlayDynamicData.boci
        msg.i = i
        spawnMonster(msg)
    end
end



function bigBossDaojishigonggao()
    local selfPlayerID = sc.GetHostPlayerID()

    local num = 23
    local t
    function daojishi()
        num = num - 1
        if num < 5 then
            if num <= 0 then
                sc.KillTimer(t)
                sc.CallUILuaFunction({selfPlayerID, "bossdjs", -1}, StringId.new("addGMGongGaoInfo"))
                return
            end
            sc.CallUILuaFunction({selfPlayerID, "bossdjs", num}, StringId.new("addGMGongGaoInfo"))
        end
    end
    t = sc.SetTimer(1000, 0, 15, daojishi,{})
end



-- 刷怪入口函数
function RefreshMonster(data)
    local selfPlayerID = sc.GetHostPlayerID()
    sc.CallUILuaFunction({selfPlayerID}, StringId.new("checkGameEnd"))

    GamePlayDynamicData.boci = GamePlayDynamicData.boci + 1

    if GamePlayDynamicData.boci == 5 then
        sc.CallUILuaFunction({sc.GetHostPlayerID(), "jnsGMGG5", GamePlayDynamicData.boci}, StringId.new("addGMGongGaoInfo"))
    elseif GamePlayDynamicData.boci == 15 then
        sc.CallUILuaFunction({sc.GetHostPlayerID(), "jnsGMGG15", GamePlayDynamicData.boci}, StringId.new("addGMGongGaoInfo"))
    elseif GamePlayDynamicData.boci == 25 then
        sc.CallUILuaFunction({sc.GetHostPlayerID(), "jnsGMGG25", GamePlayDynamicData.boci}, StringId.new("addGMGongGaoInfo"))
    end



    if GamePlayDynamicData.boci == 20 then
        GamePlayConfigData.refreshMonsterTime = GamePlayConfigData.refreshMonsterTime - 200
    end

    if GamePlayDynamicData.boci == GamePlayConfigData.refreshMonsterLineCount then
        bigBossDaojishigonggao()
    end


    -- 生成随机事件
    if GamePlayConfigData.sjsjBociInfo[GamePlayDynamicData.boci] == 1 then
        local suijshu = sc.RangedRand(1,#GamePlayConfigData.sjsjInfo)
        -- GamePlayDynamicData.suijishijian = 9
        GamePlayDynamicData.suijishijian = suijshu
        sc.CallUILuaFunction({sc.GetHostPlayerID(), "" .. GamePlayDynamicData.suijishijian}, StringId.new("addGMGongGaoInfo"))
    else
        GamePlayDynamicData.suijishijian = -1
    end


    if GamePlayDynamicData.boci > GamePlayConfigData.refreshMonsterLineCount then
    -- if GamePlayDynamicData.boci > 1 then
        -- 刷最后的boss，4波兵线都刷
        SendMsgOfRefreshMosnter({isNormal=4, boci = GamePlayDynamicData.boci}) -- 刷大boss
        -- 让倒计时变为3分钟的
        sc.CallUILuaFunction({selfPlayerID}, StringId.new("setBigBossDaojishi"))
        sc.KillTimer(T1)
        return
    else
        sc.CallUILuaFunction({selfPlayerID}, StringId.new("SetDaojishi"))
    end

    -- sc.CallUILuaFunction({selfPlayerID, GamePlayDynamicData.boci}, StringId.new("SetJidiJiDuTiao"))
    sc.CallUILuaFunction({selfPlayerID, GamePlayDynamicData.boci}, StringId.new("Refreshboci"))
    sc.CallUILuaFunction({selfPlayerID, GamePlayDynamicData.boci}, StringId.new("refreshRoundIndexText"))

    if GamePlayDynamicData.boci <= 10 then
        -- 正常刷怪逻辑
        sc.SetTimer(
            GamePlayConfigData.refreshMonsterTime,
            0,
            GamePlayConfigData.refreshMonsterCount,
            SendMsgOfRefreshMosnter,
            {isNormal = 0} -- 是正常刷怪
        )
    else
        -- 带精英怪刷怪逻辑
        sc.SetTimer(
            GamePlayConfigData.refreshMonsterTime,
            0,
            GamePlayConfigData.refreshMonsterCount,
            SendMsgOfRefreshMosnter,
            {isNormal = 1} -- 非正常刷怪
        )
    end
    if GamePlayConfigData.bossGuaiRoundIndexs[GamePlayDynamicData.boci] ~= nil then
        SendMsgOfRefreshMosnter({isNormal=2, boci = GamePlayDynamicData.boci}) -- 刷boss怪
    end

    if GamePlayConfigData.jingYingGuaiRoundIndexs[GamePlayDynamicData.boci] ~= nil then
        SendMsgOfRefreshMosnter({isNormal=3, boci = GamePlayDynamicData.boci}) -- 刷精英怪
    end
end


function daojishigonggao()
    local selfPlayerID = sc.GetHostPlayerID()

    local num = GamePlayConfigData.refreshMonsterLineTime/1000 - 1
    function daojishi()
        num = num - 1
        if num > 5 then
            sc.CallUILuaFunction({selfPlayerID, "yxks1", num}, StringId.new("addGMGongGaoInfo"))
        elseif num > 0 then
            sc.CallUILuaFunction({selfPlayerID, "yxks2", num}, StringId.new("addGMGongGaoInfo"))
        elseif num == 0 then
            sc.CallUILuaFunction({selfPlayerID, "yxks3"}, StringId.new("addGMGongGaoInfo"))
        end
    end
    sc.SetTimer(1000, 0, GamePlayConfigData.refreshMonsterLineTime/1000, daojishi,{})
end








function RefreshMonsterCtrl:FightStart()
    -- printTb("开始刷怪")
    -- 刷怪入口定时器
    local selfPlayerID = sc.GetHostPlayerID()
    T1 = sc.SetTimer(GamePlayConfigData.refreshMonsterLineTime, 0, 0 , RefreshMonster,{})
    sc.CallUILuaFunction({selfPlayerID}, StringId.new("SetDaojishi"))
    daojishigonggao()
end




