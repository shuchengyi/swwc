local cjson = require "cjson"

GlobalEvent = GlobalEvent or rtti.class("GlobalEvent")
GlobalEvent.m_binderObj = nil

--export gameobject
GlobalEvent.spawnPoint1 = nil
--export gameobject
GlobalEvent.revivePoint1 = nil

--export gameobject
GlobalEvent.spawnPoint2 = nil
--export gameobject
GlobalEvent.revivePoint2 = nil

--export gameobject
GlobalEvent.spawnPoint3 = nil
--export gameobject
GlobalEvent.revivePoint3 = nil

--export gameobject
GlobalEvent.spawnPoint4 = nil
--export gameobject
GlobalEvent.revivePoint4 = nil


local spawnPointInfoList = {}


function GlobalEvent:ctor()
end


function GlobalEvent:OnEnable()
    spawnPointInfoList = {
        {self.spawnPoint1, self.revivePoint1},
        {self.spawnPoint2, self.revivePoint2},
        {self.spawnPoint3, self.revivePoint3},
        {self.spawnPoint4, self.revivePoint4}
    }

    -- sc.AddEventListener(self, sc.enBattleEventID.UGCCustomizeFrameCmd)
    sc.AddEventListener(self, sc.enBattleEventID.ActorStartFight)
    sc.AddEventListener(self, sc.enBattleEventID.FightStart)
    -- sc.AddEventListener(self, sc.enBattleEventID.RecvUGCMsgLua)
    -- sc.AddEventListener(self, sc.enBattleEventID.RecvUGCMsg)
    -- sc.AddEventListener(self, sc.enBattleEventID.ActorDead)
    -- sc.AddEventListener(self, sc.enBattleEventID.DropItemTouch)
    -- sc.AddEventListener(self, sc.enBattleEventID.ActorRevive)
    -- sc.AddEventListener(self, sc.enBattleEventID.UpdateLogic)
    -- sc.AddEventListener(self, sc.enBattleEventID.PlayerQuitBattle)
    -- sc.AddEventListener(self, sc.enBattleEventID.ActorDamage)
end




function ActorRevive(src, atker,orignalAtker,logicAtker,bImmediateRevive) -- 角色复活自动调用，复活后传送到其对应的出生点
    if sc.GetActorSystemProperty(src, 1002) ~= 0 then
        -- print("怪物的复活")
        return
    end

    -- print("玩家的复活")
    local pid = sc.GetActorSystemProperty(src, 1007)
    local unityObj = GamePlayDynamicData.playerList[pid]["revivePoint"]
    sc.TeleportActor(src, unityObj, VInt3.new(0,0,0), VInt3.new(0,0,0))
    GamePlayDynamicData.playerLiveInfo[pid] = true
    sc.CallUILuaFunction({pid, true}, StringId.new("jiluActorDead_callBack"))
    sc.BuffAction(src, src, true, false, 103171, 0, 0)
end

function timeTickCallUI()
    sc.CallUILuaFunction({}, StringId.new("timeTick_callBack"))
end







local function hreoTypeFunc(actor, cfgID, playerID)
    GamePlayDynamicData.ciTiaoKuInfo[playerID] = {
        {"饮血"},
        {"闪电"},
        {"隐追"},
        {"无尽"},
        {"凝神"},
        {"强攻"},
        {"破甲"},
        {"神行"},
        {"刻印"},
        {"支配"},
        {"统御"},
        {"慈善"},
        {"暴怒"},
        {"坚毅"},
        {"馈赠"},
    }
    if GamePlayConfigData.tanKeInfo[cfgID] ~= nil then
        sc.BuffAction(actor, actor, true, false, 1031301, 0, 0)
    elseif GamePlayConfigData.zhanShiInfo[cfgID] ~= nil then
        sc.BuffAction(actor, actor, true, false, 1031302, 0, 0)
    elseif GamePlayConfigData.sheShouInfo[cfgID] ~= nil then
        sc.BuffAction(actor, actor, true, false, 1031303, 0, 0)
    elseif GamePlayConfigData.faShiInfo[cfgID] ~= nil then
        table.insert(GamePlayDynamicData.ciTiaoKuInfo[playerID], {"虚无"})
        table.insert(GamePlayDynamicData.ciTiaoKuInfo[playerID], {"弑神"})
        table.insert(GamePlayDynamicData.ciTiaoKuInfo[playerID], {"毁灭"})
        sc.BuffAction(actor, actor, true, false, 1031304, 0, 0)
    end

    if GamePlayConfigData.jinZhanInfo[cfgID] ~= nil then
        sc.BuffAction(actor, actor, true, false, 1031305, 0, 0)
    elseif GamePlayConfigData.yuanChenInfo[cfgID] ~= nil then
        sc.AddPassiveSkill(actor, 21010801, 0)
        sc.BuffAction(actor, actor, true, false, 112501, 0, 0)
    end
end




-- actor: 生成的Actor
function ActorStartFight(actor)
    if sc.GetActorSystemProperty(actor, 1002) == 0 then -- 英雄死亡
        printTb("英雄出生了")
    end
end



function GlobalEvent:FightStart()
    print("run in FightStart1..........")
    sc.SetTimer(5000, 0, 1 , function ()
        print("run in FightStart2..........")

        -- SetCameraParam 设置相机参数
        -- @param height 高度
        -- @param rotationX 水平偏角
        -- @param rotationY 垂直偏角
        sc.SetCameraParam(4500, 0, 0)

        -- timeTickCallUI()
        sc.SetTimer(1000, 0, 0 , timeTickCallUI,{})

        sc.CallUILuaFunction({}, StringId.new("timeTick_callBack"))

        sc.CallUILuaFunction({}, StringId.new("init_callBack"))
        print("run in FightStart2..........end")

    end,{})
    print("run in FightStart1..........end")

end



local function init(myJson)
    -- local playerIDInfo = myJson.tb
    local function getAllPlayersCallBack(player)
        local playerID = sc.GetPlayerSystemProperty(player, 1)
        -- local heroCfgID = sc.GetActorSystemProperty(actor, 1000)

        local actor
        local print_actor_info = function (actor1)
            actor = actor1
        end
        local actors = sc.GetActorsByPlayerID(playerID)

        sc.TraverseActorArray(actors, print_actor_info)



        local heroCfgID = sc.GetActorSystemProperty(actor, 1000)

        if playerID ~= 0 and GamePlayDynamicData.playerList[playerID] == nil then
            GamePlayDynamicData.playerCount = GamePlayDynamicData.playerCount + 1

            local position = sc.GetUnityObjectTransform(spawnPointInfoList[GamePlayDynamicData.playerCount][1])
            local revivePoint = spawnPointInfoList[GamePlayDynamicData.playerCount][2]

            GamePlayDynamicData.playerList[playerID] = {
                ["playerID"] = playerID,
                ["spawnPosition"] = position,
                ["revivePoint"] = revivePoint,
                ["roomIndex"] = GamePlayDynamicData.playerCount,
                -- ["playerName"] = playerName,
            }

            GamePlayDynamicData.needKillCount = GamePlayDynamicData.needKillCount + 1

            GamePlayDynamicData.playerIndexInfo[GamePlayDynamicData.playerCount] = playerID

            GamePlayDynamicData.dingshiqi[playerID] = {}


            GamePlayDynamicData.grandListInfo[playerID] = {{1,1,false},{1,1,false},{1,1,false}} -- {1级， 零阶， boss是否存活}
            GamePlayDynamicData.challengeBossInfo[playerID] = {{1,false},{1,false},{1,false}} -- {挑战boss等级，是否存活}
            GamePlayDynamicData.GMGongGaoInfo[playerID] = {}
            GamePlayDynamicData.modaiHaveInfo[playerID] = {1, false, 0, 50} -- 魔袋等级，魔袋是否存在，魔袋击杀数, 魔袋需要击杀数
            GamePlayDynamicData.daojuInfo[playerID] = {}
            GamePlayDynamicData.skillBookBuyCountList[playerID] = {0,0,0,0,0,0,0,0}
            GamePlayDynamicData.nowPassSkillIDInfo[playerID] = 0
            -- 三个元素   1武器(1.1当前阶段武器等级 1.2 当前武器boss等级 1.3武器boss是否存活) 2护甲 3戒指
            GamePlayDynamicData.CZChuiCnt[playerID] = 0
            GamePlayDynamicData.playerLiveInfo[playerID] = true
            GamePlayDynamicData.deadCntInfo[playerID] = 0
            GamePlayDynamicData.expendMoneyInfo[playerID] = 0
            GamePlayDynamicData.jinbijiacheng[playerID] = 0
            GamePlayDynamicData.killMonsterAdd[playerID] = 0

            GamePlayDynamicData.hurtTotalInfo[playerID] = 0
            GamePlayDynamicData.ciTiaoInfo[playerID] = {}
            GamePlayDynamicData.cishanInfo[playerID] = 0

            GamePlayDynamicData.jingyingguaiHPInfo[playerID] = {}

            GamePlayDynamicData.playerShareInfo[playerID] = 0

            GamePlayDynamicData.shuXingInfo[playerID] = {0,0}

            GamePlayDynamicData.erxuanyiSkillKu[playerID] = {
                "残废",
                "暗幕",
                "暴风",
                "破军",
                "破败",
                "审判",
                "回响",
                "强击",
                "荆棘",
                "献祭",
                "冰心",
                "无畏",
                "重组",
            }

            GamePlayDynamicData.allJinBi[playerID] = 0


            sc.CallUILuaFunction({playerID, sc.nowNanDu, heroCfgID, sc.playerInfo[playerID]}, StringId.new("UIInit"))

            -- 给UI发送执行校验熟练度的方法
            -- sc.CallUILuaFunction({playerID, actorId, heroCfgID}, StringId.new("checkSLD"))

            sc.AddCoin(actor, 2000, true, true, 0) -- 开场加金币
            GamePlayDynamicData.allJinBi[playerID] = GamePlayDynamicData.allJinBi[playerID] + 2000

            hreoTypeFunc(actor, heroCfgID, playerID)

            sc.SetReviveTime(actor, 5000)
        end
    end




    local players = sc.GetAllPlayers(1)
    sc.TraversePlayerArray(players, getAllPlayersCallBack)



--[[
for i = 1, #playerIDInfo do
        local playerID = playerIDInfo[i][1]
        local playerName = playerIDInfo[i][2]

        local actor
        local print_actor_info = function (actor1)
            actor = actor1
        end
        local actors = sc.GetActorsByPlayerID(playerID)

        sc.TraverseActorArray(actors, print_actor_info)



        local heroCfgID = sc.GetActorSystemProperty(actor, 1000)


        if playerID ~= 0 and GamePlayDynamicData.playerList[playerID] == nil then
            GamePlayDynamicData.playerCount = GamePlayDynamicData.playerCount + 1

            local position = sc.GetUnityObjectTransform(spawnPointInfoList[GamePlayDynamicData.playerCount][1])
            local revivePoint = spawnPointInfoList[GamePlayDynamicData.playerCount][2]

            GamePlayDynamicData.playerList[playerID] = {
                ["playerID"] = playerID,
                ["spawnPosition"] = position,
                ["revivePoint"] = revivePoint,
                ["roomIndex"] = GamePlayDynamicData.playerCount,
                ["playerName"] = playerName,
            }

            GamePlayDynamicData.needKillCount = GamePlayDynamicData.needKillCount + 1

            GamePlayDynamicData.playerIndexInfo[GamePlayDynamicData.playerCount] = playerID

            GamePlayDynamicData.dingshiqi[playerID] = {}
        end


        GamePlayDynamicData.grandListInfo[playerID] = {{1,1,false},{1,1,false},{1,1,false}} -- {1级， 零阶， boss是否存活}
        GamePlayDynamicData.challengeBossInfo[playerID] = {{1,false},{1,false},{1,false}} -- {挑战boss等级，是否存活}
        GamePlayDynamicData.GMGongGaoInfo[playerID] = {}
        GamePlayDynamicData.modaiHaveInfo[playerID] = {1, false, 0, 50} -- 魔袋等级，魔袋是否存在，魔袋击杀数, 魔袋需要击杀数
        GamePlayDynamicData.daojuInfo[playerID] = {}
        GamePlayDynamicData.skillBookBuyCountList[playerID] = {0,0,0,0,0,0,0,0}
        GamePlayDynamicData.nowPassSkillIDInfo[playerID] = 0
        -- 三个元素   1武器(1.1当前阶段武器等级 1.2 当前武器boss等级 1.3武器boss是否存活) 2护甲 3戒指
        GamePlayDynamicData.CZChuiCnt[playerID] = 0
        GamePlayDynamicData.playerLiveInfo[playerID] = true
        GamePlayDynamicData.deadCntInfo[playerID] = 0
        GamePlayDynamicData.expendMoneyInfo[playerID] = 0
        GamePlayDynamicData.jinbijiacheng[playerID] = 0
        GamePlayDynamicData.killMonsterAdd[playerID] = 0

        GamePlayDynamicData.hurtTotalInfo[playerID] = 0
        GamePlayDynamicData.ciTiaoInfo[playerID] = {}
        GamePlayDynamicData.cishanInfo[playerID] = 0

        GamePlayDynamicData.jingyingguaiHPInfo[playerID] = {}

        GamePlayDynamicData.playerShareInfo[playerID] = 0

        GamePlayDynamicData.shuXingInfo[playerID] = {0,0}

        GamePlayDynamicData.erxuanyiSkillKu[playerID] = {
            "残废",
            "暗幕",
            "暴风",
            "破军",
            "破败",
            "审判",
            "回响",
            "强击",
            "荆棘",
            "献祭",
            "冰心",
            "无畏",
            "重组",
        }

        GamePlayDynamicData.allJinBi[playerID] = 0


        sc.CallUILuaFunction({playerID, sc.nowNanDu, heroCfgID, sc.playerInfo[playerID]}, StringId.new("UIInit"))

        -- 给UI发送执行校验熟练度的方法
        -- sc.CallUILuaFunction({playerID, actorId, heroCfgID}, StringId.new("checkSLD"))

        sc.AddCoin(actor, 2000, true, true, 0) -- 开场加金币
        GamePlayDynamicData.allJinBi[playerID] = GamePlayDynamicData.allJinBi[playerID] + 2000

        hreoTypeFunc(actor, heroCfgID, playerID)

        sc.SetReviveTime(actor, 5000)
    end
]]

end

-- //src(actorRoot)    事件源
-- //atker(actorroot)   事件触发者
-- //m_hurtTotal(Int)    总伤害
-- //m_hpChanged(int)   血量变化
-- //m_critValue(int)  暴击伤害值
-- //skillId  技能效果组合Id
function ActorDamage(src,atker,m_hurtTotal,m_hpChanged,m_critValue,skillId)

    if sc.GetActorSystemProperty(atker, 1002) == 0 and sc.GetActorSystemProperty(src, 1002) == 1 then
        local pid = sc.GetActorSystemProperty(atker, 1007)
        if GamePlayDynamicData.hurtTotalInfo[pid] then
            if m_hpChanged < 0 then
                GamePlayDynamicData.hurtTotalInfo[pid] = GamePlayDynamicData.hurtTotalInfo[pid] - m_hpChanged
            end
            -- sc.CallUILuaFunction({pid, GamePlayDynamicData.hurtTotalInfo[pid]}, StringId.new("shangHaiTongJi_callBack"))
        end
    end
end





local function touchSkillBook(playerID, num)
    -- {"D级被动技能书", "Texture/Sprite/Db.sprite", 1, "随机获得一个D级被动技能", 1}
    local isHave = false
    local iscanBuy = true

    if num == 1 then
        for index = 1, #GamePlayDynamicData.daojuInfo[playerID] do
            if GamePlayDynamicData.daojuInfo[playerID][index][1] == "D级被动技能书" then
                GamePlayDynamicData.daojuInfo[playerID][index][3] = GamePlayDynamicData.daojuInfo[playerID][index][3] + 1
                isHave = true
            end
        end

        if isHave == false then
            if #GamePlayDynamicData.daojuInfo[playerID] >= 6 then
                iscanBuy = false
            else
                table.insert(GamePlayDynamicData.daojuInfo[playerID], {"D级被动技能书", "Texture/Sprite/Db.sprite", 1, "随机获得一个D级被动技能", 1})
            end
        end
    elseif num == 5 then
        for index = 1, #GamePlayDynamicData.daojuInfo[playerID] do
            if GamePlayDynamicData.daojuInfo[playerID][index][1] == "D级主动技能书" then
                GamePlayDynamicData.daojuInfo[playerID][index][3] = GamePlayDynamicData.daojuInfo[playerID][index][3] + 1
                isHave = true
            end
        end

        if isHave == false then
            if #GamePlayDynamicData.daojuInfo[playerID] >= 6 then
                iscanBuy = false
            else
                table.insert(GamePlayDynamicData.daojuInfo[playerID], {"D级主动技能书", "Texture/Sprite/Dz.sprite", 1, "随机获得一个D级主动技能", 5})
            end
        end
    elseif num == 2 then
        for index = 1, #GamePlayDynamicData.daojuInfo[playerID] do
            if GamePlayDynamicData.daojuInfo[playerID][index][1] == "C级被动技能书" then
                GamePlayDynamicData.daojuInfo[playerID][index][3] = GamePlayDynamicData.daojuInfo[playerID][index][3] + 1
                isHave = true
            end
        end

        if isHave == false then
            if #GamePlayDynamicData.daojuInfo[playerID] >= 6 then
                iscanBuy = false
            else
                table.insert(GamePlayDynamicData.daojuInfo[playerID], {"C级被动技能书", "Texture/Sprite/Cb.sprite", 1, "随机获得一个C级被动技能", 2})
            end
        end
    elseif num == 6 then
        for index = 1, #GamePlayDynamicData.daojuInfo[playerID] do
            if GamePlayDynamicData.daojuInfo[playerID][index][1] == "C级主动技能书" then
                GamePlayDynamicData.daojuInfo[playerID][index][3] = GamePlayDynamicData.daojuInfo[playerID][index][3] + 1
                isHave = true
            end
        end

        if isHave == false then
            if #GamePlayDynamicData.daojuInfo[playerID] >= 6 then
                iscanBuy = false
            else
                table.insert(GamePlayDynamicData.daojuInfo[playerID], {"C级主动技能书", "Texture/Sprite/Cz.sprite", 1, "随机获得一个C级主动技能", 6})
            end
        end
    elseif num == 3 then
        for index = 1, #GamePlayDynamicData.daojuInfo[playerID] do
            if GamePlayDynamicData.daojuInfo[playerID][index][1] == "B级被动技能书" then
                GamePlayDynamicData.daojuInfo[playerID][index][3] = GamePlayDynamicData.daojuInfo[playerID][index][3] + 1
                isHave = true
            end
        end

        if isHave == false then
            if #GamePlayDynamicData.daojuInfo[playerID] >= 6 then
                iscanBuy = false
            else
                table.insert(GamePlayDynamicData.daojuInfo[playerID], {"B级被动技能书", "Texture/Sprite/Bb.sprite", 1, "随机获得一个B级被动技能", 3})
            end
        end
    elseif num == 7 then
        for index = 1, #GamePlayDynamicData.daojuInfo[playerID] do
            if GamePlayDynamicData.daojuInfo[playerID][index][1] == "B级主动技能书" then
                GamePlayDynamicData.daojuInfo[playerID][index][3] = GamePlayDynamicData.daojuInfo[playerID][index][3] + 1
                isHave = true
            end
        end

        if isHave == false then
            if #GamePlayDynamicData.daojuInfo[playerID] >= 6 then
                iscanBuy = false
            else
                table.insert(GamePlayDynamicData.daojuInfo[playerID], {"B级主动技能书", "Texture/Sprite/Bz.sprite", 1, "随机获得一个B级主动技能", 7})
            end
        end
    elseif num == 4 then
        for index = 1, #GamePlayDynamicData.daojuInfo[playerID] do
            if GamePlayDynamicData.daojuInfo[playerID][index][1] == "A级被动技能书" then
                GamePlayDynamicData.daojuInfo[playerID][index][3] = GamePlayDynamicData.daojuInfo[playerID][index][3] + 1
                isHave = true
            end
        end

        if isHave == false then
            if #GamePlayDynamicData.daojuInfo[playerID] >= 6 then
                iscanBuy = false
            else
                table.insert(GamePlayDynamicData.daojuInfo[playerID], {"A级被动技能书", "Texture/Sprite/Ab.sprite", 1, "随机获得一个A级被动技能", 4})
            end
        end
    elseif num == 8 then
        for index = 1, #GamePlayDynamicData.daojuInfo[playerID] do
            if GamePlayDynamicData.daojuInfo[playerID][index][1] == "A级主动技能书" then
                GamePlayDynamicData.daojuInfo[playerID][index][3] = GamePlayDynamicData.daojuInfo[playerID][index][3] + 1
                isHave = true
            end
        end

        if isHave == false then
            if #GamePlayDynamicData.daojuInfo[playerID] >= 6 then
                iscanBuy = false
            else
                table.insert(GamePlayDynamicData.daojuInfo[playerID], {"A级主动技能书", "Texture/Sprite/Az.sprite", 1, "随机获得一个A级主动技能", 8})
            end
        end
    end

    return iscanBuy, isHave
end




function OnDropItemTouchEvent(src, item, id) -- touch
    local playerID = sc.GetActorSystemProperty(src, 1007)
    local isPickItem = false
    local playerRoomIndex = GamePlayDynamicData.playerList[playerID]["roomIndex"]
    if id == 120901 then -- 是重铸锤
        sc.PickUpItem(src, item, id, -1)
        sc.CallUILuaFunction({playerID}, StringId.new("addCZChuiCnt"))
        GamePlayDynamicData.CZChuiCnt[playerID] = GamePlayDynamicData.CZChuiCnt[playerID] + 1

        sc.CallUILuaFunction({playerID, "拾取：重铸锤"}, StringId.new("piaozi2"))
        return
    elseif id == GamePlayConfigData.shuxingshuDropItemInfo[playerRoomIndex][1] or id == GamePlayConfigData.shuxingshuDropItemInfo[5][1] then
        sc.PickUpItem(src, item, id, -1)
        sc.BuffAction(src, src, true, false, 103091, 0, 0)

        sc.CallUILuaFunction({playerID, "拾取：血量"}, StringId.new("piaozi2"))
        return
    elseif id == GamePlayConfigData.shuxingshuDropItemInfo[playerRoomIndex][2] or id == GamePlayConfigData.shuxingshuDropItemInfo[5][2] then
        sc.PickUpItem(src, item, id, -1)
        sc.BuffAction(src, src, true, false, 103092, 0, 0)

        sc.CallUILuaFunction({playerID, "拾取：攻击"}, StringId.new("piaozi2"))
        return
    elseif id == GamePlayConfigData.shuxingshuDropItemInfo[playerRoomIndex][3] or id == GamePlayConfigData.shuxingshuDropItemInfo[5][3] then
        sc.PickUpItem(src, item, id, -1)
        sc.BuffAction(src, src, true, false, 103093, 0, 0)

        sc.CallUILuaFunction({playerID, "拾取：防御"}, StringId.new("piaozi2"))
        return
    end

    -- local iszhaungbeizhuangMan = sc.IsPackageFull(src, 0)
    local piaoziText
    if GamePlayConfigData.dropSkillInfo[id] ~= nil then -- 是技能书
        local dropItemIDInfo = GamePlayConfigData.playerDropSkillInfo[playerRoomIndex]
        local tongyongItmeInfo = GamePlayConfigData.playerDropSkillInfo[5]

        local num
        if id == dropItemIDInfo[1] or id == tongyongItmeInfo[1] then -- D级被动
            num = 1
            piaoziText = "拾取：D级被动"
        elseif id == dropItemIDInfo[3] or id == tongyongItmeInfo[2] then -- C级被动
            num = 2
            piaoziText = "拾取：C级被动"
        elseif id == dropItemIDInfo[5] or id == tongyongItmeInfo[3] then -- B级被动
            num = 3
            piaoziText = "拾取：B级被动"
        elseif id == dropItemIDInfo[7] or id == tongyongItmeInfo[4] then -- A级被动
            num = 4
            piaoziText = "拾取：A级被动"
        elseif id == dropItemIDInfo[2] or id == tongyongItmeInfo[5] then -- D级主动
            num = 5
            piaoziText = "拾取：D级主动"
        elseif id == dropItemIDInfo[4] or id == tongyongItmeInfo[6] then -- C级主动
            num = 6
            piaoziText = "拾取：C级主动"
        elseif id == dropItemIDInfo[6] or id == tongyongItmeInfo[7] then -- B级主动
            num = 7
            piaoziText = "拾取：B级主动"
        elseif id == dropItemIDInfo[8] or id == tongyongItmeInfo[8] then -- A级主动
            num = 8
            piaoziText = "拾取：A级主动"
        else
            return
        end

        local iscanBuy, isHave = touchSkillBook(playerID, num)

        sc.CallUILuaFunction({playerID, iscanBuy, isHave, num}, StringId.new("touchSkillBook_callBack"))

        if iscanBuy == false then
            isPickItem = false
        else
            isPickItem = true
        end
    end

    if isPickItem == false then
        return
    end

    
    

    sc.PickUpItem(src, item, id, -1)
    sc.CallUILuaFunction({playerID, piaoziText}, StringId.new("piaozi2"))

    sc.CallUILuaFunction({playerID}, StringId.new("refreshEquipInfo"))
    sc.CallUILuaFunction({playerID}, StringId.new("yidong"))
end



-- 创建掉落物的函数
local function dropSkill(src, atker, skillLevel) --src 死者    atker杀怪者
    if atker then
        local pid = sc.GetActorSystemProperty(atker, 1007)
        local position, _, _ = sc.GetActorLogicPos(src)
        local newPositon = VInt3.new(position.x, 0, position.z)

        local isShare = GamePlayDynamicData.playerShareInfo[pid]
        local index
        if isShare == 1 then
            index = 5
        else
            index = GamePlayDynamicData.playerList[pid]["roomIndex"]
        end

        local dropItemInfo = GamePlayConfigData.dropSKillIDLevelInfo[index][skillLevel]
        local randomNum = sc.RangedRand(1, #dropItemInfo)
        local dropItemID = dropItemInfo[randomNum]

        local dropItem = sc.SpawnDropItem(dropItemID, GamePlayConfigData.actorCamp, true, newPositon)
        sc.SetDropItemBubbleScale(dropItem, 550)  -- 缩放泡泡
        sc.SetDropItemIconScale(dropItem, 500) -- 缩放图片

        local itemName = GamePlayConfigData.dropJNSNameInfo[dropItemID][1]
        local itemImg = GamePlayConfigData.dropJNSNameInfo[dropItemID][2]

        if isShare == 0 then
            local playerName = GamePlayDynamicData.playerList[pid]["playerName"]
            -- itemName = playerName .. "的" .. itemName
            itemName = itemName
        end

        sc.SetDropItemName(dropItem, StringId.new(itemName))
        sc.SetDropItemIconPath(dropItem, StringId.new(itemImg))
    end
end




local function dropShuXingShu2(data)
    local position = data[1]
    local pid = data[2]
    local isShare = GamePlayDynamicData.playerShareInfo[pid]
    local index
    if isShare == 1 then
        index = 5
    else
        index = GamePlayDynamicData.playerList[pid]["roomIndex"]
    end
    local dropItemInfo = GamePlayConfigData.shuxingshuDropItemInfo[index]
    local dropItemID = dropItemInfo[sc.RangedRand(1, #dropItemInfo)]
    local dropItem = sc.SpawnDropItem(dropItemID, GamePlayConfigData.actorCamp, true, position)
    sc.SetDropItemBubbleScale(dropItem, 550)  -- 缩放泡泡
    sc.SetDropItemIconScale(dropItem, 1700) -- 缩放图片

    local itemName = GamePlayConfigData.dropSXNameInfo[dropItemID][1]
    local itemImg = GamePlayConfigData.dropSXNameInfo[dropItemID][2]

    if isShare == 0 then
        local playerName = GamePlayDynamicData.playerList[pid]["playerName"]
        -- itemName = playerName .. "的" .. itemName
        itemName = itemName
    end

    sc.SetDropItemName(dropItem, StringId.new(itemName))

    sc.SetDropItemIconPath(dropItem, StringId.new(itemImg))
end








-- 创建重铸锤的函数
local function dropCZC(src, orignalAtker)
    if orignalAtker then
        local position, _, _ = sc.GetActorLogicPos(src)
        local newPositon = VInt3.new(position.x, 0, position.z)
        local dropItemID = 120901
        local dropItem = sc.SpawnDropItem(dropItemID, GamePlayConfigData.actorCamp, true, newPositon)
        sc.SetDropItemBubbleScale(dropItem, 550)  -- 缩放泡泡
        sc.SetDropItemIconScale(dropItem, 1400) -- 缩放图片
    end
end



-- 创建装备掉落物的函数
local function dropEquip1(src, orignalAtker, equipLevel)
    if orignalAtker then
        local position, _, _ = sc.GetActorLogicPos(src)

        local newPositon = VInt3.new(position.x, 0, position.z)

        local dropItemInfo = GamePlayConfigData.bossGuaiDropEquipInfo[equipLevel]
        local dropItemID = dropItemInfo[sc.RangedRand(1, #dropItemInfo)]

        sc.SpawnDropItem(dropItemID, GamePlayConfigData.actorCamp, true, newPositon)
    end
end


local function dropEquip2(src, orignalAtker, monsterLevel)
    if orignalAtker then
        local position, _, _ = sc.GetActorLogicPos(src)

        local newPositon = VInt3.new(position.x, 0, position.z)

        local dropItemInfo = GamePlayConfigData.diyuhuoDropEquipInfo[monsterLevel]
        local dropItemID = dropItemInfo[sc.RangedRand(1, #dropItemInfo)]

        sc.SpawnDropItem(dropItemID, GamePlayConfigData.actorCamp, true, newPositon)
    end
end



local dropInfo = {}
local jjbdID = 12301
local jjzdID = 12302


local function yinxueFunc(actor, pid, ciTiaoLv)
    if ciTiaoLv == 1 then
        sc.BuffAction(actor, actor, true, false, 103031, 0, 0)
    elseif ciTiaoLv == 2 then
        sc.BuffAction(actor, actor, true, false, 10303101, 0, 0)
    elseif ciTiaoLv == 3 then
        sc.BuffAction(actor, actor, true, false, 10303102, 0, 0)
    end
end


local function shandianFunc(actor, pid, ciTiaoLv)
    if ciTiaoLv == 1 then
        sc.BuffAction(actor, actor, true, false, 103032, 0, 0)
    elseif ciTiaoLv == 2 then
        sc.BuffAction(actor, actor, true, false, 103032001, 0, 0)
    elseif ciTiaoLv == 3 then
        sc.BuffAction(actor, actor, true, false, 103032012, 0, 0)
    end
end


local function yinzhuiFunc(actor, pid, ciTiaoLv)
    if ciTiaoLv == 1 then
        sc.BuffAction(actor, actor, true, false, 103033, 0, 0)
    elseif ciTiaoLv == 2 then
        sc.BuffAction(actor, actor, true, false, 10303301, 0, 0)
    elseif ciTiaoLv == 3 then
        sc.BuffAction(actor, actor, true, false, 10303302, 0, 0)
    end
end


local function ningshenFunc(actor, pid, ciTiaoLv)
    if ciTiaoLv == 1 then
        sc.BuffAction(actor, actor, true, false, 103034, 0, 0)
    elseif ciTiaoLv == 2 then
        sc.BuffAction(actor, actor, true, false, 10303401, 0, 0)
    elseif ciTiaoLv == 3 then
        sc.BuffAction(actor, actor, true, false, 10303402, 0, 0)
    end
end


local function wujinFunc(actor, pid, ciTiaoLv)
    if ciTiaoLv == 1 then
        sc.BuffAction(actor, actor, true, false, 103035, 0, 0)
    elseif ciTiaoLv == 2 then
        sc.BuffAction(actor, actor, true, false, 10303501, 0, 0)
    elseif ciTiaoLv == 3 then
        sc.BuffAction(actor, actor, true, false, 10303502, 0, 0)
    end
end

local function qianggongFunc(actor, pid, ciTiaoLv)
    if ciTiaoLv == 1 then
        sc.BuffAction(actor, actor, true, false, 103036, 0, 0)
    elseif ciTiaoLv == 2 then
        sc.BuffAction(actor, actor, true, false, 10303601, 0, 0)
    elseif ciTiaoLv == 3 then
        sc.BuffAction(actor, actor, true, false, 10303602, 0, 0)
    end
end


local function qxuwuFunc(actor, pid, ciTiaoLv)
    if ciTiaoLv == 1 then
        sc.BuffAction(actor, actor, true, false, 103037, 0, 0)
    elseif ciTiaoLv == 2 then
        sc.BuffAction(actor, actor, true, false, 10303701, 0, 0)
    elseif ciTiaoLv == 3 then
        sc.BuffAction(actor, actor, true, false, 10303702, 0, 0)
    end
end


local function shishenFunc(actor, pid, ciTiaoLv)
    if ciTiaoLv == 1 then
        sc.BuffAction(actor, actor, true, false, 103038, 0, 0)
    elseif ciTiaoLv == 2 then
        sc.BuffAction(actor, actor, true, false, 10303801, 0, 0)
    elseif ciTiaoLv == 3 then
        sc.BuffAction(actor, actor, true, false, 10303802, 0, 0)
    end
end

local function huimieFunc(actor, pid, ciTiaoLv)
    if ciTiaoLv == 1 then
        sc.BuffAction(actor, actor, true, false, 103039, 0, 0)
    elseif ciTiaoLv == 2 then
        sc.BuffAction(actor, actor, true, false, 10303901, 0, 0)
    elseif ciTiaoLv == 3 then
        sc.BuffAction(actor, actor, true, false, 10303902, 0, 0)
    end
end

local function pojiaFunc(actor, pid, ciTiaoLv)
    if ciTiaoLv == 1 then
        sc.BuffAction(actor, actor, true, false, 1030310, 0, 0)
    elseif ciTiaoLv == 2 then
        sc.BuffAction(actor, actor, true, false, 103031001, 0, 0)
    elseif ciTiaoLv == 3 then
        sc.BuffAction(actor, actor, true, false, 103031002, 0, 0)
    end
end


local function shenxingFunc(actor, pid, ciTiaoLv)
    if ciTiaoLv == 1 then
        sc.BuffAction(actor, actor, true, false, 1030311, 0, 0)
    elseif ciTiaoLv == 2 then
        sc.BuffAction(actor, actor, true, false, 103031101, 0, 0)
    elseif ciTiaoLv == 3 then
        sc.BuffAction(actor, actor, true, false, 103031102, 0, 0)
    end
end


local function keyinFunc(actor, pid, ciTiaoLv)
    if ciTiaoLv == 1 then
        sc.BuffAction(actor, actor, true, false, 1030312, 0, 0)
    elseif ciTiaoLv == 2 then
        sc.BuffAction(actor, actor, true, false, 103031201, 0, 0)
    elseif ciTiaoLv == 3 then
        sc.BuffAction(actor, actor, true, false, 103031202, 0, 0)
    end
end


local function xueshiFunc(actor, pid, ciTiaoLv)
    if ciTiaoLv == 1 then
        sc.BuffAction(actor, actor, true, false, 1030313, 0, 0)
    elseif ciTiaoLv == 2 then
        sc.BuffAction(actor, actor, true, false, 103031301, 0, 0)
    elseif ciTiaoLv == 3 then
        sc.BuffAction(actor, actor, true, false, 103031302, 0, 0)
    end
end


local function yuanquanFunc(actor, pid, ciTiaoLv)
    if ciTiaoLv == 1 then
        sc.BuffAction(actor, actor, true, false, 1030314, 0, 0)
    elseif ciTiaoLv == 2 then
        sc.BuffAction(actor, actor, true, false, 103031401, 0, 0)
    elseif ciTiaoLv == 3 then
        sc.BuffAction(actor, actor, true, false, 103031402, 0, 0)
    end
end


local function zhipeiFunc(actor, pid, ciTiaoLv)
    if ciTiaoLv == 1 then
        sc.BuffAction(actor, actor, true, false, 1030315, 0, 0)
    elseif ciTiaoLv == 2 then
        sc.BuffAction(actor, actor, true, false, 103031501, 0, 0)
    elseif ciTiaoLv == 3 then
        sc.BuffAction(actor, actor, true, false, 103031502, 0, 0)
    end
end


local function tongyuFunc(actor, pid, ciTiaoLv)
    if ciTiaoLv == 1 then
        sc.BuffAction(actor, actor, true, false, 1030316, 0, 0)
    elseif ciTiaoLv == 2 then
        sc.BuffAction(actor, actor, true, false, 103031601, 0, 0)
    elseif ciTiaoLv == 3 then
        sc.BuffAction(actor, actor, true, false, 103031602, 0, 0)
    end
end


local function cishanFunc(actor, pid, ciTiaoLv)
    if ciTiaoLv == 1 then
        GamePlayDynamicData.cishanInfo[pid] = GamePlayDynamicData.cishanInfo[pid] + 0.075
    elseif ciTiaoLv == 2 then
        GamePlayDynamicData.cishanInfo[pid] = GamePlayDynamicData.cishanInfo[pid] + 0.1
    elseif ciTiaoLv == 3 then
        GamePlayDynamicData.cishanInfo[pid] = GamePlayDynamicData.cishanInfo[pid] + 0.125
    end
end


local function baonuFunc(actor, pid, ciTiaoLv)
    if ciTiaoLv == 1 then
        sc.BuffAction(actor, actor, true, false, 1032501, 0, 0)
        GamePlayDynamicData.shuXingInfo[pid][1] = GamePlayDynamicData.shuXingInfo[pid][1] + 0.1
    elseif ciTiaoLv == 2 then
        sc.BuffAction(actor, actor, true, false, 1032502, 0, 0)
        GamePlayDynamicData.shuXingInfo[pid][1] = GamePlayDynamicData.shuXingInfo[pid][1] + 0.125
    elseif ciTiaoLv == 3 then
        sc.BuffAction(actor, actor, true, false, 1032503, 0, 0)
        GamePlayDynamicData.shuXingInfo[pid][1] = GamePlayDynamicData.shuXingInfo[pid][1] + 0.15

    end
end

local function jianyiFunc(actor, pid, ciTiaoLv)
    if ciTiaoLv == 1 then
        sc.BuffAction(actor, actor, true, false, 1032504, 0, 0)
        GamePlayDynamicData.shuXingInfo[pid][2] = GamePlayDynamicData.shuXingInfo[pid][2] + 0.1
    elseif ciTiaoLv == 2 then
        sc.BuffAction(actor, actor, true, false, 1032505, 0, 0)
        GamePlayDynamicData.shuXingInfo[pid][2] = GamePlayDynamicData.shuXingInfo[pid][2] + 0.125
    elseif ciTiaoLv == 3 then
        sc.BuffAction(actor, actor, true, false, 1032506, 0, 0)
        GamePlayDynamicData.shuXingInfo[pid][2] = GamePlayDynamicData.shuXingInfo[pid][2] + 0.15
    end
end


local function kuizengFunc(actor, pid, ciTiaoLv)
    if ciTiaoLv == 1 then
        GamePlayDynamicData.jinbijiacheng[pid] = GamePlayDynamicData.jinbijiacheng[pid] + 0.075
    elseif ciTiaoLv == 2 then
        GamePlayDynamicData.jinbijiacheng[pid] = GamePlayDynamicData.jinbijiacheng[pid] + 0.081
    elseif ciTiaoLv == 3 then
        GamePlayDynamicData.jinbijiacheng[pid] = GamePlayDynamicData.jinbijiacheng[pid] + 0.125
    end
end


local function canfeiFunc(actor, pid, ciTiaoLv)
    sc.AddPassiveSkill(actor, 103021, 0)
    sc.BuffAction(actor, actor, true, false, 103141, 0, 0)
end



local function anmuFunc(actor, pid, ciTiaoLv)
    sc.AddPassiveSkill(actor, 1030318, 0)
    sc.BuffAction(actor, actor, true, false, 103143, 0, 0)
end


local function baofengFunc(actor, pid, ciTiaoLv)
    sc.AddPassiveSkill(actor, 1030320, 0)
    sc.BuffAction(actor, actor, true, false, 103144, 0, 0)
end


local function pojunFunc(actor, pid, ciTiaoLv)
    sc.AddPassiveSkill(actor, 1030321, 0)
    sc.BuffAction(actor, actor, true, false, 103142, 0, 0)
end


local function pobaiFunc(actor, pid, ciTiaoLv)
    sc.BuffAction(actor, actor, true, false, 1030323, 0, 0)
    sc.BuffAction(actor, actor, true, false, 103146, 0, 0)
end


local function shenpanFunc(actor, pid, ciTiaoLv)
    sc.BuffAction(actor, actor, true, false, 103052, 0, 0)
    sc.BuffAction(actor, actor, true, false, 103053, 0, 0)
    sc.BuffAction(actor, actor, true, false, 103145, 0, 0)
end

local function huixiangFunc(actor, pid, ciTiaoLv)
    sc.BuffAction(actor, actor, true, false, 103042, 0, 0)
    sc.BuffAction(actor, actor, true, false, 103147, 0, 0)
end



local function qiangjiFunc(actor, pid, ciTiaoLv)
    sc.AddPassiveSkill(actor, 1030325, 0)
    sc.BuffAction(actor, actor, true, false, 103148, 0, 0)
end


local function jingjiFunc(actor, pid, ciTiaoLv)
    sc.BuffAction(actor, actor, true, false, 1030327, 0, 0)
    sc.BuffAction(actor, actor, true, false, 103149, 0, 0)
end

local function xianjiFunc(actor, pid, ciTiaoLv)
    sc.AddPassiveSkill(actor, 103051, 0)
    sc.BuffAction(actor, actor, true, false, 10305101, 0, 0)
    sc.BuffAction(actor, actor, true, false, 1031410, 0, 0)
end



local function bingxinFunc(actor, pid, ciTiaoLv)
    sc.AddPassiveSkill(actor, 1030329, 0)
    sc.BuffAction(actor, actor, true, false, 1031411, 0, 0)
end


local function wuweiFunc(actor, pid, ciTiaoLv)
    sc.AddPassiveSkill(actor, 103041, 0)
    sc.BuffAction(actor, actor, true, false, 1031412, 0, 0)
end


local function chongzuFunc(actor, pid, ciTiaoLv)
    sc.SetReviveTime(actor, 3000)
    sc.BuffAction(actor, actor, true, false, 1031413, 0, 0)
end





local ciTiaoFuncTable = {
    ["饮血"] = yinxueFunc,
    ["闪电"] = shandianFunc,
    ["隐追"] = yinzhuiFunc,
    ["凝神"] = ningshenFunc,
    ["无尽"] = wujinFunc,
    ["强攻"] = qianggongFunc,
    ["虚无"] = qxuwuFunc,
    ["弑神"] = shishenFunc,
    ["毁灭"] = huimieFunc,
    ["破甲"] = pojiaFunc,
    ["神行"] = shenxingFunc,
    ["刻印"] = keyinFunc,
    ["学识"] = xueshiFunc,
    ["源泉"] = yuanquanFunc,
    ["支配"] = zhipeiFunc,
    ["统御"] = tongyuFunc,
    ["慈善"] = cishanFunc,
    ["暴怒"] = baonuFunc, -- 最终伤害
    ["坚毅"] = jianyiFunc, -- 最终减伤
    ["馈赠"] = kuizengFunc,



    ["残废"] = canfeiFunc,
    ["暗幕"] = anmuFunc,
    ["暴风"] = baofengFunc,
    ["破军"] = pojunFunc,
    ["破败"] = pobaiFunc,
    ["审判"] = shenpanFunc,
    ["回响"] = huixiangFunc,
    ["强击"] = qiangjiFunc,
    ["荆棘"] = jingjiFunc,
    ["献祭"] = xianjiFunc,
    ["冰心"] = bingxinFunc,
    ["无畏"] = wuweiFunc,
    ["重组"] = chongzuFunc,
}



function useCiTiaoFunc(actor, pid, ciTiaoText, ciTiaoLv)
    if ciTiaoFuncTable[ciTiaoText] then
        ciTiaoFuncTable[ciTiaoText](actor, pid, ciTiaoLv)
    end
end



local function playerDead(src,atker,orignalAtker,logicAtker,bImmediateRevive,assistantList)
    -- local actorID = sc.GetActorSystemProperty(logicAtker, 1004)
    local slot = GamePlayConfigData.activeSkillSlot
    if sc.GetSkillSlotSkillID(src, slot) == 128551 or sc.GetSkillSlotSkillID(src, slot) == 1285512 then
        sc.DelPassiveSkill(src, 12855011)
        sc.DelSkill(src, slot)
        sc.AddSkill(src, 128551, 1, slot, false, false)
    elseif sc.GetSkillSlotSkillID(src, slot) == 128552 or sc.GetSkillSlotSkillID(src, slot) == 1285522 then
        sc.DelPassiveSkill(src, 1285521)
        sc.DelSkill(src, slot)
        sc.AddSkill(src, 128552, 1, slot, false, false)
    elseif sc.GetSkillSlotSkillID(src, slot) == 128553 or sc.GetSkillSlotSkillID(src, slot) == 1285532 then
        sc.DelPassiveSkill(src, 1285531)
        sc.DelSkill(src, slot)
        sc.AddSkill(src, 128553, 1, slot, false, false)
    elseif sc.GetSkillSlotSkillID(src, slot) == 12855 or sc.GetSkillSlotSkillID(src, slot) == 1285505 then
        sc.DelPassiveSkill(src, 1285501)
        sc.DelSkill(src, slot)
        sc.AddSkill(src, 12855, 1, slot, false, false)
    end

    local pid = sc.GetActorSystemProperty(src, 1007)
    sc.CallUILuaFunction({pid, "actorDead", sc.GetActorSystemProperty(src, 1004)}, StringId.new("addGMGongGaoInfo"))
    sc.CallUILuaFunction({pid,false}, StringId.new("jiluActorDead_callBack"))
    GamePlayDynamicData.playerLiveInfo[pid] = false
    GamePlayDynamicData.deadCntInfo[pid] = GamePlayDynamicData.deadCntInfo[pid] + 1
end



local function equipBossDeadFunc(monsterCfgID, callPlayerID)
    if monsterCfgID == GamePlayConfigData.equipBossCfgIDList[1] then -- 武器挑战boss
        GamePlayDynamicData.grandListInfo[callPlayerID][1][1] = 1
        GamePlayDynamicData.grandListInfo[callPlayerID][1][2] = GamePlayDynamicData.grandListInfo[callPlayerID][1][2] + 1
        GamePlayDynamicData.grandListInfo[callPlayerID][1][3] = false


        sc.CallUILuaFunction({callPlayerID}, StringId.new("SetWeaponBossGrade"))
    elseif monsterCfgID == GamePlayConfigData.equipBossCfgIDList[2] then
        GamePlayDynamicData.grandListInfo[callPlayerID][2][1] = 1
        GamePlayDynamicData.grandListInfo[callPlayerID][2][2] = GamePlayDynamicData.grandListInfo[callPlayerID][2][2] + 1
        GamePlayDynamicData.grandListInfo[callPlayerID][2][3] = false

        sc.CallUILuaFunction({callPlayerID}, StringId.new("SetGuardBossGrade"))
    elseif monsterCfgID == GamePlayConfigData.equipBossCfgIDList[3] then
        GamePlayDynamicData.grandListInfo[callPlayerID][3][1] = 1
        GamePlayDynamicData.grandListInfo[callPlayerID][3][2] = GamePlayDynamicData.grandListInfo[callPlayerID][3][2] + 1
        GamePlayDynamicData.grandListInfo[callPlayerID][3][3] = false

        sc.CallUILuaFunction({callPlayerID}, StringId.new("SetRingBossGrade"))
    end
end



local function monsterDaedGiveCoin(boci, monsterCfgID, killPID, callPlayerID, logicAtker, callPlayer, orignalAtker)
    if boci <= GamePlayConfigData.refreshMonsterLineCount and boci > 0 and GamePlayConfigData.bingXianMonsterIDInfo[monsterCfgID] then
        local jinbi = GamePlayConfigData.bingXianData[boci][3]

        if killPID == callPlayerID then
            -- print("加成过后得到的金币： " .. addjinbi)
            local addjinbi = math.ceil(GamePlayDynamicData.jinbijiacheng[killPID]*jinbi)+jinbi -- 向上取整
            sc.AddCoin(logicAtker, addjinbi, true, true, 0)
            GamePlayDynamicData.allJinBi[killPID] = GamePlayDynamicData.allJinBi[killPID] + addjinbi
        else
            local addKilljinbi = math.ceil(jinbi*0.15)
            local callKillJinbi = math.ceil(jinbi*0.85)
            local addjinbi1 = math.ceil(GamePlayDynamicData.jinbijiacheng[killPID]*addKilljinbi)+addKilljinbi -- 向上取整
            local addjinbi2 = math.ceil(GamePlayDynamicData.jinbijiacheng[callPlayerID]*callKillJinbi)+callKillJinbi -- 向上取整
            sc.AddCoin(logicAtker, addjinbi1, true, true, 0)
            sc.AddCoin(callPlayer, addjinbi2, true, true, 0)
            GamePlayDynamicData.allJinBi[killPID] = GamePlayDynamicData.allJinBi[killPID] + addjinbi1
            GamePlayDynamicData.allJinBi[callPlayerID] = GamePlayDynamicData.allJinBi[callPlayerID] + addjinbi2
        end

        local killaddNum = GamePlayDynamicData.killMonsterAdd[killPID]
        if killaddNum ~= 0 then
            sc.AddCoin(logicAtker, killaddNum, true, true, 0)
        end

        local money1 = sc.GetCoinNum(orignalAtker)
        local money2 = sc.GetCoinNum(callPlayer)
        sc.CallUILuaFunction({killPID,money1}, StringId.new("RefreshMoney"))
        sc.CallUILuaFunction({callPlayerID,money2}, StringId.new("RefreshMoney"))
    end
end



local function bossDeadFunc(monsterCfgID, callPlayer, callPlayerID, killPID,orignalAtker, src, boci)
    if monsterCfgID == GamePlayConfigData.diyuhuoCfgID then -- 击败的是地狱火
        -- 击杀深渊魔王加英雄属性
        sc.BuffAction(callPlayer, callPlayer, true, false, GamePlayConfigData.addGJLBuffList["symw"], 0, 0)

        sc.CallUILuaFunction({callPlayerID}, StringId.new("diyuhuoDead"))
        GamePlayDynamicData.challengeBossInfo[killPID][3][2] = false

        local nowMoney = sc.GetCoinNum(callPlayer)
        sc.CallUILuaFunction({callPlayerID,nowMoney}, StringId.new("RefreshMoney"))

        local level = GamePlayDynamicData.challengeBossInfo[callPlayerID][3][1]-1

        if #GamePlayDynamicData.ciTiaoKuInfo[callPlayerID] <= 0 then
            return
        end
        local ciTiao
        local ciTiaoLv = sc.RangedRand(1,3)

        local ciTiaoIndex = sc.RangedRand(1,#GamePlayDynamicData.ciTiaoKuInfo[callPlayerID])
        ciTiao = GamePlayDynamicData.ciTiaoKuInfo[callPlayerID][ciTiaoIndex][1]
        -- table.remove(GamePlayDynamicData.ciTiaoKuInfo[callPlayerID], ciTiaoIndex)

        useCiTiaoFunc(callPlayer, callPlayerID, ciTiao, ciTiaoLv)

        local num1
        local num2
        if sc.RangedRand(1,2) == 1 then -- 暴击
            num1 = 1
            sc.BuffAction(callPlayer, callPlayer, true, false, 103101, 0, 0)
        else
            num1 = 2
            sc.BuffAction(callPlayer, callPlayer, true, false, 103102, 0, 0)
        end

        if sc.RangedRand(1,2) == 1 then -- 吸血
            num2 = 1
            sc.BuffAction(callPlayer, callPlayer, true, false, 103103, 0, 0)
        else
            num2 = 2
            sc.BuffAction(callPlayer, callPlayer, true, false, 103104, 0, 0)
        end

        sc.BuffAction(callPlayer, callPlayer, true, false, 103105, 0, 0)

        sc.CallUILuaFunction({callPlayerID, ciTiao, ciTiaoLv, num1, num2}, StringId.new("addCiTiao_callBack"))
    elseif monsterCfgID == GamePlayConfigData.jinbiCfgID then -- 金币怪等级没加
        -- 击杀粮草押运官加英雄属性
        sc.BuffAction(callPlayer, callPlayer, true, false, GamePlayConfigData.addGJLBuffList["lcyyg"], 0, 0)

        local level = GamePlayDynamicData.challengeBossInfo[callPlayerID][1][1]-1
        local addCoin = GamePlayConfigData.jinbiguaiShuXing[level][5] -- 三个boss不在死的时候加等级，召唤的时候加等级
        sc.AddCoin(callPlayer, addCoin, true, true, 0)
        GamePlayDynamicData.allJinBi[callPlayerID] = GamePlayDynamicData.allJinBi[callPlayerID] + addCoin

        local nowMoney = sc.GetCoinNum(callPlayer)
        sc.CallUILuaFunction({callPlayerID, nowMoney}, StringId.new("RefreshMoney"))
    elseif monsterCfgID == GamePlayConfigData.jinkuangCfgID then
        -- 击杀魔袋长老加英雄属性
        sc.BuffAction(callPlayer, callPlayer, true, false, GamePlayConfigData.addGJLBuffList["mdzl"], 0, 0)
        sc.CallUILuaFunction({callPlayerID}, StringId.new("setIsGoldOrePrizeLive"))
        -- 赠送一个魔袋
        sc.CallUILuaFunction({callPlayerID, GamePlayDynamicData.challengeBossInfo[callPlayerID][2][1]-1}, StringId.new("GoldOrePrize"))
        GamePlayDynamicData.challengeBossInfo[callPlayerID][2][2] = false

        local needKillCount = GamePlayConfigData.bagInfo[GamePlayDynamicData.challengeBossInfo[callPlayerID][2][1]-1][4]
        GamePlayDynamicData.modaiHaveInfo[callPlayerID][1] = GamePlayDynamicData.challengeBossInfo[callPlayerID][2][1]-1
        GamePlayDynamicData.modaiHaveInfo[callPlayerID][2] = true
        GamePlayDynamicData.modaiHaveInfo[callPlayerID][3] = 0
        GamePlayDynamicData.modaiHaveInfo[callPlayerID][4] = needKillCount

    elseif GamePlayConfigData.jingYingGuaiRoundIndexsBack[monsterCfgID] ~= nil then -- 被击败的是精英怪
        if sc.GetActorSystemProperty(orignalAtker, 1002) == 0 then
            local pid = callPlayerID
            local cnt = GamePlayConfigData.jingYingGuaiData[boci][6]/4

            local position, _, _ = sc.GetActorLogicPos(orignalAtker)
            local newPositon = VInt3.new(position.x, 0, position.z)

            local position1, _, _ = sc.GetActorLogicPos(src)
            local newPositon1 = VInt3.new(position1.x, 0, position1.z)

            local pos = newPositon or newPositon1

            sc.SetTimer(150, 0, cnt , dropShuXingShu2, {pos, pid})
        end

    elseif GamePlayConfigData.bossGuaiRoundIndexsBack[monsterCfgID] ~= nil then
        -- callUI给被动选择框
        -- 得到随机的2选1被动

        local newTab = {}

        local table = GamePlayDynamicData.erxuanyiSkillKu[callPlayerID]
        local count = 2
        local length = #table
        if length > 2 then
            for i = 1, count do
                local ri = sc.RangedRand(i, length)
                local tmp = table[i]
                table[i] = table[ri]
                table[ri] = tmp
                newTab[i] = table[i]
            end

            sc.CallUILuaFunction({callPlayerID, newTab[1], newTab[2]}, StringId.new("bigPassSkill_callBack"))
        end
        -- local equipLevel = GamePlayConfigData.bossGuaiRoundIndexsBack[monsterCfgID][2]
        -- local skillLevel = GamePlayConfigData.bossGuaiRoundIndexsBack[monsterCfgID][3]
        -- dropEquip1(src, logicAtker, equipLevel)
        -- if sc.RangedRand(0,1000) <= -1 then
        --     dropSkill(src, logicAtker, skillLevel)
        -- end
    elseif monsterCfgID == GamePlayConfigData.bigBossCfgID then
        -- 在规定时间内击杀了最终boss
        -- 发送游戏结束 结果是胜利
        GamePlayDynamicData.BossKillCount = GamePlayDynamicData.BossKillCount + 1
        if GamePlayDynamicData.BossKillCount >= GamePlayDynamicData.needKillCount then
            local pidEnd = sc.GetHostPlayerID()
            sc.CallUILuaFunction({pidEnd, true}, StringId.new("killBossEnough"))
        end
    end
end





local function monsterDaed(src,atker,orignalAtker,logicAtker,bImmediateRevive,assistantList)
    local monsterCfgID = sc.GetActorSystemProperty(src, 1000)

    local boci = sc.GetCustomProperty(StringId.new("boci"), sc.enCustomType.Int, src) -- number
    if boci == nil then
        return
    end

    local callPlayer
    local callPlayerID = sc.GetCustomProperty(StringId.new("callPlayerID"), sc.enCustomType.Int, src) -- number
    if callPlayerID ~= nil then
        callPlayer = sc.GetControlActorByPlayerID(callPlayerID)
    end

    sc.AddSoulExp(logicAtker, GamePlayConfigData.jinyanzhi, 0)

    local killActorID = sc.GetActorSystemProperty(logicAtker, 1004)
    local killPID = sc.GetActorSystemProperty(logicAtker, 1007)

    -- UI怪物数-1
    if GamePlayConfigData.MonsterCfgList[monsterCfgID] then
        local selfPlayerID = sc.GetHostPlayerID()
        sc.CallUILuaFunction({selfPlayerID, false}, StringId.new("ChangeMonsterCountText"))
    end

    -- 单局的击败数记录
    sc.CallUILuaFunction({killPID}, StringId.new("addKillCount"))

    -- 装备挑战boss    的属性随击败次数而增加的召唤计数
    equipBossDeadFunc(monsterCfgID, callPlayerID)

    -- 掉落相关
    if sc.GetActorSystemProperty(logicAtker, 1002) == 0 and sc.GetActorSystemProperty(logicAtker, 1003) == GamePlayConfigData.actorCamp then


        local position, _, _ = sc.GetActorLogicPos(src)
        local newPositon = VInt3.new(position.x, 0, position.z)
        if dropInfo[killActorID] == nil then
            dropInfo[killActorID] = {1}
            local index
            if GamePlayDynamicData.playerShareInfo[killPID] == 0 then
                index = GamePlayDynamicData.playerList[killPID]["roomIndex"]
            else
                index = 5
            end
            jjbdID = GamePlayConfigData.dropSKillIDLevelInfo[index][1][1]
            local dropItem = sc.SpawnDropItem(jjbdID, GamePlayConfigData.actorCamp, true, newPositon)
            sc.SetDropItemBubbleScale(dropItem, 550)  -- 缩放泡泡
            sc.SetDropItemIconScale(dropItem, 550) -- 缩放图片

            local itemName = GamePlayConfigData.dropJNSNameInfo[jjbdID][1]
            local itemImg = GamePlayConfigData.dropJNSNameInfo[jjbdID][2]
            if GamePlayDynamicData.playerShareInfo[killPID] == 0 then
                local playerName = GamePlayDynamicData.playerList[killPID]["playerName"]
                -- itemName = playerName .. "的" .. itemName
                itemName = itemName
            end

            sc.SetDropItemName(dropItem, StringId.new(itemName))

            sc.SetDropItemIconPath(dropItem, StringId.new(itemImg))
        else
            if #dropInfo[killActorID] < 2 then
                table.insert(dropInfo[killActorID], 1)
                local index
                if GamePlayDynamicData.playerShareInfo[killPID] == 0 then
                    index = GamePlayDynamicData.playerList[killPID]["roomIndex"]
                else
                    index = 5
                end
                jjzdID = GamePlayConfigData.dropSKillIDLevelInfo[index][1][2]
                local dropItem = sc.SpawnDropItem(jjzdID, GamePlayConfigData.actorCamp, true, newPositon)
                sc.SetDropItemBubbleScale(dropItem, 550)  -- 缩放泡泡
                sc.SetDropItemIconScale(dropItem, 550) -- 缩放图片

                local itemName = GamePlayConfigData.dropJNSNameInfo[jjzdID][1]
                local itemImg = GamePlayConfigData.dropJNSNameInfo[jjzdID][2]
                if GamePlayDynamicData.playerShareInfo[killPID] == 0 then
                    local playerName = GamePlayDynamicData.playerList[killPID]["playerName"]
                    -- itemName = playerName .. "的" .. itemName
                    itemName = itemName
                end

                sc.SetDropItemName(dropItem, StringId.new(itemName))
                sc.SetDropItemIconPath(dropItem, StringId.new(itemImg))

            else
                -- 掉落物概率 几率
                if sc.RangedRand(0,10000) >= 9970 and boci then
                    local skillLevel = 1
                    if 0 < boci and boci < 5 then
                        skillLevel = 1
                    elseif 5 <= boci and boci < 15 then
                        skillLevel = 2
                    elseif 15 <= boci and boci < 25 then
                        skillLevel = 3
                    elseif 25 <= boci and boci <= 40 then
                        skillLevel = 4
                    end
                    -- print("run in 掉落物概率" .. skillLevel .. "boci: " .. boci)
                    dropSkill(src, logicAtker, skillLevel)
                end
            end
        end

        if sc.RangedRand(0,10000) >= 10001 and boci then -- 重铸锤掉率
            dropCZC(src, logicAtker)
        end

        if GamePlayDynamicData.modaiHaveInfo[killPID][2] == true then
            GamePlayDynamicData.modaiHaveInfo[killPID][3] = GamePlayDynamicData.modaiHaveInfo[killPID][3] + 1
            if GamePlayDynamicData.modaiHaveInfo[killPID][3] >= GamePlayDynamicData.modaiHaveInfo[killPID][4] then
                GamePlayDynamicData.modaiHaveInfo[killPID][2] = false
                GamePlayDynamicData.modaiHaveInfo[killPID][3] = 0
                local addmoney = GamePlayConfigData.bagInfo[GamePlayDynamicData.modaiHaveInfo[killPID][1]][5]
                sc.AddCoin(logicAtker, addmoney, false, true, 0)
                GamePlayDynamicData.allJinBi[killPID] = GamePlayDynamicData.allJinBi[killPID] + addmoney

                sc.CallUILuaFunction({killPID, GamePlayDynamicData.modaiHaveInfo[killPID][1]}, StringId.new("modaiKillEnough_callBack"))

            else
                sc.CallUILuaFunction({killPID, GamePlayDynamicData.modaiHaveInfo[killPID][3], GamePlayDynamicData.modaiHaveInfo[killPID][4]}, StringId.new("modaiKill_callBack"))
            end
        end
    end

    -- 兵线击杀给金币
    monsterDaedGiveCoin(boci, monsterCfgID, killPID, callPlayerID, logicAtker, callPlayer, orignalAtker)

    -- 击败boss后的处理
    bossDeadFunc(monsterCfgID, callPlayer, callPlayerID, killPID,orignalAtker, src, boci)
end

-- //src(ActorRoot):  死亡源
-- //atker(ActorRoot):  攻击者
-- //orignalAtker(ActorRoot): 原始攻击者   (塔击败目标的话， 原始攻击者就是塔. 逻辑攻击者就是玩家)
-- //logicAtker(ActorRoot)：逻辑攻击者
-- //bImmediateRevive(bool):  是否为立即复活 (复活甲会传True)
function ActorDead(src,atker,orignalAtker,logicAtker,bImmediateRevive,assistantList) -- deadddd
    -- if true then
    --     return
    -- end
    if sc.GetActorSystemProperty(src, 1002) == 0 then -- 英雄死亡
        playerDead(src,atker,orignalAtker,logicAtker,bImmediateRevive,assistantList)
    else -- 怪物死亡
        monsterDaed(src,atker,orignalAtker,logicAtker,bImmediateRevive,assistantList)
    end
end







-- 扣钱
local function deductMoney(money, actor, pid)
    sc.AddCoin(actor, 0-money, true, true, 0)
    local nowMoney = sc.GetCoinNum(actor)
    GamePlayDynamicData.expendMoneyInfo[pid] = GamePlayDynamicData.expendMoneyInfo[pid] + money

    sc.CallUILuaFunction({pid, nowMoney}, StringId.new("RefreshMoney"))
    sc.CallUILuaFunction({pid, money}, StringId.new("addExpendMoney"))
end



local function fumojilv(jieji)
    local isSucc = false
    if jieji == 1 then
        if sc.RangedRand(0,100) <= 100 then
            isSucc = true
        end
    elseif jieji == 2 then
        if sc.RangedRand(0,100) <= 95 then
            isSucc = true
        end
    elseif jieji == 3 then
        if sc.RangedRand(0,100) <= 90 then
            isSucc = true
        end
    elseif jieji == 4 then
        if sc.RangedRand(0,100) <= 80 then
            isSucc = true
        end
    elseif jieji == 5 then
        if sc.RangedRand(0,100) <= 70 then
            isSucc = true
        end
    elseif jieji == 6 then
        if sc.RangedRand(0,100) <= 60 then
            isSucc = true
        end
    elseif jieji == 7 then
        if sc.RangedRand(0,100) <= 50 then
            isSucc = true
        end
    elseif jieji == 8 then
        if sc.RangedRand(0,100) <= 40 then
            isSucc = true
        end
    end
    return isSucc
end


-- 武器附魔 处理函数
local function upWeapon(myJson, actor, playerID, actorID)
    if GamePlayDynamicData.grandListInfo[playerID][1][1] >= 6 or GamePlayDynamicData.grandListInfo[playerID][1][3] == true then
        return
    end

    local jieji = GamePlayDynamicData.grandListInfo[playerID][1][2]
    local needMoney = GamePlayConfigData.fumoInfo[1][jieji][4]

    if sc.GetCoinNum(actor) < needMoney then
        return
    end
    deductMoney(needMoney, actor, playerID)

    local isSucc = fumojilv(jieji)

    if GamePlayDynamicData.cishanInfo[playerID] > 0 then
        local fanhuanMoney = math.ceil(needMoney * GamePlayDynamicData.cishanInfo[playerID])
        sc.AddCoin(actor, fanhuanMoney, true, true, 0)
    end


    if isSucc == false then
        sc.CallUILuaFunction({playerID,false}, StringId.new("wp_CallBack"))
    else
        GamePlayDynamicData.grandListInfo[playerID][1][1] = GamePlayDynamicData.grandListInfo[playerID][1][1] + 1
        local str = "wpLv" .. jieji
        sc.BuffAction(actor, actor, true, false, GamePlayConfigData.addGJLBuffList[str], 0, 0)

        sc.CallUILuaFunction({playerID,true}, StringId.new("wp_CallBack"))
    end
end


-- 护甲附魔
local function upGuard(myJson, actor, playerID, actorID)
    if GamePlayDynamicData.grandListInfo[playerID][2][1] >= 6 or GamePlayDynamicData.grandListInfo[playerID][2][3] == true then
        return
    end

    local jieji = GamePlayDynamicData.grandListInfo[playerID][2][2]
    local needMoney = GamePlayConfigData.fumoInfo[2][jieji][4]

    if sc.GetCoinNum(actor) < needMoney then
        return
    end
    deductMoney(needMoney, actor, playerID)

    local isSucc = fumojilv(jieji)

    if GamePlayDynamicData.cishanInfo[playerID] > 0 then
        local fanhuanMoney = math.ceil(needMoney * GamePlayDynamicData.cishanInfo[playerID])
        sc.AddCoin(actor, fanhuanMoney, true, true, 0)
    end


    if isSucc == false then
        sc.CallUILuaFunction({playerID,false}, StringId.new("hj_CallBack"))
    else
        GamePlayDynamicData.grandListInfo[playerID][2][1] = GamePlayDynamicData.grandListInfo[playerID][2][1] + 1

        local str = "hjLv" .. jieji
        sc.BuffAction(actor, actor, true, false, GamePlayConfigData.addGJLBuffList[str], 0, 0)

        sc.CallUILuaFunction({playerID,true}, StringId.new("hj_CallBack"))
    end
end



-- 戒指附魔
local function upRing(myJson, actor, playerID, actorID)
    if GamePlayDynamicData.grandListInfo[playerID][3][1] >= 6 or GamePlayDynamicData.grandListInfo[playerID][3][3] == true then
        return
    end

    local jieji = GamePlayDynamicData.grandListInfo[playerID][3][2]
    local needMoney = GamePlayConfigData.fumoInfo[3][jieji][4]

    if sc.GetCoinNum(actor) < needMoney then
        return
    end
    deductMoney(needMoney, actor, playerID)

    local isSucc = fumojilv(jieji)

    if GamePlayDynamicData.cishanInfo[playerID] > 0 then
        local fanhuanMoney = math.ceil(needMoney * GamePlayDynamicData.cishanInfo[playerID])
        sc.AddCoin(actor, fanhuanMoney, true, true, 0)
    end

    if isSucc == false then
        sc.CallUILuaFunction({playerID,false}, StringId.new("jz_CallBack"))
    else
        GamePlayDynamicData.grandListInfo[playerID][3][1] = GamePlayDynamicData.grandListInfo[playerID][3][1] + 1

        local str = "jzLv" .. jieji
        sc.BuffAction(actor, actor, true, false, GamePlayConfigData.addGJLBuffList[str], 0, 0)

        sc.CallUILuaFunction({playerID,true}, StringId.new("jz_CallBack"))
    end
end



function getShuXing(actor, actorID, playerID)
    local wlgj = sc.GetActorSystemProperty(actor, 1)
    local zdsm = sc.GetActorSystemProperty(actor, 5)
    local wlfy = sc.GetActorSystemProperty(actor, 3)
    local gjsd = sc.GetActorSystemProperty(actor, 18)
    local bjjl = sc.GetActorSystemProperty(actor, 6)
    local smhf = sc.GetActorSystemProperty(actor, 16)
    local hjct = sc.GetActorSystemProperty(actor, 7)
    local wlxx = sc.GetActorSystemProperty(actor, 9)


    local fsgj = sc.GetActorSystemProperty(actor, 2)
    local lqsj = sc.GetActorSystemProperty(actor, 19)
    local fsfy = sc.GetActorSystemProperty(actor, 4)
    local ydsd = sc.GetActorSystemProperty(actor, 15)
    local bjsh = sc.GetActorSystemProperty(actor, 12)
    local nlhf = sc.GetActorSystemProperty(actor, 32)
    local fsct = sc.GetActorSystemProperty(actor, 8)
    local fsxx = sc.GetActorSystemProperty(actor, 10)

    local shjc = GamePlayDynamicData.shuXingInfo[playerID][1]*100
    local shmy = GamePlayDynamicData.shuXingInfo[playerID][2]*100

    sc.CallUILuaFunction({ -- zdc 显示的属性要换
        playerID,
        shjc,
        shmy,
        wlgj,
        zdsm,
        wlfy,
        gjsd,
        bjjl,
        smhf,
        hjct,
        wlxx,
        fsgj,
        lqsj,
        fsfy,
        ydsd,
        bjsh,
        nlhf,
        fsct,
        fsxx,
    }, StringId.new("refreshShuXing"))
end








-- 召唤深渊魔王
local function callHellFire(myJson, actor, playerID, actorID)
    local bossLevel = GamePlayDynamicData.challengeBossInfo[playerID][3][1]
    local position = GamePlayDynamicData.playerList[playerID]["spawnPosition"]

    -- GamePlayDynamicData.challengeBossInfo[playerID] = {{1,false},{1,false},{1,false}} -- {挑战boss等级，是否存活}

    sc.UGCSendMsgLua("callBoss", {playerID = playerID, BossID = GamePlayConfigData.diyuhuoCfgID, position = position, bossLevel = bossLevel})

    GamePlayDynamicData.challengeBossInfo[playerID][3][1] = GamePlayDynamicData.challengeBossInfo[playerID][3][1] + 1
    GamePlayDynamicData.challengeBossInfo[playerID][3][2] = true

    sc.CallUILuaFunction({playerID, GamePlayDynamicData.challengeBossInfo[playerID][3][1]}, StringId.new("callHellFire_callBack"))
    sc.CallUILuaFunction({playerID, "symw"}, StringId.new("addGMGongGaoInfo"))
end


-- 召唤粮草押运官
local function callMoneyBoss(myJson, actor, playerID, actorID)
    local bossLevel = GamePlayDynamicData.challengeBossInfo[playerID][1][1]
    local position = GamePlayDynamicData.playerList[playerID]["spawnPosition"]

    sc.SetTimer(100, 0, 10, function ()
        sc.UGCSendMsgLua("callBoss", {playerID = playerID, BossID = GamePlayConfigData.jinbiCfgID, position = position, bossLevel = bossLevel})
    end, {})
    GamePlayDynamicData.challengeBossInfo[playerID][1][1] = GamePlayDynamicData.challengeBossInfo[playerID][1][1] + 1
    GamePlayDynamicData.challengeBossInfo[playerID][1][2] = true

    sc.CallUILuaFunction({playerID, GamePlayDynamicData.challengeBossInfo[playerID][1][1]}, StringId.new("callMoneyBoss_callBack"))
    sc.CallUILuaFunction({playerID, "lcyyg"}, StringId.new("addGMGongGaoInfo"))
end


-- 召唤魔袋长老
local function callGoldOre(myJson, actor, playerID, actorID)
    local bossLevel = GamePlayDynamicData.challengeBossInfo[playerID][2][1]
    local position = GamePlayDynamicData.playerList[playerID]["spawnPosition"]

    sc.UGCSendMsgLua("callBoss", {playerID = playerID, BossID = GamePlayConfigData.jinkuangCfgID, position = position,bossLevel = bossLevel})
    GamePlayDynamicData.challengeBossInfo[playerID][2][1] = GamePlayDynamicData.challengeBossInfo[playerID][2][1] + 1
    GamePlayDynamicData.challengeBossInfo[playerID][2][2] = true

    sc.CallUILuaFunction({playerID, GamePlayDynamicData.challengeBossInfo[playerID][2][1]}, StringId.new("callGoldOre_callBack"))
    sc.CallUILuaFunction({playerID, "mdzl"}, StringId.new("addGMGongGaoInfo"))
end









local function allMedicine(myJson, actor, playerID, actorID)
    sc.BuffAction(actor, actor, true, false, 20361, 0, 0)
end




local function refreshMoney(myJson, actor, playerID, actorID)
    local money = sc.GetCoinNum(actor)

    sc.CallUILuaFunction({playerID, money}, StringId.new("RefreshMoney"))
    getShuXing(actor, actorID,playerID)
    sc.CallUILuaFunction({playerID}, StringId.new("checkMonsterCountSafe"))

    sc.CallUILuaFunction({playerID, GamePlayDynamicData.allJinBi[playerID]}, StringId.new("allJinBi_callBack"))

    sc.CallUILuaFunction({playerID, GamePlayDynamicData.hurtTotalInfo[playerID]}, StringId.new("shangHaiTongJi_callBack"))
end










local function callEquipBoss(myJson, actor, playerID, actorID)
    if GamePlayDynamicData.grandListInfo[playerID][1][3] == true then
        return
    end
    GamePlayDynamicData.grandListInfo[playerID][1][3] = true

    local bossLevel = GamePlayDynamicData.grandListInfo[playerID][1][2]
    local bossCfgID = GamePlayConfigData.equipBossCfgIDList[1]
    local position = GamePlayDynamicData.playerList[playerID]["spawnPosition"]

    sc.UGCSendMsgLua("callBoss", {playerID = playerID, BossID = bossCfgID, actor = actor, position = position,bossLevel = bossLevel})

    sc.CallUILuaFunction({playerID, "wq"}, StringId.new("addGMGongGaoInfo"))

    sc.CallUILuaFunction({playerID}, StringId.new("weapoBoss_CallBack"))
end


local function callHuJiaBoss(myJson, actor, playerID, actorID)
    if GamePlayDynamicData.grandListInfo[playerID][2][3] == true then
        return
    end
    GamePlayDynamicData.grandListInfo[playerID][2][3] = true

    local bossLevel = GamePlayDynamicData.grandListInfo[playerID][2][2]
    local bossCfgID = GamePlayConfigData.equipBossCfgIDList[2]
    local position = GamePlayDynamicData.playerList[playerID]["spawnPosition"]

    sc.UGCSendMsgLua("callBoss", {playerID = playerID, BossID = bossCfgID, actor = actor, position = position, bossLevel = bossLevel})

    sc.CallUILuaFunction({playerID, "hj"}, StringId.new("addGMGongGaoInfo"))

    sc.CallUILuaFunction({playerID}, StringId.new("hujiaBoss_CallBack"))
end




local function callJieZhiBoss(myJson, actor, playerID, actorID)
    if GamePlayDynamicData.grandListInfo[playerID][3][3] == true then
        return
    end
    GamePlayDynamicData.grandListInfo[playerID][3][3] = true

    local bossLevel = GamePlayDynamicData.grandListInfo[playerID][3][2]
    local bossCfgID = GamePlayConfigData.equipBossCfgIDList[3]
    local position = GamePlayDynamicData.playerList[playerID]["spawnPosition"]

    sc.UGCSendMsgLua("callBoss", {playerID = playerID, BossID = bossCfgID, actor = actor, position = position, bossLevel = bossLevel})

    sc.CallUILuaFunction({playerID, "jz"}, StringId.new("addGMGongGaoInfo"))

    sc.CallUILuaFunction({playerID}, StringId.new("jiezhiBoss_CallBack"))
end


local function dropEquip(myJson, actor, playerID, actorID)
    local index = myJson.index

    local equipID, _ = sc.GetPackageInfo(actor, 0, index)
    sc.RemoveEquip(actor, 0, index)

    local position, _, _ = sc.GetActorLogicPos(actor)
    local newPositon = VInt3.new(position.x, 0, position.z)

    local dropItemID = GamePlayConfigData.diuqiEquipInfo[equipID]

    sc.SpawnDropItem(dropItemID, GamePlayConfigData.actorCamp, true, newPositon)

    sc.CallUILuaFunction({playerID}, StringId.new("refreshEquipInfo"))
end


local function sellEquip(myJson, actor, playerID, actorID)
    local index = myJson.index
    sc.SellEquip(actor, 0, index)
    sc.CallUILuaFunction({playerID}, StringId.new("refreshEquipInfo"))
end







local function GMGongGaoTime(myJson, actor, playerID, actorID)
    if GamePlayDynamicData.dingshiqi[playerID] == nil then
        return
    end

    local function OnTimer(data)
        sc.CallUILuaFunction({playerID}, StringId.new("closeGongGao"))
        table.remove(GamePlayDynamicData.dingshiqi[playerID], 1)
    end
    local t = sc.SetTimer(5000, 1, 1, OnTimer, {})

    if #GamePlayDynamicData.dingshiqi[playerID] == 0 then
        table.insert(GamePlayDynamicData.dingshiqi[playerID], t)
    else
        sc.KillTimer(GamePlayDynamicData.dingshiqi[playerID][1])
        table.remove(GamePlayDynamicData.dingshiqi[playerID], 1)
        table.insert(GamePlayDynamicData.dingshiqi[playerID], t)
    end
end




local function BigGMGongGaoTime(myJson, actor, playerID, actorID)
    local function OnTimer(data)
        sc.CallUILuaFunction({playerID}, StringId.new("closeBigGongGao"))
    end
    local t = sc.SetTimer(5000, 1, 1, OnTimer, {})
end


local function delSkillBook(num, playerID) -- 使用技能后的删除
    local isRemove = false
    local skillBookName
    if num == 1 then
        skillBookName = "D级被动技能书"
    elseif num == 5 then
        skillBookName = "D级主动技能书"
    elseif num == 2 then
        skillBookName = "C级被动技能书"
    elseif num == 6 then
        skillBookName = "C级主动技能书"
    elseif num == 3 then
        skillBookName = "B级被动技能书"
    elseif num == 7 then
        skillBookName = "B级主动技能书"
    elseif num == 4 then
        skillBookName = "A级被动技能书"
    elseif num == 8 then
        skillBookName = "A级主动技能书"
    end

    for index = 1, #GamePlayDynamicData.daojuInfo[playerID] do
        if GamePlayDynamicData.daojuInfo[playerID][index][1] == skillBookName then
            if GamePlayDynamicData.daojuInfo[playerID][index][3] > 1 then
                GamePlayDynamicData.daojuInfo[playerID][index][3] = GamePlayDynamicData.daojuInfo[playerID][index][3] - 1
            elseif GamePlayDynamicData.daojuInfo[playerID][index][3] == 1 then
                table.remove(GamePlayDynamicData.daojuInfo[playerID], index)
                isRemove = true
            end
            break
        end
    end

    sc.CallUILuaFunction({playerID, isRemove, num}, StringId.new("dellSkillBook_callBack"))
end


local function useSkillBook(myJson, actor, playerID, actorID)
    local num = myJson.num
    local skillType
    local skillid
    local slot
    if num == 1 or num == 2 or num == 3 or num == 4 then
        skillType = 0
        local skillList = GamePlayConfigData.passSkillTable[num]
        slot = GamePlayConfigData.passSkillSlot
        skillid = skillList[sc.RangedRand(1,#skillList)][1]
    else
        skillType = 1
        local level = num - 4
        local skillList = GamePlayConfigData.activeSkillTable[level]
        slot = GamePlayConfigData.activeSkillSlot
        skillid = skillList[sc.RangedRand(1,#skillList)][1]
    end

    -- 如果是关献祭的状态，要和献祭被动一起删掉
    if sc.GetSkillSlotSkillID(actor, slot) == 128551 or sc.GetSkillSlotSkillID(actor, slot) == 1285512 then
        sc.DelPassiveSkill(actor, 12855011)
    elseif sc.GetSkillSlotSkillID(actor, slot) == 128552 or sc.GetSkillSlotSkillID(actor, slot) == 1285522 then
        sc.DelPassiveSkill(actor, 1285521)
    elseif sc.GetSkillSlotSkillID(actor, slot) == 128553 or sc.GetSkillSlotSkillID(actor, slot) == 1285532 then
        sc.DelPassiveSkill(actor, 1285531)
    elseif sc.GetSkillSlotSkillID(actor, slot) == 12855 or sc.GetSkillSlotSkillID(actor, slot) == 1285505 then
        sc.DelPassiveSkill(actor, 1285501)
    end

    if sc.GetSkillSlotSkillID(actor, slot) == 112401 then
        sc.BuffAction(actor, actor, false, false, 1124011, 0, 0)
    elseif sc.GetSkillSlotSkillID(actor, slot) == 112402 then
        sc.BuffAction(actor, actor, false, false, 1124021, 0, 0)
    elseif sc.GetSkillSlotSkillID(actor, slot) == 112403 then
        sc.BuffAction(actor, actor, false, false, 11240311, 0, 0)
    elseif sc.GetSkillSlotSkillID(actor, slot) == 112404 then
        sc.BuffAction(actor, actor, false, false, 11240411, 0, 0)
    end


    -- 不管是主动还是被动技能，都要删除现有那个槽位上的技能
    sc.DelSkill(actor, slot)

    sc.AddSkill(actor, skillid, 1, slot, false, false)

    local nowPassSkillID = GamePlayDynamicData.nowPassSkillIDInfo[playerID]
    if skillType == 0 then -- 被动
        if nowPassSkillID ~= 0 then
            sc.DelPassiveSkill(actor, nowPassSkillID)
        end
        sc.AddPassiveSkill(actor, skillid, 0)
        GamePlayDynamicData.nowPassSkillIDInfo[playerID] = skillid
    end

    sc.CallUILuaFunction({playerID, skillType, skillid}, StringId.new("piaozi1"))


    delSkillBook(num, playerID)
end


local function dropSkillBook(myJson, actor, playerID, actorID)
    if actor == nil then
        return
    end
    local num = myJson.num
    local position, _, _ = sc.GetActorLogicPos(actor)
    local newPositon = VInt3.new(position.x, 0, position.z)

    local dropItemID = GamePlayConfigData.playerDropSkillInfo[5][num]
    local dropItem = sc.SpawnDropItem(dropItemID, GamePlayConfigData.actorCamp, true, newPositon)
    sc.SetDropItemBubbleScale(dropItem, 550)  -- 缩放泡泡
    sc.SetDropItemIconScale(dropItem, 500) -- 缩放图片

    local itemName = GamePlayConfigData.dropJNSNameInfo[dropItemID][1]
    local itemImg = GamePlayConfigData.dropJNSNameInfo[dropItemID][2]
    sc.SetDropItemName(dropItem, StringId.new(itemName))
    sc.SetDropItemIconPath(dropItem, StringId.new(itemImg))

    delSkillBook(num, playerID)
end




local chushoujiage = {
    500, -- D级被动技能书
    1000, -- C级被动技能书
    1500, -- B级被动技能书
    2000, -- A级被动技能书
    500, -- D级主动技能书
    1000, -- C级主动技能书
    1500, -- B级主动技能书
    2000, -- A级主动技能书
}
local function chushoudaoju(myJson, actor, playerID, actorID)
    local num = myJson.num
    local addmoney = math.ceil(GamePlayConfigData.jnsInfo[num][4]*0.8)
    sc.AddCoin(actor, addmoney, true, true, 0)


    local money = sc.GetCoinNum(actor)
    sc.CallUILuaFunction({playerID, money}, StringId.new("RefreshMoney"))
    getShuXing(actor, actorID, playerID)

    delSkillBook(num, playerID)
end


local function buySkillBook(myJson, actor, playerID, actorID)
    local num = myJson.num
    local buyCnt = GamePlayDynamicData.skillBookBuyCountList[playerID][num]
    if buyCnt >= 10 then
        return
    end

    local needMoney = (2^buyCnt)*GamePlayConfigData.jnsInfo[num][4]
    if sc.GetCoinNum(actor) < needMoney then
        return
    end

    deductMoney(needMoney, actor, playerID)

    GamePlayDynamicData.skillBookBuyCountList[playerID][num] = GamePlayDynamicData.skillBookBuyCountList[playerID][num] + 1
    sc.CallUILuaFunction({playerID}, StringId.new("refresh_shop_item"))

    -- {"D级被动技能书", "Texture/Sprite/Db.sprite", 1, "随机获得一个D级被动技能"}

    local isHave = false
    local iscanBuy = true

    if num == 1 then
        for index = 1, #GamePlayDynamicData.daojuInfo[playerID] do
            if GamePlayDynamicData.daojuInfo[playerID][index][1] == "D级被动技能书" then
                GamePlayDynamicData.daojuInfo[playerID][index][3] = GamePlayDynamicData.daojuInfo[playerID][index][3] + 1
                isHave = true
            end
        end

        if isHave == false then
            if #GamePlayDynamicData.daojuInfo[playerID] >= 6 then
                iscanBuy = false
            else
                table.insert(GamePlayDynamicData.daojuInfo[playerID], {"D级被动技能书", "Texture/Sprite/Db.sprite", 1, "随机获得一个D级被动技能", 1})
            end
        end
    elseif num == 5 then
        for index = 1, #GamePlayDynamicData.daojuInfo[playerID] do
            if GamePlayDynamicData.daojuInfo[playerID][index][1] == "D级主动技能书" then
                GamePlayDynamicData.daojuInfo[playerID][index][3] = GamePlayDynamicData.daojuInfo[playerID][index][3] + 1
                isHave = true
            end
        end

        if isHave == false then
            if #GamePlayDynamicData.daojuInfo[playerID] >= 6 then
                iscanBuy = false
            else
                table.insert(GamePlayDynamicData.daojuInfo[playerID], {"D级主动技能书", "Texture/Sprite/Dz.sprite", 1, "随机获得一个D级主动技能", 5})
            end
        end
    elseif num == 2 then
        for index = 1, #GamePlayDynamicData.daojuInfo[playerID] do
            if GamePlayDynamicData.daojuInfo[playerID][index][1] == "C级被动技能书" then
                GamePlayDynamicData.daojuInfo[playerID][index][3] = GamePlayDynamicData.daojuInfo[playerID][index][3] + 1
                isHave = true
            end
        end

        if isHave == false then
            if #GamePlayDynamicData.daojuInfo[playerID] >= 6 then
                iscanBuy = false
            else
                table.insert(GamePlayDynamicData.daojuInfo[playerID], {"C级被动技能书", "Texture/Sprite/Cb.sprite", 1, "随机获得一个C级被动技能", 2})
            end
        end
    elseif num == 6 then
        for index = 1, #GamePlayDynamicData.daojuInfo[playerID] do
            if GamePlayDynamicData.daojuInfo[playerID][index][1] == "C级主动技能书" then
                GamePlayDynamicData.daojuInfo[playerID][index][3] = GamePlayDynamicData.daojuInfo[playerID][index][3] + 1
                isHave = true
            end
        end

        if isHave == false then
            if #GamePlayDynamicData.daojuInfo[playerID] >= 6 then
                iscanBuy = false
            else
                table.insert(GamePlayDynamicData.daojuInfo[playerID], {"C级主动技能书", "Texture/Sprite/Cz.sprite", 1, "随机获得一个C级主动技能", 6})
            end
        end
    elseif num == 3 then
        for index = 1, #GamePlayDynamicData.daojuInfo[playerID] do
            if GamePlayDynamicData.daojuInfo[playerID][index][1] == "B级被动技能书" then
                GamePlayDynamicData.daojuInfo[playerID][index][3] = GamePlayDynamicData.daojuInfo[playerID][index][3] + 1
                isHave = true
            end
        end

        if isHave == false then
            if #GamePlayDynamicData.daojuInfo[playerID] >= 6 then
                iscanBuy = false
            else
                table.insert(GamePlayDynamicData.daojuInfo[playerID], {"B级被动技能书", "Texture/Sprite/Bb.sprite", 1, "随机获得一个B级被动技能", 3})
            end
        end
    elseif num == 7 then
        for index = 1, #GamePlayDynamicData.daojuInfo[playerID] do
            if GamePlayDynamicData.daojuInfo[playerID][index][1] == "B级主动技能书" then
                GamePlayDynamicData.daojuInfo[playerID][index][3] = GamePlayDynamicData.daojuInfo[playerID][index][3] + 1
                isHave = true
            end
        end

        if isHave == false then
            if #GamePlayDynamicData.daojuInfo[playerID] >= 6 then
                iscanBuy = false
            else
                table.insert(GamePlayDynamicData.daojuInfo[playerID], {"B级主动技能书", "Texture/Sprite/Bz.sprite", 1, "随机获得一个B级主动技能", 7})
            end
        end
    elseif num == 4 then
        for index = 1, #GamePlayDynamicData.daojuInfo[playerID] do
            if GamePlayDynamicData.daojuInfo[playerID][index][1] == "A级被动技能书" then
                GamePlayDynamicData.daojuInfo[playerID][index][3] = GamePlayDynamicData.daojuInfo[playerID][index][3] + 1
                isHave = true
            end
        end

        if isHave == false then
            if #GamePlayDynamicData.daojuInfo[playerID] >= 6 then
                iscanBuy = false
            else
                table.insert(GamePlayDynamicData.daojuInfo[playerID], {"A级被动技能书", "Texture/Sprite/Ab.sprite", 1, "随机获得一个A级被动技能", 4})
            end
        end
    elseif num == 8 then
        for index = 1, #GamePlayDynamicData.daojuInfo[playerID] do
            if GamePlayDynamicData.daojuInfo[playerID][index][1] == "A级主动技能书" then
                GamePlayDynamicData.daojuInfo[playerID][index][3] = GamePlayDynamicData.daojuInfo[playerID][index][3] + 1
                isHave = true
            end
        end

        if isHave == false then
            if #GamePlayDynamicData.daojuInfo[playerID] >= 6 then
                iscanBuy = false
            else
                table.insert(GamePlayDynamicData.daojuInfo[playerID], {"A级主动技能书", "Texture/Sprite/Az.sprite", 1, "随机获得一个A级主动技能", 8})
            end
        end
    end

    sc.CallUILuaFunction({playerID, iscanBuy, isHave, num}, StringId.new("buySkillBook_callBack"))
end










local function sellSkillBook(myJson, actor, playerID, actorID)
    local addmoney = myJson.money
    sc.AddCoin(actor, addmoney, true, true, 0)

    local num = myJson.num
    local isRemove = false
    local skillBookName
    if num == 1 then
        skillBookName = "D级被动技能书"
    elseif num == 5 then
        skillBookName = "D级主动技能书"
    elseif num == 2 then
        skillBookName = "C级被动技能书"
    elseif num == 6 then
        skillBookName = "C级主动技能书"
    elseif num == 3 then
        skillBookName = "B级被动技能书"
    elseif num == 7 then
        skillBookName = "B级主动技能书"
    elseif num == 4 then
        skillBookName = "A级被动技能书"
    elseif num == 8 then
        skillBookName = "A级主动技能书"
    end

    local addmoney = myJson.money

    for index = 1, #GamePlayDynamicData.daojuInfo[playerID] do
        if GamePlayDynamicData.daojuInfo[playerID][index][1] == skillBookName then
            if GamePlayDynamicData.daojuInfo[playerID][index][3] > 1 then
                GamePlayDynamicData.daojuInfo[playerID][index][3] = GamePlayDynamicData.daojuInfo[playerID][index][3] - 1
            elseif GamePlayDynamicData.daojuInfo[playerID][index][3] == 1 then
                table.remove(GamePlayDynamicData.daojuInfo[playerID], index)
                isRemove = true
            end
            break
        end
    end

    sc.CallUILuaFunction({playerID, isRemove, num}, StringId.new("sellSkillBook_callBack"))
end




local function daojuMan(myJson, actor, playerID, actorID)
    local isMan = myJson.isM
    GamePlayDynamicData.isDaojuzhuangman = isMan

    if isMan == false then
        local num = myJson.num
        local id
        if num == 1 then -- zdc 这里应该是老的id，而且没做share区分
            id = 12301
        elseif num == 2 then
            id = 12302
        elseif num == 3 then
            id = 12303
        elseif num == 4 then
            id = 12304
        elseif num == 5 then
            id = 12305
        elseif num == 6 then
            id = 12306
        elseif num == 7 then
            id = 12307
        elseif num == 8 then
            id = 12308
        elseif num == -1 then
            return
        elseif num == -2 then
            local index = myJson.index
            table.remove(GamePlayDynamicData.daojuInfo, index)
            return
        end

        local tag
        for i = 1, #GamePlayDynamicData.daojuInfo do
            if GamePlayDynamicData.daojuInfo[i] == "" .. id then
                tag = i
                break
            end
        end
        table.remove(GamePlayDynamicData.daojuInfo, tag)
    end
end


local function checkGamePlayDataToUI(myJson, actor, playerID, actorID)
    sc.CallUILuaFunction({playerID}, StringId.new("refreshEquipInfo"))
end


local function czEquip(myJson, actor, playerID, actorID)
    local index = myJson.index
    local newEquipID = myJson.newEquipID
    sc.RemoveEquip(actor, 0, index)
    sc.AddEquip(actor, newEquipID, 0, index)
    GamePlayDynamicData.CZChuiCnt[playerID] = GamePlayDynamicData.CZChuiCnt[playerID] - 1
    sc.CallUILuaFunction({playerID}, StringId.new("czEquip_callBack"))
    sc.CallUILuaFunction({playerID}, StringId.new("refreshEquipInfo"))
end


local function UICallBackOfSLD(myJson, actor, playerID, actorID)
    local yxSLDLv = myJson.yxSLDLv
    local mjYxSLDCnt = myJson.mjYxSLDCnt
    if yxSLDLv >= 1 then
        -- print("当前英雄等级比较高，获得XX加成！")
    end

    if mjYxSLDCnt >= 1 then
        -- print("当前总英雄等级比较高，获得XX加成！")
    end
end



local function getJifenLv(num)
    local num1 = 0
    for i = 1, #GamePlayConfigData.chengjiuInfo["积分"] do
        if num >= GamePlayConfigData.chengjiuInfo["积分"][i][1] then
            num1 = GamePlayConfigData.chengjiuInfo["积分"][i][2]
        else
            break
        end
    end
    return num1
end



local function chengjiu(myJson, actor, playerID, actorID)
    local jifen = myJson.a
    local nanduLv = myJson.b
    local manjiHeroCnt = myJson.c
    local allheroExLv = myJson.d
    local nowHeroLevel = myJson.e


    if jifen < 0 then
        jifen = 0
    end

    if GamePlayConfigData.chengjiuInfo["通关难度"][nanduLv] then
        GamePlayConfigData.chengjiuInfo["通关难度"][nanduLv][2](playerID, actor)
    end

    local chengjiujifenLv = getJifenLv(jifen)

    if chengjiujifenLv ~= 0 then
        GamePlayConfigData.chengjiuInfo["积分"][chengjiujifenLv][3](playerID, actor)
    end

    if manjiHeroCnt then
        local manjiHeroCntChengJiuLv
        for i = 1, #GamePlayConfigData.chengjiuInfo["5星英雄数量"] do
            if manjiHeroCnt >= GamePlayConfigData.chengjiuInfo["5星英雄数量"][i][1] then
                manjiHeroCntChengJiuLv = GamePlayConfigData.chengjiuInfo["5星英雄数量"][i][2]
            else
                break
            end
        end
        if manjiHeroCntChengJiuLv then
            GamePlayConfigData.chengjiuInfo["5星英雄数量"][manjiHeroCntChengJiuLv][3](playerID, actor)
        end
    end


    if allheroExLv then
        local allheroExLvChengJiuLv
        for i = 1, #GamePlayConfigData.chengjiuInfo["英雄总等级"] do
            if allheroExLv >= GamePlayConfigData.chengjiuInfo["英雄总等级"][i][1] then
                allheroExLvChengJiuLv = GamePlayConfigData.chengjiuInfo["英雄总等级"][i][2]
            else
                break
            end
        end
        if allheroExLvChengJiuLv then
            GamePlayConfigData.chengjiuInfo["英雄总等级"][allheroExLvChengJiuLv][3](playerID, actor)
        end
    end

    if nowHeroLevel then
        if nowHeroLevel ~= 0 then
            GamePlayConfigData.chengjiuInfo["英雄等级"][nowHeroLevel][3](playerID, actor)
        end
    end
end






local function addCoin(myJson, actor, playerID, actorID)
    sc.AddCoin(actor, 10000, true, true, 0)
end

local function zdc(myJson, actor, playerID, actorID)

    local newTab = {}

    local table = GamePlayDynamicData.erxuanyiSkillKu[playerID]
    local count = 2
    local length = #table
    for i = 1, count do
        local ri = sc.RangedRand(i, length)
        local tmp = table[i]
        table[i] = table[ri]
        table[ri] = tmp
        newTab[i] = table[i]
    end

    sc.CallUILuaFunction({playerID, newTab[1], newTab[2]}, StringId.new("bigPassSkill_callBack"))
end


local function callMonster(myJson, actor, playerID, actorID)
    local monsterCfg = myJson.cfg
    local bossLevel = GamePlayDynamicData.challengeBossInfo[playerID][3][1]
    local position = GamePlayDynamicData.playerList[playerID]["spawnPosition"]

    sc.UGCSendMsgLua("callbing", {playerID = playerID, BossID = monsterCfg, position = position, bossLevel = bossLevel})


    sc.BuffAction(actor, actor, true, false, 173001, 0, 0)
end



local function chooseErXuanYi(myJson, actor, playerID, actorID)
    local chooseIndex = myJson.skill
    local skillName = GamePlayDynamicData.erxuanyiSkillKu[playerID][chooseIndex]

    useCiTiaoFunc(actor, playerID, skillName, 0)
    sc.CallUILuaFunction({playerID, skillName}, StringId.new("useTianfuFunc_callBack"))

    table.remove(GamePlayDynamicData.erxuanyiSkillKu[playerID], chooseIndex)
end



local function setShareBtn(myJson, actor, playerID, actorID)
    if GamePlayDynamicData.playerShareInfo[playerID] == 0 then
        GamePlayDynamicData.playerShareInfo[playerID] = 1
        sc.CallUILuaFunction({playerID,1}, StringId.new("setShareBtn_callBack"))
    else
        GamePlayDynamicData.playerShareInfo[playerID] = 0
        sc.CallUILuaFunction({playerID,0}, StringId.new("setShareBtn_callBack"))
    end
end



local function nL(myJson, actor, playerID, actorID)
    local nengLiangType = myJson.type
    local cfgID = myJson.cfgID
    sc.AddSkill(actor, 20101, 1, 7, false, false)

    if nengLiangType == 0 then -- 蓝条英雄
        sc.AddPassiveSkill(actor, 103132, 0) -- zdc 有问题
        sc.BuffAction(actor, actor, true, false, 10313312, 0, 0)
        sc.AddSkill(actor, 20102, 1, 5, false, false) -- 回蓝药水

        if GamePlayConfigData.faShiInfo[cfgID] ~= nil then
            table.insert(GamePlayDynamicData.ciTiaoKuInfo[playerID], {"学识"})
            table.insert(GamePlayDynamicData.ciTiaoKuInfo[playerID], {"源泉"})
        end
    else
        sc.AddPassiveSkill(actor, 103133, 0)
        sc.BuffAction(actor, actor, true, false, 10313311, 0, 0)
        sc.AddSkill(actor, 102131, 1, 5, false, false) -- 加速药水
    end
end


local function setName(myJson, actor, playerID, actorID)
    local pid = myJson.pid
    local pName = cjson.decode(myJson.name)["pName"]

    -- printTb("playerName:  ")
    -- printTb(pName)
    -- GamePlayDynamicData.playerList[pid]["playerName"] = pName
end


local function addcoin111(data)
    local actor = data[1]
    local addNum = data[2]
    sc.AddCoin(actor, addNum, false, false, 0)
end


local function addjinbimeimiao(actor, num)
    local addNum = math.ceil(num)
    sc.SetTimer(1000, 0, 0 , addcoin111,{actor, addNum})
end


local function redBaoShiFunc(actor, num, shuxingIndex, playerID)
    local cntInfo = {
        [1] = 0,
        [2] = 0,
        [3] = 0,
        [4] = 0,
        [5] = 0,
        [6] = 0,
        [7] = 0,
        [8] = 0,
        [9] = 0,
        [10] = 0,
        [11] = 0,
        [12] = 0,
        [13] = 0,
        [14] = 0,
        [15] = 0,
        [16] = 0,
    }
    local nDecimal = 10 ^ 2
    while num > 0 do
        if num >= 1000 then
            local nTemp = math.floor((num - 1000) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[16] = cntInfo[16] + 1
        elseif num >= 300 then
            local nTemp = math.floor((num - 300) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[15] = cntInfo[15] + 1
        elseif num >= 200 then
            local nTemp = math.floor((num - 200) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[14] = cntInfo[14] + 1
        elseif num >= 100 then
            local nTemp = math.floor((num - 100) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[13] = cntInfo[13] + 1
        elseif num >= 30 then
            local nTemp = math.floor((num - 30) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[12] = cntInfo[12] + 1
        elseif num >= 20 then
            local nTemp = math.floor((num - 20) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[11] = cntInfo[11] + 1
        elseif num >= 10 then
            local nTemp = math.floor((num - 10) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[10] = cntInfo[10] + 1
        elseif num >= 3 then
            local nTemp = math.floor((num - 3) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[9] = cntInfo[9] + 1
        elseif num >= 2 then
            local nTemp = math.floor((num - 2) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[8] = cntInfo[8] + 1
        elseif num >= 1 then
            local nTemp = math.floor((num - 1) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[7] = cntInfo[7] + 1
        elseif num >= 0.3 then
            local nTemp = math.floor((num - 0.3) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[6] = cntInfo[6] + 1
        elseif num >= 0.2 then
            local nTemp = math.floor((num - 0.2) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[5] = cntInfo[5] + 1
        elseif num >= 0.1 then
            local nTemp = math.floor((num - 0.1) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[4] = cntInfo[4] + 1
        elseif num >= 0.03 then
            local nTemp = math.floor((num - 0.03) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[3] = cntInfo[3] + 1
        elseif num >= 0.02 then
            local nTemp = math.floor((num - 0.02) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[2] = cntInfo[2] + 1
        elseif num >= 0.01 then
            local nTemp = math.floor((num - 0.01) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[1] = cntInfo[1] + 1
        end
    end


    for i=1, #cntInfo do
        if cntInfo[i] ~= 0 then
            local skillID
            if shuxingIndex == 1 then
                skillID = GamePlayConfigData.wuliGongJiLiSkillInfo[i]
            elseif shuxingIndex == 2 then
                skillID = GamePlayConfigData.wuliChuanTouSkillInfo[i]
            elseif shuxingIndex == 3 then
                skillID = GamePlayConfigData.faShuGongJiLiSkillInfo[i]
            elseif shuxingIndex == 4 then
                skillID = GamePlayConfigData.faShuChuanTouSkillInfo[i]
            elseif shuxingIndex == 5 then
                skillID = GamePlayConfigData.baoJiLvSkillInfo[i]
            elseif shuxingIndex == 6 then
                skillID = GamePlayConfigData.baoJiXiaoGuoSkillInfo[i]
            end
            sc.BuffAction(actor, actor, true, false, skillID, 0, 0)
        end
    end
end



local function blueBaoShiFunc(actor, num, shuxingIndex, playerID)
    local cntInfo = {
        [1] = 0,
        [2] = 0,
        [3] = 0,
        [4] = 0,
        [5] = 0,
        [6] = 0,
        [7] = 0,
        [8] = 0,
        [9] = 0,
        [10] = 0,
        [11] = 0,
        [12] = 0,
        [13] = 0,
        [14] = 0,
        [15] = 0,
        [16] = 0,
    }
    local nDecimal = 10 ^ 2
    while num > 0 do
        if num >= 1000 then
            local nTemp = math.floor((num - 1000) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[16] = cntInfo[16] + 1
        elseif num >= 300 then
            local nTemp = math.floor((num - 300) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[15] = cntInfo[15] + 1
        elseif num >= 200 then
            local nTemp = math.floor((num - 200) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[14] = cntInfo[14] + 1
        elseif num >= 100 then
            local nTemp = math.floor((num - 100) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[13] = cntInfo[13] + 1
        elseif num >= 30 then
            local nTemp = math.floor((num - 30) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[12] = cntInfo[12] + 1
        elseif num >= 20 then
            local nTemp = math.floor((num - 20) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[11] = cntInfo[11] + 1
        elseif num >= 10 then
            local nTemp = math.floor((num - 10) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[10] = cntInfo[10] + 1
        elseif num >= 3 then
            local nTemp = math.floor((num - 3) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[9] = cntInfo[9] + 1
        elseif num >= 2 then
            local nTemp = math.floor((num - 2) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[8] = cntInfo[8] + 1
        elseif num >= 1 then
            local nTemp = math.floor((num - 1) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[7] = cntInfo[7] + 1
        elseif num >= 0.3 then
            local nTemp = math.floor((num - 0.3) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[6] = cntInfo[6] + 1
        elseif num >= 0.2 then
            local nTemp = math.floor((num - 0.2) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[5] = cntInfo[5] + 1
        elseif num >= 0.1 then
            local nTemp = math.floor((num - 0.1) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[4] = cntInfo[4] + 1
        elseif num >= 0.03 then
            local nTemp = math.floor((num - 0.03) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[3] = cntInfo[3] + 1
        elseif num >= 0.02 then
            local nTemp = math.floor((num - 0.02) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[2] = cntInfo[2] + 1
        elseif num >= 0.01 then
            local nTemp = math.floor((num - 0.01) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[1] = cntInfo[1] + 1
        end
    end


    for i=1, #cntInfo do
        if cntInfo[i] ~= 0 then
            local skillID
            if shuxingIndex == 1 then
                skillID = GamePlayConfigData.meiMiaoHuiXuekillInfo[i]
            elseif shuxingIndex == 2 then
                skillID = GamePlayConfigData.meiMiaoHuiLankillInfo[i]
            elseif shuxingIndex == 3 then
                skillID = GamePlayConfigData.shengMingZhiShangXianSkillInfo[i]
            elseif shuxingIndex == 4 then
                skillID = GamePlayConfigData.moFaZhiShangXianSkillInfo[i]
            elseif shuxingIndex == 5 then
                skillID = GamePlayConfigData.faShuFangYukillInfo[i] -- zdc
            elseif shuxingIndex == 6 then
                skillID = GamePlayConfigData.wuLiFangYukillInfo[i]
            end
            sc.BuffAction(actor, actor, true, false, skillID, 0, 0)
        end
    end
end


local function purpleBaoShiFunc(actor, num, shuxingIndex, playerID)
    local cntInfo = {
        [1] = 0,
        [2] = 0,
        [3] = 0,
        [4] = 0,
        [5] = 0,
        [6] = 0,
        [7] = 0,
        [8] = 0,
        [9] = 0,
        [10] = 0,
        [11] = 0,
        [12] = 0,
        [13] = 0,
        [14] = 0,
        [15] = 0,
        [16] = 0,
    }

    printTb(shuxingIndex)
    printTb(num)

    local nDecimal = 10 ^ 2
    while num > 0 do
        if num >= 1000 then
            local nTemp = math.floor((num - 1000) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[16] = cntInfo[16] + 1
        elseif num >= 300 then
            local nTemp = math.floor((num - 300) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[15] = cntInfo[15] + 1
        elseif num >= 200 then
            local nTemp = math.floor((num - 200) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[14] = cntInfo[14] + 1
        elseif num >= 100 then
            local nTemp = math.floor((num - 100) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[13] = cntInfo[13] + 1
        elseif num >= 30 then
            local nTemp = math.floor((num - 30) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[12] = cntInfo[12] + 1
        elseif num >= 20 then
            local nTemp = math.floor((num - 20) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[11] = cntInfo[11] + 1
        elseif num >= 10 then
            local nTemp = math.floor((num - 10) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[10] = cntInfo[10] + 1
        elseif num >= 3 then
            local nTemp = math.floor((num - 3) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[9] = cntInfo[9] + 1
        elseif num >= 2 then
            local nTemp = math.floor((num - 2) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[8] = cntInfo[8] + 1
        elseif num >= 1 then
            local nTemp = math.floor((num - 1) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[7] = cntInfo[7] + 1
        elseif num >= 0.3 then
            local nTemp = math.floor((num - 0.3) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[6] = cntInfo[6] + 1
        elseif num >= 0.2 then
            local nTemp = math.floor((num - 0.2) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[5] = cntInfo[5] + 1
        elseif num >= 0.1 then
            local nTemp = math.floor((num - 0.1) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[4] = cntInfo[4] + 1
        elseif num >= 0.03 then
            local nTemp = math.floor((num - 0.03) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[3] = cntInfo[3] + 1
        elseif num >= 0.02 then
            local nTemp = math.floor((num - 0.02) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[2] = cntInfo[2] + 1
        elseif num >= 0.01 then
            local nTemp = math.floor((num - 0.01) * nDecimal)
            num = nTemp / nDecimal
            cntInfo[1] = cntInfo[1] + 1
        end
    end
    printTb(cntInfo)

    for i=1, #cntInfo do
        if cntInfo[i] ~= 0 then
            local skillID
            if shuxingIndex == 1 then
                skillID = GamePlayConfigData.wuLiXiXueSkillInfo[i]
            elseif shuxingIndex == 2 then
                skillID = GamePlayConfigData.faShuXiXueSkillInfo[i]
            elseif shuxingIndex == 3 then
                skillID = GamePlayConfigData.gongJiSuDuSkillInfo[i]
            elseif shuxingIndex == 4 then
                skillID = GamePlayConfigData.yiDongSuDuSkillInfo[i]
            end
            printTb(skillID)
            sc.BuffAction(actor, actor, true, false, skillID, 0, 0)
        end
    end
end



local function greenBaoShiFunc(actor, num, shuxingIndex, playerID)
    if shuxingIndex == 1 then
        local cntInfo = {
            [1] = 0,
            [2] = 0,
            [3] = 0,
            [4] = 0,
            [5] = 0,
            [6] = 0,
            [7] = 0,
            [8] = 0,
            [9] = 0,
            [10] = 0,
            [11] = 0,
            [12] = 0,
            [13] = 0,
            [14] = 0,
            [15] = 0,
            [16] = 0,
        }
        local nDecimal = 10 ^ 2
        while num > 0 do
            if num >= 1000 then
                local nTemp = math.floor((num - 1000) * nDecimal)
                num = nTemp / nDecimal
                cntInfo[16] = cntInfo[16] + 1
            elseif num >= 300 then
                local nTemp = math.floor((num - 300) * nDecimal)
                num = nTemp / nDecimal
                cntInfo[15] = cntInfo[15] + 1
            elseif num >= 200 then
                local nTemp = math.floor((num - 200) * nDecimal)
                num = nTemp / nDecimal
                cntInfo[14] = cntInfo[14] + 1
            elseif num >= 100 then
                local nTemp = math.floor((num - 100) * nDecimal)
                num = nTemp / nDecimal
                cntInfo[13] = cntInfo[13] + 1
            elseif num >= 30 then
                local nTemp = math.floor((num - 30) * nDecimal)
                num = nTemp / nDecimal
                cntInfo[12] = cntInfo[12] + 1
            elseif num >= 20 then
                local nTemp = math.floor((num - 20) * nDecimal)
                num = nTemp / nDecimal
                cntInfo[11] = cntInfo[11] + 1
            elseif num >= 10 then
                local nTemp = math.floor((num - 10) * nDecimal)
                num = nTemp / nDecimal
                cntInfo[10] = cntInfo[10] + 1
            elseif num >= 3 then
                local nTemp = math.floor((num - 3) * nDecimal)
                num = nTemp / nDecimal
                cntInfo[9] = cntInfo[9] + 1
            elseif num >= 2 then
                local nTemp = math.floor((num - 2) * nDecimal)
                num = nTemp / nDecimal
                cntInfo[8] = cntInfo[8] + 1
            elseif num >= 1 then
                local nTemp = math.floor((num - 1) * nDecimal)
                num = nTemp / nDecimal
                cntInfo[7] = cntInfo[7] + 1
            elseif num >= 0.3 then
                local nTemp = math.floor((num - 0.3) * nDecimal)
                num = nTemp / nDecimal
                cntInfo[6] = cntInfo[6] + 1
            elseif num >= 0.2 then
                local nTemp = math.floor((num - 0.2) * nDecimal)
                num = nTemp / nDecimal
                cntInfo[5] = cntInfo[5] + 1
            elseif num >= 0.1 then
                local nTemp = math.floor((num - 0.1) * nDecimal)
                num = nTemp / nDecimal
                cntInfo[4] = cntInfo[4] + 1
            elseif num >= 0.03 then
                local nTemp = math.floor((num - 0.03) * nDecimal)
                num = nTemp / nDecimal
                cntInfo[3] = cntInfo[3] + 1
            elseif num >= 0.02 then
                local nTemp = math.floor((num - 0.02) * nDecimal)
                num = nTemp / nDecimal
                cntInfo[2] = cntInfo[2] + 1
            elseif num >= 0.01 then
                local nTemp = math.floor((num - 0.01) * nDecimal)
                num = nTemp / nDecimal
                cntInfo[1] = cntInfo[1] + 1
            end
        end

        for i=1, #cntInfo do
            if cntInfo[i] ~= 0 then
                local skillID = GamePlayConfigData.wuLiXiXueSkillInfo[i]
                sc.BuffAction(actor, actor, true, false, skillID, 0, 0)
            end
        end
    elseif shuxingIndex == 2 then
        addjinbimeimiao(actor, math.ceil(num))
    elseif shuxingIndex == 3 then
        GamePlayDynamicData.killMonsterAdd[playerID] = math.ceil(num)
    elseif shuxingIndex == 4 then
        GamePlayDynamicData.jinbijiacheng[playerID] = GamePlayDynamicData.jinbijiacheng[playerID] + num
    end
end


local function baoshi(myJson, actor, playerID, actorID)
    local baoshiType = myJson.a
    local shuxingIndex = myJson.b
    local num = myJson.c

    if not baoshiType then
        return
    end

    if baoshiType == 1 then
        redBaoShiFunc(actor, num, shuxingIndex, playerID)
    elseif baoshiType == 2 then
        blueBaoShiFunc(actor, num, shuxingIndex, playerID)
    elseif baoshiType == 3 then
        purpleBaoShiFunc(actor, num, shuxingIndex, playerID)
    elseif baoshiType == 4 then
        greenBaoShiFunc(actor, num, shuxingIndex, playerID)
    end
end


local function baoshiTZSX(myJson, actor, playerID, actorID)
    local allJiejiNum = myJson.a
    local allDengjiNum = myJson.b

    for i=1, #GamePlayConfigData.tzsx1SkillInfo do
        if allJiejiNum >= GamePlayConfigData.tzsx1SkillInfo[i][1] then
            if i == 1 then
                GamePlayDynamicData.jinbijiacheng[playerID] = GamePlayDynamicData.jinbijiacheng[playerID] + 1
            else
                sc.BuffAction(actor, actor, true, false, GamePlayConfigData.tzsx1SkillInfo[i][2], 0, 0)
                if i == 5 then
                    GamePlayDynamicData.shuXingInfo[playerID][1] = GamePlayDynamicData.shuXingInfo[playerID][1] + 2.5
                end
            end
        else
            break
        end
    end

    printTb(allJiejiNum)
    printTb(allDengjiNum)

    for i=1, #GamePlayConfigData.tzsx2SkillInfo do
        if allDengjiNum >= GamePlayConfigData.tzsx2SkillInfo[i][1] then
            sc.BuffAction(actor, actor, true, false, GamePlayConfigData.tzsx2SkillInfo[i][2], 0, 0)
        else
            break
        end
    end
end


local functionTable = {
    ["useSkillBook"] = useSkillBook,
    ["dropSkillBook"] = dropSkillBook,
    ["wp"] = upWeapon, -- 武器附魔
    ["hj"] = upGuard,
    ["jz"] = upRing,
    ["CallHellFire"] = callHellFire,
    ["CallMoneyBoss"] = callMoneyBoss,
    ["CallGoldOre"] = callGoldOre,
    ["AllMedicine"] = allMedicine,
    ["RefreshMoney"] = refreshMoney,
    ["weapoBoss"] = callEquipBoss,
    ["hujiaBoss"] = callHuJiaBoss,
    ["jiezhiBoss"] = callJieZhiBoss,
    ["dropEquip"] = dropEquip,
    ["sellEquip"] = sellEquip,
    ["GMGongGaoTime"] = GMGongGaoTime,
    ["BigGMGongGaoTime"] = BigGMGongGaoTime,
    ["chushoudaoju"] = chushoudaoju,
    ["daojuMan"] = daojuMan,
    ["czEquip"] = czEquip,
    ["checkGamePlayDataToUI"] = checkGamePlayDataToUI,
    ["UICallBackOfSLD"] = UICallBackOfSLD,
    ["buySkillBook"] = buySkillBook,
    ["sellSkillBook"] = sellSkillBook,
    ["delSkillBook"] = delSkillBook,
    ["chengjiu"] = chengjiu,
    ["addCoin"] = addCoin,
    ["callMonster"] = callMonster,
    ["chooseErXuanYi"] = chooseErXuanYi,
    ["setShareBtn"] = setShareBtn,
    ["nL"] = nL, -- 是否蓝条英雄
    ["init"] = init,
    ["setName"] = setName,
    ["baoshi"] = baoshi,
    ["baoshiTZSX"] = baoshiTZSX,
    ["zdc"] = zdc,
}




-- 接受UI的自定义事件
function OnReceiveUGCCommand(playerId, JasonBuff)
    local myJson = cjson.decode(JasonBuff)
    local actor = sc.GetControlActorByPlayerID(playerId)
    local actorid = sc.GetActorSystemProperty(actor, 1004)


    if functionTable[myJson.cmd] then
        functionTable[myJson.cmd](myJson, actor, playerId, actorid)
    end
end
