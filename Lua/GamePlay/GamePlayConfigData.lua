GamePlayConfigData = GamePlayConfigData or {}

-- 杂项 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
GamePlayConfigData.actorCamp = 1 -- 阵营

GamePlayConfigData.loseCount = 50 -- 失败单数

GamePlayConfigData.levelName = "ZCX03"

GamePlayConfigData.GMGongGaoList = {
    ["actorDead"] = {"<color=#FF0000>", " 已经死亡！</color>"},
    ["symw"] = " 召唤了深渊魔王，请及时击杀！", -- 深渊魔王
    ["lcyyg"] = " 召唤了粮草押运官，请及时击杀！", -- 粮草押运官
    ["mdzl"] = " 召唤了魔袋长老，请及时击杀！", -- 魔袋长老
    ["wq"] = " 召唤了武器附魔挑战怪，请小心应对！", -- 武器附魔挑战怪
    ["hj"] = " 召唤了护甲附魔挑战怪，请小心应对！", -- 护甲附魔挑战怪
    ["jz"] = " 召唤了戒指附魔挑战怪，请小心应对！", -- 戒指附魔挑战怪
    ["zbg"] = "敌军统帅已经出现，请及时击杀！", -- 装备怪
    ["jyg"] = "敌军精英已经出现，请及时击杀！", -- 精英怪
    ["zzboss"] = "<color=#FF0000>敌军首领已经出现，请及时击杀！</color>", -- 最终boss
    ["sl"] = "<color=#00FF00>恭喜您！成功击杀敌军首领，获得胜利！</color>", -- 胜利
    ["bbm"] = "<color=#FF0000>背包已满</color>", -- 背包已满
    ["csx"] = "<color=#FF0000>怪物超过上限，请在下一波怪物刷新前及时清理，否则将失败！</color>", -- 超上限
    ["md"] = {"击败魔袋长老，获得", "级黄金魔袋，击败", "个敌人即可获得奖励"}, -- 获得魔袋
    ["yxks1"] = {"距离游戏开始还有", "秒"}, -- 游戏开始倒计时类型1
    ["yxks2"] = {"<color=#FF0000>距离游戏开始还有", "秒</color>"}, -- 游戏开始倒计时类型2
    ["yxks3"] = "<color=#00EE00>游戏开始，祝您好运</color>", -- 游戏开始倒计时类型3
    ["bossdjs"] = {"<color=#FF0000>敌军首领还有", "秒到达战场！</color>"}, -- boss倒计时
    ["1"] = "随机事件: 部分怪物获得自身血量20%护盾！",
    ["2"] = "随机事件: 部分怪物获得5秒无敌效果！",
    ["3"] = "随机事件: 部分怪物拥有一次复活效果！",
    ["4"] = "随机事件: 部分怪物获得50%的闪避！",
    ["5"] = "随机事件: 部分怪物获得10秒50%的攻速提升！",
    ["6"] = "随机事件: 部分怪物获得血量翻倍！",
    ["7"] = "随机事件: 部分怪物获得10秒100%的攻击力提升！",
}

-- 杂项 end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<








-- 附魔 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
GamePlayConfigData.addGJLBuffList = {
    ["lcyyg"] = 2010261,
    ["mdzl"] = 2010262,
    ["symw"] = 2010263,

    ["wpLv1"] = 2010264,
    ["wpLv2"] = 2010265,
    ["wpLv3"] = 2010266,
    ["wpLv4"] = 2010267,
    ["wpLv5"] = 2010268,
    ["wpLv6"] = 201026801,
    ["wpLv7"] = 201026802,
    ["wpLv8"] = 201026803,
    ["hjLv1"] = 2010269,
    ["hjLv2"] = 2010270,
    ["hjLv3"] = 2010271,
    ["hjLv4"] = 2010272,
    ["hjLv5"] = 2010273,
    ["hjLv6"] = 201027301,
    ["hjLv7"] = 201027302,
    ["hjLv8"] = 201027303,
}

GamePlayConfigData.equipBossCfgIDList = {
    162103,
    52002,
    52003
}



GamePlayConfigData.fumoInfo = {
    {
        {"Texture/Sprite/wuqi.sprite",'攻击力<color=#f18d00>+25</color>\n法术攻击<color=#f18d00>+25</color>',{25, 25},1000},
        {"Texture/Sprite/wuqi.sprite",'攻击力<color=#f18d00>+50</color>\n法术攻击<color=#f18d00>+50</color>',{63, 63},2500},
        {"Texture/Sprite/wuqi.sprite",'攻击力<color=#f18d00>+100</color>\n法术攻击<color=#f18d00>+100</color>',{125, 125},5000},
        {"Texture/Sprite/wuqi.sprite",'攻击力<color=#f18d00>+150</color>\n法术攻击<color=#f18d00>+150</color>',{200, 200},8000},
        {"Texture/Sprite/wuqi.sprite",'攻击力<color=#f18d00>+225</color>\n法术攻击<color=#f18d00>+225</color>',{300, 300},12000},
        {"Texture/Sprite/wuqi.sprite",'攻击力<color=#f18d00>+225</color>\n法术攻击<color=#f18d00>+225</color>',{500, 500},20000},
        {"Texture/Sprite/wuqi.sprite",'攻击力<color=#f18d00>+225</color>\n法术攻击<color=#f18d00>+225</color>',{1250, 1250},50000},
        {"Texture/Sprite/wuqi.sprite",'攻击力<color=#f18d00>+225</color>\n法术攻击<color=#f18d00>+225</color>',{2000, 2000},80000},
    },
    {
        {"Texture/Sprite/hujia.sprite",'护甲<color=#f18d00>+25</color>\n魔抗<color=#f18d00>+25</color>',{1000,10,10},1000},
        {"Texture/Sprite/hujia.sprite",'护甲<color=#f18d00>+50</color>\n魔抗<color=#f18d00>+50</color>',{1000,25,25},2500},
        {"Texture/Sprite/hujia.sprite",'护甲<color=#f18d00>+100</color>\n魔抗<color=#f18d00>+100</color>',{1000,50,50},5000},
        {"Texture/Sprite/hujia.sprite",'护甲<color=#f18d00>+150</color>\n魔抗<color=#f18d00>+150</color>',{1000,80,80},8000},
        {"Texture/Sprite/hujia.sprite",'护甲<color=#f18d00>+225</color>\n魔抗<color=#f18d00>+225</color>',{1000,120,120},12000},
        {"Texture/Sprite/hujia.sprite",'护甲<color=#f18d00>+225</color>\n魔抗<color=#f18d00>+225</color>',{1000,200,200},20000},
        {"Texture/Sprite/hujia.sprite",'护甲<color=#f18d00>+225</color>\n魔抗<color=#f18d00>+225</color>',{1000,500,500},50000},
        {"Texture/Sprite/hujia.sprite",'护甲<color=#f18d00>+225</color>\n魔抗<color=#f18d00>+225</color>',{1000,800,800},80000},
    },
}

-- 附魔 end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<












-- 英雄 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
GamePlayConfigData.heroExLevelInfo = {
    100,
    500,
    2000,
    5000,
    10000,
}




GamePlayConfigData.tanKeInfo = {
    [511] = 1,
    [198] = 1,
    [194] = 1,
    [144] = 1,
    [135] = 1,
    [134] = 1,
    [120] = 1,
    [117] = 1,
    [114] = 1,
    [113] = 1,
    [105] = 1,
    [168] = 1,
    [171] = 1,
}

GamePlayConfigData.zhanShiInfo = {
    [518] = 1,
    [522] = 1,
    [507] = 1,
    [510] = 1,
    [503] = 1,
    [502] = 1,
    [195] = 1,
    [193] = 1,
    [183] = 1,
    [178] = 1,
    [170] = 1,
    [167] = 1,
    [154] = 1,
    [150] = 1,
    [140] = 1,
    [131] = 1,
    [129] = 1,
    [128] = 1,
    [123] = 1,
    [107] = 1,
    [531] = 1,
    [527] = 1,
    [536] = 1,
    [528] = 1,
}

GamePlayConfigData.sheShouInfo = {
    [524] = 1,
    [508] = 1,
    [199] = 1,
    [174] = 1,
    [173] = 1,
    [132] = 1,
    [111] = 1,
    [133] = 1,
    [192] = 1,
}


GamePlayConfigData.faShiInfo = {
    [312] = 1,
    [137] = 1,
    [504] = 1,
    [179] = 1,
    [182] = 1,
    [190] = 1,
    [175] = 1,
    [148] = 1,
    [146] = 1,
    [124] = 1,
    [121] = 1,
    [119] = 1,
    [110] = 1,
    [106] = 1,
    [108] = 1,
    [109] = 1,
    [115] = 1,
    [118] = 1,
    [127] = 1,
    [141] = 1,
    [142] = 1,
    [152] = 1,
    [184] = 1,
    [189] = 1,
    [176] = 1,
    [197] = 1,
    [513] = 1,
    [515] = 1,
    [505] = 1,
    [523] = 1,
    [525] = 1,
    [537] = 1,
}


GamePlayConfigData.jinZhanInfo = {
    [501] = 1,
    [182] = 1,
    [175] = 1,
    [146] = 1,
    [108] = 1,
    [537] = 1,
    [511] = 1,
    [198] = 1,
    [194] = 1,
    [144] = 1,
    [135] = 1,
    [134] = 1,
    [120] = 1,
    [117] = 1,
    [114] = 1,
    [113] = 1,
    [105] = 1,
    [168] = 1,
    [171] = 1,
    [518] = 1,
    [522] = 1,
    [507] = 1,
    [510] = 1,
    [503] = 1,
    [502] = 1,
    [195] = 1,
    [193] = 1,
    [183] = 1,
    [178] = 1,
    [170] = 1,
    [167] = 1,
    [154] = 1,
    [150] = 1,
    [140] = 1,
    [131] = 1,
    [129] = 1,
    [128] = 1,
    [123] = 1,
    [107] = 1,
    [531] = 1,
    [527] = 1,
    [536] = 1,
    [528] = 1,
}


GamePlayConfigData.yuanChenInfo = {
    [189] = 1,
    [312] = 1,
    [137] = 1,
    [504] = 1,
    [179] = 1,
    [190] = 1,
    [148] = 1,
    [124] = 1,
    [121] = 1,
    [119] = 1,
    [110] = 1,
    [106] = 1,
    [109] = 1,
    [115] = 1,
    [118] = 1,
    [127] = 1,
    [141] = 1,
    [142] = 1,
    [152] = 1,
    [184] = 1,
    [176] = 1,
    [197] = 1,
    [513] = 1,
    [515] = 1,
    [505] = 1,
    [523] = 1,
    [525] = 1,
    [524] = 1,
    [508] = 1,
    [199] = 1,
    [174] = 1,
    [173] = 1,
    [132] = 1,
    [111] = 1,
    [133] = 1,
    [192] = 1,
}


GamePlayConfigData.heroEXInfo = { --
	[524] = 1, -- 蒙犽  射手
    [518] = 2, -- 马超 战士
    [522] = 3, -- 曜 战士
    [506] = 4, -- 云中君 刺客
    [529] = 5, -- 盘古 战士
    [511] = 6, -- 猪八戒 坦克
    [507] = 7, -- 李信 战士
    [312] = 8, -- 沈梦溪 法师
    [508] = 9, -- 伽罗 射手
    [137] = 10, -- 司马懿 刺客
    [510] = 11, -- 孙策 坦克
    [504] = 12, -- 米莱迪 法师
    [503] = 13, -- 狂铁 战士
    [502] = 14, -- 裴擒虎 刺客
    [199] = 15, -- 公孙离 射手
    [179] = 16, -- 女娲 法师
	[198] = 17, -- 梦奇 坦克
	[194] = 18, -- 苏烈 坦克
	[195] = 19, -- 百里玄策 刺客
	[193] = 20, -- 铠 战士
	[182] = 21, -- 干将莫邪 法师
	[190] = 22, -- 诸葛亮 法师
	[186] = 23, -- 太乙真人 辅助
	[183] = 24, -- 雅典娜 战士
	[178] = 25, -- 杨戬 战士
	[175] = 26, -- 钟馗 法师
	[174] = 27, -- 虞姬 射手
	[173] = 28, -- 李元芳 射手
	[170] = 29, -- 刘备 战士
	[167] = 30, -- 孙悟空 刺客
	[166] = 31, -- 亚瑟 战士
	[154] = 32, -- 花木兰 战士
	[153] = 33, -- 兰陵王 刺客
	[150] = 34, -- 韩信 刺客
	[148] = 35, -- 姜子牙 法师
	[146] = 36, -- 露娜 刺客
	[144] = 37, -- 程咬金 坦克
	[140] = 38, -- 关羽 战士
	[135] = 39, -- 项羽 坦克
	[134] = 40, -- 达摩 战士
	[132] = 41, -- 马可波罗 射手
	[131] = 42, -- 李白 刺客
	[129] = 43, -- 典韦 战士
	[128] = 44, -- 曹操 战士
	[126] = 45, -- 夏侯惇 坦克
	[124] = 46, -- 周瑜 法师
	[123] = 47, -- 吕布 战士
	[121] = 48, -- 芈月 法师
	[120] = 49, -- 白起 坦克
	[119] = 50, -- 扁鹊 法师
	[117] = 51, -- 钟无艳 战士
	[116] = 52, -- 阿轲 刺客
	[114] = 53, -- 刘禅 辅助
	[113] = 54, -- 庄周 辅助
	[110] = 55, -- 嬴政 法师
	[107] = 56, -- 赵云 战士
	[105] = 57, -- 廉颇 坦克
    -- [163] = 32, -- 橘右京 刺客
	-- [162] = 33, -- 娜可露露 刺客
	-- [157] = 34, -- 不知火舞 法师
}


-- 英雄 end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<























-- 技能 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
GamePlayConfigData.jnsInfo = {
    {"Texture/Sprite/Db.sprite",'获得一本D级被动技能书，每次购买所需金币翻倍，单局最多购买10次',1,1000}, -- 被动
    {"Texture/Sprite/Cb.sprite",'获得一本C级被动技能书，每次购买所需金币翻倍，单局最多购买10次',2,2500},
    {"Texture/Sprite/Bb.sprite",'获得一本B级被动技能书，每次购买所需金币翻倍，单局最多购买10次',3,6000},
    {"Texture/Sprite/Ab.sprite",'获得一本A级被动技能书，每次购买所需金币翻倍，单局最多购买10次',4,15000},

    {"Texture/Sprite/Dz.sprite",'获得一本D级主动技能书，每次购买所需金币翻倍，单局最多购买10次',1,1000}, -- 主动
    {"Texture/Sprite/Cz.sprite",'获得一本C级主动技能书，每次购买所需金币翻倍，单局最多购买10次',2,2500},
    {"Texture/Sprite/Bz.sprite",'获得一本B级主动技能书，每次购买所需金币翻倍，单局最多购买10次',3,6000},
    {"Texture/Sprite/Az.sprite",'获得一本A级主动技能书，每次购买所需金币翻倍，单局最多购买10次',4,15000},
}

GamePlayConfigData.activeSkillSlot = 6
local activeSkillSlot = 6
GamePlayConfigData.activeSkillTable = {
    [1] = {
        [1] = {12803,activeSkillSlot}, -- D级多重箭 √
        [2] = {12806,activeSkillSlot}, -- D级无光之盾 √
        [3] = {12815,activeSkillSlot}, -- D级圣光领域 √
        [4] = {128551,activeSkillSlot}, -- D级牺牲 √
        [5] = {128091,activeSkillSlot}, -- D级剑舞 √
        [6] = {128521,activeSkillSlot}, -- D级无敌术 √
        [7] = {1282011,activeSkillSlot}, -- D级点金术 √
        [8] = {128411,activeSkillSlot}, -- D级离歌 √
        [9] = {1284131,activeSkillSlot}, -- D级野性释放 √
        [10] = {128611,activeSkillSlot}, -- D级剑燃 √
        [11] = {128181,activeSkillSlot}, -- D级黑洞 √
        [12] = {1020510,activeSkillSlot}, -- D级虚弱术 √
    },
    [2] = {
        [1] = {12816,activeSkillSlot}, -- C级野性释放 √
        [2] = {128201,activeSkillSlot}, -- C级点金术 √
        [3] = {12861,activeSkillSlot}, -- C级剑燃 √
        [4] = {128031,activeSkillSlot}, -- C级多重箭 √
        [5] = {128061,activeSkillSlot}, -- C级无光之盾 √
        [6] = {128151,activeSkillSlot}, -- C级圣光领域 √
        [7] = {128552,activeSkillSlot}, -- C级牺牲 √
        [8] = {128092,activeSkillSlot}, -- C级剑舞 √
        [9] = {128522,activeSkillSlot}, -- C级无敌术 √
        [10] = {128412,activeSkillSlot}, -- C级离歌 √
        [11] = {102051,activeSkillSlot}, -- C级黑洞 √
        [12] = {1020511,activeSkillSlot}, -- C级虚弱术 √
    },
    [3] = {
        [1] = {12809,activeSkillSlot}, -- B级剑舞 √
        [2] = {12841,activeSkillSlot}, -- B级离歌 √
        [3] = {12852,activeSkillSlot}, -- B级无敌术 √
        [4] = {128032,activeSkillSlot}, -- B级多重箭 √
        [5] = {128062,activeSkillSlot}, -- B级无光之盾 √
        [6] = {128152,activeSkillSlot}, -- B级圣光领域 √
        [7] = {128553,activeSkillSlot}, -- B级牺牲 √
        [8] = {1282012,activeSkillSlot}, -- B级点金术 √
        [9] = {1284132,activeSkillSlot}, -- B级野性释放 √
        [10] = {128612,activeSkillSlot}, -- B级剑燃 √
        [11] = {102052,activeSkillSlot}, -- B级泔油 √
        [12] = {1020512,activeSkillSlot}, -- B级虚弱术 √
    },
    [4] = {
        [1] = {12855,activeSkillSlot}, -- A级牺牲 √
        [2] = {128033,activeSkillSlot}, -- A级多重箭 √
        [3] = {128063,activeSkillSlot}, -- A级无光之盾 √
        [4] = {128153,activeSkillSlot}, -- A级圣光领域 √
        [5] = {128093,activeSkillSlot}, -- A级剑舞 √
        [6] = {128523,activeSkillSlot}, -- A级无敌术 √
        [7] = {1282013,activeSkillSlot}, -- A级点金术 √
        [8] = {128413,activeSkillSlot}, -- A级离歌 √
        [9] = {1284133,activeSkillSlot}, -- A级野性释放 √
        [10] = {128613,activeSkillSlot}, -- A级剑燃 √
        [11] = {102053,activeSkillSlot}, -- A级泔油 √
        [12] = {1020513,activeSkillSlot}, -- A级虚弱术
    },
}


GamePlayConfigData.passSkillSlot = 8
local passSkillSlot = 8
GamePlayConfigData.passSkillTable = {
    [1] = {
        [1] = {12318, passSkillSlot}, -- D级重击 √
        [2] = {1230401, passSkillSlot}, -- D级活性护甲 √
        [3] = {12366, passSkillSlot}, -- D级狂怒 √
        [4] = {12339, passSkillSlot}, -- D级清算 √
        [5] = {1230201, passSkillSlot}, -- D级闪避 √
        [6] = {12314, passSkillSlot}, -- D级武器附魔 √
        [7] = {123201, passSkillSlot}, -- D级闪电过载 √
        [8] = {12309, passSkillSlot}, -- D级理财高手 √
        [9] = {12312, passSkillSlot}, -- D级刀扇装甲 √
        [10] = {123010, passSkillSlot}, -- D级属性加强 √
        [11] = {12372, passSkillSlot}, -- D级苦难光环  √
    },
    [2] = {
        [1] = {1230202, passSkillSlot}, -- C级闪避 √
        [2] = {123391, passSkillSlot}, -- C级清算 √
        [3] = {123141, passSkillSlot}, -- C级武器附魔 √
        [4] = {123181, passSkillSlot}, -- C级重击 √
        [5] = {12304011, passSkillSlot}, -- C级活性护甲 √
        [6] = {123661, passSkillSlot}, -- C级狂怒 √
        [7] = {123202, passSkillSlot}, -- C级闪电过载 √
        [8] = {123091, passSkillSlot}, -- C级理财高手 √
        [9] = {123121, passSkillSlot}, -- C级刀扇装甲 √
        [10] = {123011, passSkillSlot}, -- C级属性加强 √
        [11] = {123721, passSkillSlot}, -- C级苦难光环 √
    },
    [3] = {
        [1] = {123092, passSkillSlot}, -- D级理财高手 √
        [2] = {123203, passSkillSlot}, -- B级闪电过载 √
        [3] = {123122, passSkillSlot}, -- B级刀扇装甲 √
        [4] = {123392, passSkillSlot}, -- B级清算 √
        [5] = {123182, passSkillSlot}, -- B级重击 √
        [6] = {12304012, passSkillSlot}, -- B级活性护甲 √
        [7] = {123392, passSkillSlot}, -- B级清算 √
        [8] = {1230203, passSkillSlot}, -- B级闪避 √
        [9] = {123142, passSkillSlot}, -- B级武器附魔 √
        [10] = {123012, passSkillSlot}, -- B级属性加强 √
        [11] = {123722, passSkillSlot}, -- B级苦难光环 √
    },
    [4] = {
        [1] = {123013,passSkillSlot}, -- A级属性加强 √
        [2] = {123723,passSkillSlot}, -- A级刀扇装甲 √
        [3] = {123723,passSkillSlot}, -- A级苦难光环 √
        [4] = {123393,passSkillSlot}, -- A级清算 √
        [5] = {123183,passSkillSlot}, -- A级重击 √
        [6] = {12304013,passSkillSlot}, -- A级活性护甲 √
        [7] = {123393,passSkillSlot}, -- A级清算 √
        [8] = {1230204,passSkillSlot}, -- A级闪避 √
        [9] = {123143,passSkillSlot}, -- A级武器附魔 √
        [10] = {123204,passSkillSlot}, -- A级闪电过载 √
        [11] = {123093,passSkillSlot}, -- A级理财高手 √
    },
}


GamePlayConfigData.daojuNameColorInfo = {
    ["D级被动技能书"] = "D 级 被 动 技 能 书",
    ["D级主动技能书"] = "D 级 主 动 技 能 书",
    ["C级被动技能书"] = "<color=#1E90FF>C 级 被 动 技 能 书</color>",
    ["C级主动技能书"] = "<color=#1E90FF>C 级 主 动 技 能 书</color>",
    ["B级被动技能书"] = "<color=#8A2BE2>B 级 被 动 技 能 书</color>",
    ["B级主动技能书"] = "<color=#8A2BE2>B 级 主 动 技 能 书</color>",
    ["A级被动技能书"] = "<color=#FFA500>A 级 被 动 技 能 书</color>",
    ["A级主动技能书"] = "<color=#FFA500>A 级 主 动 技 能 书</color>"
}


GamePlayConfigData.equipLevelImgInfo = {
    [0] = "Texture/Sprite/lingFanTi.sprite",
    [1] = "Texture/Sprite/yiFanTi.sprite",
    [2] = "Texture/Sprite/erFanTi.sprite",
    [3] = "Texture/Sprite/sanFanTi.sprite",
    [4] = "Texture/Sprite/siFanTi.sprite",
    [5] = "Texture/Sprite/wuFanTi.sprite",
}


GamePlayConfigData.shopHanziInfo = {
    ["零"] = "零",
    ["壹"] = "<color=#1E90FF>壹</color>",
    ["贰"] = "<color=#8A2BE2>贰</color>",
    ["叁"] = "<color=#FFA500>叁</color>",
    ["肆"] = "<color=#FF0000>肆</color>",
    ["伍"] = "<color=#00FF00>伍</color>",
}


GamePlayConfigData.equipLevelImgInfoOld = {
    [0] = "Texture/Sprite/0.sprite",
    [1] = "Texture/Sprite/1.sprite",
    [2] = "Texture/Sprite/2.sprite",
    [3] = "Texture/Sprite/3.sprite",
    [4] = "Texture/Sprite/4.sprite",
    [5] = "Texture/Sprite/5.sprite",
}


GamePlayConfigData.skillBookOpenBoci = {
    0,5,15,25,40
}
-- 技能 end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<





-- 存档宝石 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
GamePlayConfigData.tzsx1SkillInfo = {
    {12, nil},
    {24, 173007},
    {36, 173008},
    {48, 173009},
    {60, 173010},
}

GamePlayConfigData.tzsx2SkillInfo = {
    {12, 173001},
    {24, 173002},
    {36, 173003},
    {48, 173004},
    {60, 173005},
    {72, 173006},
}



GamePlayConfigData.wuliGongJiLiSkillInfo = {
    [1] = 172714,
    [2] = 172715,
    [3] = 172716,
    [4] = 172701,
    [5] = 172702,
    [6] = 172703,
    [7] = 172704,
    [8] = 172705,
    [9] = 172706,
    [10] = 172707,
    [11] = 172708,
    [12] = 172709,
    [13] = 172710,
    [14] = 172711,
    [15] = 172712,
    [16] = 172713,
}

GamePlayConfigData.wuliChuanTouSkillInfo = {
    [1] = 1727301,
    [2] = 1727302,
    [3] = 1727303,
    [4] = 1727304,
    [5] = 1727305,
    [6] = 1727306,
    [7] = 1727307,
    [8] = 1727308,
    [9] = 1727309,
    [10] = 1727310,
    [11] = 1727311,
    [12] = 1727312,
    [13] = 1727313,
    [14] = 1727314,
    [15] = 1727315,
    [16] = 1727316,
}


GamePlayConfigData.faShuGongJiLiSkillInfo = {
    [1] = 1727201,
    [2] = 1727202,
    [3] = 1727203,
    [4] = 1727204,
    [5] = 1727205,
    [6] = 1727206,
    [7] = 1727207,
    [8] = 1727208,
    [9] = 1727209,
    [10] = 1727210,
    [11] = 1727211,
    [12] = 1727212,
    [13] = 1727213,
    [14] = 1727214,
    [15] = 1727215,
    [16] = 1727216,
}


GamePlayConfigData.faShuChuanTouSkillInfo = {
    [1] = 1727401,
    [2] = 1727402,
    [3] = 1727403,
    [4] = 1727404,
    [5] = 1727405,
    [6] = 1727406,
    [7] = 1727407,
    [8] = 1727408,
    [9] = 1727409,
    [10] = 1727410,
    [11] = 1727411,
    [12] = 1727412,
    [13] = 1727413,
    [14] = 1727414,
    [15] = 1727415,
    [16] = 1727416,
}


GamePlayConfigData.baoJiLvSkillInfo = {
    [1] = 1727501,
    [2] = 1727502,
    [3] = 1727503,
    [4] = 1727504,
    [5] = 1727505,
    [6] = 1727506,
    [7] = 1727507,
    [8] = 1727508,
    [9] = 1727509,
    [10] = 1727510,
    [11] = 1727511,
    [12] = 1727512,
    [13] = 1727513,
    [14] = 1727514,
    [15] = 1727515,
    [16] = 1727516,
}


GamePlayConfigData.baoJiXiaoGuoSkillInfo = {
    [1] = 1727601,
    [2] = 1727602,
    [3] = 1727603,
    [4] = 1727604,
    [5] = 1727605,
    [6] = 1727606,
    [7] = 1727607,
    [8] = 1727608,
    [9] = 1727609,
    [10] = 1727610,
    [11] = 1727611,
    [12] = 1727612,
    [13] = 1727613,
    [14] = 1727614,
    [15] = 1727615,
    [16] = 1727616,
}

GamePlayConfigData.meiMiaoHuiXuekillInfo = {
    [1] = 1727701,
    [2] = 1727702,
    [3] = 1727703,
    [4] = 1727704,
    [5] = 1727705,
    [6] = 1727706,
    [7] = 1727707,
    [8] = 1727708,
    [9] = 1727709,
    [10] = 1727710,
    [11] = 1727711,
    [12] = 1727712,
    [13] = 1727713,
    [14] = 1727714,
    [15] = 1727715,
    [16] = 1727716,
}



GamePlayConfigData.meiMiaoHuiLankillInfo = {
    [1] = 1727801,
    [2] = 1727802,
    [3] = 1727803,
    [4] = 1727804,
    [5] = 1727805,
    [6] = 1727806,
    [7] = 1727807,
    [8] = 1727808,
    [9] = 1727809,
    [10] = 1727810,
    [11] = 1727811,
    [12] = 1727812,
    [13] = 1727813,
    [14] = 1727814,
    [15] = 1727815,
    [16] = 1727816,
}


GamePlayConfigData.shengMingZhiShangXianSkillInfo = {
    [1] = 1727901,
    [2] = 1727902,
    [3] = 1727903,
    [4] = 1727904,
    [5] = 1727905,
    [6] = 1727906,
    [7] = 1727907,
    [8] = 1727908,
    [9] = 1727909,
    [10] = 1727910,
    [11] = 1727911,
    [12] = 1727912,
    [13] = 1727913,
    [14] = 1727914,
    [15] = 1727915,
    [16] = 1727916,
}

GamePlayConfigData.moFaZhiShangXianSkillInfo = {
    [1] = 1728101,
    [2] = 1728102,
    [3] = 1728103,
    [4] = 1728104,
    [5] = 1728105,
    [6] = 1728106,
    [7] = 1728107,
    [8] = 1728108,
    [9] = 1728109,
    [10] = 1728110,
    [11] = 1728111,
    [12] = 1728112,
    [13] = 1728113,
    [14] = 1728114,
    [15] = 1728115,
    [16] = 1728116,
}



GamePlayConfigData.faShuFangYukillInfo = {
    [1] = 1728701,
    [2] = 1728702,
    [3] = 1728703,
    [4] = 1728704,
    [5] = 1728705,
    [6] = 1728706,
    [7] = 1728707,
    [8] = 1728708,
    [9] = 1728709,
    [10] = 1728710,
    [11] = 1728711,
    [12] = 1728712,
    [13] = 1728713,
    [14] = 1728714,
    [15] = 1728715,
    [16] = 1728716,
}



GamePlayConfigData.wuLiFangYukillInfo = {
    [1] = 1727101,
    [2] = 1727102,
    [3] = 1727103,
    [4] = 1727104,
    [5] = 1727105,
    [6] = 1727106,
    [7] = 1727107,
    [8] = 1727108,
    [9] = 1727109,
    [10] = 1727110,
    [11] = 1727111,
    [12] = 1727112,
    [13] = 1727113,
    [14] = 1727114,
    [15] = 1727115,
    [16] = 1727116,
}

GamePlayConfigData.wuLiXiXueSkillInfo = {
    [1] = 1728201,
    [2] = 1728202,
    [3] = 1728203,
    [4] = 1728204,
    [5] = 1728205,
    [6] = 1728206,
    [7] = 1728207,
    [8] = 1728208,
    [9] = 1728209,
    [10] = 1728210,
    [11] = 1728211,
    [12] = 1728212,
    [13] = 1728213,
    [14] = 1728214,
    [15] = 1728215,
    [16] = 1728216,
}


GamePlayConfigData.faShuXiXueSkillInfo = {
    [1] = 1728301,
    [2] = 1728302,
    [3] = 1728303,
    [4] = 1728304,
    [5] = 1728305,
    [6] = 1728306,
    [7] = 1728307,
    [8] = 1728308,
    [9] = 1728309,
    [10] = 1728310,
    [11] = 1728311,
    [12] = 1728312,
    [13] = 1728313,
    [14] = 1728314,
    [15] = 1728315,
    [16] = 1728316,
}


GamePlayConfigData.gongJiSuDuSkillInfo = {
    [1] = 1728401,
    [2] = 1728402,
    [3] = 1728403,
    [4] = 1728404,
    [5] = 1728405,
    [6] = 1728406,
    [7] = 1728407,
    [8] = 1728408,
    [9] = 1728409,
    [10] = 1728410,
    [11] = 1728411,
    [12] = 1728412,
    [13] = 1728413,
    [14] = 1728414,
    [15] = 1728415,
    [16] = 1728416,
}

GamePlayConfigData.yiDongSuDuSkillInfo = {
    [1] = 1728501,
    [2] = 1728502,
    [3] = 1728503,
    [4] = 1728504,
    [5] = 1728505,
    [6] = 1728506,
    [7] = 1728507,
    [8] = 1728508,
    [9] = 1728509,
    [10] = 1728510,
    [11] = 1728511,
    [12] = 1728512,
    [13] = 1728513,
    [14] = 1728514,
    [15] = 1728515,
    [16] = 1728516,
}


GamePlayConfigData.lengQueSuoJianSkillInfo = {
    [1] = 1728601,
    [2] = 1728602,
    [3] = 1728603,
    [4] = 1728604,
    [5] = 1728605,
    [6] = 1728606,
    [7] = 1728607,
    [8] = 1728608,
    [9] = 1728609,
    [10] = 1728610,
    [11] = 1728611,
    [12] = 1728612,
    [13] = 1728613,
    [14] = 1728614,
    [15] = 1728615,
    [16] = 1728616,
}





-- 存档宝石 end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<













-- 组队 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
GamePlayConfigData.nanduInfo = { -- zdc   可以设置每日获取上限，然后卖上限道具
    {"初级",10,"Texture/Sprite/chuji.sprite", "Texture/Sprite/chujiHui.sprite", "Texture/Sprite/chujiText.sprite"},
    {"中级",15,"Texture/Sprite/zhongji1.sprite", "Texture/Sprite/zhongjiHui.sprite", "Texture/Sprite/zhongjiText.sprite"},
    {"高级",20,"Texture/Sprite/gaoji.sprite", "Texture/Sprite/gaojiHui.sprite", "Texture/Sprite/gaojiText.sprite"},
    {"超级",25,"Texture/Sprite/chaoji.sprite", "Texture/Sprite/chaojiHui.sprite", "Texture/Sprite/chaojiText.sprite"},
    {"王者",30,"Texture/Sprite/wangzhe.sprite", "Texture/Sprite/wangzheHui.sprite", "Texture/Sprite/wangzheText.sprite"},
    {"死亡",35,"Texture/Sprite/siwang.sprite", "Texture/Sprite/siwangHui.sprite", "Texture/Sprite/siwangText.sprite"},
    {"梦魇",40,"Texture/Sprite/mengyan.sprite", "Texture/Sprite/mengyanHui.sprite", "Texture/Sprite/mengyanText.sprite"},
    {"超脱",45,"Texture/Sprite/chaotuo.sprite", "Texture/Sprite/chaotuoHui.sprite", "Texture/Sprite/chaotuoText.sprite"},
}


GamePlayConfigData.teamInfo = {
	{"单人游戏", "1", "Texture/Sprite/danren.sprite", "Texture/Sprite/danrenHui.sprite", "Texture/Sprite/danrenText.sprite"},
	{"双人组队", "2", "Texture/Sprite/shuangren.sprite", "Texture/Sprite/shuangrenHui.sprite", "Texture/Sprite/shuangrenText.sprite"},
	{"三人组队", "3", "Texture/Sprite/sanren.sprite", "Texture/Sprite/sanrenHui.sprite", "Texture/Sprite/sanrenText.sprite"},
	{"四人组队", "4", "Texture/Sprite/siren.sprite", "Texture/Sprite/sirenHui.sprite", "Texture/Sprite/sirenText.sprite"},
}


-- 组队 end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<













-- 段位 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
GamePlayConfigData.duanweizengfu = {
    ["青铜"] = {0,20},
    ["白银"] = {60,50},
    ["黄金"] = {210,75},
    ["铂金"] = {510,100},
    ["钻石"] = {1010,200},
    ["星耀"] = {2010,400},
    ["最强王者"] = {4010,600},
    ["荣耀王者"] = {34010,800},
}


GamePlayConfigData.daunweiNuberImgInfo = {
    ["青铜"] = {"Texture/Sprite/jjqt1.sprite","Texture/Sprite/jjqt2.sprite","Texture/Sprite/jjqt3.sprite"},
    ["白银"] = {"Texture/Sprite/zxby1.sprite","Texture/Sprite/zxby2.sprite","Texture/Sprite/zxby3.sprite",},
    ["黄金"] = {"Texture/Sprite/ryhj1.sprite","Texture/Sprite/ryhj2.sprite","Texture/Sprite/ryhj3.sprite","Texture/Sprite/ryhj4.sprite"},
    ["铂金"] = {"Texture/Sprite/zgbj1.sprite","Texture/Sprite/zgbj2.sprite","Texture/Sprite/zgbj3.sprite","Texture/Sprite/zgbj4.sprite","Texture/Sprite/zgbj5.sprite"},
    ["钻石"] = {"Texture/Sprite/yhzs1.sprite","Texture/Sprite/yhzs2.sprite","Texture/Sprite/yhzs3.sprite","Texture/Sprite/yhzs4.sprite","Texture/Sprite/yhzs5.sprite"},
    ["星耀"] = {"Texture/Sprite/zzhy1.sprite","Texture/Sprite/zzhy2.sprite","Texture/Sprite/zzhy3.sprite","Texture/Sprite/zzhy4.sprite","Texture/Sprite/zzhy5.sprite",},
}


--[[
GamePlayConfigData.luomashuzi = {
    "Ⅰ",
    "Ⅱ",
    "Ⅲ",
    "Ⅳ",
    "Ⅴ",
}
]]



--[[
GamePlayConfigData.duanweiInfo = {
    ["青铜5"] = {0,19,20},
    ["青铜4"] = {20,39,20},
    ["青铜3"] = {40,59,20},
    ["青铜2"] = {60,79,20},
    ["青铜1"] = {80,99,0},
    ["白银5"] = {100,149,50},
    ["白银4"] = {150,199,50},
    ["白银3"] = {200,249,50},
    ["白银2"] = {250,299,50},
    ["白银1"] = {300,349,50},
    ["黄金5"] = {350,424,75},
    ["黄金4"] = {425,499,75},
    ["黄金3"] = {500,574,75},
    ["黄金2"] = {575,649,75},
    ["黄金1"] = {650,724,75},
    ["铂金5"] = {725,824,100},
    ["铂金4"] = {825,924,100},
    ["铂金3"] = {925,1024,100},
    ["铂金2"] = {1025,1124,100},
    ["铂金1"] = {1125,1224,100},
    ["钻石5"] = {1225,1424,200},
    ["钻石4"] = {1425,1624,200},
    ["钻石3"] = {1625,1824,200},
    ["钻石2"] = {1025,2024,200},
    ["钻石1"] = {2025,2224,200},
    ["星耀5"] = {2225,2624,400},
    ["星耀4"] = {2625,3024,400},
    ["星耀3"] = {3025,3424,400},
    ["星耀2"] = {3425,3824,400},
    ["星耀1"] = {3825,4224,400},
    ["最强王者1星"] = {4225,4824,600},
    ["最强王者49星"] = 35070,
    ["荣耀王者1星"] = 35870,
}
]]
-- 段位 end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


















-- 掉落物/装备 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
GamePlayConfigData.diuqiEquipInfo = {
    [1111] = 901, -- 铁剑
    [1112] = 921, -- 匕首
    [1113] = 1461 , -- 搏击拳套
    [1114] = 1471 , -- 吸血之镰
    [1116] = 2221 , -- 雷鸣刃                    -- 这里的东西在掉落物里找！！！
    [1211] = 1501 , -- 咒术典籍
    [1212] = 1511 , -- 蓝宝石
    [1214] = 1531 , -- 圣者法典
    [1216] = 2241 , -- 元素杖
    [1311] = 1571 , -- 红玛瑙
    [1312] = 1581 , -- 布甲
    [1121] = 911, -- 风暴巨剑
    [1122] = 1481 , -- 日冕
    [1123] = 2211 , -- 狂暴双刃
    [1124] = 931, -- 陨星
    [1129] = 1491 , -- 速击之枪
    [1154] = 2271 , -- 穿云弓
    [1221] = 941, -- 大棒
    [1222] = 1541 , -- 血族之书
    [1223] = 1551 , -- 光辉之剑
    [1224] = 951, -- 魅影面罩
    [1225] = 2281 , -- 进化水晶
    [1229] = 1561 , -- 破碎圣杯
    [1321] = 961, -- 力量腰带
    [1322] = 1781 , -- 熔炼之心
    [1324] = 1791 , -- 雪山圆盾
    [1325] = 971, -- 守护者之铠
    [1126] = 991, -- 末世
    [1134] = 1051 , -- 宗师之力
    [1135] = 1061 , -- 闪电匕首
    [1137] = 1081 , -- 暗影战斧
    [1127] = 1001 , -- 名刀
    [1233] = 1141 , -- 回响之杖
    [1226] = 1441 , -- 圣杯
    [1231] = 1121 , -- 虚无法杖
    [1327] = 1221 , -- 反伤刺甲
    [1234] = 1151 , -- 冰霜法杖
    [1336] = 1291 , -- 极寒风暴
    [1236] = 1171 , -- 巫术法杖
    [1232] = 1131 , -- 博学者之怒（帽子）
    [1133] = 1041 , -- 无尽战刃
    [1138] = 811, -- 破军
    [1238] = 1181 , -- 贤者之书
    [1332] = 1251 , -- 霸者重装
    [1338] = 1311 , -- 暴烈之甲
    [1331] = 1241 , -- 红莲斗篷
    [1136] = 1071 , -- 影刃
    [1235] = 1161 , -- 痛苦面具
}


GamePlayConfigData.dropSKillIDLevelInfo = {
    {
        {1030912, 1030913},
        {1030914, 1030915},
        {1030916, 1030917},
        {1030918, 1030919},
    },
    {
        {1030920, 1030921},
        {1030922, 1030923},
        {1030924, 1030925},
        {1030926, 1030927},
    },
    {
        {1030928, 1030929},
        {1030930, 1030931},
        {1030932, 1030933},
        {1030934, 1030935},
    },
    {
        {1030936, 1030937},
        {1030938, 1030939},
        {1030940, 1030941},
        {1030942, 1030943},
    },
    {
        {1030944, 1030945},
        {1030946, 1030947},
        {1030948, 1030949},
        {1030950, 1030951},
    },
}


GamePlayConfigData.playerDropSkillInfo = {
    {1030912, 1030913, 1030914, 1030915, 1030916, 1030917, 1030918, 1030919},
    {1030920, 1030921, 1030922, 1030923, 1030924, 1030925, 1030926, 1030927},
    {1030928, 1030929, 1030930, 1030931, 1030932, 1030933, 1030934, 1030935},
    {1030936, 1030937, 1030938, 1030939, 1030940, 1030941, 1030942, 1030943},
    {1030944, 1030946, 1030948, 1030950, 1030945, 1030947, 1030949, 1030951},
}

GamePlayConfigData.dropSkillInfo = {
    [1030912] = 1,
    [1030913] = 1,
    [1030914] = 1,
    [1030915] = 1,
    [1030916] = 1,
    [1030917] = 1,
    [1030918] = 1,
    [1030919] = 1,
    [1030920] = 1,
    [1030921] = 1,
    [1030922] = 1,
    [1030923] = 1,
    [1030924] = 1,
    [1030925] = 1,
    [1030926] = 1,
    [1030927] = 1,
    [1030928] = 1,
    [1030929] = 1,
    [1030930] = 1,
    [1030931] = 1,
    [1030932] = 1,
    [1030933] = 1,
    [1030934] = 1,
    [1030935] = 1,
    [1030936] = 1,
    [1030937] = 1,
    [1030938] = 1,
    [1030939] = 1,
    [1030940] = 1,
    [1030941] = 1,
    [1030942] = 1,
    [1030943] = 1,
    [1030944] = 1,
    [1030945] = 1,
    [1030946] = 1,
    [1030947] = 1,
    [1030948] = 1,
    [1030949] = 1,
    [1030950] = 1,
    [1030951] = 1,
}


GamePlayConfigData.dropJNSNameInfo = {
    [1030912] = {"D级被动", "Texture/Sprite/Db.sprite"},
    [1030913] = {"D级主动", "Texture/Sprite/Dz.sprite"},
    [1030914] = {"C级被动", "Texture/Sprite/Cb.sprite"},
    [1030915] = {"C级主动", "Texture/Sprite/Cz.sprite"},
    [1030916] = {"B级被动", "Texture/Sprite/Bb.sprite"},
    [1030917] = {"B级主动", "Texture/Sprite/Bz.sprite"},
    [1030918] = {"A级被动", "Texture/Sprite/Ab.sprite"},
    [1030919] = {"A级主动", "Texture/Sprite/Az.sprite"},

    [1030920] = {"D级被动", "Texture/Sprite/Db.sprite"},
    [1030921] = {"D级主动", "Texture/Sprite/Dz.sprite"},
    [1030922] = {"C级被动", "Texture/Sprite/Cb.sprite"},
    [1030923] = {"C级主动", "Texture/Sprite/Cz.sprite"},
    [1030924] = {"B级被动", "Texture/Sprite/Bb.sprite"},
    [1030925] = {"B级主动", "Texture/Sprite/Bz.sprite"},
    [1030926] = {"A级被动", "Texture/Sprite/Ab.sprite"},
    [1030927] = {"A级主动", "Texture/Sprite/Az.sprite"},

    [1030928] = {"D级被动", "Texture/Sprite/Db.sprite"},
    [1030929] = {"D级主动", "Texture/Sprite/Dz.sprite"},
    [1030930] = {"C级被动", "Texture/Sprite/Cb.sprite"},
    [1030931] = {"C级主动", "Texture/Sprite/Cz.sprite"},
    [1030932] = {"B级被动", "Texture/Sprite/Bb.sprite"},
    [1030933] = {"B级主动", "Texture/Sprite/Bz.sprite"},
    [1030934] = {"A级被动", "Texture/Sprite/Ab.sprite"},
    [1030935] = {"A级主动", "Texture/Sprite/Az.sprite"},

    [1030936] = {"D级被动", "Texture/Sprite/Db.sprite"},
    [1030937] = {"D级主动", "Texture/Sprite/Dz.sprite"},
    [1030938] = {"C级被动", "Texture/Sprite/Cb.sprite"},
    [1030939] = {"C级主动", "Texture/Sprite/Cz.sprite"},
    [1030940] = {"B级被动", "Texture/Sprite/Bb.sprite"},
    [1030941] = {"B级主动", "Texture/Sprite/Bz.sprite"},
    [1030942] = {"A级被动", "Texture/Sprite/Ab.sprite"},
    [1030943] = {"A级主动", "Texture/Sprite/Az.sprite"},

    [1030944] = {"D级被动", "Texture/Sprite/Db.sprite"},
    [1030945] = {"D级主动", "Texture/Sprite/Dz.sprite"},
    [1030946] = {"C级被动", "Texture/Sprite/Cb.sprite"},
    [1030947] = {"C级主动", "Texture/Sprite/Cz.sprite"},
    [1030948] = {"B级被动", "Texture/Sprite/Bb.sprite"},
    [1030949] = {"B级主动", "Texture/Sprite/Bz.sprite"},
    [1030950] = {"A级被动", "Texture/Sprite/Ab.sprite"},
    [1030951] = {"A级主动", "Texture/Sprite/Az.sprite"},
}


GamePlayConfigData.dropSXNameInfo = {
    [103081] = {"血量", "Texture/Sprite/shengmingdrop.sprite"},
    [103082] = {"攻击", "Texture/Sprite/gongjidrop.sprite"},
    [103083] = {"防御", "Texture/Sprite/fangyudrop.sprite"},

    [103091] = {"血量", "Texture/Sprite/shengmingdrop.sprite"},
    [103092] = {"攻击", "Texture/Sprite/gongjidrop.sprite"},
    [103093] = {"防御", "Texture/Sprite/fangyudrop.sprite"},

    [103094] = {"血量", "Texture/Sprite/shengmingdrop.sprite"},
    [103095] = {"攻击", "Texture/Sprite/gongjidrop.sprite"},
    [103096] = {"防御", "Texture/Sprite/fangyudrop.sprite"},

    [103097] = {"血量", "Texture/Sprite/shengmingdrop.sprite"},
    [103098] = {"攻击", "Texture/Sprite/gongjidrop.sprite"},
    [103099] = {"防御", "Texture/Sprite/fangyudrop.sprite"},

    [1030997] = {"血量", "Texture/Sprite/shengmingdrop.sprite"},
    [1030998] = {"攻击", "Texture/Sprite/gongjidrop.sprite"},
    [1030999] = {"防御", "Texture/Sprite/fangyudrop.sprite"},
}




GamePlayConfigData.shuxingshuDropItemInfo = {
    {
        103081,
        103082,
        103083,
    },
    {
        103091,
        103092,
        103093,
    },
    {
        103094,
        103095,
        103096,
    },
    {
        103097,
        103098,
        103099,
    },
    {
        1030997,
        1030998,
        1030999,
    },
}



GamePlayConfigData.DropEquipInfoLvSe = { -- 绿色装备
    901, -- 铁剑
    921, -- 匕首
    1461, -- 搏击拳套
    1471, -- 吸血之镰
    2221, -- 雷鸣刃                    -- 这里的东西在掉落物里找！！！
    1501, -- 咒术典籍
    1511, -- 蓝宝石
    1531, -- 圣者法典
    2241, -- 元素杖
    1571, -- 红玛瑙
    1581, -- 布甲
}


GamePlayConfigData.DropEquipInfoLanSe = { -- 蓝色装备
    911, -- 风暴巨剑
    1481, -- 日冕
    2211, -- 狂暴双刃
    931, -- 陨星
    1491, -- 速击之枪
    2271, -- 穿云弓
    941, -- 大棒
    1541, -- 血族之书
    1551, -- 光辉之剑
    951, -- 魅影面罩
    2281, -- 进化水晶
    1561, -- 破碎圣杯
    961, -- 力量腰带
    1781, -- 熔炼之心
    1791, -- 雪山圆盾
    971, -- 守护者之铠
}


GamePlayConfigData.DropEquipInfoZiSe = { -- 紫色装备
    991, -- 末世
    1051, -- 宗师之力
    1061, -- 闪电匕首
    1081, -- 暗影战斧
    1001, -- 名刀
    1141, -- 回响之杖
    1441, -- 圣杯
    1121, -- 虚无法杖
    1221, -- 反伤刺甲
    1151, -- 冰霜法杖
    1291, -- 极寒风暴
    1171, -- 巫术法杖
}


GamePlayConfigData.DropEquipInfoChengSe = { -- 橙色装备
    1131, -- 博学者之怒（帽子）
    1041, -- 无尽战刃
    811, -- 破军
    1181, -- 贤者之书
    1251, -- 霸者重装
    1311, -- 暴烈之甲
    1241, -- 红莲斗篷
    -- 1031, -- 泣血之刃
    1071, -- 影刃
    1161, -- 痛苦面具
}


GamePlayConfigData.DropEquipInfoLvSeAndLanSe = { -- 绿+蓝色装备
    901, -- 铁剑
    921, -- 匕首
    1461, -- 搏击拳套
    1471, -- 吸血之镰
    2221, -- 雷鸣刃                    -- 这里的东西在掉落物里找！！！
    1501, -- 咒术典籍
    1511, -- 蓝宝石
    1531, -- 圣者法典
    2241, -- 元素杖
    1571, -- 红玛瑙
    1581, -- 布甲
    911, -- 风暴巨剑
    1481, -- 日冕
    2211, -- 狂暴双刃
    931, -- 陨星
    1491, -- 速击之枪
    2271, -- 穿云弓
    941, -- 大棒
    1541, -- 血族之书
    1551, -- 光辉之剑
    951, -- 魅影面罩
    2281, -- 进化水晶
    1561, -- 破碎圣杯
    961, -- 力量腰带
    1781, -- 熔炼之心
    1791, -- 雪山圆盾
    971, -- 守护者之铠
}

GamePlayConfigData.DropEquipInfoLanSeAndZiSe = { -- 蓝+紫色装备
    911, -- 风暴巨剑
    1481, -- 日冕
    2211, -- 狂暴双刃
    931, -- 陨星
    1491, -- 速击之枪
    2271, -- 穿云弓
    941, -- 大棒
    1541, -- 血族之书
    1551, -- 光辉之剑
    951, -- 魅影面罩
    2281, -- 进化水晶
    1561, -- 破碎圣杯
    961, -- 力量腰带
    1781, -- 熔炼之心
    1791, -- 雪山圆盾
    971, -- 守护者之铠
    991, -- 末世
    1051, -- 宗师之力
    1061, -- 闪电匕首
    1081, -- 暗影战斧
    1001, -- 名刀
    1141, -- 回响之杖
    1441, -- 圣杯
    1121, -- 虚无法杖
    1221, -- 反伤刺甲
    1151, -- 冰霜法杖
    1291, -- 极寒风暴
    1171, -- 巫术法杖
}

GamePlayConfigData.DropEquipInfoZiSeAndChengSe = { -- 紫+橙色装备
    991, -- 末世
    1051, -- 宗师之力
    1061, -- 闪电匕首
    1081, -- 暗影战斧
    1001, -- 名刀
    1141, -- 回响之杖
    1441, -- 圣杯
    1121, -- 虚无法杖
    1221, -- 反伤刺甲
    1151, -- 冰霜法杖
    1291, -- 极寒风暴
    1171, -- 巫术法杖
    1131, -- 博学者之怒（帽子）
    1041, -- 无尽战刃
    811, -- 破军
    1181, -- 贤者之书
    1251, -- 霸者重装
    1311, -- 暴烈之甲
    1241, -- 红莲斗篷
    1071, -- 影刃
    1161, -- 痛苦面具
}


GamePlayConfigData.oneLvEquipCZInfo = {
    1111,
    1112,
    1113,
    1114,
    1116,
    1211,
    1212,
    1214,
    1216,
    1311,
    1312,
}

GamePlayConfigData.twoLvEquipCZInfo = {
    1121,
    1122,
    1123,
    1124,
    1129,
    1154,
    1221,
    1222,
    1223,
    1224,
    1225,
    1229,
    1321,
    1322,
    1324,
    1325,
}


GamePlayConfigData.threeLvEquipCZInfo = {
    1126,
    1134,
    1135,
    1137,
    1127,
    1233,
    1226,
    1231,
    1327,
    1234,
    1336,
    1236,
}


GamePlayConfigData.fourLvEquipCZInfo = {
    1232,
    1133,
    1138,
    1238,
    1332,
    1338,
    1331,
    1136,
    1235,
}


GamePlayConfigData.equipImgInfo = {
    [1111] = {"UGUI/Sprite/System/BattleEquip/1111", 1},
    [1112] = {"UGUI/Sprite/System/BattleEquip/1112", 1},
    [1113] = {"UGUI/Sprite/System/BattleEquip/1113", 1},
    [1114] = {"UGUI/Sprite/System/BattleEquip/1114", 1},
    [1116] = {"UGUI/Sprite/System/BattleEquip/1116", 1},
    [1211] = {"UGUI/Sprite/System/BattleEquip/1211", 1},
    [1212] = {"UGUI/Sprite/System/BattleEquip/1212", 1},
    [1214] = {"UGUI/Sprite/System/BattleEquip/1214", 1},
    [1216] = {"UGUI/Sprite/System/BattleEquip/1216", 1},
    [1311] = {"UGUI/Sprite/System/BattleEquip/1311", 1},
    [1312] = {"UGUI/Sprite/System/BattleEquip/1312", 1},
    [1121] = {"UGUI/Sprite/System/BattleEquip/1121", 2},
    [1122] = {"UGUI/Sprite/System/BattleEquip/1122", 2},
    [1123] = {"UGUI/Sprite/System/BattleEquip/1123", 2},
    [1124] = {"UGUI/Sprite/System/BattleEquip/1124", 2},
    [1129] = {"UGUI/Sprite/System/BattleEquip/1129", 2},
    [1154] = {"UGUI/Sprite/System/BattleEquip/1154", 2},
    [1221] = {"UGUI/Sprite/System/BattleEquip/1221", 2},
    [1222] = {"UGUI/Sprite/System/BattleEquip/1222", 2},
    [1223] = {"UGUI/Sprite/System/BattleEquip/1223", 2},
    [1224] = {"UGUI/Sprite/System/BattleEquip/1224", 2},
    [1225] = {"UGUI/Sprite/System/BattleEquip/1225", 2},
    [1229] = {"UGUI/Sprite/System/BattleEquip/1229", 2},
    [1321] = {"UGUI/Sprite/System/BattleEquip/1321", 2},
    [1322] = {"UGUI/Sprite/System/BattleEquip/1322", 2},
    [1324] = {"UGUI/Sprite/System/BattleEquip/1324", 2},
    [1325] = {"UGUI/Sprite/System/BattleEquip/1325", 2},
    [1126] = {"UGUI/Sprite/System/BattleEquip/1126", 3},
    [1134] = {"UGUI/Sprite/System/BattleEquip/1134", 3},
    [1135] = {"UGUI/Sprite/System/BattleEquip/1135", 3},
    [1137] = {"UGUI/Sprite/System/BattleEquip/1137", 3},
    [1127] = {"UGUI/Sprite/System/BattleEquip/1127", 3},
    [1233] = {"UGUI/Sprite/System/BattleEquip/1233", 3},
    [1226] = {"UGUI/Sprite/System/BattleEquip/1226", 3},
    [1231] = {"UGUI/Sprite/System/BattleEquip/1231", 3},
    [1327] = {"UGUI/Sprite/System/BattleEquip/1327", 3},
    [1234] = {"UGUI/Sprite/System/BattleEquip/1234", 3},
    [1336] = {"UGUI/Sprite/System/BattleEquip/1336", 3},
    [1236] = {"UGUI/Sprite/System/BattleEquip/1236", 3},
    [1232] = {"UGUI/Sprite/System/BattleEquip/1232", 4},
    [1133] = {"UGUI/Sprite/System/BattleEquip/1133", 4},
    [1138] = {"UGUI/Sprite/System/BattleEquip/1138", 4},
    [1238] = {"UGUI/Sprite/System/BattleEquip/1238", 4},
    [1332] = {"UGUI/Sprite/System/BattleEquip/1332", 4},
    [1338] = {"UGUI/Sprite/System/BattleEquip/1338", 4},
    [1331] = {"UGUI/Sprite/System/BattleEquip/1331", 4},
    [1132] = {"UGUI/Sprite/System/BattleEquip/1132", 4},
    [1136] = {"UGUI/Sprite/System/BattleEquip/1136", 4},
    [1235] = {"UGUI/Sprite/System/BattleEquip/1235", 4},
}




-- GamePlayConfigData.jingyingGuaiDropEquipInfo = {
--     [1] = GamePlayConfigData.jingyingGuaiDropEquipInfoLvSe,
--     [2] = GamePlayConfigData.jingyingGuaiDropEquipInfoLanSe,
--     [3] = GamePlayConfigData.jingyingGuaiDropEquipInfoZiSe,
--     [4] = GamePlayConfigData.jingyingGuaiDropEquipInfoChengSe,
-- }

GamePlayConfigData.diyuhuoDropEquipInfo = {
    [1] = GamePlayConfigData.DropEquipInfoLanSe,
    [2] = GamePlayConfigData.DropEquipInfoLanSeAndZiSe,
    [3] = GamePlayConfigData.DropEquipInfoZiSe,
    [4] = GamePlayConfigData.DropEquipInfoZiSeAndChengSe,
    [5] = GamePlayConfigData.DropEquipInfoChengSe,
}


GamePlayConfigData.bossGuaiDropEquipInfo = {
    [1] = GamePlayConfigData.DropEquipInfoLanSe,
    [2] = GamePlayConfigData.DropEquipInfoLanSeAndZiSe,
    [3] = GamePlayConfigData.DropEquipInfoZiSe,
}


--[[
-- 掉落物相关数据 Start +++++++++++
GamePlayConfigData.levelInfo = {
    [901] = 1, -- 铁剑
    [921] = 1, -- 匕首
    [1461] = 1, -- 搏击拳套
    [1471] = 1, -- 吸血之镰
    [2221] = 1, -- 雷鸣刃                    -- 这里的东西在掉落物里找！！！
    [1501] = 1, -- 咒术典籍
    [1511] = 1, -- 蓝宝石
    [1531] = 1, -- 圣者法典
    [2241] = 1, -- 元素杖
    [1571] = 1, -- 红玛瑙
    [1581] = 1, -- 布甲
    [911] = 2, -- 风暴巨剑
    [1481] = 2, -- 日冕
    [2211] = 2, -- 狂暴双刃
    [931] = 2, -- 陨星
    [1491] = 2, -- 速击之枪
    [2271] = 2, -- 穿云弓
    [941] = 2, -- 大棒
    [1541] = 2, -- 血族之书
    [1551] = 2, -- 光辉之剑
    [951] = 2, -- 魅影面罩
    [2281] = 2, -- 进化水晶
    [1561] = 2, -- 破碎圣杯
    [961] = 2, -- 力量腰带
    [1781] = 2, -- 熔炼之心
    [1791] = 2, -- 雪山圆盾
    [971] = 2, -- 守护者之铠
    [991] = 3, -- 末世
    [1051] = 3, -- 宗师之力
    [1061] = 3, -- 闪电匕首
    [1081] = 3, -- 暗影战斧
    [1001] = 3, -- 名刀
    [1141] = 3, -- 回响之杖
    [1441] = 3, -- 圣杯
    [1121] = 3, -- 虚无法杖
    [1221] = 3, -- 反伤刺甲
    [1151] = 3, -- 冰霜法杖
    [1291] = 3, -- 极寒风暴
    [1171] = 3, -- 巫术法杖
    [1131] = 4, -- 博学者之怒（帽子）
    [1041] = 4, -- 无尽战刃
    [811] = 4, -- 破军
    [1181] = 4, -- 贤者之书
    [1251] = 4, -- 霸者重装
    [1311] = 4, -- 暴烈之甲
    [1241] = 4, -- 红莲斗篷
    -- [1031] = 4, -- 泣血之刃
    [1071] = 4, -- 影刃
    [1161] = 4, -- 痛苦面具
}
]]
-- 掉落物 end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
























-- 兵线/怪物 start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
GamePlayConfigData.refreshMonsterLineTime = 24000  -- 每波兵间隔
GamePlayConfigData.refreshMonsterTime = 1000  -- 每个兵间隔
GamePlayConfigData.refreshMonsterLineCount = 40 -- 总共要刷多少波兵
GamePlayConfigData.refreshMonsterCount = 18 -- 每波刷几个兵



GamePlayConfigData.jingyingguaiLiveTime = 30000

GamePlayConfigData.bagInfo = {
    {'黄金魔袋1级',"Texture/Sprite/modai1.sprite",'击败50个敌人后黄金魔袋被消耗，奖励1000金币，同时只能激活一个魔袋',50,1000}, -- 击败数，奖励金钱数
    {'黄金魔袋2级',"Texture/Sprite/modai2.sprite",'击败50个敌人后黄金魔袋被消耗，奖励1500金币，同时只能激活一个魔袋',50,1500},
    {'黄金魔袋3级',"Texture/Sprite/modai3.sprite",'击败50个敌人后黄金魔袋被消耗，奖励2250金币，同时只能激活一个魔袋',50,2250},
    {'黄金魔袋4级',"Texture/Sprite/modai4.sprite",'击败50个敌人后黄金魔袋被消耗，奖励3375金币，同时只能激活一个魔袋',50,3375},
    {'黄金魔袋5级',"Texture/Sprite/modai4.sprite",'击败50个敌人后黄金魔袋被消耗，奖励5063金币，同时只能激活一个魔袋',50,5063},
    {'黄金魔袋6级',"Texture/Sprite/modai4.sprite",'击败50个敌人后黄金魔袋被消耗，奖励7594金币，同时只能激活一个魔袋',50,7594},
    {'黄金魔袋7级',"Texture/Sprite/modai4.sprite",'击败50个敌人后黄金魔袋被消耗，奖励11391金币，同时只能激活一个魔袋',50,11391},
    {'黄金魔袋8级',"Texture/Sprite/modai4.sprite",'击败50个敌人后黄金魔袋被消耗，奖励17086金币，同时只能激活一个魔袋',50,17086},
    {'黄金魔袋9级',"Texture/Sprite/modai4.sprite",'击败50个敌人后黄金魔袋被消耗，奖励25629金币，同时只能激活一个魔袋',50,25629},
    {'黄金魔袋10级',"Texture/Sprite/modai4.sprite",'击败50个敌人后黄金魔袋被消耗，奖励38443金币，同时只能激活一个魔袋',50,38443},
    {'黄金魔袋10级',"Texture/Sprite/modai4.sprite",'击败50个敌人后黄金魔袋被消耗，奖励38443金币，同时只能激活一个魔袋',50,38443},
    {'黄金魔袋10级',"Texture/Sprite/modai4.sprite",'击败50个敌人后黄金魔袋被消耗，奖励38443金币，同时只能激活一个魔袋',50,38443},
    {'黄金魔袋10级',"Texture/Sprite/modai4.sprite",'击败50个敌人后黄金魔袋被消耗，奖励38443金币，同时只能激活一个魔袋',50,38443},
    {'黄金魔袋10级',"Texture/Sprite/modai4.sprite",'击败50个敌人后黄金魔袋被消耗，奖励38443金币，同时只能激活一个魔袋',50,38443},
    {'黄金魔袋10级',"Texture/Sprite/modai4.sprite",'击败50个敌人后黄金魔袋被消耗，奖励38443金币，同时只能激活一个魔袋',50,38443},
    {'黄金魔袋10级',"Texture/Sprite/modai4.sprite",'击败50个敌人后黄金魔袋被消耗，奖励38443金币，同时只能激活一个魔袋',50,38443},
    {'黄金魔袋10级',"Texture/Sprite/modai4.sprite",'击败50个敌人后黄金魔袋被消耗，奖励38443金币，同时只能激活一个魔袋',50,38443},
    {'黄金魔袋10级',"Texture/Sprite/modai4.sprite",'击败50个敌人后黄金魔袋被消耗，奖励38443金币，同时只能激活一个魔袋',50,38443},
    {'黄金魔袋10级',"Texture/Sprite/modai4.sprite",'击败50个敌人后黄金魔袋被消耗，奖励38443金币，同时只能激活一个魔袋',50,38443},
}

-- 随机事件波次
GamePlayConfigData.sjsjBociInfo = {
    [3] = 1,
    [6] = 1,
    [9] = 1,
    [12] = 1,
    [15] = 1,
    [18] = 1,
    [21] = 1,
    [24] = 1,
    [27] = 1,
    [30] = 1,
    [33] = 1,
    [36] = 1,
    [39] = 1,
    [42] = 1,
    [45] = 1,
    [48] = 1,
    [51] = 1,
    [54] = 1,
    [57] = 1,
    [60] = 1,
}

-- 随机事件效果
-- 小怪随机buff：减伤20%盾永久，无敌盾5s，死亡复活，闪避50%永久，攻击速度提升50%持续10s，血量提升100%永久，攻击力提升100%持续10s
GamePlayConfigData.sjsjInfo = {
    [1] = "血量盾", -- √
    [2] = "无敌盾", -- √
    [3] = "死亡复活", -- √
    [4] = "50%闪避", -- √
    [5] = "10s攻速提升50%", -- √
    [6] = "血量翻倍", -- √
    [7] = "10s攻击力提升100%", -- √
    [8] = "减少英雄蓝量", -- √
    [9] = "减少英雄移速", -- √
    [10] = "减少英雄攻速", -- √
    [11] = "减少英雄攻击力", -- √
}

-- 所有会出生的怪物的CfgID
GamePlayConfigData.MonsterCfgList = {
    [20921] = true,
    [162102] = true,
    [162101] = true,
    [162108] = true,
    [162301] = true,
    [162106] = true,
    [20790] = true,
    [20791] = true,
    [20792] = true,
    [20781] = true,
    [162111] = true,
    [162112] = true,
    [162402] = true,
    [162109] = true,
    [20788] = true,
    [20789] = true,
    [52004] = true,
    [1032601] = true,
    [52005] = true,
    [162103] = true,
    [52002] = true,
    [52003] = true,
    [162113] = true,
    [162401] = true,
    [162502] = true,
}


-- 最终boss CfgID
GamePlayConfigData.bigBossCfgID = 161802

-- 小兵库CfgID
GamePlayConfigData.bingXianMonsterIDInfo = {
    [162102] = 162102,
    [162101] = 162101,
}






-- 装备怪--> 敌军统帅
-- 精英怪--> 敌军精英
-- boss怪刷新波次  掉技能和装备的
GamePlayConfigData.bossGuaiRoundIndexs = {
    [11] = 162108, -- 兵线索引 = boss怪CfgID
    [21] = 162301,
    [31] = 162106,
}

GamePlayConfigData.bossGuaiRoundIndexsBack = {
    [162108] = {11,1,2},  -- 波次 技能等级 装备等级
    [162301] = {21,2,23},
    [162106] = {31,3,3},
}

-- boss怪数值
GamePlayConfigData.bossGuaiData = {
    [11] = {51957,525,0,0,0},
    [21] = {168722,1362,0,0,0},
    [31] = {547893,3533,0,0,0},
}







-- 精英怪刷新波次
GamePlayConfigData.jingYingGuaiRoundIndexs = { -- 哪几波兵线刷新精英怪
    [5] = 162113, -- 兵线索引 = 精英怪CfgID
    [15] = 162111,
    [25] = 162402,
    [35] = 162401,
}

GamePlayConfigData.jingYingGuaiRoundIndexsBack = {
    [162113] = {5, 1},  -- 波次 装备等级
    [162111] = {15, 2},
    [162402] = {25, 3},
    [162401] = {35, 3},
}

-- 精英怪数值
GamePlayConfigData.jingYingGuaiData = {
    -- 1精英怪血量 2精英怪攻击 3精英怪怪掉落金币 4精英怪物防 5精英怪魔抗 6属性书掉落数量
    [5] = {21624,146,110,0,0,24},
    [15] = {70221,380,342,0,0,36},
    [25] = {228031,985,1063,0,0,48},
    [35] = {740491,2555,3300,0,0,60},
}




-- 小兵数值
GamePlayConfigData.bingXianData = {
    --  1小兵血量 2小兵攻击 3小怪掉落金币 4小兵物防 5小兵魔抗
    [-2] = {0,0,0,0,0,0},
    [1] = {1350,70,7,0,0},
    [2] = {1519,77,8,0,0},
    [3] = {1709,85,9,0,0},
    [4] = {1922,93,10,0,0},
    [5] = {2162,102,11,0,0},
    [6] = {2433,113,12,0,0},
    [7] = {2737,124,14,0,0},
    [8] = {3079,136,15,0,0},
    [9] = {3464,150,17,0,0},
    [10] = {3897,165,19,0,0},
    [11] = {4384,182,22,0,0},
    [12] = {4932,200,24,0,0},
    [13] = {5548,220,27,0,0},
    [14] = {6242,242,31,0,0},
    [15] = {7022,266,34,0,0},
    [16] = {7900,292,38,0,0},
    [17] = {8887,322,43,0,0},
    [18] = {9998,354,48,0,0},
    [19] = {11248,389,54,0,0},
    [20] = {12654,428,60,0,0},
    [21] = {14236,471,68,0,0},
    [22] = {16015,518,76,0,0},
    [23] = {18017,570,85,0,0},
    [24] = {20269,627,95,0,0},
    [25] = {22803,689,106,0,0},
    [26] = {25654,758,119,0,0},
    [27] = {28860,834,133,0,0},
    [28] = {32468,918,149,0,0},
    [29] = {36526,1009,167,0,0},
    [30] = {41092,1110,187,0,0},
    [31] = {46228,1221,210,0,0},
    [32] = {52007,1344,235,0,0},
    [33] = {58508,1478,263,0,0},
    [34] = {65821,1626,295,0,0},
    [35] = {74049,1788,330,0,0},
    [36] = {83305,1967,370,0,0},
    [37] = {93718,2164,414,0,0},
    [38] = {105433,2380,464,0,0},
    [39] = {118612,2618,519,0,0},
    [40] = {133439,2880,582,0,0},
}


GamePlayConfigData.bigBossData = {2001582, 10080, 0, 0, 0}
-- GamePlayConfigData.bigBossData = {1,1,0,1,1}



GamePlayConfigData.jinyanzhi = 30
-- GamePlayConfigData.jinyanzhi = 0

-- 附魔BossCfgID
GamePlayConfigData.wuQiBossCfgID = 162103
GamePlayConfigData.huJiaBossCfgID = 52002
GamePlayConfigData.jieZhiBossCfgID = 52003


GamePlayConfigData.diyuhuoCfgID = 162502
GamePlayConfigData.jinbiCfgID = 1032601
GamePlayConfigData.jinkuangCfgID = 52005



GamePlayConfigData.jinKuangPrice = 1000
GamePlayConfigData.diyuhuoPrice = 2000
GamePlayConfigData.jinbiguaiPrice = 1000


GamePlayConfigData.jinbiguaiShuXing = {
    -- 1生命值   2攻击  3物理防御 4魔抗 5每个怪的金币数
    {1500,25,0,0,100},
    {1525,35,0,0,125},
    {1625,49,0,0,175},
    {2275,69,0,0,250},
    {3185,96,0,0,350},
    {4459,134,0,0,475},
    {6243,188,0,0,625},
    {8740,264,0,0,800},
    {12235,369,0,0,1000},
    {17130,517,0,0,1225},
    {23982,723,0,0,1475},
    {33574,1012,0,0,1750},
    {47004,1417,0,0,2050},
    {65805,1984,0,0,2375},
    {92128,2778,0,0,2725},
    {128979,3889,0,0,3100},
}



GamePlayConfigData.jinKuangShuXing = {
    -- 1生命值   2攻击  3物理防御 4魔抗
    {3500,0,0,0},
    {5250,0,0,0},
    {7875,0,0,0},
    {11813,0,0,0},
    {17719,0,0,0},
    {26578,0,0,0},
    {39867,0,0,0},
    {59801,0,0,0},
    {89701,0,0,0},
    {134552,0,0,0},
    {201828,0,0,0},
    {302741,0,0,0},
    {454112,0,0,0},
    {681168,0,0,0},
    {1021752,0,0,0},
    {1532629,0,0,0},
    {1532629,0,0,0},
    {1532629,0,0,0},
    {1532629,0,0,0},
}




GamePlayConfigData.diyuhuoShuXing = {
    -- 1生命值   2攻击  3物理防御 4魔抗
    {14000,300,0,0},
    {23800,450,0,0},
    {40460,675,0,0},
    {68782,1013,0,0},
    {116929,1519,0,0},
    {198780,2278,0,0},
    {337926,3417,0,0},
    {574474,5126,0,0},
    {976606,7689,0,0},
    {1660230,11533,0,0},
    {1660230,11533,0,0},
    {1660230,11533,0,0},
    {1660230,11533,0,0},
    {1660230,11533,0,0},
    {1660230,11533,0,0},
    {1660230,11533,0,0},
    {1660230,11533,0,0},
    {1660230,11533,0,0},
    {1660230,11533,0,0},
    {1660230,11533,0,0},
}


-- 附魔boss属性
GamePlayConfigData.fuMoBossShuXing = {
    -- 1血量 2攻击 3物法防御
    {20000,150,0},
    {40000,263,0},
    {80000,459,0},
    {160000,804,0},
    {320000,1407,0},
    {640000,2462,0},
    {1280000,4308,0},
    {1280000,7540,0},
    {1280000,7540,0},
    {1280000,7540,0},
}



GamePlayConfigData.huJiaBossShuXing = {
    {25000,100,0},
    {50000,175,0},
    {100000,306,0},
    {200000,536,0},
    {400000,938,0},
    {800000,1641,0},
    {1600000,2872,0},
    {1600000,5027,0},
    {1600000,5027,0},
    {1600000,5027,0},
}


GamePlayConfigData.jieZhiBossShuXing = {
    {6000,320,0},
    {18000,480,0},
    {54000,720,0},
    {162000,1080,0},
    {486000,1620,0},
    {1458000,2430,0},
    {4374000,3645,0},
    {4374000,3645,0},
    {4374000,3645,0},
    {4374000,3645,0},
    {4374000,3645,0},
}

-- 兵线/怪物 end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

-- 成就start




local function jifen1(pid,actor)
    print("积分1: 初始金币*1000")
    sc.AddCoin(actor, 1000, true, true, 0)
    GamePlayDynamicData.allJinBi[pid] = GamePlayDynamicData.allJinBi[pid] + 1000
end

local function jifen2(pid,actor)
    print("积分2: 随机D级技能书*1")
    jifen1(pid,actor)

    local randomNum = sc.RangedRand(1,2)
    if randomNum == 1 then
        table.insert(GamePlayDynamicData.daojuInfo[pid], {"D级被动技能书", "Texture/Sprite/Db.sprite", 1, "随机获得一个D级被动技能", 1})
        sc.CallUILuaFunction({pid, true, false, 1}, StringId.new("touchSkillBook_callBack"))
    else
        table.insert(GamePlayDynamicData.daojuInfo[pid], {"D级主动技能书", "Texture/Sprite/Dz.sprite", 1, "随机获得一个D级主动技能", 5})
        sc.CallUILuaFunction({pid, true, false, 5}, StringId.new("touchSkillBook_callBack"))
    end
end

local function jifen3(pid,actor)
    print("积分3：初始金币*4000")
    jifen2(pid,actor)
    sc.AddCoin(actor, 4000, true, true, 0)
    GamePlayDynamicData.allJinBi[pid] = GamePlayDynamicData.allJinBi[pid] + 4000
end

local function jifen4(pid,actor)
    print("积分4：随机C级技能书*1")
    jifen3(pid,actor)
    local randomNum = sc.RangedRand(1,2)
    if randomNum == 1 then
        table.insert(GamePlayDynamicData.daojuInfo[pid], {"C级被动技能书", "Texture/Sprite/Cb.sprite", 1, "随机获得一个C级被动技能", 2})
        sc.CallUILuaFunction({pid, true, false, 2}, StringId.new("touchSkillBook_callBack"))
    else
        table.insert(GamePlayDynamicData.daojuInfo[pid], {"C级主动技能书", "Texture/Sprite/Cz.sprite", 1, "随机获得一个C级主动技能", 6})
        sc.CallUILuaFunction({pid, true, false, 6}, StringId.new("touchSkillBook_callBack"))
    end
end


local function jifen5(pid,actor)
    print("积分5：随机英雄次数*1")
    jifen4(pid,actor)
end

local function jifen6(pid,actor)
    print("积分6：初始金币*8000")
    jifen5(pid,actor)
    sc.AddCoin(actor, 8000, true, true, 0)
    GamePlayDynamicData.allJinBi[pid] = GamePlayDynamicData.allJinBi[pid] + 8000

end

local function jifen7(pid,actor)
    print("积分7：随机英雄次数*1")
    jifen6(pid,actor)
end



local function jifen8(pid,actor)
    print("积分8：随机A级技能书*1")
    jifen7(pid,actor)


    if sc.RangedRand(0,1) == 0 then
        table.insert(GamePlayDynamicData.daojuInfo[pid], {"A级被动技能书", "Texture/Sprite/Ab.sprite", 1, "随机获得一个A级被动技能", 4})
        sc.CallUILuaFunction({pid, true, false, 4}, StringId.new("touchSkillBook_callBack"))
    else
        table.insert(GamePlayDynamicData.daojuInfo[pid], {"A级主动技能书", "Texture/Sprite/Az.sprite", 1, "随机获得一个A级主动技能", 8})
        sc.CallUILuaFunction({pid, true, false, 8}, StringId.new("touchSkillBook_callBack"))
    end
end

local function jifen9(pid,actor)
    print("积分9：伤害减免*20%")
    jifen8(pid,actor)
    sc.BuffAction(actor, actor, true, false, 104012, 0, 0)
    GamePlayDynamicData.shuXingInfo[pid][2] = GamePlayDynamicData.shuXingInfo[pid][2] + 0.2
end

local function jifen10(pid,actor)
    print("积分10：伤害提升*65%")
    jifen9(pid,actor)
    sc.BuffAction(actor, actor, true, false, 104011, 0, 0)
    GamePlayDynamicData.shuXingInfo[pid][1] = GamePlayDynamicData.shuXingInfo[pid][1] + 0.65
end




-- 难度成就start
local function nandu1(pid,actor)
    print("难度1：金币收益*2%")
    GamePlayDynamicData.jinbijiacheng[pid] = GamePlayDynamicData.jinbijiacheng[pid] + 0.02
end


local function nandu2(pid,actor)
    print("难度2：伤害提升*2%")
    nandu1(pid,actor)
    sc.BuffAction(actor, actor, true, false, 104013, 0, 0)
    GamePlayDynamicData.shuXingInfo[pid][1] = GamePlayDynamicData.shuXingInfo[pid][1] + 0.02
end

local function nandu3(pid,actor)
    print("难度3：伤害减免*2%")
    nandu2(pid,actor)
    sc.BuffAction(actor, actor, true, false, 104014, 0, 0)
    GamePlayDynamicData.shuXingInfo[pid][2] = GamePlayDynamicData.shuXingInfo[pid][2] + 0.02
end

local function nandu4(pid,actor)
    print("难度4：金币收益*5%")
    nandu3(pid,actor)
    GamePlayDynamicData.jinbijiacheng[pid] = GamePlayDynamicData.jinbijiacheng[pid] + 0.05
end

local function nandu5(pid,actor)
    print("难度5：伤害提升*5%")
    nandu4(pid,actor)
    sc.BuffAction(actor, actor, true, false, 104015, 0, 0)
    GamePlayDynamicData.shuXingInfo[pid][1] = GamePlayDynamicData.shuXingInfo[pid][1] + 0.05
end

local function nandu6(pid,actor)
    print("难度6：伤害减免*5%")
    nandu5(pid,actor)
    sc.BuffAction(actor, actor, true, false, 104016, 0, 0)
    GamePlayDynamicData.shuXingInfo[pid][2] = GamePlayDynamicData.shuXingInfo[pid][2] + 0.05
end

local function nandu7(pid,actor)
    print("难度7：金币收益*8%")
    nandu6(actor)
    GamePlayDynamicData.jinbijiacheng[pid] = GamePlayDynamicData.jinbijiacheng[pid] + 0.08
end

local function nandu8(pid,actor)
    print("难度8：伤害减免*8%")
    nandu7(actor)
    sc.BuffAction(actor, actor, true, false, 104018, 0, 0)
    GamePlayDynamicData.shuXingInfo[pid][2] = GamePlayDynamicData.shuXingInfo[pid][2] + 0.08
end

-- 难度成就end


local function heroExLv1(pid,actor)
    print("英雄等级1：全属性*5%")
    sc.BuffAction(actor, actor, true, false, 102071, 0, 0)
end

local function heroExLv2(pid,actor)
    print("英雄等级2：全属性*10%")
    heroExLv1(pid,actor)
    sc.BuffAction(actor, actor, true, false, 102072, 0, 0)
end

local function heroExLv3(pid,actor)
    print("英雄等级3：全属性*20%")
    heroExLv2(pid,actor)
    sc.BuffAction(actor, actor, true, false, 102073, 0, 0)
end

local function heroExLv4(pid,actor)
    print("英雄等级4：全属性*35%")
    heroExLv3(pid,actor)
    sc.BuffAction(actor, actor, true, false, 102074, 0, 0)
end


local function heroExLv5(pid,actor)
    print("英雄等级5：全属性*50%")
    heroExLv4(pid,actor)
    sc.BuffAction(actor, actor, true, false, 102075, 0, 0)
end




local function maxHeroExLvCnt1(pid,actor)
    print("5星英雄数量1：金币收益*10%")
    GamePlayDynamicData.jinbijiacheng[pid] = GamePlayDynamicData.jinbijiacheng[pid] + 0.1
end

local function maxHeroExLvCnt2(pid,actor)
    print("5星英雄数量2：伤害提升*10%")
    maxHeroExLvCnt1(pid,actor)
    sc.BuffAction(actor, actor, true, false, 104019, 0, 0)
    GamePlayDynamicData.shuXingInfo[pid][1] = GamePlayDynamicData.shuXingInfo[pid][1] + 0.1

end



local function maxHeroExLvCnt3(pid,actor)
    print("5星英雄数量：金币收益*15%")
    maxHeroExLvCnt2(pid,actor)
    GamePlayDynamicData.jinbijiacheng[pid] = GamePlayDynamicData.jinbijiacheng[pid] + 0.15
end

local function maxHeroExLvCnt4(pid,actor)
    print("5星英雄数量4：伤害提升*25%")
    maxHeroExLvCnt3(pid,actor)
    sc.BuffAction(actor, actor, true, false, 1040110, 0, 0)
    GamePlayDynamicData.shuXingInfo[pid][1] = GamePlayDynamicData.shuXingInfo[pid][1] + 0.25

end


local function maxHeroExLvCnt5(pid,actor)
    print("5星英雄数量5：金币收益*30%")
    maxHeroExLvCnt4(pid,actor)
    GamePlayDynamicData.jinbijiacheng[pid] = GamePlayDynamicData.jinbijiacheng[pid] + 0.3
end


local function maxHeroExLvCnt6(pid,actor)
    print("5星英雄数量6：伤害提升*50%")
    maxHeroExLvCnt5(pid,actor)
    sc.BuffAction(actor, actor, true, false, 1040111, 0, 0)
    GamePlayDynamicData.shuXingInfo[pid][1] = GamePlayDynamicData.shuXingInfo[pid][1] + 0.5
end











local function allHeroExLv1(pid,actor)
    print("英雄总等级1：伤害提升*6%")
    sc.BuffAction(actor, actor, true, false, 1040112, 0, 0)
    GamePlayDynamicData.shuXingInfo[pid][1] = GamePlayDynamicData.shuXingInfo[pid][1] + 0.06
end

local function allHeroExLv2(pid,actor)
    print("英雄总等级2：金币收益*8%")
    allHeroExLv1(pid,actor)
    GamePlayDynamicData.jinbijiacheng[pid] = GamePlayDynamicData.jinbijiacheng[pid] + 0.08
end

local function allHeroExLv3(pid,actor)
    print("英雄总等级3：伤害提升*12%")
    allHeroExLv2(pid,actor)
    sc.BuffAction(actor, actor, true, false, 1040113, 0, 0)
    GamePlayDynamicData.shuXingInfo[pid][1] = GamePlayDynamicData.shuXingInfo[pid][1] + 0.12
end


local function allHeroExLv4(pid,actor)
    print("英雄总等级4：金币收益*18%")
    allHeroExLv3(pid,actor)
    GamePlayDynamicData.jinbijiacheng[pid] = GamePlayDynamicData.jinbijiacheng[pid] + 0.18
end

local function allHeroExLv5(pid,actor)
    print("英雄总等级5：伤害提升*20%")
    allHeroExLv4(pid,actor)
    sc.BuffAction(actor, actor, true, false, 1040114, 0, 0)
    GamePlayDynamicData.shuXingInfo[pid][1] = GamePlayDynamicData.shuXingInfo[pid][1] + 0.2
end

local function allHeroExLv6(pid,actor)
    print("英雄总等级6：金币收益*25%")
    allHeroExLv5(pid,actor)
    GamePlayDynamicData.jinbijiacheng[pid] = GamePlayDynamicData.jinbijiacheng[pid] + 0.25
end


local function allHeroExLv7(pid,actor)
    print("英雄总等级7：伤害提升*35%")
    allHeroExLv6(pid,actor)
    sc.BuffAction(actor, actor, true, false, 1040115, 0, 0)
    GamePlayDynamicData.shuXingInfo[pid][1] = GamePlayDynamicData.shuXingInfo[pid][1] + 0.35
end


local function allHeroExLv8(pid,actor)
    print("英雄总等级8：金币收益*50%")
    allHeroExLv7(pid,actor)
    GamePlayDynamicData.jinbijiacheng[pid] = GamePlayDynamicData.jinbijiacheng[pid] + 0.5
end








GamePlayConfigData.chengjiuInfo = {
    ["积分"] = {
        {30, 1, jifen1},
        {160, 2, jifen2},
        {280, 3, jifen3},
        {560, 4, jifen4},
        {920, 5, jifen5},
        {1400, 6, jifen6},
        {2000, 7, jifen7},
        {2640, 8, jifen8},
        {3280, 9, jifen9},
        {4000, 10 ,jifen10},
    },
    ["通关难度"] = {
        [1] = {1,nandu1},
        [2] = {1,nandu2},
        [3] = {1,nandu3},
        [4] = {1,nandu4},
        [5] = {1,nandu5},
        [6] = {1,nandu6},
        [7] = {2,nandu7},
        [8] = {3,nandu8},
    },
    ["英雄等级"] = {
        {100, 1, heroExLv1},
        {600, 2, heroExLv2},
        {1600, 3, heroExLv3},
        {3600, 4, heroExLv4},
        {6600, 5, heroExLv5},
    },
    ["5星英雄数量"] = {
        {10, 1, maxHeroExLvCnt1},
        {20, 2, maxHeroExLvCnt2},
        {30, 3, maxHeroExLvCnt3},
        {40, 4, maxHeroExLvCnt4},
        {50, 5, maxHeroExLvCnt5},
        {60, 6, maxHeroExLvCnt6},
    },
    ["英雄总等级"] = {
        {30, 1, allHeroExLv1},
        {60, 2, allHeroExLv2},
        {120, 3, allHeroExLv3},
        {180, 4, allHeroExLv4},
        {210, 5, allHeroExLv5},
        {240, 6, allHeroExLv6},
        {270, 7, allHeroExLv7},
        {300, 8, allHeroExLv8},
    },
}
-- 成就end





-- 一些方法
local insert = table.insert
local format = string.format
local tostring = tostring
local type = type

function serialize(t)
    local mark={}
    local assign={}

    local tb_cache = {}
    local function tb(len)
        if tb_cache[len] then 
            return tb_cache[len]
        end
        local ret = ''
        while len > 1 do
            ret = ret .. '       '
            len = len - 1
        end
        if len >= 1 then
            ret = ret .. '    '
        end
        tb_cache[len] = ret
        return ret
    end

    local function table2str(t, parent, deep)
        if type(t) == "table" and t.__tostring then 
            return tostring(t)
        end

        deep = deep or 0
        mark[t] = parent
        local ret = {}
        table.foreach(t, function(f, v)
            local k = type(f)=="number" and "["..f.."]" or tostring(f)
            local t = type(v)
            if t == "userdata" or t == "function" or t == "thread" or t == "proto" or t == "upval" then
                insert(ret, format("%s=%q", k, tostring(v)))
            elseif t == "table" then
                local dotkey = parent..(type(f)=="number" and k or "."..k)
                if mark[v] then
                    insert(assign, dotkey.."="..mark[v])
                else
                    insert(ret, format("%s=%s", k, table2str(v, dotkey, deep + 1)))
                end
            elseif t == "string" then
                insert(ret, format("%s=%q", k, v))
            elseif t == "number" then
                if v == math.huge then
                    insert(ret, format("%s=%s", k, "math.huge"))
                elseif v == -math.huge then
                    insert(ret, format("%s=%s", k, "-math.huge"))
                else
                    insert(ret, format("%s=%s", k, tostring(v)))
                end
            else
                insert(ret, format("%s=%s", k, tostring(v)))
            end
        end)
        return "{\n" .. tb(deep + 2) .. table.concat(ret,",\n" .. tb(deep + 2)) .. '\n' .. tb(deep+1) .."}"
    end

    if type(t) == "table" then
        if t.__tostring then 
            return tostring(t)
        end
        local str = format("%s%s",  table2str(t,"_"), table.concat(assign," "))
        return "\n<<GamePlay_table>>" .. str
    else
        return tostring(t)
    end
end


function printTb(...)
    local n = select('#', ...)

    local tb = {}

    table.insert(tb, '[' .. os.date() .. ']')
    for i = 1, n do
        local v = select(i, ...)
        local str = serialize(v)
        table.insert(tb, str)
    end

    local ret = table.concat(tb, '  ')

    print(ret)

    return ret
end




--[[
    intArrInfo = {
        [1] = "总场次"
        [2] = "通关场次"
        [3] = "通关难度"
        [4] = "积分"
        [5] = "满星经验英雄个数"
        [6] = "英雄经验总等级"
        [7] = "通用经验值"
        [8] = "游戏时长"
        [9] = "游戏时长"
        [10] = "时间戳"
        [11] = "今日活跃度"
        [12] = "今日活跃宝箱领取状况"
        [13] = "今日活跃宝箱领取状况"
        [14] = "今日活跃宝箱领取状况"
        [15] = "今日活跃宝箱领取状况"
        [16] = "今日活跃宝箱领取状况"
        [17] = "今日活跃宝箱领取状况"
        [18] = "原石数"
    }
]]


--[[
    stringArrInfo = {
        [1] = "熟练度"
        [2] = "各个模式战斗信息"
        [3] = "空"
        [4] = "宝石仓库"
        [5] = "宝石装配"
    }
]]