GamePlayDynamicData = GamePlayDynamicData or {}


-- 生成的玩家的playID列表
GamePlayDynamicData.playerList = {}
GamePlayDynamicData.playerCount = 0
GamePlayDynamicData.playerIndexInfo = {}



function GamePlayDynamicData:Init()
end

GamePlayDynamicData.modaiLevel = 1
GamePlayDynamicData.isDaojuzhuangman = false

GamePlayDynamicData.dingshiqi = {}

GamePlayDynamicData.playerInfo = {
    -- [actorID] = pid
}

GamePlayDynamicData.jinbiguaiLevel = 0
GamePlayDynamicData.diyuhuoLevel = 0


GamePlayDynamicData.daojuInfo = {}


GamePlayDynamicData.suijishijian = -1
GamePlayDynamicData.killMonsterAdd = {}


-- GamePlayDynamicData.playerList = {
--     actor = {
--         ["isModaiCounting"] = false,
--         ["modaiKillCount"] = 0,
--         ["modaiNeedKillCount"] = 0,
--         ["spawnPosition"] = position,
--     }
-- }


GamePlayDynamicData.grandListInfo = {} -- {{1,1,false},{1,1,false},{1,1,false}} -- {1级， 零阶， boss是否存活}
GamePlayDynamicData.challengeBossInfo = {} -- {{1,false},{1,false},{1,false}} -- {boss等级，是否存活}
GamePlayDynamicData.GMGongGaoInfo = {} -- {}
GamePlayDynamicData.modaiHaveInfo = {} -- {1, false, 0, 50} -- 魔袋等级，魔袋是否存在，魔袋击杀数, 魔袋需要击杀数


GamePlayDynamicData.skillBookBuyCountList = {
    -- 0,0,0,0,0,0,0,0
}

GamePlayDynamicData.CZChuiCnt = {}

GamePlayDynamicData.playerLiveInfo = {}

GamePlayDynamicData.needKillCount = 0
GamePlayDynamicData.BossKillCount = 0

GamePlayDynamicData.nowPassSkillIDInfo = {}

GamePlayDynamicData.deadCntInfo = {}

GamePlayDynamicData.expendMoneyInfo = {}

GamePlayDynamicData.jinbijiacheng = {}

GamePlayDynamicData.hurtTotalInfo = {}

GamePlayDynamicData.ciTiaoInfo = {}

GamePlayDynamicData.cishanInfo = {}

GamePlayDynamicData.ciTiaoKuInfo = {}

GamePlayDynamicData.erxuanyiSkillKu = {}


GamePlayDynamicData.jingyingguaiHPInfo = {}

GamePlayDynamicData.playerShareInfo = {}

GamePlayDynamicData.allJinBi = {}

GamePlayDynamicData.shuXingInfo = {}